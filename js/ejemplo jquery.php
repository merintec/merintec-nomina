      <script src="../bootstrap/js/jquery.js"> </script>
      <script type="text/javascript">
        function registrar_mp(id, id_venta, id_mpago, mont_pago, iden_per_pago, nomb_per_pago, fech_pago, status_pago, num_refe_banc, msg, correo, sexo, orden)
        {
          var parametros = {
            "var_tabla" : "pago",
            "id_venta" : id_venta,
            "id_mpago" : id_mpago, 
            "mont_pago" : mont_pago, 
            "iden_per_pago" : iden_per_pago,
            "nomb_per_pago" : nomb_per_pago,
            "fech_pago" : fech_pago, 
            "status_pago" : status_pago, 
            "num_refe_banc" : num_refe_banc,
				"nueva" : $("#nueva"+id).val()
          }
          var url="../comunes/funcion_guardar.php"; 
          $.ajax
          ({
              type: "POST",
              url: url,
              data: parametros,
              beforeSend: function(){
              },
              success: function(data)
              {
                var codigo, datatemp, mensaje;
                datatemp=data;
                datatemp=datatemp.split(":::");
                codigo=datatemp[0];
                mensaje=datatemp[1];
                if (codigo==001)
                {
                  actualizar_dato_mp('venta','status_venta','pagado','id_venta',id_venta);
                  fecha_act = '<?php echo date('Y-m-d'); ?>';
                  actualizar_dato_mp('venta','fech_stat', fecha_act,'id_venta',id_venta);
                  enviar_correo_mp(orden,nomb_per_pago,correo,sexo,mont_pago);
                  setTimeout(function() {
                      window.location=('pagos.php');
                  },4000);
                }
                $("#resultado").html(msg);
				$("#norm_gua"+id).val(msg);
                setTimeout(function() {
                  $("#msg_act").fadeOut(3000);
                },3000);               
              }
          });
          return false;
        }
      </script>