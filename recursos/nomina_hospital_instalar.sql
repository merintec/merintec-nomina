-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-06-2016 a las 17:39:40
-- Versión del servidor: 5.5.43
-- Versión de PHP: 5.4.45-0+deb7u2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nomina_hospital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE IF NOT EXISTS `actividades` (
  `cod_act` varchar(5) NOT NULL COMMENT 'codigo de la Actividad',
  `nom_act` varchar(150) NOT NULL COMMENT 'nombre de la actividad',
  PRIMARY KEY (`cod_act`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adt_ing_cat`
--

CREATE TABLE IF NOT EXISTS `adt_ing_cat` (
  `cod_cat` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Categoría',
  `des_cat` varchar(50) NOT NULL COMMENT 'Descripción de Categoría',
  PRIMARY KEY (`cod_cat`),
  UNIQUE KEY `des_cat` (`des_cat`),
  UNIQUE KEY `des_cat_2` (`des_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las Categorias de los recibos para Auditorias' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adt_ing_pro`
--

CREATE TABLE IF NOT EXISTS `adt_ing_pro` (
  `serie_id` int(11) NOT NULL,
  `recibo` int(11) NOT NULL,
  `monto` decimal(9,2) NOT NULL,
  `cod_cat` int(2) NOT NULL,
  `con_esp` varchar(50) NOT NULL COMMENT 'Condición Especial',
  UNIQUE KEY `serie_id` (`serie_id`,`recibo`,`monto`,`cod_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anular_form`
--

CREATE TABLE IF NOT EXISTS `anular_form` (
  `cod_anl` int(11) NOT NULL AUTO_INCREMENT,
  `fch_anl` date NOT NULL,
  `tip_anl` varchar(1) NOT NULL,
  `num_anl` int(5) unsigned zerofill NOT NULL,
  `mot_anl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_anl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los Formularios Anulados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones`
--

CREATE TABLE IF NOT EXISTS `asignaciones` (
  `cod_cnp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_cnp` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `ncuo_cnp` int(3) NOT NULL COMMENT 'Numero de cuotas de la asignacion',
  `per_cnp` varchar(1) NOT NULL COMMENT 'Permanencia de la asignacion',
  `mcuo_cnp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la asignacion',
  `por_cnp` double(9,2) NOT NULL COMMENT 'Porcentaje de asignacion',
  `bpor_cnp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `ced_per` varchar(12) NOT NULL COMMENT 'La cedula de la persona',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_cnp` date NOT NULL COMMENT 'Fecha de Registro de la asignacion',
  `ncp_cnp` int(3) NOT NULL COMMENT 'N?mero de cuotas pagadas',
  `des_cnp` varchar(1) NOT NULL COMMENT 'Incide sobre salario base (B), integral (I) o ninguno (N)',
  `asig_mas` int(11) DEFAULT NULL COMMENT 'Tendra un valor al ser una asignacion masiva',
  PRIMARY KEY (`cod_cnp`),
  KEY `cod_con` (`cod_con`),
  KEY `ced_per` (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE IF NOT EXISTS `auditoria` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_backup`
--

CREATE TABLE IF NOT EXISTS `auditoria_backup` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE IF NOT EXISTS `banco` (
  `cod_ban` int(11) NOT NULL AUTO_INCREMENT,
  `nom_ban` varchar(50) DEFAULT NULL,
  `cue_ban` varchar(20) DEFAULT NULL,
  `des_ban` varchar(50) NOT NULL COMMENT 'Destino de la Cuenta',
  PRIMARY KEY (`cod_ban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_conciliacion`
--

CREATE TABLE IF NOT EXISTS `banco_conciliacion` (
  `cod_ban_conc` int(11) NOT NULL AUTO_INCREMENT,
  `cod_ban` int(11) NOT NULL,
  `mes_ban_conc` int(11) NOT NULL,
  `ano_ban_conc` int(4) NOT NULL,
  `sal_ban_conc` decimal(9,2) NOT NULL,
  `obs_ban_conc` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ban_conc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las concialiaciones bancarias' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_movimientos`
--

CREATE TABLE IF NOT EXISTS `banco_movimientos` (
  `cod_ban_mov` int(11) NOT NULL AUTO_INCREMENT,
  `fch_ban_mov` date NOT NULL,
  `tip_ban_mov` varchar(10) NOT NULL,
  `mon_ban_mov` double(9,2) NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `des_ban_mov` varchar(255) NOT NULL,
  `obs_ban_mov` varchar(255) NOT NULL,
  `ref_ban_mov` varchar(50) NOT NULL,
  `sta_ban_mov` int(11) NOT NULL,
  PRIMARY KEY (`cod_ban_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de los movimientos bancarios' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE IF NOT EXISTS `cargos` (
  `cod_car` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo del cargo',
  `num_car` int(3) unsigned zerofill NOT NULL COMMENT 'numero del cargo',
  `nom_car` varchar(100) NOT NULL COMMENT 'nombre del cargo',
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `est_car` varchar(1) NOT NULL COMMENT 'estado del cargo',
  `cod_tcar` int(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `ced_per` varchar(12) NOT NULL,
  `fch_asg` date NOT NULL COMMENT 'Fecha de asignacion  del cargo',
  `fch_vac` date NOT NULL,
  `cod_hor` int(11) NOT NULL,
  `tip_ces` varchar(30) NOT NULL,
  `noc_car` varchar(1) NOT NULL,
  `car_nn` varchar(10) NOT NULL COMMENT 'Cargo  nueve nueve',
  `escalafon` double(9,2) NOT NULL COMMENT 'Escalafon del Cargo',
  `medico` varchar(1) DEFAULT NULL COMMENT 'Marcado si es medico',
  PRIMARY KEY (`cod_car`),
  UNIQUE KEY `num_car` (`num_car`,`cod_dep`),
  KEY `cod_sue` (`cod_sue`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coberturas_med`
--

CREATE TABLE IF NOT EXISTS `coberturas_med` (
  `cod_cob` int(11) NOT NULL AUTO_INCREMENT,
  `nom_cob` varchar(50) NOT NULL,
  `mon_cob` double(9,2) NOT NULL,
  `bas_cob` int(1) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  PRIMARY KEY (`cod_cob`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `cod_com` int(11) NOT NULL AUTO_INCREMENT,
  `fch_com` date NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `nor_com` int(4) unsigned zerofill NOT NULL,
  `frm_com` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `sol_com` varchar(20) DEFAULT NULL,
  `tip_com` varchar(15) NOT NULL,
  `for_com` varchar(15) DEFAULT NULL,
  `adl_com` double(9,2) DEFAULT NULL,
  `fre_com` varchar(255) DEFAULT NULL,
  `mon_com` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `rif_pro` varchar(10) NOT NULL,
  `npr_com` int(2) NOT NULL,
  `iva_com` int(2) NOT NULL,
  `ela_com` varchar(50) NOT NULL,
  `rev_com` varchar(50) NOT NULL,
  `obs_com` varchar(255) DEFAULT NULL,
  KEY `id` (`cod_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concesiones`
--

CREATE TABLE IF NOT EXISTS `concesiones` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de concesion',
  `nom_con` varchar(60) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre del tipo de concesion',
  `des_con` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de concesion',
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `constancias_per`
--

CREATE TABLE IF NOT EXISTS `constancias_per` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT,
  `fch_con` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `nom_dep` varchar(100) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `des_con` varchar(50) NOT NULL,
  `sue_con` varchar(1) NOT NULL,
  `prm_con` varchar(1) NOT NULL,
  `cst_con` varchar(1) NOT NULL,
  `sue_mon` decimal(9,2) NOT NULL,
  `prm_mon` decimal(9,2) NOT NULL,
  `cst_mon` decimal(9,2) NOT NULL,
  `mot_con` varchar(120) NOT NULL,
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Constancias Solicitadas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cst_tk_pagada`
--

CREATE TABLE IF NOT EXISTS `cst_tk_pagada` (
  `cod_cst` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código de Cesta Ticket Pagada',
  `mes_cst` int(11) NOT NULL COMMENT 'Mes de la Cesta Ticket pagada',
  `ano_cst` int(11) NOT NULL COMMENT 'Año de de la Cesta Ticket pagada',
  `ced_per` int(11) NOT NULL COMMENT 'Cédula del Personal de la Cesta Ticket pagada',
  `dias_cst` int(11) NOT NULL COMMENT 'Total de días del mes de la Cesta Ticket pagada',
  `dias_cst_pag` int(11) NOT NULL COMMENT 'Total de días pagados por mes de la Cesta Ticket pagada',
  `ut_cst` double(9,2) NOT NULL COMMENT 'Bs. por día de la Cesta Ticket pagada',
  `mnt_cst` double(9,2) NOT NULL COMMENT 'Total Bs. por personal de la Cesta Ticket pagada',
  PRIMARY KEY (`cod_cst`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de la Cesta Ticket pagada' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE IF NOT EXISTS `cuentas` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `num_cue` varchar(20) NOT NULL COMMENT 'numero de cuenta de la persona',
  `tip_cue` varchar(1) NOT NULL COMMENT 'tipo de la cuenta de la persona',
  `fcam_cue` date NOT NULL COMMENT 'fecha de cambio del numero de cuenta',
  `ban_cue` varchar(50) NOT NULL,
  PRIMARY KEY (`ced_per`),
  UNIQUE KEY `num_cue` (`num_cue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducciones`
--

CREATE TABLE IF NOT EXISTS `deducciones` (
  `cod_dsp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la Deduccion',
  `con_dsp` varchar(100) NOT NULL COMMENT 'Concepto de la Deduccion',
  `ncuo_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas de la deduccion',
  `per_dsp` varchar(1) NOT NULL COMMENT 'Permanencia de la Deduccion',
  `mcuo_dsp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la deduccion',
  `por_dsp` int(3) NOT NULL COMMENT 'Porcentaje de deduccion',
  `bpor_dsp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del Descuento',
  `ced_per` varchar(12) NOT NULL COMMENT 'Cedula del Personal',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_dsp` date NOT NULL COMMENT 'Fecha de Registro de la Deduccion',
  `ncp_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas pagadas',
  `dedu_mas` int(11) NOT NULL,
  PRIMARY KEY (`cod_dsp`),
  KEY `cod_des` (`cod_des`),
  KEY `cod_per` (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencias`
--

CREATE TABLE IF NOT EXISTS `dependencias` (
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `nom_dep` varchar(150) NOT NULL COMMENT 'nombre de la dependencia',
  `cod_act` varchar(5) NOT NULL COMMENT 'actividad de la dependencia',
  `cod_pro` varchar(5) NOT NULL COMMENT 'programa de la dependencia',
  PRIMARY KEY (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentos`
--

CREATE TABLE IF NOT EXISTS `descuentos` (
  `cod_des` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de descuento',
  `nom_des` varchar(50) NOT NULL COMMENT 'Nombre del tipo de descuento',
  `des_des` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de descuento',
  PRIMARY KEY (`cod_des`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `educacion`
--

CREATE TABLE IF NOT EXISTS `educacion` (
  `cod_edu` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `niv_edu` varchar(50) NOT NULL,
  `ins_edu` varchar(100) NOT NULL,
  `fch_edu` date NOT NULL,
  `reg_edu` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_edu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Estudios Realizados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egresos`
--

CREATE TABLE IF NOT EXISTS `egresos` (
  `cod_egr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_egr` date NOT NULL,
  `paga_imp` varchar(20) NOT NULL COMMENT 'Si paga impuesto indique que tipo',
  `nor_egr` int(4) unsigned zerofill NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `chq_egr` varchar(25) NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `con_egr` varchar(255) NOT NULL,
  `ret_iva_egr` int(3) NOT NULL,
  `ret_isrl_egr` int(2) NOT NULL,
  `ela_egr` varchar(50) NOT NULL,
  `rev_egr` varchar(50) NOT NULL,
  `apr_egr` varchar(50) NOT NULL,
  `cont_egr` varchar(50) NOT NULL,
  `obs_egr` varchar(255) DEFAULT NULL,
  `ded_egr` double(9,2) NOT NULL COMMENT 'Otras Deducciones',
  `sin_par_egr` decimal(9,2) NOT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `sta_ban_egr` int(11) NOT NULL,
  KEY `id` (`cod_egr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE IF NOT EXISTS `entradas` (
  `cod_ntr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de la Entrada',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `fecha_ntr` date NOT NULL COMMENT 'fecha de la entrada',
  `hora_ntr` char(15) NOT NULL COMMENT 'Hora de la Entrada',
  PRIMARY KEY (`cod_ntr`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Entradas de los Usuarios al Sistema' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`cod_ntr`, `cod_usr`, `fecha_ntr`, `hora_ntr`) VALUES
(1, '11111111111', '2016-06-03', '05:36:58 pm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas_pagos`
--

CREATE TABLE IF NOT EXISTS `facturas_pagos` (
  `cod_fac_pag` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pag` int(11) NOT NULL,
  `fch_fac_pag` date NOT NULL,
  `num_fac_pag` varchar(15) NOT NULL,
  `con_fac_pag` varchar(15) NOT NULL,
  `mon_fac_pag` double(9,2) NOT NULL,
  `iva_fac_pag` double(9,2) NOT NULL,
  `isrl_fac_pag` double(9,2) NOT NULL,
  `iva_pag_pag` date NOT NULL,
  `iva_pag_mes` int(2) NOT NULL,
  `iva_pag_ano` int(4) NOT NULL,
  PRIMARY KEY (`cod_fac_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las facturas pagadas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familiares`
--

CREATE TABLE IF NOT EXISTS `familiares` (
  `cod_fam` int(2) NOT NULL AUTO_INCREMENT COMMENT 'C?digo del Familiar',
  `ced_per` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del Personal',
  `ced_fam` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del  del Familiar',
  `nom_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre  del Familiar',
  `ape_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Apellido  del Familiar',
  `sex_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Sexo del Familiar',
  `fnac_fam` date NOT NULL COMMENT 'Fecha de Nacimiento del Familiar',
  `par_fam` varchar(25) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Parentesco  del Familiar',
  `est_fam` varchar(100) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Estudia el Familiar?',
  `obs_fam` varchar(255) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Observaciones acerca del Familiar',
  `dis_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Hijos con discapacidad?',
  `beca_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Beca de familiar',
  PRIMARY KEY (`cod_fam`),
  UNIQUE KEY `ced_per` (`ced_per`,`nom_fam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los Familiares del Personal' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feriados`
--

CREATE TABLE IF NOT EXISTS `feriados` (
  `fch_frd` date NOT NULL,
  `des_frd` varchar(50) NOT NULL,
  `tip_frd` varchar(25) NOT NULL,
  `tck_frd` varchar(2) NOT NULL,
  PRIMARY KEY (`fch_frd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de dias Feridos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `cod_grp` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del grupo',
  `nom_grp` varchar(20) NOT NULL COMMENT 'nombre del grupo',
  `des_grp` varchar(255) NOT NULL COMMENT 'descripcion del grupo',
  PRIMARY KEY (`cod_grp`),
  UNIQUE KEY `nom_grp` (`nom_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `guardias_feriados`
--

CREATE TABLE IF NOT EXISTS `guardias_feriados` (
  `cod_gua` int(11) NOT NULL AUTO_INCREMENT,
  `con_gua` varchar(100) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fec_gua` date NOT NULL,
  `sta_gua` char(1) NOT NULL DEFAULT 'A',
  `nor_gua` int(11) NOT NULL,
  `fer_gua` int(11) NOT NULL,
  `dom_gua` int(11) NOT NULL,
  `dof_gua` int(11) NOT NULL,
  `dis_gua` int(11) NOT NULL,
  `guar_mas` int(11) NOT NULL,
  PRIMARY KEY (`cod_gua`),
  UNIQUE KEY `ced_per` (`ced_per`,`cod_car`,`guar_mas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE IF NOT EXISTS `horarios` (
  `cod_hor` int(11) NOT NULL AUTO_INCREMENT,
  `nom_hor` varchar(25) NOT NULL,
  `fch_dsd` date NOT NULL,
  `fch_hst` date NOT NULL,
  `lun_ini_hor` time NOT NULL,
  `lun_fin_hor` time NOT NULL,
  `lun_ini_hor2` time NOT NULL,
  `lun_fin_hor2` time NOT NULL,
  `mar_ini_hor` time NOT NULL,
  `mar_fin_hor` time NOT NULL,
  `mar_ini_hor2` time NOT NULL,
  `mar_fin_hor2` time NOT NULL,
  `mie_ini_hor` time NOT NULL,
  `mie_fin_hor` time NOT NULL,
  `mie_ini_hor2` time NOT NULL,
  `mie_fin_hor2` time NOT NULL,
  `jue_ini_hor` time NOT NULL,
  `jue_fin_hor` time NOT NULL,
  `jue_ini_hor2` time NOT NULL,
  `jue_fin_hor2` time NOT NULL,
  `vie_ini_hor` time NOT NULL,
  `vie_fin_hor` time NOT NULL,
  `vie_ini_hor2` time NOT NULL,
  `vie_fin_hor2` time NOT NULL,
  `sab_ini_hor` time NOT NULL,
  `sab_fin_hor` time NOT NULL,
  `dom_ini_hor` time NOT NULL,
  `dom_fin_hor` time NOT NULL,
  `tol_hor` int(11) NOT NULL,
  `obs_hor` longtext NOT NULL,
  PRIMARY KEY (`cod_hor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inasistencias`
--

CREATE TABLE IF NOT EXISTS `inasistencias` (
  `cod_ina` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_ina`),
  UNIQUE KEY `clave_unica` (`ced_per`,`cod_car`,`fch_ina`),
  KEY `cod_car` (`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias`
--

CREATE TABLE IF NOT EXISTS `incidencias` (
  `cod_inc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_inc` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `mes_inc` int(2) NOT NULL COMMENT 'Mes en que se paga la incidencia',
  `ano_inc` int(4) NOT NULL COMMENT 'Ano en que se paga la incidencia',
  `fini_inc` date NOT NULL COMMENT 'Fecha de inicio del Periodo de Inicidencia',
  `ffin_inc` date NOT NULL COMMENT 'Fecha de fin del Periodo de Inicidencia',
  PRIMARY KEY (`cod_inc`),
  UNIQUE KEY `mes_inc` (`mes_inc`,`ano_inc`,`fini_inc`,`ffin_inc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias_sueldos`
--

CREATE TABLE IF NOT EXISTS `incidencias_sueldos` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de incidencias',
  `cod_sue` int(2) NOT NULL COMMENT 'Codigo del sueldo',
  `mnt_inc` double(9,2) NOT NULL COMMENT 'Monto o porcentaje de la incidencia',
  `bas_inc` varchar(1) NOT NULL COMMENT 'Base para el calculo de la Incidencia',
  UNIQUE KEY `cod_inc` (`cod_inc`,`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de inicidencias para cada sueldo';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ing_pat_vhi`
--

CREATE TABLE IF NOT EXISTS `ing_pat_vhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `serie` int(3) NOT NULL,
  `inicio` int(8) NOT NULL,
  `fin` int(8) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los ingresos por Patente Vehicular' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justificativos_per`
--

CREATE TABLE IF NOT EXISTS `justificativos_per` (
  `cod_sol_jus` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_jus` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_jus` int(2) NOT NULL,
  `ini_sol_jus` date NOT NULL,
  `fin_sol_jus` date NOT NULL,
  `obs_sol_jus` longtext NOT NULL,
  `mot_sol_jus` varchar(25) NOT NULL,
  `apro_sol_jus` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_jus`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_asign`
--

CREATE TABLE IF NOT EXISTS `nominapre_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_deduc`
--

CREATE TABLE IF NOT EXISTS `nominapre_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  UNIQUE KEY `ano_ded` (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nominapre_pagar`
--

CREATE TABLE IF NOT EXISTS `nominapre_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones',
  `prm_rie` double(9,2) NOT NULL,
  `prm_pat` double(9,2) NOT NULL,
  `prm_coo` double(9,2) NOT NULL,
  `comp_eval` double(9,2) NOT NULL COMMENT 'Monto por compensación por evaluación y Desempeño',
  `bono_noct` double(9,2) NOT NULL COMMENT 'Monto \r\npor bono Nocturno',
  `prm_tra` double(9,2) NOT NULL COMMENT 'Monto \r\npor prima de Transporte',
  `prm_asis` double(9,2) NOT NULL COMMENT 'Monto \r\npor prima Sistencial',
  `prm_spn` double(9,2) NOT NULL COMMENT 'Monto \r\npor prima Sistema Público Nacional',
  `cah_nom` varchar(100) NOT NULL COMMENT 'Nombre de la Caja de Ahorro',
  `num_dal` int(2) NOT NULL COMMENT 'Día Adicional laborado',
  `mon_dal` double(9,2) NOT NULL COMMENT 'Monto por Día Adicional laborado',
  `dias_agui` int(2) NOT NULL COMMENT 'Días de Aguinaldos Pagados',
  `mont_agui` double(9,2) NOT NULL COMMENT 'Monto por Días de Aguinaldos Pagados',
  `num_car` int(3) unsigned zerofill NOT NULL,
  `horas` double(9,2) NOT NULL,
  `prm_mesp` double(9,2) NOT NULL COMMENT 'Prima Medico Especialista',
  `prm_exc` double(9,2) NOT NULL COMMENT 'Prima de Exclusividad',
  `prm_exc_por` double(9,2) NOT NULL COMMENT 'Porcentaje de prima de exclusividad',
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_asign`
--

CREATE TABLE IF NOT EXISTS `nomina_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_deduc`
--

CREATE TABLE IF NOT EXISTS `nomina_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  UNIQUE KEY `ano_ded` (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_incidencias`
--

CREATE TABLE IF NOT EXISTS `nomina_incidencias` (
  `ano_inc` int(4) NOT NULL,
  `mes_inc` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `mnt_inc` double(9,2) NOT NULL,
  `con_inc` varchar(255) NOT NULL,
  `ivss_inc` double(9,2) NOT NULL,
  `spf_inc` double(9,2) NOT NULL,
  `cah_inc` double(9,2) NOT NULL,
  `bnoc_inc` double(9,2) NOT NULL,
  `bvac_inc` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_inc`,`mes_inc`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_pagar`
--

CREATE TABLE IF NOT EXISTS `nomina_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones',
  `prm_rie` double(9,2) NOT NULL,
  `prm_pat` double(9,2) NOT NULL,
  `prm_coo` double(9,2) NOT NULL,
  `comp_eval` double(9,2) NOT NULL COMMENT 'Monto por compensación por evaluación y Desempeño',
  `bono_noct` double(9,2) NOT NULL COMMENT 'Monto \r\npor bono Nocturno',
  `prm_tra` double(9,2) NOT NULL COMMENT 'Monto \r\npor prima de Transporte',
  `prm_asis` double(9,2) NOT NULL COMMENT 'Monto \r\npor prima Sistencial',
  `prm_spn` double(9,2) NOT NULL COMMENT 'Monto \r\npor prima Sistema Público Nacional',
  `cah_nom` varchar(100) NOT NULL COMMENT 'Nombre de la Caja de Ahorro',
  `num_dal` int(2) NOT NULL COMMENT 'Día Adicional laborado',
  `mon_dal` double(9,2) NOT NULL COMMENT 'Monto por Día Adicional laborado',
  `dias_agui` int(2) NOT NULL COMMENT 'Días de Aguinaldos Pagados',
  `mont_agui` double(9,2) NOT NULL COMMENT 'Monto por Días de Aguinaldos Pagados',
  `num_car` int(3) unsigned zerofill NOT NULL,
  `horas` double(9,2) NOT NULL,
  `prm_mesp` double(9,2) NOT NULL COMMENT 'Prima Medico Especialista',
  `prm_exc` double(9,2) NOT NULL COMMENT 'Prima de Exclusividad',
  `prm_exc_por` double(9,2) NOT NULL COMMENT 'Porcentaje de prima de exclusividad',
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficios_enviados`
--

CREATE TABLE IF NOT EXISTS `oficios_enviados` (
  `cod_ofi` int(11) NOT NULL AUTO_INCREMENT,
  `tip_ofi` varchar(10) NOT NULL,
  `num_ofi` int(4) unsigned zerofill NOT NULL,
  `fch_ofi` date NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `rdp_ofi` varchar(75) NOT NULL,
  `rpr_ofi` varchar(75) NOT NULL,
  `ddp_ofi` varchar(75) NOT NULL,
  `dpr_ofi` varchar(75) NOT NULL,
  `des_ofi` varchar(255) NOT NULL,
  `nds_ofi` int(2) NOT NULL,
  `fen_ofi` date DEFAULT NULL,
  `ftr_ofi` date DEFAULT NULL,
  `frs_ofi` date DEFAULT NULL,
  `ref_ofi` varchar(7) NOT NULL,
  `obs_ofi` varchar(255) NOT NULL,
  `est_ofi` varchar(1) NOT NULL COMMENT 'Estado del Oficio',
  PRIMARY KEY (`cod_ofi`),
  UNIQUE KEY `tip_ofi` (`tip_ofi`,`num_ofi`,`fch_ofi`,`rdp_ofi`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Oficios Enviados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficios_recibidos`
--

CREATE TABLE IF NOT EXISTS `oficios_recibidos` (
  `cod_ofr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_ofr` date NOT NULL,
  `fch_emi_ofr` date NOT NULL,
  `num_ofr` varchar(60) NOT NULL,
  `rem_ofr` varchar(60) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `con_ofr` varchar(150) NOT NULL,
  `ane_ofr` varchar(2) NOT NULL,
  `obs_ofr` varchar(255) NOT NULL,
  `prc_ofr` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ofr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Oficios Recibidos' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagados`
--

CREATE TABLE IF NOT EXISTS `pagados` (
  `ced_per` varchar(12) NOT NULL,
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagados_frc`
--

CREATE TABLE IF NOT EXISTS `pagados_frc` (
  `cod_car` varchar(12) NOT NULL,
  PRIMARY KEY (`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `fch_pag` date NOT NULL,
  `nor_pag` int(4) unsigned zerofill NOT NULL,
  `frm_pag` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `frm_com` int(6) unsigned zerofill NOT NULL,
  `mon_pag` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `ela_pag` varchar(50) NOT NULL,
  `rev_pag` varchar(50) NOT NULL,
  `apr_pag` varchar(50) NOT NULL,
  `obs_pag` varchar(255) DEFAULT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `frm_egr_iva` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar iva',
  `frm_egr_isrl` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar isrl',
  KEY `id` (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidas_compras`
--

CREATE TABLE IF NOT EXISTS `partidas_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_com` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidas_pagos`
--

CREATE TABLE IF NOT EXISTS `partidas_pagos` (
  `cod_pro_pag` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pag` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `isrl_pro_pag` varchar(2) DEFAULT NULL,
  `mon_pro_pag` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part_presup`
--

CREATE TABLE IF NOT EXISTS `part_presup` (
  `cod_par` int(11) NOT NULL AUTO_INCREMENT,
  `sec_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `pro_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `act_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `ram_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `par_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `gen_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `esp_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `sub_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `des_par` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `obs_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cod_par`),
  UNIQUE KEY `sector` (`par_par`,`gen_par`,`esp_par`,`sub_par`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part_presup_mov`
--

CREATE TABLE IF NOT EXISTS `part_presup_mov` (
  `cod_par_mov` int(11) NOT NULL AUTO_INCREMENT,
  `tip_par_mov` varchar(15) NOT NULL,
  `fch_par_mov` date NOT NULL,
  `ano_par_mov` varchar(10) NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `con_par_mov` longtext NOT NULL,
  `mon_par_mov` double(9,2) NOT NULL,
  `fin_par_mov` date NOT NULL,
  PRIMARY KEY (`cod_par_mov`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `ing_prm` varchar(1) NOT NULL COMMENT 'permisos para ingresar',
  `mod_prm` varchar(1) NOT NULL COMMENT 'permisos para modificar',
  `con_prm` varchar(1) NOT NULL COMMENT 'permisos para consultar',
  `eli_prm` varchar(1) NOT NULL COMMENT 'permisos para eliminar',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion',
  KEY `cod_grp` (`cod_grp`),
  KEY `cod_sec` (`cod_sec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permisologia para los Usuarios del Sistema';

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`ing_prm`, `mod_prm`, `con_prm`, `eli_prm`, `cod_grp`, `cod_sec`) VALUES
('', '', '', '', 5, 11),
('', '', '', '', 5, 2),
('', '', '', '', 5, 8),
('', '', '', '', 5, 3),
('', '', '', '', 5, 7),
('', '', '', '', 5, 6),
('', '', '', '', 5, 1),
('', '', '', '', 5, 14),
('', '', '', '', 5, 15),
('', '', '', '', 5, 13),
('', '', '', '', 5, 12),
('', '', '', '', 5, 4),
('', '', '', '', 5, 5),
('', '', '', '', 5, 16),
('', '', '', '', 5, 10),
('', '', '', '', 5, 9),
('A', 'A', 'A', 'A', 5, 17),
('', '', '', '', 4, 18),
('', '', '', '', 4, 19),
('', '', '', '', 4, 11),
('', '', '', '', 4, 2),
('', '', '', '', 4, 8),
('', '', '', '', 4, 3),
('', '', '', '', 4, 7),
('', '', '', '', 4, 6),
('', '', '', '', 4, 1),
('', '', '', '', 4, 14),
('', '', '', '', 4, 23),
('', '', '', '', 4, 20),
('', '', '', '', 4, 21),
('', '', '', '', 4, 22),
('', '', '', '', 4, 15),
('', '', '', '', 4, 13),
('', '', '', '', 4, 12),
('', '', '', '', 4, 4),
('', '', '', '', 4, 5),
('', '', '', '', 4, 16),
('', '', '', '', 4, 10),
('', '', '', '', 4, 9),
('', '', '', '', 4, 17),
('A', 'A', 'A', 'A', 4, 24),
('', '', '', '', 4, 25),
('', '', '', '', 4, 26),
('', '', '', '', 6, 18),
('', '', '', '', 6, 19),
('', '', '', '', 6, 11),
('', '', '', '', 6, 2),
('', '', '', '', 6, 8),
('', '', '', '', 6, 3),
('', '', '', '', 6, 7),
('', '', '', '', 6, 6),
('', '', 'A', '', 6, 1),
('', '', '', '', 6, 14),
('', '', '', '', 6, 23),
('A', '', 'A', '', 6, 20),
('A', '', 'A', '', 6, 21),
('A', '', 'A', '', 6, 22),
('', '', '', '', 6, 15),
('', '', 'A', '', 6, 13),
('', '', '', '', 6, 12),
('', '', 'A', '', 6, 4),
('', '', '', '', 6, 5),
('', '', '', '', 6, 16),
('', '', '', '', 6, 10),
('A', 'A', 'A', 'A', 6, 9),
('A', 'A', 'A', 'A', 6, 27),
('', '', '', '', 6, 17),
('', '', '', '', 6, 24),
('', '', 'A', '', 6, 25),
('A', '', 'A', '', 6, 26),
('', '', '', '', 7, 18),
('', '', 'A', '', 7, 19),
('', '', '', '', 7, 11),
('', '', '', '', 7, 2),
('', '', '', '', 7, 8),
('', '', '', '', 7, 3),
('', '', '', '', 7, 7),
('', '', '', '', 7, 6),
('A', 'A', 'A', '', 7, 1),
('', '', '', '', 7, 14),
('', '', '', '', 7, 23),
('A', '', 'A', '', 7, 20),
('A', '', 'A', '', 7, 21),
('A', '', 'A', '', 7, 22),
('', '', '', '', 7, 15),
('', '', 'A', '', 7, 13),
('', '', '', '', 7, 12),
('', '', 'A', '', 7, 4),
('', '', '', '', 7, 5),
('A', 'A', 'A', '', 7, 16),
('A', 'A', 'A', '', 7, 10),
('', '', '', '', 7, 9),
('', '', '', '', 7, 27),
('', '', '', '', 7, 17),
('', '', '', '', 7, 24),
('', '', 'A', '', 7, 25),
('A', 'A', 'A', 'A', 7, 26),
('', '', '', '', 3, 18),
('', '', '', '', 3, 28),
('A', 'A', 'A', 'A', 3, 19),
('', '', '', '', 3, 11),
('', '', '', '', 3, 2),
('', '', '', '', 3, 8),
('', '', '', '', 3, 3),
('', '', '', '', 3, 7),
('', '', '', '', 3, 6),
('', '', 'A', '', 3, 1),
('', '', '', '', 3, 14),
('', '', '', '', 3, 23),
('A', '', 'A', '', 3, 20),
('A', '', 'A', '', 3, 21),
('A', '', 'A', '', 3, 22),
('', '', 'A', '', 3, 15),
('', '', 'A', '', 3, 13),
('', '', '', '', 3, 12),
('', '', 'A', '', 3, 4),
('', '', '', '', 3, 5),
('', '', '', '', 3, 16),
('', '', '', '', 3, 10),
('', '', '', '', 3, 9),
('', '', '', '', 3, 27),
('', '', '', '', 3, 24),
('', '', '', '', 3, 17),
('', '', 'A', '', 3, 25),
('A', '', 'A', '', 3, 26),
('A', 'A', 'A', 'A', 2, 18),
('A', 'A', 'A', 'A', 2, 2),
('A', 'A', 'A', 'A', 2, 8),
('A', 'A', 'A', 'A', 2, 3),
('A', 'A', 'A', 'A', 2, 7),
('A', 'A', 'A', 'A', 2, 6),
('A', 'A', 'A', 'A', 2, 1),
('A', 'A', 'A', 'A', 2, 14),
('A', 'A', 'A', 'A', 2, 29),
('A', 'A', 'A', 'A', 2, 30),
('A', 'A', 'A', 'A', 2, 34),
('', '', '', '', 2, 35),
('', '', '', '', 2, 36),
('', '', '', '', 2, 32),
('', '', '', '', 2, 33),
('A', 'A', 'A', 'A', 2, 15),
('A', 'A', 'A', 'A', 2, 13),
('A', 'A', 'A', 'A', 2, 12),
('A', 'A', 'A', 'A', 2, 4),
('', '', '', '', 2, 31),
('A', 'A', 'A', 'A', 2, 26),
('A', 'A', 'A', 'A', 1, 18),
('A', 'A', 'A', 'A', 1, 2),
('A', 'A', 'A', 'A', 1, 8),
('A', 'A', 'A', 'A', 1, 3),
('A', 'A', 'A', 'A', 1, 7),
('A', 'A', 'A', 'A', 1, 6),
('A', 'A', 'A', 'A', 1, 1),
('A', 'A', 'A', 'A', 1, 14),
('A', 'A', 'A', 'A', 1, 29),
('A', 'A', 'A', 'A', 1, 30),
('A', 'A', 'A', 'A', 1, 34),
('A', 'A', 'A', 'A', 1, 35),
('A', 'A', 'A', 'A', 1, 36),
('A', 'A', 'A', 'A', 1, 32),
('A', 'A', 'A', 'A', 1, 33),
('A', 'A', 'A', 'A', 1, 15),
('A', 'A', 'A', 'A', 1, 13),
('A', 'A', 'A', 'A', 1, 12),
('A', 'A', 'A', 'A', 1, 4),
('A', 'A', 'A', 'A', 1, 31),
('A', 'A', 'A', 'A', 1, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_per`
--

CREATE TABLE IF NOT EXISTS `permisos_per` (
  `cod_sol_perm` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_perm` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_perm` int(2) NOT NULL,
  `ini_sol_perm` date NOT NULL,
  `fin_sol_perm` date NOT NULL,
  `tip_sol_perm` varchar(20) NOT NULL,
  `obs_sol_perm` longtext NOT NULL,
  `mot_sol_perm` varchar(25) NOT NULL,
  `apro_sol_perm` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_perm`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `nac_per` varchar(1) NOT NULL COMMENT 'nacionalidad de la persona',
  `nom_per` varchar(50) NOT NULL COMMENT 'nombre de la persona',
  `ape_per` varchar(50) NOT NULL COMMENT 'apellido de la persona',
  `sex_per` varchar(1) NOT NULL COMMENT 'sexo de la persona',
  `fnac_per` date NOT NULL COMMENT 'fecha de nacimiento de la persona',
  `lnac_per` varchar(50) NOT NULL COMMENT 'lugar de nacimiento de la persona',
  `cor_per` varchar(50) NOT NULL COMMENT 'correo de la persona',
  `pro_per` varchar(50) NOT NULL COMMENT 'profesion de la persona',
  `abr_per` varchar(10) NOT NULL COMMENT 'Abreviatura de Profesión',
  `dir_per` varchar(255) NOT NULL COMMENT 'direccion de la persona',
  `tel_per` varchar(12) NOT NULL COMMENT 'telefono de la persona',
  `cel_per` varchar(12) NOT NULL COMMENT 'celular de la persona',
  `lph_des` varchar(1) NOT NULL COMMENT 'Descuento de ley politica habitacional',
  `spf_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro de paro forzoso',
  `sso_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro social obligatorio',
  `cah_des` varchar(20) NOT NULL COMMENT 'Descuento de caja de ahorro',
  `sfu_des` varchar(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Servicio funerario',
  `hog_asg` varchar(1) NOT NULL COMMENT 'Prima por hogar',
  `hij_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos',
  `ant_asg` varchar(1) NOT NULL COMMENT 'Prima por antiguedad',
  `pro_asg` varchar(1) NOT NULL COMMENT 'Prima por profesionalizaci?n',
  `hje_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos excepcionales',
  `jer_asg` varchar(1) NOT NULL COMMENT 'Prima por Jerarquia',
  `otr_asg` varchar(1) NOT NULL COMMENT 'Prima por Postgrado',
  `tra_asg` varchar(1) NOT NULL,
  `asi_asg` varchar(1) NOT NULL,
  `spn_asg` varchar(1) NOT NULL,
  `fch_reg` date NOT NULL COMMENT 'Fecha de registro en el personal',
  `fnj_des` varchar(1) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `rie_asg` varchar(1) NOT NULL COMMENT 'Prima por riesgo',
  `pat_asg` varchar(1) NOT NULL COMMENT 'Prima Patrullero',
  `coo_asg` varchar(1) NOT NULL COMMENT 'Prima Coordinador',
  `bono_util` varchar(1) NOT NULL,
  `bono_juge` varchar(1) NOT NULL,
  `comp_eva` decimal(9,3) NOT NULL,
  `ano_adm_per` varchar(1) NOT NULL COMMENT 'Años en la administración publica',
  `med_asg` varchar(1) NOT NULL COMMENT 'Prima Medico especialista',
  `comp_eval_ant` double(9,2) NOT NULL COMMENT 'Valor de compensación anterior',
  `hog_asg_inc` double(9,2) NOT NULL COMMENT 'incremento hogar',
  `hij_asg_inc` double(9,2) NOT NULL COMMENT 'incremento hijos',
  `ant_asg_inc` double(9,2) NOT NULL COMMENT 'incremento antiguedad',
  `pro_asg_inc` double(9,2) NOT NULL COMMENT 'incremento  profesionalización',
  `tra_asg_inc` double(9,2) NOT NULL COMMENT 'incremento transporte',
  `asi_asg_inc` double(9,2) NOT NULL COMMENT 'incremento asitencial',
  `spn_asg_inc` double(9,2) NOT NULL COMMENT 'incremento sector publico',
  `med_asg_inc` double(9,2) NOT NULL COMMENT 'incremento  medico especialista',
  `exc_asg` varchar(1) NOT NULL COMMENT 'Prima exclusividad',
  `exc_asg_inc` double(9,2) NOT NULL COMMENT 'incremento exclusividad',
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_compras`
--

CREATE TABLE IF NOT EXISTS `productos_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cnt_pro_com` int(4) NOT NULL,
  `uni_pro_com` int(4) NOT NULL,
  `con_pro_com` varchar(255) NOT NULL,
  `pun_pro_com` double(9,3) NOT NULL,
  `exc_pro_com` varchar(2) DEFAULT NULL,
  KEY `id` (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_part_mov`
--

CREATE TABLE IF NOT EXISTS `productos_part_mov` (
  `cod_pro_par_mov` int(11) NOT NULL AUTO_INCREMENT,
  `cod_par_mov` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_par_mov` double(9,2) NOT NULL,
  `tip_pro_par_mov` varchar(10) NOT NULL DEFAULT 'Ingreso',
  PRIMARY KEY (`cod_pro_par_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='partidas que pertenecen a un movimiento presupuestario' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas`
--

CREATE TABLE IF NOT EXISTS `programas` (
  `cod_pro` varchar(5) NOT NULL COMMENT 'codigo del Programa',
  `nom_pro` varchar(150) NOT NULL COMMENT 'nombre del Programa',
  PRIMARY KEY (`cod_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prog_movimientos`
--

CREATE TABLE IF NOT EXISTS `prog_movimientos` (
  `cod_mov` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `accion` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fch_asg` date NOT NULL,
  `fch_vac` date NOT NULL,
  `pagado` varchar(5) NOT NULL,
  `escalafon` double(9,2) NOT NULL COMMENT 'escalafón en moviemientos',
  PRIMARY KEY (`cod_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prog_mov_pagos`
--

CREATE TABLE IF NOT EXISTS `prog_mov_pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `con_pag` varchar(150) NOT NULL,
  `fch_pag` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_vac` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `cod_pro` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fch_pro` date NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `dir_pro` varchar(255) NOT NULL,
  `act_pro` varchar(50) NOT NULL,
  `cap_pro` double(9,2) NOT NULL,
  `cas_pro` double(9,2) NOT NULL,
  `tel_pro` varchar(11) NOT NULL,
  `fax_pro` varchar(11) NOT NULL,
  `per_pro` varchar(1) NOT NULL,
  `ofi_reg_pro` varchar(50) NOT NULL,
  `num_reg_pro` int(5) NOT NULL,
  `tom_reg_pro` varchar(25) NOT NULL,
  `fol_reg_pro` varchar(4) NOT NULL,
  `fch_reg_pro` date NOT NULL,
  `num_mod_pro` int(5) NOT NULL,
  `tom_mod_pro` varchar(25) NOT NULL,
  `fol_mod_pro` varchar(4) NOT NULL,
  `fch_mod_pro` date NOT NULL,
  `pat_pro` varchar(25) NOT NULL,
  `ivss_pro` varchar(25) NOT NULL,
  `ince_pro` varchar(25) NOT NULL,
  `nom_rep_pro` varchar(50) NOT NULL,
  `ape_rep_pro` varchar(50) NOT NULL,
  `ced_rep_pro` varchar(8) NOT NULL,
  `dir_rep_pro` varchar(255) NOT NULL,
  `tel_rep_pro` varchar(11) NOT NULL,
  `car_rep_pro` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_pro`),
  UNIQUE KEY `rif_pro` (`rif_pro`),
  UNIQUE KEY `nom_pro` (`nom_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Proveedores' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reembolsos_med`
--

CREATE TABLE IF NOT EXISTS `reembolsos_med` (
  `cod_ree` int(11) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `cod_cob` int(11) NOT NULL,
  `fec_reg` date NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `rif_pro` varchar(12) NOT NULL,
  `tel_pro` int(11) NOT NULL,
  `nro_fac` varchar(20) NOT NULL,
  `fec_fac` date NOT NULL,
  `mon_fac` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `fec_pag` date NOT NULL DEFAULT '0000-00-00',
  `fec_nom` date NOT NULL DEFAULT '0000-00-00',
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones_isrl`
--

CREATE TABLE IF NOT EXISTS `retenciones_isrl` (
  `cod_ret_isrl` int(11) NOT NULL AUTO_INCREMENT,
  `com_ret_isrl` int(5) unsigned zerofill NOT NULL,
  `egr_ret_isrl` int(6) unsigned zerofill NOT NULL,
  `fch_ret_isrl` date NOT NULL,
  `obs_ret_isrl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ret_isrl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retenciones_iva`
--

CREATE TABLE IF NOT EXISTS `retenciones_iva` (
  `cod_ret_iva` int(11) NOT NULL AUTO_INCREMENT,
  `com_ret_iva` int(8) unsigned zerofill NOT NULL,
  `egr_ret_iva` int(6) unsigned zerofill NOT NULL,
  `fch_ret_iva` date NOT NULL,
  `obs_ret_iva` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ret_iva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `cod_sec` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la seccion',
  `nom_sec` varchar(25) NOT NULL COMMENT 'nombre de la seccion',
  `des_sec` varchar(255) NOT NULL COMMENT 'descripcion de la seccion',
  `dir_sec` varchar(50) NOT NULL COMMENT 'direccion para apuntar a la seccion',
  `tar_sec` varchar(15) NOT NULL COMMENT 'target de apertura de la seccion',
  `ord_sec` double(9,2) NOT NULL COMMENT 'orden de aparicion en el menu',
  PRIMARY KEY (`cod_sec`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Secciones Disponibles para los Usuarios' AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`cod_sec`, `nom_sec`, `des_sec`, `dir_sec`, `tar_sec`, `ord_sec`) VALUES
(1, 'Personal', 'Datos del Personal adscrito a: ', 'personal.php', 'contenido', 7.00),
(2, 'Dependencias', 'Dependencias adcritas a:', 'dependencias.php', 'contenido', 2.00),
(3, 'Cargos', 'Cargos adscritos a:', 'cargos.php', 'contenido', 4.00),
(4, 'Nomina', 'Nomina adscrita a:', 'nomina.php', 'contenido', 12.00),
(6, 'Tipos de Descuentos', 'Descuentos del Personal adscrito a:', 'descuentos.php', 'contenido', 6.00),
(7, 'Tipo de Asignaciones', 'Concesiones del Personal adscrito a:', 'concesiones.php', 'contenido', 5.00),
(8, 'Sueldos', 'Tabla de Sueldos', 'sueldos.php', 'contenido', 3.00),
(12, 'Pre-Nomina', 'Prenomina Adscrita a: ', 'nominapre.php', 'contenido', 11.00),
(13, 'Cesta Ticket', 'Reporte de Cesta Ticket del personal adscrito a:', 'cesta_ticket.php', 'contenido', 10.00),
(14, 'Inasistencias', 'Registro de las inasistencias del personal adscrito a:', 'inasistencias.php', 'contenido', 8.00),
(18, 'Valores', 'Valores utilizados para los cáculos', 'valores.php', 'contenido', 0.00),
(26, 'Constancias', 'Solicitud de Constancias', 'constancias.php', 'contenido', 17.00),
(29, 'Asignaciones Por Lotes', 'Asignaciones a Nomina a todo el personal', 'personal_asignaciones_general.php', 'contenido', 8.50),
(30, 'Deducciones Por Lotes', 'Deducciones Generales en Nomina', 'personal_deduciones_general.php', 'contenido', 8.60),
(31, 'Reportes', 'Reportes Varios', 'reportes.php', 'contenido', 13.00),
(32, 'Pre-pagar Reembolsos', 'Prepago de reembolsos adscritos a: ', 'reemb_nominapre.php', 'contenido', 8.91),
(33, 'Pagar Reembolsos', 'Pago de reembolsos adscritos a: ', 'reemb_nomina.php', 'contenido', 8.91),
(34, 'Guardias', 'Asignación de guardias por tipo de personal', 'personal_guardias_feriados.php', 'contenido', 8.70),
(35, 'Tipos de Reembolso', 'Valores de Tipos de Reembolso', 'coberturas_med.php', 'contenido', 8.80),
(36, 'Reembolsos Medicos', 'Registro de Reembolsos Medicos', 'reembolsos_med.php', 'contenido', 8.90);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos`
--

CREATE TABLE IF NOT EXISTS `sueldos` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  `hora_sue` double(9,2) DEFAULT NULL,
  `grad_sue` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldos_ant`
--

CREATE TABLE IF NOT EXISTS `sueldos_ant` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp_presup`
--

CREATE TABLE IF NOT EXISTS `temp_presup` (
  `fch_prs` date NOT NULL,
  `det_prs` varchar(255) NOT NULL,
  `nor_prs` varchar(7) NOT NULL,
  `asg_prs` double(9,2) NOT NULL,
  `inc_prs` double(9,2) NOT NULL,
  `ing_prs` double(9,2) NOT NULL,
  `egr_prs` double(9,2) NOT NULL,
  `com_prs` double(9,2) NOT NULL,
  `cau_prs` double(9,2) NOT NULL,
  `pag_prs` double(9,2) NOT NULL,
  `rin_prs` double(9,2) NOT NULL,
  `generado` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`generado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporal para generar ejecucion presupuestaria' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_cargos`
--

CREATE TABLE IF NOT EXISTS `tipos_cargos` (
  `cod_tcar` int(11) NOT NULL AUTO_INCREMENT,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(4) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_tcar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `tipos_cargos`
--

INSERT INTO `tipos_cargos` (`cod_tcar`, `nom_tcar`, `abr_tcar`) VALUES
(1, 'OBRERO CONTRATADO', 'OC'),
(2, 'OBRERO FIJO', 'OF'),
(3, 'ADMINISTRATIVO CONTRATADO', 'AC'),
(4, 'ADMINISTRATIVO FIJO', 'AF'),
(5, 'ASISTENCIAL CONTRATADO', 'ASC'),
(6, 'ASISTENCIAL FIJO', 'ASF'),
(7, 'SUPLENTE OBRERO', 'SO'),
(8, 'SUPLENTE ADMINISTRATIVO', 'SA'),
(9, 'SUPLENTE ASISTENCIAL', 'SAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `cod_usr` varchar(12) NOT NULL COMMENT 'codigo del usuario',
  `nom_usr` varchar(50) NOT NULL COMMENT 'nombre del usuario',
  `ape_usr` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `freg_usr` date NOT NULL COMMENT 'fecha de registro del usuario',
  `log_usr` varchar(15) NOT NULL COMMENT 'login del usuario',
  `pas_usr` text NOT NULL COMMENT 'password del usuario',
  `sts_usr` varchar(1) NOT NULL DEFAULT 'A' COMMENT 'Estado en el que se encuentra el Usuario',
  `int_usr` int(1) NOT NULL DEFAULT '0' COMMENT 'Intentos fallidos de inicio de sesi?n',
  `fcam_usr` date NOT NULL COMMENT 'fecha de cambio del usuario',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  PRIMARY KEY (`cod_usr`),
  KEY `cod_grp` (`cod_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuarios del Sistema';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`cod_usr`, `nom_usr`, `ape_usr`, `freg_usr`, `log_usr`, `pas_usr`, `sts_usr`, `int_usr`, `fcam_usr`, `cod_grp`) VALUES
('11111111111', 'Administrador', 'Administrador', '2011-02-16', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'A', 0, '2016-09-22', 1),
('15031097', 'Luis Felipe', 'Márquez Briceño', '2011-01-01', 'felix', '7ad4109292636b7498a9f5042081becf', 'A', 0, '2016-11-11', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacaciones_per`
--

CREATE TABLE IF NOT EXISTS `vacaciones_per` (
  `cod_sol_vac` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_vac` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `dias_sol_vac` int(2) NOT NULL,
  `ini_sol_vac` date NOT NULL,
  `fin_sol_vac` date NOT NULL,
  `tip_sol_vac` varchar(20) NOT NULL,
  `peri_sol_vac` date NOT NULL,
  `perf_sol_vac` date NOT NULL,
  `obs_sol_vac` longtext NOT NULL,
  `apro_sol_vac` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_vac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vac_dias_per`
--

CREATE TABLE IF NOT EXISTS `vac_dias_per` (
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fch_pag_vac` date NOT NULL,
  `dias_vac` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de días de Vacaciones a disfrutar';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores`
--

CREATE TABLE IF NOT EXISTS `valores` (
  `cod_val` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor',
  PRIMARY KEY (`cod_val`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Volcado de datos para la tabla `valores`
--

INSERT INTO `valores` (`cod_val`, `des_val`, `val_val`, `con_val`) VALUES
(1, 'PH', 150.000, 'Prima por Hijo'),
(2, 'PPT', 0.140, 'Prima de Profesionalización TSU'),
(3, 'PANT1_H30', 0.020, 'Prima Antiguedad de 0 a 5 años (30 Horas)'),
(4, 'PANT1_H36', 0.040, 'Prima Antiguedad de 0 a 5 años (36 Horas)'),
(5, 'PPP', 0.160, 'Prima de Profesionalización Profesional'),
(6, 'PANT1_H40', 0.060, 'Prima Antiguedad de 0 a 5 años (40 Horas)'),
(7, 'PANT2_H30', 0.040, 'Prima Antiguedad de 6 a 10 años (30 Horas)'),
(8, 'PANT2_H36', 0.060, 'Prima Antiguedad de 6 a 10 años (36 Horas)'),
(9, 'PANT2_H40', 0.080, 'Prima Antiguedad de 6 a 10 años (40 Horas)'),
(10, 'PPPO', 0.180, 'Prima de Profesionalización Postgrado'),
(11, 'PANT3_H30', 0.060, 'Prima Antiguedad de 11 a 15 años (30 Horas)'),
(12, 'PANT3_H36', 0.080, 'Prima Antiguedad de 11 a 15 años (36 Horas)'),
(13, 'PANT3_H40', 0.100, 'Prima Antiguedad de 11 a 15 años (40 Horas)'),
(14, 'PANT4_H30', 0.080, 'Prima Antiguedad de 16 a 20 años (30 Horas)'),
(15, 'PANT4_H36', 0.100, 'Prima Antiguedad de 16 a 20 años (36 Horas)'),
(16, 'PANT4_H40', 0.120, 'Prima Antiguedad de 16 a 20 años (40 Horas)'),
(17, 'PANT5_H30', 0.100, 'Prima Antiguedad 21 o mas años (30 Horas)'),
(18, 'PSPS36', 1500.000, 'Prima Sistema Publico de Salud (36 Horas)'),
(19, 'PANT5_H36', 0.120, 'Prima Antiguedad 21 o mas años (36 Horas)'),
(20, 'PANT5_H40', 0.140, 'Prima Antiguedad 21 o mas años (40 Horas)'),
(21, 'PSPS42', 2500.000, 'Prima Sistema Publico de Salud (40-42 Horas)'),
(22, 'PT', 300.000, 'Prima de Transporte'),
(23, ' PA30', 600.000, 'Prima Asistencial (30 Horas)'),
(24, 'PA36', 750.000, 'Prima Asistencial (36 Horas)'),
(25, 'PA42', 850.000, 'Prima Asistencial (40-42 Horas)'),
(26, 'BV30', 54.000, 'Días de Bono Vacacional (30 Horas)'),
(27, 'BV36', 60.000, 'Días de Bono Vacacional (36 Horas)'),
(28, 'BV42', 66.000, 'Días de Bono Vacacional (40-42 Horas)'),
(29, 'BN', 0.500, 'Bono Nocturno'),
(30, 'GDF', 2.000, 'Guardia de Domingo o Feriado'),
(31, 'GDYF', 3.000, 'Guardia de Domingo y Feriado'),
(32, 'PHOG', 0.750, 'Prima por Hogar'),
(33, 'UT', 177.000, 'Unidad Tributaria'),
(34, 'U.T.', 177.000, 'Valor de la Unidad Tributaria'),
(35, 'CT', 2.500, 'Valor del Cesta Ticket diario'),
(48, 'BVI', 40.000, 'Días de Bono Vacacional Asistencial Grado I'),
(49, 'BVII', 40.000, 'Días de Bono Vacacional Asistencial Grado II'),
(50, 'BVIII', 40.000, 'Días de Bono Vacacional Asistencial Grado III'),
(51, 'BVIV', 40.000, 'Días de Bono Vacacional Asistencial Grado IV'),
(52, 'BVV', 40.000, 'Días de Bono Vacacional Asistencial Grado V'),
(53, 'BVVI', 40.000, 'Días de Bono Vacacional Asistencial Grado VI'),
(54, 'BVVII', 40.000, 'Días de Bono Vacacional Asistencial Grado VII'),
(55, 'BVVIII', 42.000, 'Días de Bono Vacacional Asistencial Grado VIII'),
(56, 'BVIX', 44.000, 'Días de Bono Vacacional Asistencial Grado IX'),
(57, 'BVX', 46.000, 'Días de Bono Vacacional Asistencial Grado X'),
(58, 'BVXI', 48.000, 'Días de Bono Vacacional Asistencial Grado XI'),
(59, 'BVXII', 50.000, 'Días de Bono Vacacional Asistencial Grado XII'),
(60, 'AGUI', 90.000, 'Días de Aguinaldos'),
(63, 'APLPH', 2.000, 'Procentaje de Aporte LPH'),
(64, 'RTLPH', 1.000, 'Procentaje de Retención LPH'),
(65, 'APSSO', 9.000, 'Procentaje de Aporte SSO'),
(66, 'RTSSO', 4.000, 'Procentaje de Retención SSO'),
(69, 'SM', 1.000, 'Sueldo Mínimo Vigenge'),
(70, 'SMMSSO', 1.000, 'Cantidad de sueldos mínimos máximos para calculos del IVSS'),
(71, 'APPIE', 2.000, 'Procentaje de Aporte PIE'),
(72, 'RTPIE', 0.005, 'Procentaje de Retención PIE'),
(73, 'APCAH', 10.000, 'Procentaje de Aporte Caja Ahorro'),
(74, 'RTCAH', 10.000, 'Procentaje de Retención Caja Ahorro'),
(75, 'MEDESP', 1000.000, 'Prima Medico Especialista'),
(76, 'BECAPRE', 400.000, 'Monto en Bs. por beca Preescolar'),
(77, 'BECAPRI', 300.000, 'Monto en Bs. por beca Primaria'),
(78, 'BECASEC', 400.000, 'Monto en Bs. por beca Secundaria'),
(79, 'BECAUNI', 400.000, 'Monto en Bs. por beca Universitaria'),
(80, 'JUGUETE', 300.000, 'Monto en Bs. por Juguetes'),
(81, 'BALI', 200.000, 'Monto para Bono de Alimientación'),
(82, 'PEXC', 0.300, 'Prima de exclusividad'),
(83, 'd', 10.000, 'd');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_asignaciones_per`
--
CREATE TABLE IF NOT EXISTS `vista_asignaciones_per` (
`cod_cnp` int(11)
,`ced_per` varchar(12)
,`nom_con` varchar(60)
,`con_cnp` varchar(100)
,`ncuo_cnp` int(3)
,`ncp_cnp` int(3)
,`nom_car` varchar(100)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_cargos_per`
--
CREATE TABLE IF NOT EXISTS `vista_cargos_per` (
`nac_per` varchar(1)
,`ced_per` varchar(12)
,`cod_car` int(11)
,`cod_tcar` int(11)
,`cod_dep` varchar(5)
,`num_car` int(3) unsigned zerofill
,`nom_car` varchar(100)
,`fch_asg` date
,`fch_vac` date
,`nom_per` varchar(50)
,`ape_per` varchar(50)
,`nom_tcar` varchar(50)
,`mon_sue` double(9,2)
,`des_sue` varchar(50)
,`mon_sue_ant` double(9,2)
,`num_cue` varchar(20)
,`ban_cue` varchar(50)
,`tip_cue` varchar(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_cargos_salida`
--
CREATE TABLE IF NOT EXISTS `vista_cargos_salida` (
`nac_per` varchar(1)
,`ced_per` varchar(12)
,`cod_car` int(11)
,`cod_tcar` int(11)
,`cod_dep` varchar(5)
,`num_car` int(3) unsigned zerofill
,`nom_car` varchar(100)
,`fch_asg` date
,`fch_vac` date
,`nom_per` varchar(50)
,`ape_per` varchar(50)
,`nom_tcar` varchar(50)
,`mon_sue` double(9,2)
,`des_sue` varchar(50)
,`mon_sue_ant` double(9,2)
,`num_cue` varchar(20)
,`ban_cue` varchar(50)
,`tip_cue` varchar(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_deducciones_per`
--
CREATE TABLE IF NOT EXISTS `vista_deducciones_per` (
`cod_dsp` int(11)
,`ced_per` varchar(12)
,`nom_des` varchar(50)
,`con_dsp` varchar(100)
,`ncuo_dsp` int(3)
,`ncp_dsp` int(3)
,`nom_car` varchar(100)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_ing_pat`
--
CREATE TABLE IF NOT EXISTS `vista_ing_pat` (
`id` int(11)
,`fecha` date
,`serie` int(3)
,`inicio` int(8)
,`fin` int(8)
,`tipo` varchar(2)
,`serie_id` int(11)
,`recibo` int(11)
,`monto` decimal(9,2)
,`cod_cat` int(2)
,`con_esp` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominapre_proc`
--
CREATE TABLE IF NOT EXISTS `vista_nominapre_proc` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominaspre_pagadas`
--
CREATE TABLE IF NOT EXISTS `vista_nominaspre_pagadas` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominaspre_proc`
--
CREATE TABLE IF NOT EXISTS `vista_nominaspre_proc` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominas_pagadas`
--
CREATE TABLE IF NOT EXISTS `vista_nominas_pagadas` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nominpre_tot_sum`
--
CREATE TABLE IF NOT EXISTS `vista_nominpre_tot_sum` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
,`cod_tcar` int(11)
,`abr_tcar` varchar(2)
,`cod_dep` varchar(5)
,`nom_tcar` varchar(50)
,`tot_sue` double(19,2)
,`tot_lph` double(19,2)
,`tot_spf` double(19,2)
,`tot_sso` double(19,2)
,`tot_cah` double(19,2)
,`tot_sfu` double(19,2)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_nomin_tot_sum`
--
CREATE TABLE IF NOT EXISTS `vista_nomin_tot_sum` (
`ano_nom` int(4)
,`mes_nom` int(2)
,`por_nom` int(1)
,`cod_tcar` int(11)
,`abr_tcar` varchar(2)
,`cod_dep` varchar(5)
,`nom_tcar` varchar(50)
,`tot_sue` double(19,2)
,`tot_lph` double(19,2)
,`tot_spf` double(19,2)
,`tot_sso` double(19,2)
,`tot_cah` double(19,2)
,`tot_sfu` double(19,2)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_personal`
--
CREATE TABLE IF NOT EXISTS `vista_personal` (
`ced_per` varchar(12)
,`nombre` varchar(101)
,`nac_per` varchar(1)
,`abr_per` varchar(10)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_reembolsos_per`
--
CREATE TABLE IF NOT EXISTS `vista_reembolsos_per` (
`cod_ree` int(11)
,`ced_per` varchar(12)
,`cod_car` int(11)
,`cod_cob` int(11)
,`fec_reg` date
,`nom_pro` varchar(50)
,`rif_pro` varchar(12)
,`tel_pro` int(11)
,`nro_fac` varchar(20)
,`fec_fac` date
,`mon_fac` double(9,2)
,`mon_pag` double(9,2)
,`fec_pag` date
,`fec_nom` date
,`nom_per` varchar(50)
,`ape_per` varchar(50)
,`nom_car` varchar(100)
,`num_cue` varchar(20)
,`tip_cue` varchar(1)
,`ban_cue` varchar(50)
,`nombre_per` varchar(101)
,`nom_cob` varchar(50)
,`mon_cob` double(9,2)
,`bas_cob` bigint(11)
,`cod_tcar` bigint(11)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_salida_sincargo`
--
CREATE TABLE IF NOT EXISTS `vista_salida_sincargo` (
`cod_pag` int(11)
,`mon_pag` double(9,2)
,`ced_per` varchar(12)
,`con_pag` varchar(150)
,`des_car` varchar(1)
,`fch_pag` date
,`cod_dep` varchar(5)
,`nom_dep` varchar(150)
,`cod_car` int(11)
,`nom_car` varchar(100)
,`fch_vac` date
,`cod_tcar` int(11)
,`nom_tcar` varchar(50)
,`abr_tcar` varchar(2)
);
-- --------------------------------------------------------

--
-- Estructura para la vista `vista_asignaciones_per`
--
DROP TABLE IF EXISTS `vista_asignaciones_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_asignaciones_per` AS select `asg`.`cod_cnp` AS `cod_cnp`,`asg`.`ced_per` AS `ced_per`,`cn`.`nom_con` AS `nom_con`,`asg`.`con_cnp` AS `con_cnp`,`asg`.`ncuo_cnp` AS `ncuo_cnp`,`asg`.`ncp_cnp` AS `ncp_cnp`,`c`.`nom_car` AS `nom_car` from ((`asignaciones` `asg` join `concesiones` `cn`) join `cargos` `c`) where ((`asg`.`cod_con` = `cn`.`cod_con`) and (`c`.`cod_car` = `asg`.`cod_car`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_cargos_per`
--
DROP TABLE IF EXISTS `vista_cargos_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_cargos_per` AS select `p`.`nac_per` AS `nac_per`,`c`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_tcar` AS `cod_tcar`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`fch_asg` AS `fch_asg`,`c`.`fch_vac` AS `fch_vac`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant`,(select `cuentas`.`num_cue` from `cuentas` where (`cuentas`.`ced_per` = `c`.`ced_per`)) AS `num_cue`,(select `cuentas`.`ban_cue` from `cuentas` where (`cuentas`.`ced_per` = `c`.`ced_per`)) AS `ban_cue`,(select `cuentas`.`tip_cue` from `cuentas` where (`cuentas`.`ced_per` = `c`.`ced_per`)) AS `tip_cue` from ((((`cargos` `c` join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `c`.`ced_per`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_cargos_salida`
--
DROP TABLE IF EXISTS `vista_cargos_salida`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_cargos_salida` AS select `p`.`nac_per` AS `nac_per`,`p`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_tcar` AS `cod_tcar`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`pm`.`fch_asg` AS `fch_asg`,`pm`.`fch_vac` AS `fch_vac`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant`,(select `cuentas`.`num_cue` from `cuentas` where (`cuentas`.`ced_per` = `p`.`ced_per`)) AS `num_cue`,(select `cuentas`.`ban_cue` from `cuentas` where (`cuentas`.`ced_per` = `p`.`ced_per`)) AS `ban_cue`,(select `cuentas`.`tip_cue` from `cuentas` where (`cuentas`.`ced_per` = `c`.`ced_per`)) AS `tip_cue` from (((((`prog_movimientos` `pm` join `cargos` `c`) join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`pm`.`cod_car` = `c`.`cod_car`) and (`pm`.`accion` = 2) and (`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `pm`.`ced_per`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_deducciones_per`
--
DROP TABLE IF EXISTS `vista_deducciones_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_deducciones_per` AS select `dd`.`cod_dsp` AS `cod_dsp`,`dd`.`ced_per` AS `ced_per`,`ds`.`nom_des` AS `nom_des`,`dd`.`con_dsp` AS `con_dsp`,`dd`.`ncuo_dsp` AS `ncuo_dsp`,`dd`.`ncp_dsp` AS `ncp_dsp`,`c`.`nom_car` AS `nom_car` from ((`deducciones` `dd` join `descuentos` `ds`) join `cargos` `c`) where ((`dd`.`cod_des` = `ds`.`cod_des`) and (`c`.`cod_car` = `dd`.`cod_car`));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_ing_pat`
--
DROP TABLE IF EXISTS `vista_ing_pat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_ing_pat` AS (select `ing_pat`.`id` AS `id`,`ing_pat`.`fecha` AS `fecha`,`ing_pat`.`serie` AS `serie`,`ing_pat`.`inicio` AS `inicio`,`ing_pat`.`fin` AS `fin`,`ing_pat`.`tipo` AS `tipo`,`adt_ing_pro`.`serie_id` AS `serie_id`,`adt_ing_pro`.`recibo` AS `recibo`,`adt_ing_pro`.`monto` AS `monto`,`adt_ing_pro`.`cod_cat` AS `cod_cat`,`adt_ing_pro`.`con_esp` AS `con_esp` from (`ing_pat_vhi` `ing_pat` join `adt_ing_pro`) where ((`adt_ing_pro`.`serie_id` = `ing_pat`.`id`) and (`adt_ing_pro`.`recibo` >= `ing_pat`.`inicio`) and (`adt_ing_pro`.`recibo` <= `ing_pat`.`fin`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominapre_proc`
--
DROP TABLE IF EXISTS `vista_nominapre_proc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominapre_proc` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominaspre_pagadas`
--
DROP TABLE IF EXISTS `vista_nominaspre_pagadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominaspre_pagadas` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominaspre_proc`
--
DROP TABLE IF EXISTS `vista_nominaspre_proc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominaspre_proc` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominas_pagadas`
--
DROP TABLE IF EXISTS `vista_nominas_pagadas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominas_pagadas` AS select `nomina_pagar`.`ano_nom` AS `ano_nom`,`nomina_pagar`.`mes_nom` AS `mes_nom`,`nomina_pagar`.`por_nom` AS `por_nom` from `nomina_pagar` group by `nomina_pagar`.`ano_nom`,`nomina_pagar`.`mes_nom`,`nomina_pagar`.`por_nom`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nominpre_tot_sum`
--
DROP TABLE IF EXISTS `vista_nominpre_tot_sum`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nominpre_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nominapre_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_nomin_tot_sum`
--
DROP TABLE IF EXISTS `vista_nomin_tot_sum`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_nomin_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nomina_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_personal`
--
DROP TABLE IF EXISTS `vista_personal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_personal` AS select `personal`.`ced_per` AS `ced_per`,concat(`personal`.`nom_per`,' ',`personal`.`ape_per`) AS `nombre`,`personal`.`nac_per` AS `nac_per`,`personal`.`abr_per` AS `abr_per` from `personal`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_reembolsos_per`
--
DROP TABLE IF EXISTS `vista_reembolsos_per`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_reembolsos_per` AS select `rm`.`cod_ree` AS `cod_ree`,`rm`.`ced_per` AS `ced_per`,`rm`.`cod_car` AS `cod_car`,`rm`.`cod_cob` AS `cod_cob`,`rm`.`fec_reg` AS `fec_reg`,`rm`.`nom_pro` AS `nom_pro`,`rm`.`rif_pro` AS `rif_pro`,`rm`.`tel_pro` AS `tel_pro`,`rm`.`nro_fac` AS `nro_fac`,`rm`.`fec_fac` AS `fec_fac`,`rm`.`mon_fac` AS `mon_fac`,`rm`.`mon_pag` AS `mon_pag`,`rm`.`fec_pag` AS `fec_pag`,`rm`.`fec_nom` AS `fec_nom`,`rm`.`nom_per` AS `nom_per`,`rm`.`ape_per` AS `ape_per`,`rm`.`nom_car` AS `nom_car`,`rm`.`num_cue` AS `num_cue`,`rm`.`tip_cue` AS `tip_cue`,`rm`.`ban_cue` AS `ban_cue`,(select concat(`personal`.`ape_per`,' ',`personal`.`nom_per`) from `personal` where (convert(`personal`.`ced_per` using utf8) = `rm`.`ced_per`)) AS `nombre_per`,(select `coberturas_med`.`nom_cob` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `nom_cob`,(select `coberturas_med`.`mon_cob` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `mon_cob`,(select `coberturas_med`.`bas_cob` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `bas_cob`,(select `coberturas_med`.`cod_tcar` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `cod_tcar` from `reembolsos_med` `rm`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_salida_sincargo`
--
DROP TABLE IF EXISTS `vista_salida_sincargo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_salida_sincargo` AS select `prog_mov_pagos`.`cod_pag` AS `cod_pag`,`prog_mov_pagos`.`mon_pag` AS `mon_pag`,`prog_mov_pagos`.`ced_per` AS `ced_per`,`prog_mov_pagos`.`con_pag` AS `con_pag`,`prog_mov_pagos`.`des_car` AS `des_car`,`prog_mov_pagos`.`fch_pag` AS `fch_pag`,`prog_mov_pagos`.`cod_dep` AS `cod_dep`,`prog_mov_pagos`.`nom_dep` AS `nom_dep`,`prog_mov_pagos`.`cod_car` AS `cod_car`,`prog_mov_pagos`.`nom_car` AS `nom_car`,`prog_mov_pagos`.`fch_vac` AS `fch_vac`,`prog_mov_pagos`.`cod_tcar` AS `cod_tcar`,`prog_mov_pagos`.`nom_tcar` AS `nom_tcar`,`prog_mov_pagos`.`abr_tcar` AS `abr_tcar` from (`prog_mov_pagos` left join `cargos` on((`cargos`.`ced_per` = `prog_mov_pagos`.`ced_per`))) where (not(`prog_mov_pagos`.`ced_per` in (select distinct `cargos`.`ced_per` from `cargos`)));

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inasistencias`
--
ALTER TABLE `inasistencias`
  ADD CONSTRAINT `inasistencias_ibfk_1` FOREIGN KEY (`ced_per`) REFERENCES `personal` (`ced_per`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inasistencias_ibfk_2` FOREIGN KEY (`cod_car`) REFERENCES `cargos` (`cod_car`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
