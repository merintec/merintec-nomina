-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: nomina_hospital
-- ------------------------------------------------------
-- Server version	5.5.38-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actividades`
--

DROP TABLE IF EXISTS `actividades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actividades` (
  `cod_act` varchar(5) NOT NULL COMMENT 'codigo de la Actividad',
  `nom_act` varchar(150) NOT NULL COMMENT 'nombre de la actividad',
  PRIMARY KEY (`cod_act`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actividades`
--

LOCK TABLES `actividades` WRITE;
/*!40000 ALTER TABLE `actividades` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adt_ing_cat`
--

DROP TABLE IF EXISTS `adt_ing_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adt_ing_cat` (
  `cod_cat` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Categoría',
  `des_cat` varchar(50) NOT NULL COMMENT 'Descripción de Categoría',
  PRIMARY KEY (`cod_cat`),
  UNIQUE KEY `des_cat` (`des_cat`),
  UNIQUE KEY `des_cat_2` (`des_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las Categorias de los recibos para Auditorias';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adt_ing_cat`
--

LOCK TABLES `adt_ing_cat` WRITE;
/*!40000 ALTER TABLE `adt_ing_cat` DISABLE KEYS */;
/*!40000 ALTER TABLE `adt_ing_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adt_ing_pro`
--

DROP TABLE IF EXISTS `adt_ing_pro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adt_ing_pro` (
  `serie_id` int(11) NOT NULL,
  `recibo` int(11) NOT NULL,
  `monto` decimal(9,2) NOT NULL,
  `cod_cat` int(2) NOT NULL,
  `con_esp` varchar(50) NOT NULL COMMENT 'Condición Especial',
  UNIQUE KEY `serie_id` (`serie_id`,`recibo`,`monto`,`cod_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adt_ing_pro`
--

LOCK TABLES `adt_ing_pro` WRITE;
/*!40000 ALTER TABLE `adt_ing_pro` DISABLE KEYS */;
/*!40000 ALTER TABLE `adt_ing_pro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anular_form`
--

DROP TABLE IF EXISTS `anular_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anular_form` (
  `cod_anl` int(11) NOT NULL AUTO_INCREMENT,
  `fch_anl` date NOT NULL,
  `tip_anl` varchar(1) NOT NULL,
  `num_anl` int(5) unsigned zerofill NOT NULL,
  `mot_anl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_anl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los Formularios Anulados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anular_form`
--

LOCK TABLES `anular_form` WRITE;
/*!40000 ALTER TABLE `anular_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `anular_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignaciones`
--

DROP TABLE IF EXISTS `asignaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignaciones` (
  `cod_cnp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_cnp` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `ncuo_cnp` int(3) NOT NULL COMMENT 'Numero de cuotas de la asignacion',
  `per_cnp` varchar(1) NOT NULL COMMENT 'Permanencia de la asignacion',
  `mcuo_cnp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la asignacion',
  `por_cnp` double(9,2) NOT NULL COMMENT 'Porcentaje de asignacion',
  `bpor_cnp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `ced_per` varchar(12) NOT NULL COMMENT 'La cedula de la persona',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_cnp` date NOT NULL COMMENT 'Fecha de Registro de la asignacion',
  `ncp_cnp` int(3) NOT NULL COMMENT 'N?mero de cuotas pagadas',
  `des_cnp` varchar(1) NOT NULL COMMENT 'Incide sobre salario base (B), integral (I) o ninguno (N)',
  `asig_mas` int(11) DEFAULT NULL COMMENT 'Tendra un valor al ser una asignacion masiva',
  PRIMARY KEY (`cod_cnp`),
  KEY `cod_con` (`cod_con`),
  KEY `ced_per` (`ced_per`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignaciones`
--

LOCK TABLES `asignaciones` WRITE;
/*!40000 ALTER TABLE `asignaciones` DISABLE KEYS */;
INSERT INTO `asignaciones` VALUES (26,'Pago diciembre 2',3,'S',122.00,1.00,'B',1,'16657064',2,'2015-06-15',1,'S',1),(27,'Pago diciembre 2',3,'S',122.00,1.00,'B',1,'15031097',9,'2015-06-15',1,'S',1),(28,'Pago diciembre 2',3,'S',122.00,1.00,'B',1,'15098765',12,'2015-06-15',1,'S',1),(29,'Prima Especial',1,'S',0.00,1.18,'B',1,'15031097',14,'2016-04-04',0,'B',NULL),(30,'Prima especial #2',1,'S',0.00,250.00,'U',1,'15031097',14,'2016-04-04',0,'B',NULL),(31,'Prima especial # 3',1,'S',1000.00,0.00,'',1,'15031097',14,'2016-04-04',0,'I',NULL),(32,'Solo N cuotas',3,'',1200.00,0.00,'',1,'15031097',14,'2016-04-04',0,'N',NULL),(33,'Prima especial #4',5,'',100.00,0.00,'',1,'15031097',14,'2016-04-04',0,'B',NULL);
/*!40000 ALTER TABLE `asignaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` VALUES (1,'15031097','insertar','cod_val=0001;des_val=prueba de valores;val_val=12000;con_val=prueba','valores','2015-05-22','14:18:00'),(2,'15031097','insertar','cod_val=;des_val=prueba de valores;val_val=12000;con_val=prueba','valores','2015-05-25','15:50:00'),(3,'15031097','insertar','cod_dep=POL;nom_dep=POLICIA MUNICIPAL;cod_act=;cod_pro=','dependencias','2015-05-25','15:57:00'),(4,'15031097','insertar','mon_sue=6000;des_sue=ASISTENTE ADMINISTRATIVO','sueldos','2015-05-25','16:07:00'),(5,'15031097','insertar','mon_sue=6000;des_sue=SECRETARIO','sueldos','2015-05-25','16:07:00'),(6,'15031097','insertar','mon_sue=6291;des_sue=ESCOLTA','sueldos','2015-05-25','16:09:00'),(7,'15031097','insertar','mon_sue=9776.22;des_sue=ABOGADO','sueldos','2015-05-25','16:11:00'),(8,'15031097','insertar','mon_sue=14664.34;des_sue=DIRECTOR GENERAL','sueldos','2015-05-25','16:15:00'),(9,'15031097','insertar','mon_sue=11242.65;des_sue=DIRECTORA DE ADMINISTRACION','sueldos','2015-05-25','16:15:00'),(10,'15031097','insertar','mon_sue=11242.65;des_sue=DIRECTORA RECURSOS HUMANOS','sueldos','2015-05-25','16:16:00'),(11,'15031097','insertar','mon_sue=7923;des_sue=SERVIDOR AGREGADO','sueldos','2015-05-25','16:19:00'),(12,'15031097','insertar','mon_sue=6672;des_sue=OFICIAL JEFE Nº2','sueldos','2015-05-25','16:20:00'),(13,'15031097','insertar','mon_sue=6672;des_sue=OFICIAL JEFE Nº3','sueldos','2015-05-25','16:20:00'),(14,'15031097','eliminar','mon_sue=6672.00;des_sue=OFICIAL JEFE Nº3','sueldos','2015-05-25','16:21:00'),(15,'15031097','modificar','','sueldos','2015-05-25','16:22:00'),(16,'15031097','insertar','mon_sue=6291;des_sue=OFICIAL AGREGADO','sueldos','2015-05-25','16:22:00'),(17,'15031097','insertar','mon_sue=7404;des_sue=SEPERVISOR','sueldos','2015-05-25','16:23:00'),(18,'15031097','insertar','mon_sue=5880;des_sue=OFICIAL','sueldos','2015-05-25','16:24:00'),(19,'15031097','insertar','mon_sue=5376.92;des_sue=Personal Civil','sueldos','2015-05-25','16:38:00'),(20,'15031097','insertar','num_car=;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=;est_car=;cod_tcar=;cod_dep=;cod_hor=;des_car=X','cargos','2015-05-25','16:53:00'),(21,'15031097','eliminar','num_car=000;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=0;est_car=;cod_tcar=0;cod_dep=;cod_hor=0;des_car=X','cargos','2015-05-25','16:53:00'),(22,'15031097','insertar','num_car=1;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=1;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','16:59:00'),(23,'15031097','insertar','num_car=2;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=1;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:06:00'),(24,'15031097','insertar','num_car=3;nom_car=ASISTENTE ADMINISTRATIVO;cod_sue=1;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:06:00'),(25,'15031097','insertar','num_car=4;nom_car=SECRETARIO;cod_sue=2;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:07:00'),(26,'15031097','insertar','num_car=5;nom_car=SECRETARIO;cod_sue=2;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:07:00'),(27,'15031097','insertar','num_car=6;nom_car=ESCOLTA;cod_sue=3;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:08:00'),(28,'15031097','insertar','num_car=7;nom_car=ABOGADO;cod_sue=4;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:10:00'),(29,'15031097','insertar','num_car=8;nom_car=OFICIAL Nº1;cod_sue=13;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:14:00'),(30,'15031097','insertar','num_car=9;nom_car=OFICIAL Nº2;cod_sue=13;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:14:00'),(31,'15031097','insertar','num_car=10;nom_car=OFICIAL Nº 1;cod_sue=13;est_car=;cod_tcar=2;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-25','17:14:00'),(32,'15031097','modificar','','cargos','2015-05-25','17:15:00'),(33,'15031097','insertar','nom_con=Retroactivo aumento ;des_con=sASAsaS','concesiones','2015-05-25','17:20:00'),(34,'15031097','insertar','nom_des=Comedor;des_des=Comedor','descuentos','2015-05-25','17:25:00'),(35,'15031097','insertar','nom_des=Plan Coorporativo;des_des=Plan Coorporativo','descuentos','2015-05-25','17:26:00'),(36,'15031097','insertar','nom_des=Reino del Mueble;des_des=Reino del Mueble','descuentos','2015-05-25','17:26:00'),(37,'15031097','insertar','ced_per=16657064;nac_per=V;nom_per=JUAN JOSE;ape_per=MARQUEZ TREJO;sex_per=M;fnac_per=1993-04-15;lnac_per=MERIDA;cor_per=JUANJMT@GMAIL.COM;pro_per=ING.;abr_per=ING.;dir_per=EJIDO;tel_per=000000000;cel_per=000000000;fch_reg=2015-05-25;lph_des=A;spf_des=A;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=A;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=','personal','2015-05-25','17:49:00'),(38,'15031097','insertar','ced_per=16657064;tit_edu=Ingeniero;niv_edu=Univ;ins_edu=IUTE;fch_edu=2015-05-19;reg_edu=S','educacion','2015-05-25','18:05:00'),(39,'15031097','insertar','ced_per=16657064;fch_ina=2015-05-27;tip_ina=No Remunerada;des_ina=NO VINO','inasistencias','2015-05-27','17:59:00'),(40,'15031097','modificar','','personal','2015-05-27','18:14:00'),(41,'15031097','insertar','con_cnp=retractivo;ncuo_cnp=1;per_cnp=S;mcuo_cnp=120;por_cnp=10;bpor_cnp=B;cod_con=1;ced_per=16657064;cod_car=2;fch_cnp=2015-05-27;ncp_cnp=;des_cnp=','asignaciones','2015-05-27','18:28:00'),(42,'15031097','eliminar','con_cnp=retractivo;ncuo_cnp=1;per_cnp=S;mcuo_cnp=120.00;por_cnp=10;bpor_cnp=B;cod_con=1;ced_per=16657064;cod_car=2;fch_cnp=2015-05-27;ncp_cnp=0;des_cnp=','asignaciones','2015-05-27','18:28:00'),(43,'15031097','insertar','con_cnp=retractivo;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=10;bpor_cnp=B;cod_con=1;ced_per=16657064;cod_car=2;fch_cnp=2015-05-27;ncp_cnp=;des_cnp=S','asignaciones','2015-05-27','18:31:00'),(44,'15031097','modificar','','asignaciones','2015-05-27','18:31:00'),(45,'15031097','eliminar','con_cnp=retractivo;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0.00;por_cnp=12;bpor_cnp=B;cod_con=1;ced_per=16657064;cod_car=2;fch_cnp=2015-05-27;ncp_cnp=0;des_cnp=S','asignaciones','2015-05-27','18:31:00'),(46,'15031097','insertar','con_cnp=retractivo;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=12;bpor_cnp=B;cod_con=1;ced_per=16657064;cod_car=2;fch_cnp=2015-05-27;ncp_cnp=12;des_cnp=S','asignaciones','2015-05-27','18:32:00'),(47,'15031097','insertar','ced_per=16657064;tit_edu=Ingeniero quimica;niv_edu=Univ;ins_edu=IUTE;fch_edu=2015-05-19;reg_edu=S','educacion','2015-05-27','18:38:00'),(48,'15031097','modificar','','educacion','2015-05-27','18:38:00'),(49,'15031097','insertar','con_dsp=Comida chatarra;ncuo_dsp=1;per_dsp=;mcuo_dsp=200;por_dsp=0;bpor_dsp=;cod_des=1;ced_per=16657064;cod_car=2;fch_dsp=2015-05-11;ncp_dsp=','deducciones','2015-05-27','18:39:00'),(50,'15031097','insertar','con_dsp=Comida chatarra;ncuo_dsp=1;per_dsp=S;mcuo_dsp=200;por_dsp=0;bpor_dsp=;cod_des=2;ced_per=16657064;cod_car=2;fch_dsp=2015-05-11;ncp_dsp=','deducciones','2015-05-27','18:40:00'),(51,'15031097','modificar','','deducciones','2015-05-27','18:43:00'),(52,'15031097','insertar','ced_per=;fch_ina=2015-05-25;tip_ina=Remunerada;des_ina=Medico','inasistencias','2015-05-27','18:49:00'),(53,'15031097','insertar','ced_per=16657064;fch_ina=2015-05-25;tip_ina=Remunerada;des_ina=Medico','inasistencias','2015-05-27','18:50:00'),(54,'15031097','insertar','ced_per=16657064;fch_ina=2015-05-24;tip_ina=Remunerada;des_ina=Medico','inasistencias','2015-05-27','18:51:00'),(55,'15031097','insertar','fch_frd=2015-05-29;des_frd=dia de yo;tip_frd=feriado;tck_frd=SI','feriados','2015-05-27','18:55:00'),(56,'15031097','insertar','cod_val=;des_val=VALCES;val_val=0.75;con_val=Porcentaje diario para el pago de cesta tiket','valores','2015-05-27','18:58:00'),(57,'15031097','insertar','fch_frd=2015-05-18;des_frd=Prueba Feriado;tip_frd=feriado;tck_frd=NO','feriados','2015-05-28','15:38:00'),(58,'15031097','modificar','','feriados','2015-05-28','15:40:00'),(59,'15031097','eliminar','fch_frd=2015-05-29;des_frd=dia feriado;tip_frd=feriado;tck_frd=SI','feriados','2015-05-28','15:40:00'),(60,'15031097','insertar','ced_per=15098765;nac_per=V;nom_per=Pedro;ape_per= Perez;sex_per=M;fnac_per=1993-04-15;lnac_per=MERIDA;cor_per=JUANJMT@GMAIL.COM;pro_per=ING.;abr_per=ING.;dir_per=Ejido;tel_per=0274221356;cel_per=0274221356;fch_reg=2015-05-28;lph_des=A;spf_des=A;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=A;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=A','personal','2015-05-28','15:43:00'),(61,'15031097','insertar','num_car=11;nom_car=Personal Civil;cod_sue=14;est_car=;cod_tcar=4;cod_dep=POL;cod_hor=1;des_car=X','cargos','2015-05-28','15:45:00'),(62,'15031097','modificar','','personal','2015-05-28','16:45:00'),(63,'15031097','insertar','ced_per=15031097;nac_per=V;nom_per=Luis;ape_per=Marquez;sex_per=M;fnac_per=1981-03-09;lnac_per=Merida;cor_per=felix@felix.com;pro_per=Ingeniero;abr_per=Ing;dir_per=Ejido;tel_per=22100000;cel_per=04260000;fch_reg=2015-05-28;lph_des=A;spf_des=A;sso_des=A;cah_des=A;sfu_des=A;hog_asg=A;hij_asg=A;ant_asg=;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=;rie_asg=A;pat_asg=A;coo_asg=A','personal','2015-05-28','16:49:00'),(64,'15031097','modificar','','personal','2015-05-28','16:50:00'),(65,'15031097','modificar','','personal','2015-05-28','16:51:00'),(66,'15031097','modificar','','valores','2015-05-29','18:09:00'),(67,'15031097','modificar','','valores','2015-05-29','18:10:00'),(68,'15031097','modificar','','valores','2015-05-29','18:59:00'),(69,'15031097','insertar','cod_val=;des_val=PRIRIE;val_val=410.50;con_val=PRIMA DE RIEGO CON MONTO EN BS.','valores','2015-05-29','19:20:00'),(70,'15031097','insertar','cod_val=;des_val=PRIPAT;val_val=300;con_val=PRIMA DE PATRULLERO MONTO EN BS','valores','2015-05-29','19:22:00'),(71,'15031097','insertar','cod_val=;des_val=PRICOO;val_val=300;con_val=PRIMA DE COORDINADOR MONTO EN BS','valores','2015-05-29','19:23:00'),(72,'15031097','modificar','','valores','2015-06-02','10:28:00'),(73,'15031097','modificar','','valores','2015-06-02','10:35:00'),(74,'15031097','modificar','','valores','2015-06-02','10:36:00'),(75,'15031097','insertar','nom_con=Bono Hallaquero;des_con=Bono Hallaquero','concesiones','2015-06-02','11:15:00'),(76,'15031097','insertar','con_cnp=Pago diciembre ;ncuo_cnp=1;per_cnp=;mcuo_cnp=1200;por_cnp=1;bpor_cnp=B;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=S','asignaciones','2015-06-02','11:19:00'),(77,'15031097','insertar','con_cnp=Pago diciembre ;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:23:00'),(78,'15031097','insertar','con_cnp=Pago diciembre ;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15031097;cod_car=9;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:23:00'),(79,'15031097','insertar','con_cnp=Pago diciembre ;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15098765;cod_car=12;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:23:00'),(80,'15031097','insertar','con_cnp=Pago diciembre 2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:24:00'),(81,'15031097','insertar','con_cnp=Pago diciembre 2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15031097;cod_car=9;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:24:00'),(82,'15031097','insertar','con_cnp=Pago diciembre 2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15098765;cod_car=12;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:24:00'),(83,'15031097','insertar','con_cnp=Pago diciembre 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:26:00'),(84,'15031097','insertar','con_cnp=Pago diciembre 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15031097;cod_car=9;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:26:00'),(85,'15031097','insertar','con_cnp=Pago diciembre 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15098765;cod_car=12;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:26:00'),(86,'15031097','insertar','con_cnp=Pago diciembre 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:28:00'),(87,'15031097','insertar','con_cnp=Pago diciembre 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15031097;cod_car=9;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:28:00'),(88,'15031097','insertar','con_cnp=Pago diciembre 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1200;por_cnp=1;bpor_cnp=;cod_con=2;ced_per=15098765;cod_car=12;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=','asignaciones','2015-06-02','11:28:00'),(89,'15031097','insertar','con_cnp=bono;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=12;bpor_cnp=B;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=12;des_cnp=S;asig_mas=1','asignaciones','2015-06-02','16:31:00'),(90,'15031097','insertar','con_cnp=bono;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=12;bpor_cnp=B;cod_con=2;ced_per=15031097;cod_car=9;fch_cnp=2015-06-15;ncp_cnp=12;des_cnp=S;asig_mas=1','asignaciones','2015-06-02','16:31:00'),(91,'15031097','insertar','con_cnp=bono;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=12;bpor_cnp=B;cod_con=2;ced_per=15098765;cod_car=12;fch_cnp=2015-06-15;ncp_cnp=12;des_cnp=S;asig_mas=1','asignaciones','2015-06-02','16:31:00'),(92,'15031097','insertar','con_cnp=bono 2 ;ncuo_cnp=1;per_cnp=S;mcuo_cnp=232323;por_cnp=12;bpor_cnp=B;cod_con=2;ced_per=16657064;cod_car=2;fch_cnp=2015-06-15;ncp_cnp=12;des_cnp=S;asig_mas=2','asignaciones','2015-06-02','16:33:00'),(93,'15031097','insertar','con_cnp=bono 2 ;ncuo_cnp=1;per_cnp=S;mcuo_cnp=232323;por_cnp=12;bpor_cnp=B;cod_con=2;ced_per=15031097;cod_car=9;fch_cnp=2015-06-15;ncp_cnp=12;des_cnp=S;asig_mas=2','asignaciones','2015-06-02','16:33:00'),(94,'15031097','insertar','con_cnp=bono 2 ;ncuo_cnp=1;per_cnp=S;mcuo_cnp=232323;por_cnp=12;bpor_cnp=B;cod_con=2;ced_per=15098765;cod_car=12;fch_cnp=2015-06-15;ncp_cnp=12;des_cnp=S;asig_mas=2','asignaciones','2015-06-02','16:33:00'),(95,'11111111111','insertar','con_dsp=Todos compraron muebles;ncuo_dsp=1;per_dsp=S;mcuo_dsp=0;por_dsp=10;bpor_dsp=B;cod_des=3;ced_per=16657064;cod_car=2;fch_dsp=2015-05-11;ncp_dsp=1;dedu_mas=1','deducciones','2015-06-02','22:11:00'),(96,'11111111111','insertar','con_dsp=Todos compraron muebles;ncuo_dsp=1;per_dsp=S;mcuo_dsp=0;por_dsp=10;bpor_dsp=B;cod_des=3;ced_per=15031097;cod_car=9;fch_dsp=2015-05-11;ncp_dsp=1;dedu_mas=1','deducciones','2015-06-02','22:11:00'),(97,'11111111111','insertar','con_dsp=Todos compraron muebles;ncuo_dsp=1;per_dsp=S;mcuo_dsp=0;por_dsp=10;bpor_dsp=B;cod_des=3;ced_per=15098765;cod_car=12;fch_dsp=2015-05-11;ncp_dsp=1;dedu_mas=1','deducciones','2015-06-02','22:11:00'),(98,'11111111111','insertar','con_dsp=Movilnet;ncuo_dsp=1;per_dsp=S;mcuo_dsp=0;por_dsp=50;bpor_dsp=;cod_des=2;ced_per=16657064;cod_car=2;fch_dsp=2015-06-01;ncp_dsp=1;dedu_mas=2','deducciones','2015-06-02','22:49:00'),(99,'11111111111','insertar','con_dsp=Movilnet;ncuo_dsp=1;per_dsp=S;mcuo_dsp=0;por_dsp=50;bpor_dsp=;cod_des=2;ced_per=15031097;cod_car=9;fch_dsp=2015-06-01;ncp_dsp=1;dedu_mas=2','deducciones','2015-06-02','22:49:00'),(100,'11111111111','insertar','con_dsp=Movilnet;ncuo_dsp=1;per_dsp=S;mcuo_dsp=0;por_dsp=50;bpor_dsp=;cod_des=2;ced_per=15098765;cod_car=12;fch_dsp=2015-06-01;ncp_dsp=1;dedu_mas=2','deducciones','2015-06-02','22:49:00'),(101,'15031097','modificar','','asignaciones','2015-06-03','09:39:00'),(102,'15031097','modificar','','asignaciones','2015-06-03','09:40:00'),(103,'15031097','insertar','con_cnp=Retroactivo aumento 1er de mayo;ncuo_cnp=1;per_cnp=;mcuo_cnp=1200;por_cnp=5;bpor_cnp=B;cod_con=1;ced_per=16657064;cod_car=2;fch_cnp=2015-06-08;ncp_cnp=1;des_cnp=S;asig_mas=1','asignaciones','2015-06-03','09:48:00'),(104,'15031097','insertar','con_cnp=Retroactivo aumento 1er de mayo;ncuo_cnp=1;per_cnp=;mcuo_cnp=1200;por_cnp=5;bpor_cnp=B;cod_con=1;ced_per=15031097;cod_car=9;fch_cnp=2015-06-08;ncp_cnp=1;des_cnp=S;asig_mas=1','asignaciones','2015-06-03','09:48:00'),(105,'15031097','insertar','con_cnp=Retroactivo aumento 1er de mayo;ncuo_cnp=1;per_cnp=;mcuo_cnp=1200;por_cnp=5;bpor_cnp=B;cod_con=1;ced_per=15098765;cod_car=12;fch_cnp=2015-06-08;ncp_cnp=1;des_cnp=S;asig_mas=1','asignaciones','2015-06-03','09:48:00'),(106,'15031097','modificar','','asignaciones','2015-06-03','09:49:00'),(107,'15031097','insertar','con_cnp=Pago diciembre 2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=122;por_cnp=1;bpor_cnp=B;cod_con=1;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=S;ced_per=16657064;cod_car=2;asig_mas=1','asignaciones','2015-06-03','09:56:00'),(108,'15031097','insertar','con_cnp=Pago diciembre 2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=122;por_cnp=1;bpor_cnp=B;cod_con=1;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=S;ced_per=15031097;cod_car=9;asig_mas=1','asignaciones','2015-06-03','09:56:00'),(109,'15031097','insertar','con_cnp=Pago diciembre 2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=122;por_cnp=1;bpor_cnp=B;cod_con=1;fch_cnp=2015-06-15;ncp_cnp=1;des_cnp=S;ced_per=15098765;cod_car=12;asig_mas=1','asignaciones','2015-06-03','09:56:00'),(110,'15031097','modificar','','asignaciones','2015-06-03','09:57:00'),(111,'15031097','insertar','cod_con=;fch_con=2015-06-05;ced_per=15031097;nom_per=Luis Felipe Márquez Briceño;nom_dep=POLICIA MUNICIPAL;nom_car=OFICIAL Nº1;fch_ing=2015-05-20;des_con=Credito;sue_con=A;prm_con=;cst_con=;sue_mon=5880.00;prm_mon=317.5;cst_mon=1397;mot_con=;tit_edu=','constancias_per','2015-06-05','10:24:00'),(112,'15031097','modificar','','valores','2015-06-05','11:29:00'),(113,'15031097','insertar','cod_con=;fch_con=2015-06-08;ced_per=16657064;nom_per= ;nom_dep=POLICIA MUNICIPAL;nom_car=ASISTENTE ADMINISTRATIVO;fch_ing=2015-05-05;des_con=Credito;sue_con=A;prm_con=;cst_con=;sue_mon=6000.00;prm_mon=525;cst_mon=1650;mot_con=;tit_edu=','constancias_per','2015-06-08','08:49:00'),(114,'15031097','insertar','cod_con=;fch_con=2015-06-08;ced_per=16657064;nom_per=JUAN JOSE MARQUEZ TREJO;nom_dep=POLICIA MUNICIPAL;nom_car=ASISTENTE ADMINISTRATIVO;fch_ing=2015-05-05;des_con=Credito;sue_con=;prm_con=;cst_con=;sue_mon=6000.00;prm_mon=525;cst_mon=1650;mot_con=fsdfsdfdsfdsfdsfdsf;tit_edu=Ingeniero','constancias_per','2015-06-08','09:12:00'),(115,'15031097','insertar','cod_con=;fch_con=2015-06-08;ced_per=16657064;nom_per=JUAN JOSE MARQUEZ TREJO;nom_dep=POLICIA MUNICIPAL;nom_car=ASISTENTE ADMINISTRATIVO;fch_ing=2015-05-05;des_con=Credito;sue_con=A;prm_con=A;cst_con=A;sue_mon=6000.00;prm_mon=525;cst_mon=1650;mot_con=fsdfsdfdsfdsfdsfdsf;tit_edu=Ingeniero','constancias_per','2015-06-08','09:13:00'),(116,'15031097','eliminar','cod_val=51;des_val=prueba de valores;val_val=12000.000;con_val=prueba','valores','2016-01-20','17:19:00'),(117,'15031097','eliminar','cod_dep=POL;nom_dep=POLICIA MUNICIPAL;cod_act=;cod_pro=','dependencias','2016-01-20','17:21:00'),(118,'15031097','insertar','nom_con=prima;des_con=prima','concesiones','2016-01-20','18:00:00'),(119,'15031097','insertar','cod_dep=UC;nom_dep=UNIDAD DE CONTABILIDAD;cod_act=;cod_pro=','dependencias','2016-01-21','16:02:00'),(120,'15031097','insertar','cod_dep=DRH;nom_dep=DIRECCION DE RECURSOS HUMANOS','dependencias','2016-01-21','16:11:00'),(121,'15031097','insertar','cod_dep=EA;nom_dep=EMERGENCIA ADULTO','dependencias','2016-01-21','16:18:00'),(122,'15031097','insertar','cod_dep=CG;nom_dep=CIRUGIA GENERAL','dependencias','2016-01-21','16:18:00'),(123,'15031097','insertar','cod_dep=TO;nom_dep=TOMOGRAFIA','dependencias','2016-01-21','16:19:00'),(124,'15031097','insertar','cod_dep=ND;nom_dep=NUTRICION Y DIETETICA','dependencias','2016-01-21','16:19:00'),(125,'15031097','insertar','mon_sue=9648.18;des_sue=OBRERO GRADO 1','sueldos','2016-01-21','16:35:00'),(126,'15031097','insertar','mon_sue=10244.16;des_sue=OBRERO GRADO 5','sueldos','2016-01-21','16:35:00'),(127,'15031097','insertar','mon_sue=11871.35;des_sue=ENFERMERA/O PIII','sueldos','2016-01-21','16:43:00'),(128,'15031097','insertar','mon_sue=11871.35;des_sue=BIOANALISTA PIII','sueldos','2016-01-21','16:44:00'),(129,'15031097','insertar','mon_sue=15258;des_sue=MEDICO ESPECIALISTA II (6HRS)','sueldos','2016-01-21','16:46:00'),(130,'15031097','insertar','mon_sue=7630;des_sue=MEDICO ESPECIALISTA II (3HRS)','sueldos','2016-01-21','16:46:00'),(131,'15031097','modificar','','sueldos','2016-01-21','16:54:00'),(132,'15031097','eliminar','mon_sue=11871.35;des_sue=BIOANALISTA PIII','sueldos','2016-01-21','16:55:00'),(133,'15031097','insertar','mon_sue=15911.58;des_sue=ADMINISTRATIVO PIII','sueldos','2016-01-21','16:57:00'),(134,'15031097','insertar','mon_sue=10309.73;des_sue=ADMINISTRATIVO BIII','sueldos','2016-01-21','16:59:00'),(135,'15031097','insertar','num_car=1;nom_car=Secretaria del Director;cod_sue=21;est_car=;cod_tcar=4;cod_dep=DRH;des_car=X;tip_ces=completo;num_hrs=40','cargos','2016-01-21','17:24:00'),(136,'15031097','insertar','num_car=1;nom_car=Medico de Medicina Interna;cod_sue=20;est_car=;cod_tcar=6;cod_dep=CG;des_car=X;tip_ces=completo;num_hrs=40','cargos','2016-01-21','17:28:00'),(137,'15031097','insertar','num_car=1;nom_car=Medico de Medicina Interna;cod_sue=20;est_car=;cod_tcar=6;cod_dep=CG;des_car=X;tip_ces=completo;num_hrs=40','cargos','2016-01-21','17:31:00'),(138,'15031097','insertar','num_car=1;nom_car=ASISTENTE ADMINISTRATIVO BIII;cod_sue=22;est_car=;cod_tcar=3;cod_dep=DRH;des_car=X;tip_ces=completo;num_hrs=36','cargos','2016-01-21','17:38:00'),(139,'15031097','insertar','num_car=2;nom_car=ASISTENTE ADMINISTRATIVO BIII;cod_sue=22;est_car=;cod_tcar=3;cod_dep=DRH;des_car=X;tip_ces=completo;num_hrs=40','cargos','2016-01-21','17:38:00'),(140,'15031097','insertar','num_car=3;nom_car=DIRECTOR GENERAL;cod_sue=21;est_car=;cod_tcar=4;cod_dep=DRH;des_car=X;tip_ces=completo;num_hrs=40','cargos','2016-01-21','17:41:00'),(141,'15031097','insertar','num_car=1;nom_car=VIGILANTE;cod_sue=16;est_car=;cod_tcar=2;cod_dep=EA;des_car=X;tip_ces=completo;num_hrs=40','cargos','2016-01-21','17:47:00'),(142,'15031097','insertar','num_car=2;nom_car=VIGILANTE;cod_sue=16;est_car=;cod_tcar=2;cod_dep=CG;des_car=;tip_ces=completo;num_hrs=36','cargos','2016-01-21','17:49:00'),(143,'15031097','insertar','ced_per=16657064;nac_per=V;nom_per=Juan José;ape_per=Márquez Trejo;sex_per=M;fnac_per=1983-06-09;lnac_per=Merida;cor_per=juanjmt@gmail.com;pro_per=Ingeniero;abr_per=Ing;dir_per=Ejido;tel_per=04265772755;cel_per=04265772755;fch_reg=2016-01-22;lph_des=A;spf_des=;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=;rie_asg=;pat_asg=;coo_asg=;bono_util=;bono_juge=','personal','2016-01-22','15:28:00'),(144,'15031097','insertar','ced_per=16657064;nac_per=V;nom_per=Juan José;ape_per=Márquez Trejo;sex_per=M;fnac_per=1983-06-09;lnac_per=Merida;cor_per=juanjmt@gmail.com;pro_per=Ingeniero;abr_per=Ing;dir_per=Ejido;tel_per=04265772755;cel_per=04265772755;fch_reg=2016-01-22;lph_des=A;spf_des=;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=;rie_asg=;pat_asg=;coo_asg=;bono_util=;bono_juge=','personal','2016-01-22','15:39:00'),(145,'15031097','insertar','ced_per=16657064;nac_per=V;nom_per=Juan José;ape_per=Márquez Trejo;sex_per=M;fnac_per=1983-06-09;lnac_per=Merida;cor_per=juanjmt@gmail.com;pro_per=Ingeniero;abr_per=Ing;dir_per=Ejido;tel_per=04265772755;cel_per=04265772755;fch_reg=2016-01-22;lph_des=A;spf_des=;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=;rie_asg=;pat_asg=;coo_asg=;bono_util=;bono_juge=','personal','2016-01-22','15:40:00'),(146,'15031097','insertar','ced_per=16657064;nac_per=V;nom_per=Juan José;ape_per=Márquez Trejo;sex_per=M;fnac_per=1983-06-09;lnac_per=Merida;cor_per=juanjmt@gmail.com;pro_per=Ingeniero;abr_per=Ing;dir_per=Ejido;tel_per=04265772755;cel_per=04265772755;fch_reg=2016-01-22;lph_des=A;spf_des=;sso_des=A;cah_des=A;sfu_des=;hog_asg=A;hij_asg=A;ant_asg=;pro_asg=A;hje_asg=;jer_asg=;otr_asg=;fnj_des=;rie_asg=;pat_asg=;coo_asg=;bono_util=;bono_juge=','personal','2016-01-22','15:41:00'),(147,'15031097','insertar','ced_per=16657064;tit_edu=Ingeniero;niv_edu=Univ;ins_edu=UPTM;fch_edu=2016-01-11;reg_edu=S','educacion','2016-01-22','16:26:00'),(148,'15031097','insertar','cod_val=;des_val=PH;val_val=150;con_val=Prima por Hijo','valores','2016-01-22','17:06:00'),(149,'15031097','insertar','cod_val=;des_val=PPT;val_val=0.14;con_val=Prima de Profesionalización TSU','valores','2016-01-22','17:09:00'),(150,'15031097','insertar','cod_val=;des_val=PANT1_H30;val_val=0.02;con_val=Prima Antiguedad de 0 a 5 años (30 Horas)','valores','2016-01-22','17:10:00'),(151,'15031097','insertar','cod_val=;des_val=PANT1_H36;val_val=0.04;con_val=Prima Antiguedad de 0 a 5 años (36 Horas)','valores','2016-01-22','17:10:00'),(152,'15031097','insertar','cod_val=;des_val=PPP;val_val=0.16;con_val=Prima de Profesionalización Profesional','valores','2016-01-22','17:10:00'),(153,'15031097','insertar','cod_val=;des_val=PANT1_H40;val_val=0.06;con_val=Prima Antiguedad de 0 a 5 años (40 Horas)','valores','2016-01-22','17:10:00'),(154,'15031097','insertar','cod_val=;des_val=PANT2_H30;val_val=0.04;con_val=Prima Antiguedad de 6 a 10 años (30 Horas)','valores','2016-01-22','17:11:00'),(155,'15031097','insertar','cod_val=;des_val=PANT2_H36;val_val=0.06;con_val=Prima Antiguedad de 6 a 10 años (36 Horas)','valores','2016-01-22','17:12:00'),(156,'15031097','insertar','cod_val=;des_val=PANT2_H40;val_val=0.08;con_val=Prima Antiguedad de 6 a 10 años (40 Horas)','valores','2016-01-22','17:12:00'),(157,'15031097','insertar','cod_val=;des_val=PPPO;val_val=0.18;con_val=Prima de Profesionalización Postgrado','valores','2016-01-22','17:13:00'),(158,'15031097','insertar','cod_val=;des_val=PANT3_H30;val_val=0.06;con_val=Prima Antiguedad de 11 a 15 años (30 Horas)','valores','2016-01-22','17:13:00'),(159,'15031097','insertar','cod_val=;des_val=PANT3_H36;val_val=0.08;con_val=Prima Antiguedad de 11 a 15 años (36 Horas)','valores','2016-01-22','17:14:00'),(160,'15031097','insertar','cod_val=;des_val=PANT3_H40;val_val=0.10;con_val=Prima Antiguedad de 11 a 15 años (40 Horas)','valores','2016-01-22','17:14:00'),(161,'15031097','insertar','cod_val=;des_val=PANT4_H30;val_val=0.08;con_val=Prima Antiguedad de 16 a 20 años (30 Horas)','valores','2016-01-22','17:15:00'),(162,'15031097','insertar','cod_val=;des_val=PANT4_H36;val_val=0.10;con_val=Prima Antiguedad de 16 a 20 años (36 Horas)','valores','2016-01-22','17:15:00'),(163,'15031097','insertar','cod_val=;des_val=PANT4_H40;val_val=0.12;con_val=Prima Antiguedad de 16 a 20 años (40 Horas)','valores','2016-01-22','17:16:00'),(164,'15031097','insertar','cod_val=;des_val=PANT5_H30;val_val=0.10;con_val=Prima Antiguedad 21 o mas años (30 Horas)','valores','2016-01-22','17:16:00'),(165,'15031097','insertar','cod_val=;des_val=PSPS36;val_val=1500;con_val=Prima Sistema Publico de Salud 36','valores','2016-01-22','17:16:00'),(166,'15031097','insertar','cod_val=;des_val=PANT5_H36;val_val=0.12;con_val=Prima Antiguedad 21 o mas años (36 Horas)','valores','2016-01-22','17:16:00'),(167,'15031097','insertar','cod_val=;des_val=PANT5_H40;val_val=0.14;con_val=Prima Antiguedad 21 o mas años (40 Horas)','valores','2016-01-22','17:17:00'),(168,'15031097','insertar','cod_val=;des_val=PSPS42;val_val=2500;con_val=Prima Sistema Publico de Salud 40-42','valores','2016-01-22','17:18:00'),(169,'15031097','modificar','','valores','2016-01-22','17:19:00'),(170,'15031097','modificar','','valores','2016-01-22','17:20:00'),(171,'15031097','modificar','','valores','2016-01-22','17:22:00'),(172,'15031097','insertar','cod_val=;des_val=PT;val_val=300;con_val=Prima de Transporte','valores','2016-01-22','17:25:00'),(173,'15031097','insertar','cod_val=;des_val= PA30;val_val=600;con_val=Prima Asistencial (30 Horas)','valores','2016-01-22','17:29:00'),(174,'15031097','insertar','cod_val=;des_val=PA36;val_val=750;con_val=Prima Asistencial (36 Horas)','valores','2016-01-22','17:29:00'),(175,'15031097','insertar','cod_val=;des_val=PA42;val_val=850;con_val=Prima Asistencial (40-42 Horas)','valores','2016-01-22','17:30:00'),(176,'15031097','insertar','cod_val=;des_val=BV30;val_val=54;con_val=Bono Vacacional (30 Horas)','valores','2016-01-22','17:33:00'),(177,'15031097','insertar','cod_val=;des_val=BV36;val_val=60;con_val=Bono Vacacional (36 Horas)','valores','2016-01-22','17:34:00'),(178,'15031097','insertar','cod_val=;des_val=BV42;val_val=66;con_val=Bono Vacacional (40-42 Horas)','valores','2016-01-22','17:35:00'),(179,'15031097','insertar','cod_val=;des_val=BN;val_val=0.50;con_val=Bono Nocturno','valores','2016-01-22','17:38:00'),(180,'15031097','insertar','cod_val=;des_val=GDF;val_val=2;con_val=Guardia de Domingo o Feriado','valores','2016-01-22','17:39:00'),(181,'15031097','insertar','cod_val=;des_val=GDYF;val_val=3;con_val=Guardia de Domingo y Feriado','valores','2016-01-22','17:42:00'),(182,'15031097','insertar','ced_per=15031097;nac_per=V;nom_per=Luis Felipe;ape_per=Márquez Briceño;sex_per=M;fnac_per=1981-03-09;lnac_per=Mérida;cor_per=felixpe09@gmail.com;pro_per=Ingeniero en Sistemas;abr_per=Ing.;dir_per=Asoprieto Calle 4A #242 PA. Ejido;tel_per=02742217980;cel_per=04266736316;fch_reg=2016-03-08;lph_des=A;spf_des=A;sso_des=A;cah_des=A;sfu_des=A;hog_asg=A;hij_asg=A;ant_asg=;pro_asg=A;hje_asg=;jer_asg=A;otr_asg=;fnj_des=;rie_asg=A;pat_asg=A;coo_asg=A;bono_util=A;bono_juge=A','personal','2016-03-08','17:19:00'),(183,'15031097','insertar','cod_val=;des_val=PHOG;val_val=0.75;con_val=Prima por Hogar','valores','2016-03-30','17:03:00'),(184,'15031097','modificar','','familiares','2016-03-30','17:14:00'),(185,'15031097','modificar','','familiares','2016-03-30','17:14:00'),(186,'15031097','modificar','','familiares','2016-03-30','17:14:00'),(187,'15031097','modificar','','familiares','2016-03-30','17:15:00'),(188,'15031097','modificar','','familiares','2016-03-30','17:15:00'),(189,'15031097','insertar','ced_per=15031097;tit_edu=TSU Informatica;niv_edu=TecS;ins_edu=IUTE;fch_edu=2016-03-01;reg_edu=S','educacion','2016-03-31','11:08:00'),(190,'15031097','insertar','ced_per=15031097;tit_edu=Ing. Informatica;niv_edu=Univ;ins_edu=Universidad Politécnica Klever;fch_edu=2016-03-08;reg_edu=S','educacion','2016-03-31','11:09:00'),(191,'15031097','modificar','','educacion','2016-03-31','11:16:00'),(192,'15031097','modificar','','educacion','2016-03-31','11:17:00'),(193,'15031097','insertar','ced_per=15031097;tit_edu=Esp. Informatica;niv_edu=5;ins_edu=Universidad Politécnica Klever;fch_edu=2016-03-31;reg_edu=S','educacion','2016-03-31','11:17:00'),(194,'15031097','modificar','','cargos','2016-03-31','16:37:00'),(195,'15031097','modificar','','cargos','2016-03-31','16:39:00'),(196,'15031097','modificar','','cargos','2016-03-31','16:40:00'),(197,'15031097','modificar','','personal','2016-03-31','17:14:00'),(198,'15031097','modificar','','personal','2016-03-31','17:20:00'),(199,'15031097','insertar','con_cnp=Prima Especial;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=10;bpor_cnp=B;cod_con=1;ced_per=15031097;cod_car=14;fch_cnp=2016-04-04;ncp_cnp=;des_cnp=S','asignaciones','2016-04-04','15:56:00'),(200,'15031097','insertar','con_cnp=Prima especial #2;ncuo_cnp=1;per_cnp=S;mcuo_cnp=0;por_cnp=99;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=14;fch_cnp=2016-04-04;ncp_cnp=;des_cnp=S','asignaciones','2016-04-04','15:58:00'),(201,'15031097','modificar','','asignaciones','2016-04-04','15:58:00'),(202,'15031097','insertar','con_cnp=Prima especial # 3;ncuo_cnp=1;per_cnp=S;mcuo_cnp=1000;por_cnp=;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=14;fch_cnp=2016-04-04;ncp_cnp=;des_cnp=S','asignaciones','2016-04-04','15:59:00'),(203,'15031097','insertar','con_cnp=Solo N cuotas;ncuo_cnp=3;per_cnp=;mcuo_cnp=1200;por_cnp=;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=14;fch_cnp=2016-04-04;ncp_cnp=;des_cnp=','asignaciones','2016-04-04','16:02:00'),(204,'15031097','modificar','','asignaciones','2016-04-04','16:22:00'),(205,'15031097','insertar','cod_val=;des_val=UT;val_val=177;con_val=Unidad Tributaria','valores','2016-04-04','16:24:00'),(206,'15031097','insertar','con_cnp=Prima especial #4;ncuo_cnp=1;per_cnp=;mcuo_cnp=100;por_cnp=;bpor_cnp=;cod_con=1;ced_per=15031097;cod_car=14;fch_cnp=2016-04-04;ncp_cnp=;des_cnp=S','asignaciones','2016-04-04','16:32:00'),(207,'15031097','modificar','','asignaciones','2016-04-04','16:42:00'),(208,'15031097','modificar','','asignaciones','2016-04-04','16:52:00'),(209,'15031097','modificar','','asignaciones','2016-04-04','17:34:00'),(210,'15031097','modificar','','asignaciones','2016-04-04','17:35:00'),(211,'15031097','modificar','','sueldos','2016-04-07','08:43:00');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditoria_backup`
--

DROP TABLE IF EXISTS `auditoria_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria_backup` (
  `cod_aud` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de Auditor?a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci?n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci?n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu? la acci?n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci?n',
  `hora` time NOT NULL COMMENT 'hora de la acci?n',
  PRIMARY KEY (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria_backup`
--

LOCK TABLES `auditoria_backup` WRITE;
/*!40000 ALTER TABLE `auditoria_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditoria_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `cod_ban` int(11) NOT NULL AUTO_INCREMENT,
  `nom_ban` varchar(50) DEFAULT NULL,
  `cue_ban` varchar(20) DEFAULT NULL,
  `des_ban` varchar(50) NOT NULL COMMENT 'Destino de la Cuenta',
  PRIMARY KEY (`cod_ban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco_conciliacion`
--

DROP TABLE IF EXISTS `banco_conciliacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco_conciliacion` (
  `cod_ban_conc` int(11) NOT NULL AUTO_INCREMENT,
  `cod_ban` int(11) NOT NULL,
  `mes_ban_conc` int(11) NOT NULL,
  `ano_ban_conc` int(4) NOT NULL,
  `sal_ban_conc` decimal(9,2) NOT NULL,
  `obs_ban_conc` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ban_conc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las concialiaciones bancarias';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco_conciliacion`
--

LOCK TABLES `banco_conciliacion` WRITE;
/*!40000 ALTER TABLE `banco_conciliacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `banco_conciliacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco_movimientos`
--

DROP TABLE IF EXISTS `banco_movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco_movimientos` (
  `cod_ban_mov` int(11) NOT NULL AUTO_INCREMENT,
  `fch_ban_mov` date NOT NULL,
  `tip_ban_mov` varchar(10) NOT NULL,
  `mon_ban_mov` double(9,2) NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `des_ban_mov` varchar(255) NOT NULL,
  `obs_ban_mov` varchar(255) NOT NULL,
  `ref_ban_mov` varchar(50) NOT NULL,
  `sta_ban_mov` int(11) NOT NULL,
  PRIMARY KEY (`cod_ban_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de los movimientos bancarios';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco_movimientos`
--

LOCK TABLES `banco_movimientos` WRITE;
/*!40000 ALTER TABLE `banco_movimientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `banco_movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargos` (
  `cod_car` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo del cargo',
  `num_car` int(3) unsigned zerofill NOT NULL COMMENT 'numero del cargo',
  `nom_car` varchar(100) NOT NULL COMMENT 'nombre del cargo',
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `est_car` varchar(1) NOT NULL COMMENT 'estado del cargo',
  `cod_tcar` int(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `ced_per` varchar(12) NOT NULL,
  `fch_asg` date NOT NULL COMMENT 'Fecha de asignacion  del cargo',
  `fch_vac` date NOT NULL,
  `cod_hor` int(11) NOT NULL,
  `tip_ces` varchar(30) NOT NULL,
  `noc_car` varchar(1) NOT NULL,
  `car_nn` varchar(10) NOT NULL COMMENT 'Cargo  nueve nueve',
  PRIMARY KEY (`cod_car`),
  UNIQUE KEY `num_car` (`num_car`,`cod_dep`),
  KEY `cod_sue` (`cod_sue`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

LOCK TABLES `cargos` WRITE;
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
INSERT INTO `cargos` VALUES (13,001,'Medico de Medicina Interna',20,'A',6,'CG','16657064','2016-01-22','2016-01-01',0,'completo','',''),(14,001,'ASISTENTE ADMINISTRATIVO BIII',22,'A',3,'DRH','15031097','2016-03-09','2005-10-01',0,'completo','X',''),(15,002,'ASISTENTE ADMINISTRATIVO BIII',22,'',3,'DRH','','0000-00-00','0000-00-00',0,'completo','',''),(16,003,'DIRECTOR GENERAL',21,'',4,'DRH','','0000-00-00','0000-00-00',0,'completo','',''),(17,001,'VIGILANTE',16,'',2,'EA','','0000-00-00','0000-00-00',0,'completo','',''),(18,002,'VIGILANTE',16,'D',2,'CG','','0000-00-00','0000-00-00',0,'completo','','');
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coberturas_med`
--

DROP TABLE IF EXISTS `coberturas_med`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coberturas_med` (
  `cod_cob` int(11) NOT NULL AUTO_INCREMENT,
  `nom_cob` varchar(50) NOT NULL,
  `mon_cob` double(9,2) NOT NULL,
  `bas_cob` int(1) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  PRIMARY KEY (`cod_cob`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coberturas_med`
--

LOCK TABLES `coberturas_med` WRITE;
/*!40000 ALTER TABLE `coberturas_med` DISABLE KEYS */;
/*!40000 ALTER TABLE `coberturas_med` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras` (
  `cod_com` int(11) NOT NULL AUTO_INCREMENT,
  `fch_com` date NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `nor_com` int(4) unsigned zerofill NOT NULL,
  `frm_com` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `sol_com` varchar(20) DEFAULT NULL,
  `tip_com` varchar(15) NOT NULL,
  `for_com` varchar(15) DEFAULT NULL,
  `adl_com` double(9,2) DEFAULT NULL,
  `fre_com` varchar(255) DEFAULT NULL,
  `mon_com` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `rif_pro` varchar(10) NOT NULL,
  `npr_com` int(2) NOT NULL,
  `iva_com` int(2) NOT NULL,
  `ela_com` varchar(50) NOT NULL,
  `rev_com` varchar(50) NOT NULL,
  `obs_com` varchar(255) DEFAULT NULL,
  KEY `id` (`cod_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `concesiones`
--

DROP TABLE IF EXISTS `concesiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concesiones` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de concesion',
  `nom_con` varchar(60) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre del tipo de concesion',
  `des_con` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de concesion',
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `concesiones`
--

LOCK TABLES `concesiones` WRITE;
/*!40000 ALTER TABLE `concesiones` DISABLE KEYS */;
INSERT INTO `concesiones` VALUES (1,'prima','prima');
/*!40000 ALTER TABLE `concesiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `constancias_per`
--

DROP TABLE IF EXISTS `constancias_per`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `constancias_per` (
  `cod_con` int(11) NOT NULL AUTO_INCREMENT,
  `fch_con` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `nom_dep` varchar(100) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `des_con` varchar(50) NOT NULL,
  `sue_con` varchar(1) NOT NULL,
  `prm_con` varchar(1) NOT NULL,
  `cst_con` varchar(1) NOT NULL,
  `sue_mon` decimal(9,2) NOT NULL,
  `prm_mon` decimal(9,2) NOT NULL,
  `cst_mon` decimal(9,2) NOT NULL,
  `mot_con` varchar(120) NOT NULL,
  PRIMARY KEY (`cod_con`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Constancias Solicitadas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `constancias_per`
--

LOCK TABLES `constancias_per` WRITE;
/*!40000 ALTER TABLE `constancias_per` DISABLE KEYS */;
/*!40000 ALTER TABLE `constancias_per` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cst_tk_pagada`
--

DROP TABLE IF EXISTS `cst_tk_pagada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cst_tk_pagada` (
  `cod_cst` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código de Cesta Ticket Pagada',
  `mes_cst` int(11) NOT NULL COMMENT 'Mes de la Cesta Ticket pagada',
  `ano_cst` int(11) NOT NULL COMMENT 'Año de de la Cesta Ticket pagada',
  `ced_per` int(11) NOT NULL COMMENT 'Cédula del Personal de la Cesta Ticket pagada',
  `dias_cst` int(11) NOT NULL COMMENT 'Total de días del mes de la Cesta Ticket pagada',
  `dias_cst_pag` int(11) NOT NULL COMMENT 'Total de días pagados por mes de la Cesta Ticket pagada',
  `ut_cst` int(11) NOT NULL COMMENT 'Bs. por día de la Cesta Ticket pagada',
  `mnt_cst` int(11) NOT NULL COMMENT 'Total Bs. por personal de la Cesta Ticket pagada',
  PRIMARY KEY (`cod_cst`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de la Cesta Ticket pagada';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cst_tk_pagada`
--

LOCK TABLES `cst_tk_pagada` WRITE;
/*!40000 ALTER TABLE `cst_tk_pagada` DISABLE KEYS */;
/*!40000 ALTER TABLE `cst_tk_pagada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentas` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `num_cue` varchar(20) NOT NULL COMMENT 'numero de cuenta de la persona',
  `tip_cue` varchar(1) NOT NULL COMMENT 'tipo de la cuenta de la persona',
  `fcam_cue` date NOT NULL COMMENT 'fecha de cambio del numero de cuenta',
  `ban_cue` varchar(50) NOT NULL,
  PRIMARY KEY (`ced_per`),
  UNIQUE KEY `num_cue` (`num_cue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas`
--

LOCK TABLES `cuentas` WRITE;
/*!40000 ALTER TABLE `cuentas` DISABLE KEYS */;
INSERT INTO `cuentas` VALUES ('16657064','01234567890123456789','1','2016-01-22','Venezuela');
/*!40000 ALTER TABLE `cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deducciones`
--

DROP TABLE IF EXISTS `deducciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deducciones` (
  `cod_dsp` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la Deduccion',
  `con_dsp` varchar(100) NOT NULL COMMENT 'Concepto de la Deduccion',
  `ncuo_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas de la deduccion',
  `per_dsp` varchar(1) NOT NULL COMMENT 'Permanencia de la Deduccion',
  `mcuo_dsp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la deduccion',
  `por_dsp` int(3) NOT NULL COMMENT 'Porcentaje de deduccion',
  `bpor_dsp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del Descuento',
  `ced_per` varchar(12) NOT NULL COMMENT 'Cedula del Personal',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_dsp` date NOT NULL COMMENT 'Fecha de Registro de la Deduccion',
  `ncp_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas pagadas',
  `dedu_mas` int(11) NOT NULL,
  PRIMARY KEY (`cod_dsp`),
  KEY `cod_des` (`cod_des`),
  KEY `cod_per` (`ced_per`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deducciones`
--

LOCK TABLES `deducciones` WRITE;
/*!40000 ALTER TABLE `deducciones` DISABLE KEYS */;
INSERT INTO `deducciones` VALUES (2,'Movilnet',1,'S',200.00,0,'',2,'16657064',2,'2015-05-11',0,0),(3,'Todos compraron muebles',1,'S',0.00,10,'B',3,'16657064',2,'2015-05-11',1,1),(4,'Todos compraron muebles',1,'S',0.00,10,'B',3,'15031097',9,'2015-05-11',1,1),(5,'Todos compraron muebles',1,'S',0.00,10,'B',3,'15098765',12,'2015-05-11',1,1),(6,'Movilnet',1,'S',0.00,50,'',2,'16657064',2,'2015-06-01',1,2),(7,'Movilnet',1,'S',0.00,50,'',2,'15031097',9,'2015-06-01',1,2),(8,'Movilnet',1,'S',0.00,50,'',2,'15098765',12,'2015-06-01',1,2);
/*!40000 ALTER TABLE `deducciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencias`
--

DROP TABLE IF EXISTS `dependencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencias` (
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `nom_dep` varchar(150) NOT NULL COMMENT 'nombre de la dependencia',
  `cod_act` varchar(5) NOT NULL COMMENT 'actividad de la dependencia',
  `cod_pro` varchar(5) NOT NULL COMMENT 'programa de la dependencia',
  PRIMARY KEY (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencias`
--

LOCK TABLES `dependencias` WRITE;
/*!40000 ALTER TABLE `dependencias` DISABLE KEYS */;
INSERT INTO `dependencias` VALUES ('CG','CIRUGIA GENERAL','',''),('DRH','DIRECCION DE RECURSOS HUMANOS','',''),('EA','EMERGENCIA ADULTO','',''),('ND','NUTRICION Y DIETETICA','',''),('TO','TOMOGRAFIA','',''),('UC','UNIDAD DE CONTABILIDAD','','');
/*!40000 ALTER TABLE `dependencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descuentos`
--

DROP TABLE IF EXISTS `descuentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descuentos` (
  `cod_des` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de descuento',
  `nom_des` varchar(50) NOT NULL COMMENT 'Nombre del tipo de descuento',
  `des_des` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de descuento',
  PRIMARY KEY (`cod_des`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descuentos`
--

LOCK TABLES `descuentos` WRITE;
/*!40000 ALTER TABLE `descuentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `descuentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `educacion`
--

DROP TABLE IF EXISTS `educacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educacion` (
  `cod_edu` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `niv_edu` varchar(50) NOT NULL,
  `ins_edu` varchar(100) NOT NULL,
  `fch_edu` date NOT NULL,
  `reg_edu` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_edu`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Datos de los Estudios Realizados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educacion`
--

LOCK TABLES `educacion` WRITE;
/*!40000 ALTER TABLE `educacion` DISABLE KEYS */;
INSERT INTO `educacion` VALUES (1,'16657064','Ingeniero','4','UPTM','2016-01-11','S'),(2,'15031097','TSU Informatica','3','IUTE','2016-03-01','S'),(3,'15031097','Ing. Informatica','4','Universidad Politécnica Klever','2016-03-08','S'),(4,'15031097','Esp. Informatica','5','Universidad Politécnica Klever','2016-03-31','S');
/*!40000 ALTER TABLE `educacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `egresos`
--

DROP TABLE IF EXISTS `egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `egresos` (
  `cod_egr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_egr` date NOT NULL,
  `paga_imp` varchar(20) NOT NULL COMMENT 'Si paga impuesto indique que tipo',
  `nor_egr` int(4) unsigned zerofill NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `chq_egr` varchar(25) NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `con_egr` varchar(255) NOT NULL,
  `ret_iva_egr` int(3) NOT NULL,
  `ret_isrl_egr` int(2) NOT NULL,
  `ela_egr` varchar(50) NOT NULL,
  `rev_egr` varchar(50) NOT NULL,
  `apr_egr` varchar(50) NOT NULL,
  `cont_egr` varchar(50) NOT NULL,
  `obs_egr` varchar(255) DEFAULT NULL,
  `ded_egr` double(9,2) NOT NULL COMMENT 'Otras Deducciones',
  `sin_par_egr` decimal(9,2) NOT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `sta_ban_egr` int(11) NOT NULL,
  KEY `id` (`cod_egr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `egresos`
--

LOCK TABLES `egresos` WRITE;
/*!40000 ALTER TABLE `egresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entradas`
--

DROP TABLE IF EXISTS `entradas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entradas` (
  `cod_ntr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C?digo de la Entrada',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C?digo del usuario',
  `fecha_ntr` date NOT NULL COMMENT 'fecha de la entrada',
  `hora_ntr` char(15) NOT NULL COMMENT 'Hora de la Entrada',
  PRIMARY KEY (`cod_ntr`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1 COMMENT='Entradas de los Usuarios al Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entradas`
--

LOCK TABLES `entradas` WRITE;
/*!40000 ALTER TABLE `entradas` DISABLE KEYS */;
INSERT INTO `entradas` VALUES (1,'15031097','2015-06-08','08:19:52 pm'),(2,'11111111111','2015-06-08','08:23:27 pm'),(3,'15031097','2016-01-19','03:52:44 pm'),(4,'15031097','2016-01-19','04:56:55 pm'),(5,'15031097','2016-01-20','05:18:30 pm'),(6,'15031097','2016-01-21','03:29:47 pm'),(7,'15031097','2016-01-21','06:54:43 pm'),(8,'15031097','2016-01-22','03:25:39 pm'),(9,'15031097','2016-01-22','05:07:07 pm'),(10,'15031097','2016-03-07','05:33:33 pm'),(11,'15031097','2016-03-08','03:03:04 pm'),(12,'15031097','2016-03-08','06:05:03 pm'),(13,'15031097','2016-03-09','03:59:24 pm'),(14,'15031097','2016-03-09','05:09:53 pm'),(15,'15031097','2016-03-09','05:35:03 pm'),(16,'15031097','2016-03-10','11:00:30 am'),(17,'15031097','2016-03-10','11:22:35 am'),(18,'15031097','2016-03-10','11:51:51 am'),(19,'15031097','2016-03-10','03:29:18 pm'),(20,'15031097','2016-03-10','05:12:03 pm'),(21,'15031097','2016-03-10','05:35:14 pm'),(22,'15031097','2016-03-11','02:55:12 pm'),(23,'15031097','2016-03-11','04:51:13 pm'),(24,'15031097','2016-03-11','05:37:54 pm'),(25,'15031097','2016-03-11','06:08:13 pm'),(26,'15031097','2016-03-17','03:19:21 pm'),(27,'15031097','2016-03-17','04:01:19 pm'),(28,'15031097','2016-03-30','09:12:03 am'),(29,'15031097','2016-03-30','10:33:05 am'),(30,'15031097','2016-03-30','03:03:06 pm'),(31,'15031097','2016-03-30','04:56:17 pm'),(32,'15031097','2016-03-31','10:17:03 am'),(33,'15031097','2016-03-31','02:48:12 pm'),(34,'15031097','2016-03-31','04:28:24 pm'),(35,'15031097','2016-03-31','05:24:16 pm'),(36,'15031097','2016-03-31','05:53:34 pm'),(37,'15031097','2016-03-31','06:19:10 pm'),(38,'15031097','2016-04-01','04:29:23 pm'),(39,'15031097','2016-04-04','03:45:29 pm'),(40,'15031097','2016-04-04','05:26:14 pm'),(41,'15031097','2016-04-07','08:13:44 am'),(42,'15031097','2016-04-07','09:29:44 am'),(43,'15031097','2016-04-07','02:50:06 pm');
/*!40000 ALTER TABLE `entradas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas_pagos`
--

DROP TABLE IF EXISTS `facturas_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturas_pagos` (
  `cod_fac_pag` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pag` int(11) NOT NULL,
  `fch_fac_pag` date NOT NULL,
  `num_fac_pag` varchar(15) NOT NULL,
  `con_fac_pag` varchar(15) NOT NULL,
  `mon_fac_pag` double(9,2) NOT NULL,
  `iva_fac_pag` double(9,2) NOT NULL,
  `isrl_fac_pag` double(9,2) NOT NULL,
  `iva_pag_pag` date NOT NULL,
  `iva_pag_mes` int(2) NOT NULL,
  `iva_pag_ano` int(4) NOT NULL,
  PRIMARY KEY (`cod_fac_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las facturas pagadas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturas_pagos`
--

LOCK TABLES `facturas_pagos` WRITE;
/*!40000 ALTER TABLE `facturas_pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturas_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familiares`
--

DROP TABLE IF EXISTS `familiares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familiares` (
  `cod_fam` int(2) NOT NULL AUTO_INCREMENT COMMENT 'C?digo del Familiar',
  `ced_per` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del Personal',
  `ced_fam` varchar(12) COLLATE latin1_spanish_ci NOT NULL COMMENT 'C?dula del  del Familiar',
  `nom_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Nombre  del Familiar',
  `ape_fam` varchar(50) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Apellido  del Familiar',
  `sex_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Sexo del Familiar',
  `fnac_fam` date NOT NULL COMMENT 'Fecha de Nacimiento del Familiar',
  `par_fam` varchar(25) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Parentesco  del Familiar',
  `est_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Estudia el Familiar?',
  `obs_fam` varchar(255) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Observaciones acerca del Familiar',
  `dis_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Hijos con discapacidad?',
  `beca_fam` varchar(1) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Beca de familiar',
  PRIMARY KEY (`cod_fam`),
  UNIQUE KEY `ced_per` (`ced_per`,`nom_fam`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los Familiares del Personal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familiares`
--

LOCK TABLES `familiares` WRITE;
/*!40000 ALTER TABLE `familiares` DISABLE KEYS */;
INSERT INTO `familiares` VALUES (1,'16657064','','prueba de ','hijo con discapacidad','M','2015-01-06','H','S','','S',''),(2,'16657064','','Juanchin','Marquez','M','1981-03-09','H','S','','S',''),(3,'15031097','','Luis Angel','Marquez Dávila','M','2008-05-22','H','S','','','S');
/*!40000 ALTER TABLE `familiares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feriados`
--

DROP TABLE IF EXISTS `feriados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feriados` (
  `fch_frd` date NOT NULL,
  `des_frd` varchar(50) NOT NULL,
  `tip_frd` varchar(25) NOT NULL,
  `tck_frd` varchar(2) NOT NULL,
  PRIMARY KEY (`fch_frd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de dias Feridos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feriados`
--

LOCK TABLES `feriados` WRITE;
/*!40000 ALTER TABLE `feriados` DISABLE KEYS */;
INSERT INTO `feriados` VALUES ('2015-05-18','Prueba Feriado','feriado','NO');
/*!40000 ALTER TABLE `feriados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupos`
--

DROP TABLE IF EXISTS `grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupos` (
  `cod_grp` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del grupo',
  `nom_grp` varchar(20) NOT NULL COMMENT 'nombre del grupo',
  `des_grp` varchar(255) NOT NULL COMMENT 'descripcion del grupo',
  PRIMARY KEY (`cod_grp`),
  UNIQUE KEY `nom_grp` (`nom_grp`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupos`
--

LOCK TABLES `grupos` WRITE;
/*!40000 ALTER TABLE `grupos` DISABLE KEYS */;
INSERT INTO `grupos` VALUES (1,'SuperUsuarios','Con todos los privilegios'),(2,'Administración','Grupo con todos los privilegios del área de Usuarios'),(3,'Personal','Grupo para el personal adscrito a la institución');
/*!40000 ALTER TABLE `grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guardias_feriados`
--

DROP TABLE IF EXISTS `guardias_feriados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guardias_feriados` (
  `cod_gua` int(11) NOT NULL AUTO_INCREMENT,
  `con_gua` varchar(100) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fec_gua` date NOT NULL,
  `sta_gua` char(1) NOT NULL DEFAULT 'A',
  `nor_gua` int(11) NOT NULL,
  `fer_gua` int(11) NOT NULL,
  `dom_gua` int(11) NOT NULL,
  `dof_gua` int(11) NOT NULL,
  `guar_mas` int(11) NOT NULL,
  PRIMARY KEY (`cod_gua`),
  UNIQUE KEY `ced_per` (`ced_per`,`cod_car`,`guar_mas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guardias_feriados`
--

LOCK TABLES `guardias_feriados` WRITE;
/*!40000 ALTER TABLE `guardias_feriados` DISABLE KEYS */;
/*!40000 ALTER TABLE `guardias_feriados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarios` (
  `cod_hor` int(11) NOT NULL AUTO_INCREMENT,
  `nom_hor` varchar(25) NOT NULL,
  `fch_dsd` date NOT NULL,
  `fch_hst` date NOT NULL,
  `lun_ini_hor` time NOT NULL,
  `lun_fin_hor` time NOT NULL,
  `lun_ini_hor2` time NOT NULL,
  `lun_fin_hor2` time NOT NULL,
  `mar_ini_hor` time NOT NULL,
  `mar_fin_hor` time NOT NULL,
  `mar_ini_hor2` time NOT NULL,
  `mar_fin_hor2` time NOT NULL,
  `mie_ini_hor` time NOT NULL,
  `mie_fin_hor` time NOT NULL,
  `mie_ini_hor2` time NOT NULL,
  `mie_fin_hor2` time NOT NULL,
  `jue_ini_hor` time NOT NULL,
  `jue_fin_hor` time NOT NULL,
  `jue_ini_hor2` time NOT NULL,
  `jue_fin_hor2` time NOT NULL,
  `vie_ini_hor` time NOT NULL,
  `vie_fin_hor` time NOT NULL,
  `vie_ini_hor2` time NOT NULL,
  `vie_fin_hor2` time NOT NULL,
  `sab_ini_hor` time NOT NULL,
  `sab_fin_hor` time NOT NULL,
  `dom_ini_hor` time NOT NULL,
  `dom_fin_hor` time NOT NULL,
  `tol_hor` int(11) NOT NULL,
  `obs_hor` longtext NOT NULL,
  PRIMARY KEY (`cod_hor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horarios`
--

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
INSERT INTO `horarios` VALUES (1,'Horario Normal','2012-01-01','2012-07-19','08:15:00','11:55:00','14:40:00','00:00:00','08:15:00','11:55:00','14:40:00','16:55:00','08:15:00','11:55:00','14:40:00','16:55:00','08:15:00','11:55:00','14:40:00','16:55:00','08:15:00','11:55:00','14:40:00','16:55:00','00:00:00','00:00:00','00:00:00','00:00:00',15,'');
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inasistencias`
--

DROP TABLE IF EXISTS `inasistencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inasistencias` (
  `cod_ina` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_ina`),
  UNIQUE KEY `ced_per` (`ced_per`,`fch_ina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inasistencias`
--

LOCK TABLES `inasistencias` WRITE;
/*!40000 ALTER TABLE `inasistencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `inasistencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencias`
--

DROP TABLE IF EXISTS `incidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidencias` (
  `cod_inc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la asignacion',
  `con_inc` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `mes_inc` int(2) NOT NULL COMMENT 'Mes en que se paga la incidencia',
  `ano_inc` int(4) NOT NULL COMMENT 'Ano en que se paga la incidencia',
  `fini_inc` date NOT NULL COMMENT 'Fecha de inicio del Periodo de Inicidencia',
  `ffin_inc` date NOT NULL COMMENT 'Fecha de fin del Periodo de Inicidencia',
  PRIMARY KEY (`cod_inc`),
  UNIQUE KEY `mes_inc` (`mes_inc`,`ano_inc`,`fini_inc`,`ffin_inc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencias`
--

LOCK TABLES `incidencias` WRITE;
/*!40000 ALTER TABLE `incidencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `incidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencias_sueldos`
--

DROP TABLE IF EXISTS `incidencias_sueldos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidencias_sueldos` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de incidencias',
  `cod_sue` int(2) NOT NULL COMMENT 'Codigo del sueldo',
  `mnt_inc` double(9,2) NOT NULL COMMENT 'Monto o porcentaje de la incidencia',
  `bas_inc` varchar(1) NOT NULL COMMENT 'Base para el calculo de la Incidencia',
  UNIQUE KEY `cod_inc` (`cod_inc`,`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de inicidencias para cada sueldo';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencias_sueldos`
--

LOCK TABLES `incidencias_sueldos` WRITE;
/*!40000 ALTER TABLE `incidencias_sueldos` DISABLE KEYS */;
/*!40000 ALTER TABLE `incidencias_sueldos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ing_pat_vhi`
--

DROP TABLE IF EXISTS `ing_pat_vhi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ing_pat_vhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `serie` int(3) NOT NULL,
  `inicio` int(8) NOT NULL,
  `fin` int(8) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los ingresos por Patente Vehicular';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ing_pat_vhi`
--

LOCK TABLES `ing_pat_vhi` WRITE;
/*!40000 ALTER TABLE `ing_pat_vhi` DISABLE KEYS */;
/*!40000 ALTER TABLE `ing_pat_vhi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `justificativos_per`
--

DROP TABLE IF EXISTS `justificativos_per`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `justificativos_per` (
  `cod_sol_jus` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_jus` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_jus` int(2) NOT NULL,
  `ini_sol_jus` date NOT NULL,
  `fin_sol_jus` date NOT NULL,
  `obs_sol_jus` longtext NOT NULL,
  `mot_sol_jus` varchar(25) NOT NULL,
  `apro_sol_jus` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_jus`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `justificativos_per`
--

LOCK TABLES `justificativos_per` WRITE;
/*!40000 ALTER TABLE `justificativos_per` DISABLE KEYS */;
/*!40000 ALTER TABLE `justificativos_per` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_asign`
--

DROP TABLE IF EXISTS `nomina_asign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(60) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_asign`
--

LOCK TABLES `nomina_asign` WRITE;
/*!40000 ALTER TABLE `nomina_asign` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_asign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_deduc`
--

DROP TABLE IF EXISTS `nomina_deduc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_deduc`
--

LOCK TABLES `nomina_deduc` WRITE;
/*!40000 ALTER TABLE `nomina_deduc` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_deduc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_incidencias`
--

DROP TABLE IF EXISTS `nomina_incidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_incidencias` (
  `ano_inc` int(4) NOT NULL,
  `mes_inc` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `mnt_inc` double(9,2) NOT NULL,
  `con_inc` varchar(255) NOT NULL,
  `ivss_inc` double(9,2) NOT NULL,
  `spf_inc` double(9,2) NOT NULL,
  `cah_inc` double(9,2) NOT NULL,
  `bnoc_inc` double(9,2) NOT NULL,
  `bvac_inc` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_inc`,`mes_inc`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_incidencias`
--

LOCK TABLES `nomina_incidencias` WRITE;
/*!40000 ALTER TABLE `nomina_incidencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_incidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nomina_pagar`
--

DROP TABLE IF EXISTS `nomina_pagar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomina_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones',
  `prm_rie` double(9,2) NOT NULL,
  `prm_pat` double(9,2) NOT NULL,
  `prm_coo` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nomina_pagar`
--

LOCK TABLES `nomina_pagar` WRITE;
/*!40000 ALTER TABLE `nomina_pagar` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomina_pagar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nominapre_asign`
--

DROP TABLE IF EXISTS `nominapre_asign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nominapre_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nominapre_asign`
--

LOCK TABLES `nominapre_asign` WRITE;
/*!40000 ALTER TABLE `nominapre_asign` DISABLE KEYS */;
/*!40000 ALTER TABLE `nominapre_asign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nominapre_deduc`
--

DROP TABLE IF EXISTS `nominapre_deduc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nominapre_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nominapre_deduc`
--

LOCK TABLES `nominapre_deduc` WRITE;
/*!40000 ALTER TABLE `nominapre_deduc` DISABLE KEYS */;
/*!40000 ALTER TABLE `nominapre_deduc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nominapre_pagar`
--

DROP TABLE IF EXISTS `nominapre_pagar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nominapre_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  `int_sue` double(9,2) NOT NULL COMMENT 'Sueldo Integral',
  `fnj_des` double(9,3) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `fnj_apr` double(9,3) NOT NULL COMMENT 'aporte de fondo de jubilaciones',
  `lph_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje FAOV',
  `spf_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje PIE',
  `sso_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje SSO',
  `cah_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje de Caja de ahorro',
  `fnj_des_por` double(9,3) NOT NULL COMMENT 'Porcentaje Fondo Jubilaciones',
  `lph_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte FAOV',
  `spf_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte PIE',
  `sso_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte SSO',
  `cah_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Caja de ahorro',
  `fnj_apr_por` double(9,3) NOT NULL COMMENT 'Porcentaje aporte Fondo Jubilaciones',
  `prm_rie` double(9,2) NOT NULL,
  `prm_pat` double(9,2) NOT NULL,
  `prm_coo` double(9,2) NOT NULL,
  PRIMARY KEY (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nominapre_pagar`
--

LOCK TABLES `nominapre_pagar` WRITE;
/*!40000 ALTER TABLE `nominapre_pagar` DISABLE KEYS */;
/*!40000 ALTER TABLE `nominapre_pagar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oficios_enviados`
--

DROP TABLE IF EXISTS `oficios_enviados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oficios_enviados` (
  `cod_ofi` int(11) NOT NULL AUTO_INCREMENT,
  `tip_ofi` varchar(10) NOT NULL,
  `num_ofi` int(4) unsigned zerofill NOT NULL,
  `fch_ofi` date NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `rdp_ofi` varchar(75) NOT NULL,
  `rpr_ofi` varchar(75) NOT NULL,
  `ddp_ofi` varchar(75) NOT NULL,
  `dpr_ofi` varchar(75) NOT NULL,
  `des_ofi` varchar(255) NOT NULL,
  `nds_ofi` int(2) NOT NULL,
  `fen_ofi` date DEFAULT NULL,
  `ftr_ofi` date DEFAULT NULL,
  `frs_ofi` date DEFAULT NULL,
  `ref_ofi` varchar(7) NOT NULL,
  `obs_ofi` varchar(255) NOT NULL,
  `est_ofi` varchar(1) NOT NULL COMMENT 'Estado del Oficio',
  PRIMARY KEY (`cod_ofi`),
  UNIQUE KEY `tip_ofi` (`tip_ofi`,`num_ofi`,`fch_ofi`,`rdp_ofi`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Oficios Enviados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oficios_enviados`
--

LOCK TABLES `oficios_enviados` WRITE;
/*!40000 ALTER TABLE `oficios_enviados` DISABLE KEYS */;
/*!40000 ALTER TABLE `oficios_enviados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oficios_recibidos`
--

DROP TABLE IF EXISTS `oficios_recibidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oficios_recibidos` (
  `cod_ofr` int(11) NOT NULL AUTO_INCREMENT,
  `fch_ofr` date NOT NULL,
  `fch_emi_ofr` date NOT NULL,
  `num_ofr` varchar(60) NOT NULL,
  `rem_ofr` varchar(60) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `con_ofr` varchar(150) NOT NULL,
  `ane_ofr` varchar(2) NOT NULL,
  `obs_ofr` varchar(255) NOT NULL,
  `prc_ofr` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ofr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Oficios Recibidos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oficios_recibidos`
--

LOCK TABLES `oficios_recibidos` WRITE;
/*!40000 ALTER TABLE `oficios_recibidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `oficios_recibidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagados`
--

DROP TABLE IF EXISTS `pagados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagados` (
  `ced_per` varchar(12) NOT NULL,
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagados`
--

LOCK TABLES `pagados` WRITE;
/*!40000 ALTER TABLE `pagados` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagados_frc`
--

DROP TABLE IF EXISTS `pagados_frc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagados_frc` (
  `cod_car` varchar(12) NOT NULL,
  PRIMARY KEY (`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagados_frc`
--

LOCK TABLES `pagados_frc` WRITE;
/*!40000 ALTER TABLE `pagados_frc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagados_frc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `fch_pag` date NOT NULL,
  `nor_pag` int(4) unsigned zerofill NOT NULL,
  `frm_pag` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `frm_com` int(6) unsigned zerofill NOT NULL,
  `mon_pag` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `ela_pag` varchar(50) NOT NULL,
  `rev_pag` varchar(50) NOT NULL,
  `apr_pag` varchar(50) NOT NULL,
  `obs_pag` varchar(255) DEFAULT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `frm_egr_iva` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar iva',
  `frm_egr_isrl` int(6) unsigned zerofill NOT NULL COMMENT 'numero de formulacio para pagar isrl',
  KEY `id` (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_presup`
--

DROP TABLE IF EXISTS `part_presup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `part_presup` (
  `cod_par` int(11) NOT NULL AUTO_INCREMENT,
  `sec_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `pro_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `act_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `ram_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `par_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `gen_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `esp_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `sub_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `des_par` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `obs_par` varchar(11) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cod_par`),
  UNIQUE KEY `sector` (`par_par`,`gen_par`,`esp_par`,`sub_par`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_presup`
--

LOCK TABLES `part_presup` WRITE;
/*!40000 ALTER TABLE `part_presup` DISABLE KEYS */;
/*!40000 ALTER TABLE `part_presup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_presup_mov`
--

DROP TABLE IF EXISTS `part_presup_mov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `part_presup_mov` (
  `cod_par_mov` int(11) NOT NULL AUTO_INCREMENT,
  `tip_par_mov` varchar(15) NOT NULL,
  `fch_par_mov` date NOT NULL,
  `ano_par_mov` varchar(10) NOT NULL,
  `idn_par_mov` varchar(50) NOT NULL,
  `con_par_mov` longtext NOT NULL,
  `mon_par_mov` double(9,2) NOT NULL,
  `fin_par_mov` date NOT NULL,
  PRIMARY KEY (`cod_par_mov`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_presup_mov`
--

LOCK TABLES `part_presup_mov` WRITE;
/*!40000 ALTER TABLE `part_presup_mov` DISABLE KEYS */;
/*!40000 ALTER TABLE `part_presup_mov` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidas_compras`
--

DROP TABLE IF EXISTS `partidas_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidas_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_com` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidas_compras`
--

LOCK TABLES `partidas_compras` WRITE;
/*!40000 ALTER TABLE `partidas_compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `partidas_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidas_pagos`
--

DROP TABLE IF EXISTS `partidas_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidas_pagos` (
  `cod_pro_pag` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pag` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `isrl_pro_pag` varchar(2) DEFAULT NULL,
  `mon_pro_pag` double(9,2) NOT NULL,
  PRIMARY KEY (`cod_pro_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidas_pagos`
--

LOCK TABLES `partidas_pagos` WRITE;
/*!40000 ALTER TABLE `partidas_pagos` DISABLE KEYS */;
/*!40000 ALTER TABLE `partidas_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisos`
--

DROP TABLE IF EXISTS `permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisos` (
  `ing_prm` varchar(1) NOT NULL COMMENT 'permisos para ingresar',
  `mod_prm` varchar(1) NOT NULL COMMENT 'permisos para modificar',
  `con_prm` varchar(1) NOT NULL COMMENT 'permisos para consultar',
  `eli_prm` varchar(1) NOT NULL COMMENT 'permisos para eliminar',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion',
  KEY `cod_grp` (`cod_grp`),
  KEY `cod_sec` (`cod_sec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permisologia para los Usuarios del Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos`
--

LOCK TABLES `permisos` WRITE;
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` VALUES ('','','','',5,11),('','','','',5,2),('','','','',5,8),('','','','',5,3),('','','','',5,7),('','','','',5,6),('','','','',5,1),('','','','',5,14),('','','','',5,15),('','','','',5,13),('','','','',5,12),('','','','',5,4),('','','','',5,5),('','','','',5,16),('','','','',5,10),('','','','',5,9),('A','A','A','A',5,17),('','','','',4,18),('','','','',4,19),('','','','',4,11),('','','','',4,2),('','','','',4,8),('','','','',4,3),('','','','',4,7),('','','','',4,6),('','','','',4,1),('','','','',4,14),('','','','',4,23),('','','','',4,20),('','','','',4,21),('','','','',4,22),('','','','',4,15),('','','','',4,13),('','','','',4,12),('','','','',4,4),('','','','',4,5),('','','','',4,16),('','','','',4,10),('','','','',4,9),('','','','',4,17),('A','A','A','A',4,24),('','','','',4,25),('','','','',4,26),('','','','',6,18),('','','','',6,19),('','','','',6,11),('','','','',6,2),('','','','',6,8),('','','','',6,3),('','','','',6,7),('','','','',6,6),('','','A','',6,1),('','','','',6,14),('','','','',6,23),('A','','A','',6,20),('A','','A','',6,21),('A','','A','',6,22),('','','','',6,15),('','','A','',6,13),('','','','',6,12),('','','A','',6,4),('','','','',6,5),('','','','',6,16),('','','','',6,10),('A','A','A','A',6,9),('A','A','A','A',6,27),('','','','',6,17),('','','','',6,24),('','','A','',6,25),('A','','A','',6,26),('','','','',7,18),('','','A','',7,19),('','','','',7,11),('','','','',7,2),('','','','',7,8),('','','','',7,3),('','','','',7,7),('','','','',7,6),('A','A','A','',7,1),('','','','',7,14),('','','','',7,23),('A','','A','',7,20),('A','','A','',7,21),('A','','A','',7,22),('','','','',7,15),('','','A','',7,13),('','','','',7,12),('','','A','',7,4),('','','','',7,5),('A','A','A','',7,16),('A','A','A','',7,10),('','','','',7,9),('','','','',7,27),('','','','',7,17),('','','','',7,24),('','','A','',7,25),('A','A','A','A',7,26),('','','','',3,18),('','','','',3,28),('A','A','A','A',3,19),('','','','',3,11),('','','','',3,2),('','','','',3,8),('','','','',3,3),('','','','',3,7),('','','','',3,6),('','','A','',3,1),('','','','',3,14),('','','','',3,23),('A','','A','',3,20),('A','','A','',3,21),('A','','A','',3,22),('','','A','',3,15),('','','A','',3,13),('','','','',3,12),('','','A','',3,4),('','','','',3,5),('','','','',3,16),('','','','',3,10),('','','','',3,9),('','','','',3,27),('','','','',3,24),('','','','',3,17),('','','A','',3,25),('A','','A','',3,26),('','A','A','A',1,18),('','','','',1,28),('','','','',1,19),('','','','',1,11),('A','A','A','A',1,2),('A','A','A','A',1,8),('A','A','A','A',1,3),('A','A','A','A',1,7),('A','A','A','A',1,6),('A','A','A','A',1,1),('A','A','A','A',1,14),('','','','',1,23),('A','A','A','A',1,29),('A','','A','',1,20),('','','','',1,21),('A','A','A','A',1,30),('','','','',1,22),('A','A','A','A',1,15),('A','A','A','A',1,13),('A','A','A','A',1,12),('A','A','A','A',1,4),('','','','',1,5),('','','','',1,16),('','','','',1,10),('','','','',1,9),('','','','',1,27),('','','','',1,24),('','','','',1,17),('A','A','A','A',1,26),('','','','',1,25),('A','A','A','A',2,18),('','','','',2,28),('','','','',2,19),('','','','',2,11),('A','A','A','A',2,2),('A','A','A','A',2,8),('A','A','A','A',2,3),('A','A','A','A',2,7),('A','A','A','A',2,6),('A','A','A','A',2,1),('A','A','A','A',2,14),('','','','',2,23),('A','A','A','A',2,29),('','','','',2,20),('','','','',2,21),('A','A','A','A',2,30),('','','','',2,22),('A','A','A','A',2,15),('A','A','A','A',2,13),('A','A','A','A',2,12),('A','A','A','A',2,4),('','','','',2,5),('','','','',2,16),('','','','',2,10),('','','','',2,9),('','','','',2,27),('','','','',2,24),('','','','',2,17),('A','A','A','A',2,26),('','','','',2,25);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisos_per`
--

DROP TABLE IF EXISTS `permisos_per`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisos_per` (
  `cod_sol_perm` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_perm` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_perm` int(2) NOT NULL,
  `ini_sol_perm` date NOT NULL,
  `fin_sol_perm` date NOT NULL,
  `tip_sol_perm` varchar(20) NOT NULL,
  `obs_sol_perm` longtext NOT NULL,
  `mot_sol_perm` varchar(25) NOT NULL,
  `apro_sol_perm` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_perm`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos_per`
--

LOCK TABLES `permisos_per` WRITE;
/*!40000 ALTER TABLE `permisos_per` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisos_per` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal`
--

DROP TABLE IF EXISTS `personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `nac_per` varchar(1) NOT NULL COMMENT 'nacionalidad de la persona',
  `nom_per` varchar(50) NOT NULL COMMENT 'nombre de la persona',
  `ape_per` varchar(50) NOT NULL COMMENT 'apellido de la persona',
  `sex_per` varchar(1) NOT NULL COMMENT 'sexo de la persona',
  `fnac_per` date NOT NULL COMMENT 'fecha de nacimiento de la persona',
  `lnac_per` varchar(50) NOT NULL COMMENT 'lugar de nacimiento de la persona',
  `cor_per` varchar(50) NOT NULL COMMENT 'correo de la persona',
  `pro_per` varchar(50) NOT NULL COMMENT 'profesion de la persona',
  `abr_per` varchar(10) NOT NULL COMMENT 'Abreviatura de Profesión',
  `dir_per` varchar(255) NOT NULL COMMENT 'direccion de la persona',
  `tel_per` varchar(12) NOT NULL COMMENT 'telefono de la persona',
  `cel_per` varchar(12) NOT NULL COMMENT 'celular de la persona',
  `lph_des` varchar(1) NOT NULL COMMENT 'Descuento de ley politica habitacional',
  `spf_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro de paro forzoso',
  `sso_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro social obligatorio',
  `cah_des` varchar(1) NOT NULL COMMENT 'Descuento de caja de ahorro',
  `sfu_des` varchar(1) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'Servicio funerario',
  `hog_asg` varchar(1) NOT NULL COMMENT 'Prima por hogar',
  `hij_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos',
  `ant_asg` varchar(1) NOT NULL COMMENT 'Prima por antiguedad',
  `pro_asg` varchar(1) NOT NULL COMMENT 'Prima por profesionalizaci?n',
  `hje_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos excepcionales',
  `jer_asg` varchar(1) NOT NULL COMMENT 'Prima por Jerarquia',
  `otr_asg` varchar(1) NOT NULL COMMENT 'Prima por Postgrado',
  `tra_asg` varchar(1) NOT NULL,
  `asi_asg` varchar(1) NOT NULL,
  `spn_asg` varchar(1) NOT NULL,
  `fch_reg` date NOT NULL COMMENT 'Fecha de registro en el personal',
  `fnj_des` varchar(1) NOT NULL COMMENT 'Fondo de Jubilaciones',
  `rie_asg` varchar(1) NOT NULL COMMENT 'Prima por riesgo',
  `pat_asg` varchar(1) NOT NULL COMMENT 'Prima Patrullero',
  `coo_asg` varchar(1) NOT NULL COMMENT 'Prima Coordinador',
  `bono_util` varchar(1) NOT NULL,
  `bono_juge` varchar(1) NOT NULL,
  `comp_eva` decimal(9,3) NOT NULL,
  `ano_adm_per` varchar(1) NOT NULL COMMENT 'Años en la administración publica',
  `med_asg` varchar(1) NOT NULL COMMENT 'Prima Medico especialista',
  `comp_eval_ant` double(9,2) NOT NULL COMMENT 'Valor de compensación anterior',
  `hog_asg_inc` double(9,2) NOT NULL COMMENT 'incremento hogar',
  `hij_asg_inc` double(9,2) NOT NULL COMMENT 'incremento hijos',
  `ant_asg_inc` double(9,2) NOT NULL COMMENT 'incremento antiguedad',
  `pro_asg_inc` double(9,2) NOT NULL COMMENT 'incremento  profesionalización',
  `tra_asg_inc` double(9,2) NOT NULL COMMENT 'incremento transporte',
  `asi_asg_inc` double(9,2) NOT NULL COMMENT 'incremento asitencial',
  `spn_asg_inc` double(9,2) NOT NULL COMMENT 'incremento sector publico',
  `med_asg_inc` double(9,2) NOT NULL COMMENT 'incremento  medico especialista',
  PRIMARY KEY (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal`
--

LOCK TABLES `personal` WRITE;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` VALUES ('15031097','V','Luis Felipe','Márquez Briceño','M','1981-03-09','Mérida','felixpe09@gmail.com','Ingeniero en Sistemas','Ing.','Asoprieto Calle 4A #242 PA. Ejido','02742217980','04266736316','A','A','A','A','A','A','A','A','A','','A','','A','A','A','2016-03-08','','A','A','A','A','A',0.075,'','',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00),('16657064','V','Juan José','Márquez Trejo','M','1983-06-09','Merida','juanjmt@gmail.com','Ingeniero','Ing','Ejido','04265772755','04265772755','A','','A','A','','A','A','A','A','','','','A','A','A','2016-01-22','','','','','','',0.000,'','',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00);
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_compras`
--

DROP TABLE IF EXISTS `productos_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_compras` (
  `cod_pro_com` int(11) NOT NULL AUTO_INCREMENT,
  `cod_com` int(11) NOT NULL,
  `cnt_pro_com` int(4) NOT NULL,
  `uni_pro_com` int(4) NOT NULL,
  `con_pro_com` varchar(255) NOT NULL,
  `pun_pro_com` double(9,3) NOT NULL,
  `exc_pro_com` varchar(2) DEFAULT NULL,
  KEY `id` (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_compras`
--

LOCK TABLES `productos_compras` WRITE;
/*!40000 ALTER TABLE `productos_compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_part_mov`
--

DROP TABLE IF EXISTS `productos_part_mov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_part_mov` (
  `cod_pro_par_mov` int(11) NOT NULL AUTO_INCREMENT,
  `cod_par_mov` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_par_mov` double(9,2) NOT NULL,
  `tip_pro_par_mov` varchar(10) NOT NULL DEFAULT 'Ingreso',
  PRIMARY KEY (`cod_pro_par_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='partidas que pertenecen a un movimiento presupuestario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_part_mov`
--

LOCK TABLES `productos_part_mov` WRITE;
/*!40000 ALTER TABLE `productos_part_mov` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos_part_mov` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prog_mov_pagos`
--

DROP TABLE IF EXISTS `prog_mov_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prog_mov_pagos` (
  `cod_pag` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `con_pag` varchar(150) NOT NULL,
  `fch_pag` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_vac` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_pag`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prog_mov_pagos`
--

LOCK TABLES `prog_mov_pagos` WRITE;
/*!40000 ALTER TABLE `prog_mov_pagos` DISABLE KEYS */;
INSERT INTO `prog_mov_pagos` VALUES (1,'15031097',3414.72,'Fraccion de salario por salida de cargo (VIGILANTE)','2016-03-10','X','','',18,'','0000-00-00',0,'','');
/*!40000 ALTER TABLE `prog_mov_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prog_movimientos`
--

DROP TABLE IF EXISTS `prog_movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prog_movimientos` (
  `cod_mov` int(11) NOT NULL AUTO_INCREMENT,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `accion` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fch_asg` date NOT NULL,
  `fch_vac` date NOT NULL,
  `pagado` varchar(5) NOT NULL,
  PRIMARY KEY (`cod_mov`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prog_movimientos`
--

LOCK TABLES `prog_movimientos` WRITE;
/*!40000 ALTER TABLE `prog_movimientos` DISABLE KEYS */;
INSERT INTO `prog_movimientos` VALUES (1,'16657064',13,1,0,'2016-01-22','2016-01-01',''),(3,'15031097',18,1,0,'2016-02-08','2005-10-01',''),(4,'15031097',18,2,0,'2016-03-08','2005-10-01',''),(5,'15031097',14,1,0,'2016-03-09','2005-10-01','');
/*!40000 ALTER TABLE `prog_movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programas`
--

DROP TABLE IF EXISTS `programas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programas` (
  `cod_pro` varchar(5) NOT NULL COMMENT 'codigo del Programa',
  `nom_pro` varchar(150) NOT NULL COMMENT 'nombre del Programa',
  PRIMARY KEY (`cod_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programas`
--

LOCK TABLES `programas` WRITE;
/*!40000 ALTER TABLE `programas` DISABLE KEYS */;
INSERT INTO `programas` VALUES ('1','PROGRAMA 01');
/*!40000 ALTER TABLE `programas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `cod_pro` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fch_pro` date NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `dir_pro` varchar(255) NOT NULL,
  `act_pro` varchar(50) NOT NULL,
  `cap_pro` double(9,2) NOT NULL,
  `cas_pro` double(9,2) NOT NULL,
  `tel_pro` varchar(11) NOT NULL,
  `fax_pro` varchar(11) NOT NULL,
  `per_pro` varchar(1) NOT NULL,
  `ofi_reg_pro` varchar(50) NOT NULL,
  `num_reg_pro` int(5) NOT NULL,
  `tom_reg_pro` varchar(25) NOT NULL,
  `fol_reg_pro` varchar(4) NOT NULL,
  `fch_reg_pro` date NOT NULL,
  `num_mod_pro` int(5) NOT NULL,
  `tom_mod_pro` varchar(25) NOT NULL,
  `fol_mod_pro` varchar(4) NOT NULL,
  `fch_mod_pro` date NOT NULL,
  `pat_pro` varchar(25) NOT NULL,
  `ivss_pro` varchar(25) NOT NULL,
  `ince_pro` varchar(25) NOT NULL,
  `nom_rep_pro` varchar(50) NOT NULL,
  `ape_rep_pro` varchar(50) NOT NULL,
  `ced_rep_pro` varchar(8) NOT NULL,
  `dir_rep_pro` varchar(255) NOT NULL,
  `tel_rep_pro` varchar(11) NOT NULL,
  `car_rep_pro` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_pro`),
  UNIQUE KEY `rif_pro` (`rif_pro`),
  UNIQUE KEY `nom_pro` (`nom_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Proveedores';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reembolsos_med`
--

DROP TABLE IF EXISTS `reembolsos_med`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reembolsos_med` (
  `cod_ree` int(11) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `cod_cob` int(11) NOT NULL,
  `fec_reg` date NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `rif_pro` varchar(12) NOT NULL,
  `tel_pro` int(11) NOT NULL,
  `nro_fac` varchar(20) NOT NULL,
  `fec_fac` date NOT NULL,
  `mon_fac` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `fec_pag` date NOT NULL DEFAULT '0000-00-00',
  `fec_nom` date NOT NULL DEFAULT '0000-00-00',
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `nom_car` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reembolsos_med`
--

LOCK TABLES `reembolsos_med` WRITE;
/*!40000 ALTER TABLE `reembolsos_med` DISABLE KEYS */;
/*!40000 ALTER TABLE `reembolsos_med` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retenciones_isrl`
--

DROP TABLE IF EXISTS `retenciones_isrl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retenciones_isrl` (
  `cod_ret_isrl` int(11) NOT NULL AUTO_INCREMENT,
  `com_ret_isrl` int(5) unsigned zerofill NOT NULL,
  `egr_ret_isrl` int(6) unsigned zerofill NOT NULL,
  `fch_ret_isrl` date NOT NULL,
  `obs_ret_isrl` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ret_isrl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retenciones_isrl`
--

LOCK TABLES `retenciones_isrl` WRITE;
/*!40000 ALTER TABLE `retenciones_isrl` DISABLE KEYS */;
/*!40000 ALTER TABLE `retenciones_isrl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retenciones_iva`
--

DROP TABLE IF EXISTS `retenciones_iva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retenciones_iva` (
  `cod_ret_iva` int(11) NOT NULL AUTO_INCREMENT,
  `com_ret_iva` int(8) unsigned zerofill NOT NULL,
  `egr_ret_iva` int(6) unsigned zerofill NOT NULL,
  `fch_ret_iva` date NOT NULL,
  `obs_ret_iva` varchar(255) NOT NULL,
  PRIMARY KEY (`cod_ret_iva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retenciones_iva`
--

LOCK TABLES `retenciones_iva` WRITE;
/*!40000 ALTER TABLE `retenciones_iva` DISABLE KEYS */;
/*!40000 ALTER TABLE `retenciones_iva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secciones`
--

DROP TABLE IF EXISTS `secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secciones` (
  `cod_sec` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la seccion',
  `nom_sec` varchar(25) NOT NULL COMMENT 'nombre de la seccion',
  `des_sec` varchar(255) NOT NULL COMMENT 'descripcion de la seccion',
  `dir_sec` varchar(50) NOT NULL COMMENT 'direccion para apuntar a la seccion',
  `tar_sec` varchar(15) NOT NULL COMMENT 'target de apertura de la seccion',
  `ord_sec` double(9,2) NOT NULL COMMENT 'orden de aparicion en el menu',
  PRIMARY KEY (`cod_sec`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 COMMENT='Secciones Disponibles para los Usuarios';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secciones`
--

LOCK TABLES `secciones` WRITE;
/*!40000 ALTER TABLE `secciones` DISABLE KEYS */;
INSERT INTO `secciones` VALUES (1,'Personal','Datos del Personal adscrito a: ','personal.php','contenido',7.00),(2,'Dependencias','Dependencias adcritas a:','dependencias.php','contenido',2.00),(3,'Cargos','Cargos adscritos a:','cargos.php','contenido',4.00),(4,'Nomina','Nomina adscrita a:','nomina.php','contenido',12.00),(6,'Tipos de Descuentos','Descuentos del Personal adscrito a:','descuentos.php','contenido',6.00),(7,'Tipo de Asignaciones','Concesiones del Personal adscrito a:','concesiones.php','contenido',5.00),(8,'Sueldos','Tabla de Sueldos','sueldos.php','contenido',3.00),(12,'Pre-Nomina','Prenomina Adscrita a: ','nominapre.php','contenido',11.00),(13,'Cesta Ticket','Reporte de Cesta Ticket del personal adscrito a:','cesta_ticket.php','contenido',10.00),(14,'Inasistencias','Registro de las inasistencias del personal adscrito a:','inasistencias.php','contenido',8.00),(15,'Feriados','Registro de días feriados para','feriados.php','contenido',9.00),(18,'Valores','Valores utilizados para los cáculos','valores.php','contenido',0.00),(26,'Constancias','Solicitud de Constancias','constancias.php','contenido',17.00),(29,'Asignaciones Por Lotes','Asignaciones a Nomina a todo el personal','personal_asignaciones_general.php','contenido',8.50),(30,'Deducciones Por Lotes','Deducciones Generales en Nomina','personal_deduciones_general.php','contenido',8.60),(31,'Reportes','Reportes Varios','reportes.php','contenido',13.00),(32,'Pre-pagar Reembolsos','Prepago de reembolsos adscritos a: ','reemb_nominapre.php','contenido',8.91),(33,'Pagar Reembolsos','Pago de reembolsos adscritos a: ','reemb_nomina.php','contenido',8.91);
/*!40000 ALTER TABLE `secciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sueldos`
--

DROP TABLE IF EXISTS `sueldos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sueldos` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  `hora_sue` double(9,2) DEFAULT NULL,
  `grad_sue` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sueldos`
--

LOCK TABLES `sueldos` WRITE;
/*!40000 ALTER TABLE `sueldos` DISABLE KEYS */;
INSERT INTO `sueldos` VALUES (15,9648.18,'OBRERO GRADO 1',NULL,NULL),(16,10244.16,'OBRERO GRADO 5',NULL,NULL),(17,11871.35,'ASISTENCIAL PIII',NULL,NULL),(19,15258.00,'MEDICO ESPECIALISTA II (6HRS)',NULL,NULL),(20,7630.00,'MEDICO ESPECIALISTA II (3HRS)',NULL,NULL),(21,15911.58,'ADMINISTRATIVO PIII',NULL,NULL),(22,9085.80,'ADMINISTRATIVO (BIII / VII)',40.00,'VII');
/*!40000 ALTER TABLE `sueldos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sueldos_ant`
--

DROP TABLE IF EXISTS `sueldos_ant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sueldos_ant` (
  `cod_sue` int(2) NOT NULL AUTO_INCREMENT COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY (`cod_sue`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sueldos_ant`
--

LOCK TABLES `sueldos_ant` WRITE;
/*!40000 ALTER TABLE `sueldos_ant` DISABLE KEYS */;
INSERT INTO `sueldos_ant` VALUES (15,9648.18,'OBRERO GRADO 1'),(16,10244.16,'OBRERO GRADO 5'),(17,11871.35,'ASISTENCIAL PIII'),(19,15258.00,'MEDICO ESPECIALISTA II (6HRS)'),(20,7630.00,'MEDICO ESPECIALISTA II (3HRS)'),(21,15911.58,'ADMINISTRATIVO PIII'),(22,10309.73,'ADMINISTRATIVO BIII');
/*!40000 ALTER TABLE `sueldos_ant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_presup`
--

DROP TABLE IF EXISTS `temp_presup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_presup` (
  `fch_prs` date NOT NULL,
  `det_prs` varchar(255) NOT NULL,
  `nor_prs` varchar(7) NOT NULL,
  `asg_prs` double(9,2) NOT NULL,
  `inc_prs` double(9,2) NOT NULL,
  `ing_prs` double(9,2) NOT NULL,
  `egr_prs` double(9,2) NOT NULL,
  `com_prs` double(9,2) NOT NULL,
  `cau_prs` double(9,2) NOT NULL,
  `pag_prs` double(9,2) NOT NULL,
  `rin_prs` double(9,2) NOT NULL,
  `generado` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`generado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporal para generar ejecucion presupuestaria';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_presup`
--

LOCK TABLES `temp_presup` WRITE;
/*!40000 ALTER TABLE `temp_presup` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_presup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_cargos`
--

DROP TABLE IF EXISTS `tipos_cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_cargos` (
  `cod_tcar` int(11) NOT NULL AUTO_INCREMENT,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(4) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_tcar`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_cargos`
--

LOCK TABLES `tipos_cargos` WRITE;
/*!40000 ALTER TABLE `tipos_cargos` DISABLE KEYS */;
INSERT INTO `tipos_cargos` VALUES (1,'OBRERO CONTRATADO','OC'),(2,'OBRERO FIJO','OF'),(3,'ADMINISTRATIVO CONTRATADO','AC'),(4,'ADMINISTRATIVO FIJO','AF'),(5,'ASISTENCIAL CONTRATADO','ASC'),(6,'ASISTENCIAL FIJO','ASF'),(7,'SUPLENTE OBRERO','SO'),(8,'SUPLENTE ADMINISTRATIVO','SA'),(9,'SUPLENTE ASISTENCIAL','SAS');
/*!40000 ALTER TABLE `tipos_cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `cod_usr` varchar(12) NOT NULL COMMENT 'codigo del usuario',
  `nom_usr` varchar(50) NOT NULL COMMENT 'nombre del usuario',
  `ape_usr` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `freg_usr` date NOT NULL COMMENT 'fecha de registro del usuario',
  `log_usr` varchar(15) NOT NULL COMMENT 'login del usuario',
  `pas_usr` text NOT NULL COMMENT 'password del usuario',
  `sts_usr` varchar(1) NOT NULL DEFAULT 'A' COMMENT 'Estado en el que se encuentra el Usuario',
  `int_usr` int(1) NOT NULL DEFAULT '0' COMMENT 'Intentos fallidos de inicio de sesi?n',
  `fcam_usr` date NOT NULL COMMENT 'fecha de cambio del usuario',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  PRIMARY KEY (`cod_usr`),
  KEY `cod_grp` (`cod_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuarios del Sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('11111111111','Administrador','Administrador','2011-02-16','admin','admin','A',0,'2016-09-22',1),('15031097','Luis Felipe','Márquez Briceño','2011-01-01','felix','7ad4109292636b7498a9f5042081becf','A',0,'2016-11-11',2);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vac_dias_per`
--

DROP TABLE IF EXISTS `vac_dias_per`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vac_dias_per` (
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fch_pag_vac` date NOT NULL,
  `dias_vac` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de días de Vacaciones a disfrutar';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vac_dias_per`
--

LOCK TABLES `vac_dias_per` WRITE;
/*!40000 ALTER TABLE `vac_dias_per` DISABLE KEYS */;
/*!40000 ALTER TABLE `vac_dias_per` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacaciones_per`
--

DROP TABLE IF EXISTS `vacaciones_per`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacaciones_per` (
  `cod_sol_vac` int(11) NOT NULL AUTO_INCREMENT,
  `fch_sol_vac` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `dias_sol_vac` int(2) NOT NULL,
  `ini_sol_vac` date NOT NULL,
  `fin_sol_vac` date NOT NULL,
  `tip_sol_vac` varchar(20) NOT NULL,
  `peri_sol_vac` date NOT NULL,
  `perf_sol_vac` date NOT NULL,
  `obs_sol_vac` longtext NOT NULL,
  `apro_sol_vac` varchar(2) NOT NULL,
  PRIMARY KEY (`cod_sol_vac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacaciones_per`
--

LOCK TABLES `vacaciones_per` WRITE;
/*!40000 ALTER TABLE `vacaciones_per` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacaciones_per` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valores`
--

DROP TABLE IF EXISTS `valores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valores` (
  `cod_val` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor',
  PRIMARY KEY (`cod_val`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valores`
--

LOCK TABLES `valores` WRITE;
/*!40000 ALTER TABLE `valores` DISABLE KEYS */;
INSERT INTO `valores` VALUES (1,'PH',150.000,'Prima por Hijo'),(2,'PPT',0.140,'Prima de Profesionalización TSU'),(3,'PANT1_H30',0.020,'Prima Antiguedad de 0 a 5 años (30 Horas)'),(4,'PANT1_H36',0.040,'Prima Antiguedad de 0 a 5 años (36 Horas)'),(5,'PPP',0.160,'Prima de Profesionalización Profesional'),(6,'PANT1_H40',0.060,'Prima Antiguedad de 0 a 5 años (40 Horas)'),(7,'PANT2_H30',0.040,'Prima Antiguedad de 6 a 10 años (30 Horas)'),(8,'PANT2_H36',0.060,'Prima Antiguedad de 6 a 10 años (36 Horas)'),(9,'PANT2_H40',0.080,'Prima Antiguedad de 6 a 10 años (40 Horas)'),(10,'PPPO',0.180,'Prima de Profesionalización Postgrado'),(11,'PANT3_H30',0.060,'Prima Antiguedad de 11 a 15 años (30 Horas)'),(12,'PANT3_H36',0.080,'Prima Antiguedad de 11 a 15 años (36 Horas)'),(13,'PANT3_H40',0.100,'Prima Antiguedad de 11 a 15 años (40 Horas)'),(14,'PANT4_H30',0.080,'Prima Antiguedad de 16 a 20 años (30 Horas)'),(15,'PANT4_H36',0.100,'Prima Antiguedad de 16 a 20 años (36 Horas)'),(16,'PANT4_H40',0.120,'Prima Antiguedad de 16 a 20 años (40 Horas)'),(17,'PANT5_H30',0.100,'Prima Antiguedad 21 o mas años (30 Horas)'),(18,'PSPS36',1500.000,'Prima Sistema Publico de Salud (36 Horas)'),(19,'PANT5_H36',0.120,'Prima Antiguedad 21 o mas años (36 Horas)'),(20,'PANT5_H40',0.140,'Prima Antiguedad 21 o mas años (40 Horas)'),(21,'PSPS42',2500.000,'Prima Sistema Publico de Salud (40-42 Horas)'),(22,'PT',300.000,'Prima de Transporte'),(23,' PA30',600.000,'Prima Asistencial (30 Horas)'),(24,'PA36',750.000,'Prima Asistencial (36 Horas)'),(25,'PA42',850.000,'Prima Asistencial (40-42 Horas)'),(26,'BV30',54.000,'Bono Vacacional (30 Horas)'),(27,'BV36',60.000,'Bono Vacacional (36 Horas)'),(28,'BV42',66.000,'Bono Vacacional (40-42 Horas)'),(29,'BN',0.500,'Bono Nocturno'),(30,'GDF',2.000,'Guardia de Domingo o Feriado'),(31,'GDYF',3.000,'Guardia de Domingo y Feriado'),(32,'PHOG',0.750,'Prima por Hogar'),(33,'UT',177.000,'Unidad Tributaria'),(34,'U.T.',177.000,'Valor de la Unidad Tributaria'),(35,'CT',2.500,'Valor del Cesta Ticket diario');
/*!40000 ALTER TABLE `valores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vista_asignaciones_per`
--

DROP TABLE IF EXISTS `vista_asignaciones_per`;
/*!50001 DROP VIEW IF EXISTS `vista_asignaciones_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_asignaciones_per` (
  `cod_cnp` tinyint NOT NULL,
  `ced_per` tinyint NOT NULL,
  `nom_con` tinyint NOT NULL,
  `con_cnp` tinyint NOT NULL,
  `ncuo_cnp` tinyint NOT NULL,
  `ncp_cnp` tinyint NOT NULL,
  `nom_car` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_cargos_per`
--

DROP TABLE IF EXISTS `vista_cargos_per`;
/*!50001 DROP VIEW IF EXISTS `vista_cargos_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_cargos_per` (
  `ced_per` tinyint NOT NULL,
  `cod_car` tinyint NOT NULL,
  `cod_tcar` tinyint NOT NULL,
  `cod_dep` tinyint NOT NULL,
  `num_car` tinyint NOT NULL,
  `nom_car` tinyint NOT NULL,
  `fch_asg` tinyint NOT NULL,
  `fch_vac` tinyint NOT NULL,
  `nom_per` tinyint NOT NULL,
  `ape_per` tinyint NOT NULL,
  `nom_tcar` tinyint NOT NULL,
  `mon_sue` tinyint NOT NULL,
  `des_sue` tinyint NOT NULL,
  `mon_sue_ant` tinyint NOT NULL,
  `num_cue` tinyint NOT NULL,
  `ban_cue` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_cargos_salida`
--

DROP TABLE IF EXISTS `vista_cargos_salida`;
/*!50001 DROP VIEW IF EXISTS `vista_cargos_salida`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_cargos_salida` (
  `ced_per` tinyint NOT NULL,
  `cod_car` tinyint NOT NULL,
  `cod_tcar` tinyint NOT NULL,
  `cod_dep` tinyint NOT NULL,
  `num_car` tinyint NOT NULL,
  `nom_car` tinyint NOT NULL,
  `fch_asg` tinyint NOT NULL,
  `fch_vac` tinyint NOT NULL,
  `nom_per` tinyint NOT NULL,
  `ape_per` tinyint NOT NULL,
  `nom_tcar` tinyint NOT NULL,
  `mon_sue` tinyint NOT NULL,
  `des_sue` tinyint NOT NULL,
  `mon_sue_ant` tinyint NOT NULL,
  `num_cue` tinyint NOT NULL,
  `ban_cue` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_deducciones_per`
--

DROP TABLE IF EXISTS `vista_deducciones_per`;
/*!50001 DROP VIEW IF EXISTS `vista_deducciones_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_deducciones_per` (
  `cod_dsp` tinyint NOT NULL,
  `ced_per` tinyint NOT NULL,
  `nom_des` tinyint NOT NULL,
  `con_dsp` tinyint NOT NULL,
  `ncuo_dsp` tinyint NOT NULL,
  `ncp_dsp` tinyint NOT NULL,
  `nom_car` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_ing_pat`
--

DROP TABLE IF EXISTS `vista_ing_pat`;
/*!50001 DROP VIEW IF EXISTS `vista_ing_pat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_ing_pat` (
  `id` tinyint NOT NULL,
  `fecha` tinyint NOT NULL,
  `serie` tinyint NOT NULL,
  `inicio` tinyint NOT NULL,
  `fin` tinyint NOT NULL,
  `tipo` tinyint NOT NULL,
  `serie_id` tinyint NOT NULL,
  `recibo` tinyint NOT NULL,
  `monto` tinyint NOT NULL,
  `cod_cat` tinyint NOT NULL,
  `con_esp` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nomin_tot_sum`
--

DROP TABLE IF EXISTS `vista_nomin_tot_sum`;
/*!50001 DROP VIEW IF EXISTS `vista_nomin_tot_sum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nomin_tot_sum` (
  `ano_nom` tinyint NOT NULL,
  `mes_nom` tinyint NOT NULL,
  `por_nom` tinyint NOT NULL,
  `cod_tcar` tinyint NOT NULL,
  `abr_tcar` tinyint NOT NULL,
  `cod_dep` tinyint NOT NULL,
  `nom_tcar` tinyint NOT NULL,
  `tot_sue` tinyint NOT NULL,
  `tot_lph` tinyint NOT NULL,
  `tot_spf` tinyint NOT NULL,
  `tot_sso` tinyint NOT NULL,
  `tot_cah` tinyint NOT NULL,
  `tot_sfu` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nominas_pagadas`
--

DROP TABLE IF EXISTS `vista_nominas_pagadas`;
/*!50001 DROP VIEW IF EXISTS `vista_nominas_pagadas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nominas_pagadas` (
  `ano_nom` tinyint NOT NULL,
  `mes_nom` tinyint NOT NULL,
  `por_nom` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nominaspre_pagadas`
--

DROP TABLE IF EXISTS `vista_nominaspre_pagadas`;
/*!50001 DROP VIEW IF EXISTS `vista_nominaspre_pagadas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nominaspre_pagadas` (
  `ano_nom` tinyint NOT NULL,
  `mes_nom` tinyint NOT NULL,
  `por_nom` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_nominpre_tot_sum`
--

DROP TABLE IF EXISTS `vista_nominpre_tot_sum`;
/*!50001 DROP VIEW IF EXISTS `vista_nominpre_tot_sum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_nominpre_tot_sum` (
  `ano_nom` tinyint NOT NULL,
  `mes_nom` tinyint NOT NULL,
  `por_nom` tinyint NOT NULL,
  `cod_tcar` tinyint NOT NULL,
  `abr_tcar` tinyint NOT NULL,
  `cod_dep` tinyint NOT NULL,
  `nom_tcar` tinyint NOT NULL,
  `tot_sue` tinyint NOT NULL,
  `tot_lph` tinyint NOT NULL,
  `tot_spf` tinyint NOT NULL,
  `tot_sso` tinyint NOT NULL,
  `tot_cah` tinyint NOT NULL,
  `tot_sfu` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_personal`
--

DROP TABLE IF EXISTS `vista_personal`;
/*!50001 DROP VIEW IF EXISTS `vista_personal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_personal` (
  `ced_per` tinyint NOT NULL,
  `nombre` tinyint NOT NULL,
  `nac_per` tinyint NOT NULL,
  `abr_per` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_reembolsos_per`
--

DROP TABLE IF EXISTS `vista_reembolsos_per`;
/*!50001 DROP VIEW IF EXISTS `vista_reembolsos_per`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_reembolsos_per` (
  `cod_ree` tinyint NOT NULL,
  `ced_per` tinyint NOT NULL,
  `cod_car` tinyint NOT NULL,
  `cod_cob` tinyint NOT NULL,
  `fec_reg` tinyint NOT NULL,
  `nom_pro` tinyint NOT NULL,
  `rif_pro` tinyint NOT NULL,
  `tel_pro` tinyint NOT NULL,
  `nro_fac` tinyint NOT NULL,
  `fec_fac` tinyint NOT NULL,
  `mon_fac` tinyint NOT NULL,
  `mon_pag` tinyint NOT NULL,
  `fec_pag` tinyint NOT NULL,
  `fec_nom` tinyint NOT NULL,
  `nom_per` tinyint NOT NULL,
  `ape_per` tinyint NOT NULL,
  `nom_car` tinyint NOT NULL,
  `nombre_per` tinyint NOT NULL,
  `nom_cob` tinyint NOT NULL,
  `mon_cob` tinyint NOT NULL,
  `bas_cob` tinyint NOT NULL,
  `cod_tcar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vista_salida_sincargo`
--

DROP TABLE IF EXISTS `vista_salida_sincargo`;
/*!50001 DROP VIEW IF EXISTS `vista_salida_sincargo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vista_salida_sincargo` (
  `cod_pag` tinyint NOT NULL,
  `mon_pag` tinyint NOT NULL,
  `ced_per` tinyint NOT NULL,
  `con_pag` tinyint NOT NULL,
  `des_car` tinyint NOT NULL,
  `fch_pag` tinyint NOT NULL,
  `cod_dep` tinyint NOT NULL,
  `nom_dep` tinyint NOT NULL,
  `cod_car` tinyint NOT NULL,
  `nom_car` tinyint NOT NULL,
  `fch_vac` tinyint NOT NULL,
  `cod_tcar` tinyint NOT NULL,
  `nom_tcar` tinyint NOT NULL,
  `abr_tcar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vista_asignaciones_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_asignaciones_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_asignaciones_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_asignaciones_per` AS select `asg`.`cod_cnp` AS `cod_cnp`,`asg`.`ced_per` AS `ced_per`,`cn`.`nom_con` AS `nom_con`,`asg`.`con_cnp` AS `con_cnp`,`asg`.`ncuo_cnp` AS `ncuo_cnp`,`asg`.`ncp_cnp` AS `ncp_cnp`,`c`.`nom_car` AS `nom_car` from ((`asignaciones` `asg` join `concesiones` `cn`) join `cargos` `c`) where ((`asg`.`cod_con` = `cn`.`cod_con`) and (`c`.`cod_car` = `asg`.`cod_car`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_cargos_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_cargos_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_cargos_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_cargos_per` AS select `c`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_tcar` AS `cod_tcar`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`c`.`fch_asg` AS `fch_asg`,`c`.`fch_vac` AS `fch_vac`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant`,(select `cuentas`.`num_cue` from `cuentas` where (`cuentas`.`ced_per` = `c`.`ced_per`)) AS `num_cue`,(select `cuentas`.`ban_cue` from `cuentas` where (`cuentas`.`ced_per` = `c`.`ced_per`)) AS `ban_cue` from ((((`cargos` `c` join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `c`.`ced_per`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_cargos_salida`
--

/*!50001 DROP TABLE IF EXISTS `vista_cargos_salida`*/;
/*!50001 DROP VIEW IF EXISTS `vista_cargos_salida`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_cargos_salida` AS select `p`.`ced_per` AS `ced_per`,`c`.`cod_car` AS `cod_car`,`c`.`cod_tcar` AS `cod_tcar`,`c`.`cod_dep` AS `cod_dep`,`c`.`num_car` AS `num_car`,`c`.`nom_car` AS `nom_car`,`pm`.`fch_asg` AS `fch_asg`,`pm`.`fch_vac` AS `fch_vac`,`p`.`nom_per` AS `nom_per`,`p`.`ape_per` AS `ape_per`,`t`.`nom_tcar` AS `nom_tcar`,`s`.`mon_sue` AS `mon_sue`,`s`.`des_sue` AS `des_sue`,`sa`.`mon_sue` AS `mon_sue_ant`,(select `cuentas`.`num_cue` from `cuentas` where (`cuentas`.`ced_per` = `p`.`ced_per`)) AS `num_cue`,(select `cuentas`.`ban_cue` from `cuentas` where (`cuentas`.`ced_per` = `p`.`ced_per`)) AS `ban_cue` from (((((`prog_movimientos` `pm` join `cargos` `c`) join `tipos_cargos` `t`) join `sueldos` `s`) join `sueldos_ant` `sa`) join `personal` `p`) where ((`pm`.`cod_car` = `c`.`cod_car`) and (`pm`.`accion` = 2) and (`t`.`cod_tcar` = `c`.`cod_tcar`) and (`s`.`cod_sue` = `c`.`cod_sue`) and (`sa`.`cod_sue` = `c`.`cod_sue`) and (`p`.`ced_per` = `pm`.`ced_per`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_deducciones_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_deducciones_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_deducciones_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_deducciones_per` AS select `dd`.`cod_dsp` AS `cod_dsp`,`dd`.`ced_per` AS `ced_per`,`ds`.`nom_des` AS `nom_des`,`dd`.`con_dsp` AS `con_dsp`,`dd`.`ncuo_dsp` AS `ncuo_dsp`,`dd`.`ncp_dsp` AS `ncp_dsp`,`c`.`nom_car` AS `nom_car` from ((`deducciones` `dd` join `descuentos` `ds`) join `cargos` `c`) where ((`dd`.`cod_des` = `ds`.`cod_des`) and (`c`.`cod_car` = `dd`.`cod_car`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_ing_pat`
--

/*!50001 DROP TABLE IF EXISTS `vista_ing_pat`*/;
/*!50001 DROP VIEW IF EXISTS `vista_ing_pat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_ing_pat` AS (select `ing_pat`.`id` AS `id`,`ing_pat`.`fecha` AS `fecha`,`ing_pat`.`serie` AS `serie`,`ing_pat`.`inicio` AS `inicio`,`ing_pat`.`fin` AS `fin`,`ing_pat`.`tipo` AS `tipo`,`adt_ing_pro`.`serie_id` AS `serie_id`,`adt_ing_pro`.`recibo` AS `recibo`,`adt_ing_pro`.`monto` AS `monto`,`adt_ing_pro`.`cod_cat` AS `cod_cat`,`adt_ing_pro`.`con_esp` AS `con_esp` from (`ing_pat_vhi` `ing_pat` join `adt_ing_pro`) where ((`adt_ing_pro`.`serie_id` = `ing_pat`.`id`) and (`adt_ing_pro`.`recibo` >= `ing_pat`.`inicio`) and (`adt_ing_pro`.`recibo` <= `ing_pat`.`fin`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nomin_tot_sum`
--

/*!50001 DROP TABLE IF EXISTS `vista_nomin_tot_sum`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nomin_tot_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nomin_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nomina_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nominas_pagadas`
--

/*!50001 DROP TABLE IF EXISTS `vista_nominas_pagadas`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nominas_pagadas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nominas_pagadas` AS select `nomina_pagar`.`ano_nom` AS `ano_nom`,`nomina_pagar`.`mes_nom` AS `mes_nom`,`nomina_pagar`.`por_nom` AS `por_nom` from `nomina_pagar` group by `nomina_pagar`.`ano_nom`,`nomina_pagar`.`mes_nom`,`nomina_pagar`.`por_nom` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nominaspre_pagadas`
--

/*!50001 DROP TABLE IF EXISTS `vista_nominaspre_pagadas`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nominaspre_pagadas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nominaspre_pagadas` AS select `nominapre_pagar`.`ano_nom` AS `ano_nom`,`nominapre_pagar`.`mes_nom` AS `mes_nom`,`nominapre_pagar`.`por_nom` AS `por_nom` from `nominapre_pagar` group by `nominapre_pagar`.`ano_nom`,`nominapre_pagar`.`mes_nom`,`nominapre_pagar`.`por_nom` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_nominpre_tot_sum`
--

/*!50001 DROP TABLE IF EXISTS `vista_nominpre_tot_sum`*/;
/*!50001 DROP VIEW IF EXISTS `vista_nominpre_tot_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_nominpre_tot_sum` AS (select `np`.`ano_nom` AS `ano_nom`,`np`.`mes_nom` AS `mes_nom`,`np`.`por_nom` AS `por_nom`,`np`.`cod_tcar` AS `cod_tcar`,`np`.`abr_tcar` AS `abr_tcar`,`np`.`cod_dep` AS `cod_dep`,`tc`.`nom_tcar` AS `nom_tcar`,sum(`np`.`mon_sue`) AS `tot_sue`,sum(`np`.`lph_des`) AS `tot_lph`,sum(`np`.`spf_des`) AS `tot_spf`,sum(`np`.`sso_des`) AS `tot_sso`,sum(`np`.`cah_des`) AS `tot_cah`,sum(`np`.`sfu_des`) AS `tot_sfu` from (`nominapre_pagar` `np` join `tipos_cargos` `tc`) where (`np`.`cod_tcar` = `tc`.`cod_tcar`) group by `np`.`ano_nom`,`np`.`mes_nom`,`np`.`por_nom`,`np`.`cod_tcar`,`np`.`cod_dep`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_personal`
--

/*!50001 DROP TABLE IF EXISTS `vista_personal`*/;
/*!50001 DROP VIEW IF EXISTS `vista_personal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_personal` AS select `personal`.`ced_per` AS `ced_per`,concat(`personal`.`nom_per`,' ',`personal`.`ape_per`) AS `nombre`,`personal`.`nac_per` AS `nac_per`,`personal`.`abr_per` AS `abr_per` from `personal` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_reembolsos_per`
--

/*!50001 DROP TABLE IF EXISTS `vista_reembolsos_per`*/;
/*!50001 DROP VIEW IF EXISTS `vista_reembolsos_per`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_reembolsos_per` AS select `rm`.`cod_ree` AS `cod_ree`,`rm`.`ced_per` AS `ced_per`,`rm`.`cod_car` AS `cod_car`,`rm`.`cod_cob` AS `cod_cob`,`rm`.`fec_reg` AS `fec_reg`,`rm`.`nom_pro` AS `nom_pro`,`rm`.`rif_pro` AS `rif_pro`,`rm`.`tel_pro` AS `tel_pro`,`rm`.`nro_fac` AS `nro_fac`,`rm`.`fec_fac` AS `fec_fac`,`rm`.`mon_fac` AS `mon_fac`,`rm`.`mon_pag` AS `mon_pag`,`rm`.`fec_pag` AS `fec_pag`,`rm`.`fec_nom` AS `fec_nom`,`rm`.`nom_per` AS `nom_per`,`rm`.`ape_per` AS `ape_per`,`rm`.`nom_car` AS `nom_car`,(select concat(`personal`.`ape_per`,' ',`personal`.`nom_per`) from `personal` where (convert(`personal`.`ced_per` using utf8) = `rm`.`ced_per`)) AS `nombre_per`,(select `coberturas_med`.`nom_cob` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `nom_cob`,(select `coberturas_med`.`mon_cob` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `mon_cob`,(select `coberturas_med`.`bas_cob` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `bas_cob`,(select `coberturas_med`.`cod_tcar` from `coberturas_med` where (`coberturas_med`.`cod_cob` = `rm`.`cod_cob`)) AS `cod_tcar` from `reembolsos_med` `rm` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vista_salida_sincargo`
--

/*!50001 DROP TABLE IF EXISTS `vista_salida_sincargo`*/;
/*!50001 DROP VIEW IF EXISTS `vista_salida_sincargo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vista_salida_sincargo` AS select `prog_mov_pagos`.`cod_pag` AS `cod_pag`,`prog_mov_pagos`.`mon_pag` AS `mon_pag`,`prog_mov_pagos`.`ced_per` AS `ced_per`,`prog_mov_pagos`.`con_pag` AS `con_pag`,`prog_mov_pagos`.`des_car` AS `des_car`,`prog_mov_pagos`.`fch_pag` AS `fch_pag`,`prog_mov_pagos`.`cod_dep` AS `cod_dep`,`prog_mov_pagos`.`nom_dep` AS `nom_dep`,`prog_mov_pagos`.`cod_car` AS `cod_car`,`prog_mov_pagos`.`nom_car` AS `nom_car`,`prog_mov_pagos`.`fch_vac` AS `fch_vac`,`prog_mov_pagos`.`cod_tcar` AS `cod_tcar`,`prog_mov_pagos`.`nom_tcar` AS `nom_tcar`,`prog_mov_pagos`.`abr_tcar` AS `abr_tcar` from (`prog_mov_pagos` left join `cargos` on((`cargos`.`ced_per` = `prog_mov_pagos`.`ced_per`))) where (not(`prog_mov_pagos`.`ced_per` in (select distinct `cargos`.`ced_per` from `cargos`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-08 15:14:39
