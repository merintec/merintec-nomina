<?php //archivo que contiene todas las funciones utilizadas en los formularios ?>
<SCRIPT>
function valor_acampo(valor, campo)
{
   document.form1[campo].value = valor;
}
function actualizar_padre()
{
	window.opener.document.form1.confirmar.value = 'actualizar padre';
        window.opener.document.form1.submit();
}
function confirmacion_delista_func(accion, valor)
{
	<?php include ('../comunes/mensajes.php'); ?>
	if (accion == 'Eliminar' || accion == 'Actualizar') 
	{
		var aceptar = confirm(" \u00BFRealmente desea " + accion + " la informaci\u00f3n? ");
		if (aceptar) 
		{ 
		}
		else return false;
	}
        document.form1.confirmar.value = accion + " de la lista";
		document.form1.confirmar_val.value = valor; 
        document.form1.submit();
}
function confirmacion_func(accion)
{
	<?php include ('../comunes/mensajes.php'); ?>
	if (accion == 'Procesar') 
	{
		var aceptar = confirm(" \u00BFRealmente desea " + accion + " la informaci\u00f3n? ");
		if (aceptar) 
		{ valor_acampo("SI","procesar_val");
		}
		else { alert ("<?php echo $msg_procesado_cancel_alert; ?>"); return false};
	}
	if (accion == 'Eliminar' || accion == 'Actualizar' || accion == 'Guardar.') 
	{
		var aceptar = confirm(" \u00BFRealmente desea " + accion + " la informaci\u00f3n? ");
		if (aceptar) 
		{ 
		}
		else { return false};
	}
        document.form1.confirmar.value = accion; 
        document.form1.submit();
}
// funciones para ocultar las filas de una tabla
function ocultar_celda(tabla) {
  var elementos=document.getElementById(tabla);
      elementos=elementos.getElementsByTagName('tr')
  for (k = 1; k< elementos.length; k++) {
               elementos[k].style.display = "none";
    }
}
function mostrar_celda(celda) {
    ocultar_celda('tabla');
    var elementos = document.getElementsByName(celda);
    for (k = 0; k< elementos.length; k++) {
               elementos[k].style.display = "";
    }
}
// funciones para ocultar opciones en combos anidados
function ocultar_opcion(combo) {
  var elementos=document.getElementById(combo);
      elementos=elementos.getElementsByTagName('option');
      elementos[0].selected = "true";
  for (k = 1; k< elementos.length; k++) {
               elementos[k].style.display = "none";
    }
}
function mostrar_opcion(combo, opcion_mostrar) {
	ocultar_opcion(combo);
    var elementos = document.getElementsByName(opcion_mostrar);
    for (k = 0; k< elementos.length; k++) {
               elementos[k].style.display = "";
    }
}

</SCRIPT>
<?php
$existe='';
// Crear matriz de datos
// Genera una una matriz con todos los datos que se manipulan en el formulario 
function crear_datos ($nombre,$etiqueta,$valor,$min,$max,$tipo)
{
	$dato[0] = "$nombre";	//nombre del campo
	$dato[1] = "$etiqueta";	//etiqueta del campo 
	$dato[2] = "$valor";	//valor del campo
	$dato[3] = "$min";	//longitud minima
	$dato[4] = "$max";	//longitud Maxima
	$dato[5] = "$tipo";	//tipo de datos
	return $dato;
}
// Crear Busqueda
function crear_busqueda_func ($ncriterios,$criterios,$campos,$boton)
{
	include ('../comunes/mensajes.php');
	echo '<table width=100%><tr><td><hr></td></tr><tr><td align="center">Busqueda por:';
	$contador = 0;
	while ($contador < $ncriterios)
	{
	  echo '<input name="criterio" type="radio" value="'.$campos[$contador].'" ';
	  if ($contador==0) { echo 'checked="checked" '; }
	  if (($boton=='Guardar')||($boton=='Actualizar')) { echo 'disabled'; }
	  echo 'title="'.$msg_btn_criterio.'"> ';
	  echo $criterios[$contador];
	  $contador ++;
	  if ($contador == 3) { echo '<br>'; }
	}
	echo '</td></tr>';
        echo '<tr><td align="center">';
	echo '<input name="buscar_a" type="text" id="buscar_a"';
	if (($boton=="Guardar")||($boton=="Actualizar")) { echo "disabled"; }
	echo ' title="'.$msg_campo_busqueda.'">';
	echo '<input type="Submit" name="Buscar" value="Buscar" title="'.$msg_btn_buscar.'"';
	if (($boton=='Guardar')||($boton=='Actualizar')) { echo 'disabled'; }
	echo '></td></tr><tr><td><hr></td></tr></table>';
}
// Busqueda
function busqueda_func ($buscar,$criterio,$tabla,$pagina,$tipo)
{
	include ('../comunes/mensajes.php');
	// verificacion de criterio de Busqueda
	if ((! $buscar)||(! $criterio)||(! $tabla))
	{ 
		echo '<SCRIPT> alert ("'.$msg_busqueda_vacia.'");</SCRIPT>';
		echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
	else
	{
		if ($tipo == "general")
		{
 		    $result=mysql_query("select * from $tabla WHERE $criterio LIKE '%$buscar%'");
		}
		if ($tipo == "individual")
		{
		    $result=mysql_query("select * from $tabla WHERE $criterio = '$buscar'");
		}
		if ($buscar == "Todos")
		{
		    $result=mysql_query("select * from $tabla");
		}
		if (mysql_num_rows($result) == 0)
		{
		    echo '<SCRIPT>alert("'.$msg_busqueda_sinresultado.'");</SCRIPT>';
		    echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
		    return $result;
		}
		if (mysql_num_rows($result) > 1)
		{
		    echo '<SCRIPT>alert("'.$msg_busqueda_resultadosmul.'");</SCRIPT>';
		    return $result;
		}
		return $result;
	}
	
}
// Eliminación
function eliminar_func ($elim,$criterio,$tabla,$pagina)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$sql="Delete From $tabla Where $criterio='$elim'";
	mysql_query($sql);
	if (mysql_error()){ 
		echo '<SCRIPT>alert("'.$msg_elim_noexito.' \nError: '.mysql_error().'");</SCRIPT>';  
		echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
	else {
		echo '<SCRIPT>alert("'.$msg_elim_exito.'");</SCRIPT>';
		echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
}
// Insersión
function insertar_func($ncampos,$datos,$tabla)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $campos .= $datos[$contador][0].',';
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $campos .= $datos[$contador][0];
	}
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $valores .= "'".$datos[$contador][2]."',";
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $valores .= "'".$datos[$contador][2]."'";
	}
	$sql = "insert into $tabla ($campos) values ($valores)";
	mysql_query($sql);
	echo '<SCRIPT>alert("'.$msg_ins_exito.'");</SCRIPT>';
	echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';	
}

// Modificación
function modificar_func($ncampos,$datos,$tabla,$condicion,$valorcondicion)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$sql = "Update $tabla Set ";
	$contador = 0;
	while ($contador < $ncampos - 1) 
        {
	    $campo = $datos[$contador][0];
	    $sql .= "$campo='".$datos[$contador][2]."', ";
	    $contador = $contador + 1;
	}
	if ($contador == $ncampos - 1)
	{
	    $campo = $datos[$contador][0];
	    $sql .= "$campo='".$datos[$contador][2]."' ";
	}
	$sql .= "Where $condicion='".$valorcondicion."'";
	mysql_query($sql);
	if (mysql_error()){ 
		echo '<SCRIPT>alert("'.$msg_mod_noexito.' \nError: '.mysql_error().'");</SCRIPT>';
		echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}
	else {
		echo '<SCRIPT>alert("'.$msg_mod_exito.'");</SCRIPT>';
		echo '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina.'";</SCRIPT>';
	}	
}
// Comprobar existencia
function comp_exist($campos,$valores,$tabla,$boton,$sino,$en)
{
	include ('../comunes/mensajes.php');
	$sql="select $campos from $tabla where $campos = '$valores'";
	if($sino=='si'){
		if($reg=mysql_fetch_array(mysql_query($sql))){
			echo '<SCRIPT>alert("En '.$en.'\n'.$msg_existe_si.'");</SCRIPT>';
			$boton="Verificar";
		}
	}elseif($sino=='no'){
		if(!$reg=mysql_fetch_array(mysql_query($sql))){
			echo '<SCRIPT>alert("En '.$en.'\n'.$msg_existe_no.'");</SCRIPT>';
			$boton="Verificar";	
		}	
	}
	return $boton;
}
function comp_num_exist($campos,$valores,$tabla,$boton,$max,$en)
{
	include ('../comunes/mensajes.php');
	$sql="select COUNT($campos) as cont from $tabla where $campos = '$valores'";
	if($reg=mysql_fetch_array(mysql_query($sql))){
		if($reg['cont']>=$max){
			echo '<SCRIPT>alert("'.$msg_existe_max.' esta cedula\n'.$max.' registros en: '.$en.'");</SCRIPT>';
			$boton="Verificar";
		}
	}
	return $boton;
}
include ('valida.php');
// Validar los campos
function validando_campos ($ncampos,$data)
{
	for($i=0;$i<$ncampos;$i++){
	   if (validar_tipocampo ($data[$i])==0)
	   { return; }
	}
	return 1;
}
// Funcion de combos
function combo($nombre, $valor, $tabla, $link, $n_cod, $n_mos, $n_mos2, $n_mos3, $id_opt, $funcion, $boton, $adicional){
	if ($boton!='Modificar')
	{
	    $sql = "select * from ".$tabla." ".$adicional;
	    $res=mysql_query($sql,$link);
	    echo "<select name='$nombre' id='$nombre' ".$funcion; echo" >";
	    echo "<option value=''>Seleccione...</option>";
	    while ($row=mysql_fetch_array($res)){
		echo "<option ";
		echo 'id="'.$row[$id_opt].'" name="'.$row[$id_opt].'"';
		if ($row[$n_cod]==$valor) echo " selected";
		echo " value=".$row[$n_cod]."> ".$row[$n_mos]." -- ".$row[$n_mos2]." "; if($n_mos3!=""){ echo "(".$row[$n_mos3].")"; } echo "</option>";
	    }
	    echo "</select> ";
	}
	else
	{ 
	    $sql = "select * from ".$tabla." WHERE ".$nombre."='".$valor."'";
	    $row=mysql_fetch_array(mysql_query($sql));
	    echo '<input type="hidden" name="'.$nombre.'" id="'.$nombre.'" value="'.$valor.'">';
	    echo $row[$n_mos2];
	}
}
// Función para activar permisos del usuario
function llamar_permisos ($seccion){
	$sql = "select * from permisos WHERE cod_grp=".$_COOKIE['uspriv']." AND cod_sec = ".$seccion;
	$row = mysql_fetch_array (mysql_query($sql));
	$prm[0] = $row["ing_prm"];
	$prm[1] = $row["mod_prm"];
	$prm[2] = $row["con_prm"];
	$prm[3] = $row["eli_prm"];
	return $prm;
}
// Función de auditoria para registrar las acciones de los usuarios sobre la base de datos
function auditoria_func ($accion, $ncampos, $datos, $tabla)
{
	$cod_urs = $_COOKIE['uscod'];
	$fecha = date ("Y-m-d");
	$hora = date(" H:i",time()); 
	if ($accion != "modificar")
	{
	    $contador = 0;
	    while ($contador < $ncampos - 1) 
            { 	
	        $detalle .= $datos[$contador][0].'='.$datos[$contador][2].';';
	        $contador ++;	
	    }
	    if ($contador == $ncampos - 1)
	    {
	        $detalle .= $datos[$contador][0].'='.$datos[$contador][2];
	    }
	}
	else
	{
	    $detalle = $datos;
	}
	$sql  = "insert into auditoria (cod_usr,accion,detalle,tabla,fecha,hora)"; 
	$sql .= "values ('$cod_urs','$accion','$detalle','$tabla','$fecha','$hora')";
	mysql_query($sql);
	$sql = "select * from auditoria";
	$result = mysql_query($sql);
	if (mysql_num_rows($result) == 1000)
	{
	    $sql = "TRUNCATE TABLE auditoria_backup";
  	    mysql_query($sql);
	    $sql = "insert into auditoria_backup SELECT * FROM auditoria";
	    mysql_query($sql);
	    $sql = "TRUNCATE TABLE auditoria";
  	    mysql_query($sql);
	}	
}

// Función para crear una capa
function crear_capa ($nombre,$mostrar,$ancho,$alto,$visibilidad,$borde)
{
?>
<style>
#capa {
	position:absolute;
	width:<?php echo $ancho.'px;'; ?>
	height:<?php echo $alto.'px;'; ?>
	z-index:1;
	overflow:auto;
	background-color: #FFFFFF;
	border: <?php echo $borde.'px;'; ?> solid #000000;
}
</style>
<div id="capa" name="<?php echo $nombre; ?>" style="visibility:<?php echo $visibilidad; ?>">
<table width="100%" border="0" cellpadding="5" cellspacing="5">
  <tr>
    <td>
      <?php include($mostrar);?>
    </td>
  </tr>
</table>
</div>
<?php
}
function abrir_ventana($direccion,$nom_id_bot,$boton,$valor){
?>
<input type="button" name="<?php echo $nom_id_bot; ?>" id="<?php echo $nom_id_bot; ?>" value="<?php echo $boton; ?>" onclick="window.open('<?php echo $direccion; ?>?<?php echo $valor; ?>','_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500')"/>
<?php
}
//funcion para calcular una fecha sumando o restando días a una fecha inicial
function calculo_fecha ($fecha_ini,$operador_calculo,$dias_calculo)
{ 
   $fechaComparacion = strtotime($fecha_ini);
   $contador = 1;
   while ($contador <= $dias_calculo)
   {
      $calculo = strtotime($operador_calculo."1 days", $fechaComparacion); //Le suamos 1 días
      if ((date("D",$calculo) != "Sun") && (date("D",$calculo) != "Sat")) //Verificamos que no sea Sabado o Domingo
      {
         $fechaComparacion = $calculo;
         $contador ++;
      }
      if ((date("D",$calculo) == "Sat") || (date("D",$calculo) == "Sun")) 
      {
         $calculo= strtotime($operador_calculo."1 days", $fechaComparacion); //Le suamos 1 días
         $fechaComparacion = $calculo;
      }
   }
   $fecha_nueva = date("Y-m-d", $calculo);
   return $fecha_nueva;
}
// Calculo de dias de la resta de dos fechas
function restaFechas($dFecIni, $dFecFin)
{
$dFecIni = str_replace("-","",$dFecIni);
$dFecIni = str_replace("/","",$dFecIni);
$dFecFin = str_replace("-","",$dFecFin);
$dFecFin = str_replace("/","",$dFecFin);

ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);

$date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[1], $aFecIni[3]);
$date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[1], $aFecFin[3]);

return round(($date2 - $date1) / (60 * 60 * 24));
}
function edad($fecha_nac)
{
	$dia=date("j");
	$mes=date("n");
	$anno=date("Y");
	$dia_nac=substr($fecha_nac, 8, 2);
	$mes_nac=substr($fecha_nac, 5, 2);
	$anno_nac=substr($fecha_nac, 0, 4);
	if($mes_nac>$mes)
	{
		$calc_edad= $anno-$anno_nac-1;
	}
	else
	{
		if($mes==$mes_nac AND $dia_nac>$dia)
		{
			$calc_edad= $anno-$anno_nac-1; 
		}
		else
		{
			$calc_edad= $anno-$anno_nac;
		}
	}
	$calc_edad=$calc_edad.' a&ntilde;os';
	return $calc_edad;
}
// funcion para calcular los dias del mes
function dias_mes ($mes,$ano){
	return date('t', mktime(0,0,0, $mes, 1, $ano));
}

// funcion para escribir Mes
function escribir_mes ($mes){
    switch ($mes) {
    case 1:
        echo "Enero";
        break;
    case 2:
        echo "Febrero";
        break;
    case 3:
        echo "Marzo";
        break;
    case 4:
        echo "Abril";
        break;
    case 5:
        echo "Mayo";
        break;
    case 6:
        echo "Junio";
        break;
    case 7:
        echo "Julio";
        break;
    case 8:
        echo "Agosto";
        break;
    case 9:
        echo "Septiembre";
        break;
    case 10:
        echo "Octubre";
        break;
    case 11:
        echo "Noviembre";
        break;
    case 12:
        echo "Diciembre";
        break;
    }
}
///funcion para calcular la cantidad de meses entre dos fechas
function meses($fech_ini,$fech_fin) { 
   /* 
   FELIPE DE JESUS SANTOS SALAZAR, LIFER35@HOTMAIL.COM 
   SEP-2010 
 
   ESTA FUNCION NOS REGRESA LA CANTIDAD DE MESES ENTRE 2 FECHAS 
 
   EL FORMATO DE LAS VARIABLES DE ENTRADA $fech_ini Y $fech_fin ES YYYY-MM-DD 
 
   $fech_ini TIENE QUE SER MENOR QUE $fech_fin 
 
   ESTA FUNCION TAMBIEN SE PUEDE HACER CON LA FUNCION date 
 
   SI ENCUENTRAS ALGUN ERROR FAVOR DE HACERMELO SABER 
 
   ESPERO TE SEA DE UTILIDAD, POR FAVOR NO QUIERES ESTE COMENTARIO, GRACIAS 
 
   */ 
 
 
 
   //SEPARO LOS VALORES DEL ANIO, MES Y DIA PARA LA FECHA INICIAL EN DIFERENTES 
   //VARIABLES PARASU MEJOR MANEJO 
 
   $fIni_yr=substr($fech_ini,0,4); 
    $fIni_mon=substr($fech_ini,5,2); 
    $fIni_day=substr($fech_ini,8,2); 
 
   //SEPARO LOS VALORES DEL ANIO, MES Y DIA PARA LA FECHA FINAL EN DIFERENTES 
   //VARIABLES PARASU MEJOR MANEJO 
   $fFin_yr=substr($fech_fin,0,4); 
    $fFin_mon=substr($fech_fin,5,2); 
    $fFin_day=substr($fech_fin,8,2); 
 
   $yr_dif=$fFin_yr - $fIni_yr; 
   //echo "la diferencia de años es -> ".$yr_dif."<br>"; 
   //LA FUNCION strtotime NOS PERMITE COMPARAR CORRECTAMENTE LAS FECHAS 
   //TAMBIEN ES UTIL CON LA FUNCION date 
   if(strtotime($fech_ini) > strtotime($fech_fin)){ 
      echo 'ERROR -> la fecha inicial es mayor a la fecha final <br>'; 
      exit(); 
   } 
   else{ 
       if($yr_dif == 1){ 
         $fIni_mon = 12 - $fIni_mon; 
         $meses = $fFin_mon + $fIni_mon; 
         return $meses; 
         //LA FUNCION utf8_encode NOS SIRVE PARA PODER MOSTRAR ACENTOS Y 
         //CARACTERES RAROS 
         //echo utf8_encode("la diferencia de meses con un año de diferencia es -> ".$meses."<br>"); 
      } 
      else{ 
          if($yr_dif == 0){ 
             $meses=$fFin_mon - $fIni_mon; 
            return $meses; 
            //echo utf8_encode("la diferencia de meses con cero años de diferencia es -> ".$meses.", donde el mes inicial es ".$fIni_mon.", el mes final es ".$fFin_mon."<br>"); 
         } 
         else{ 
             if($yr_dif > 1){ 
               $fIni_mon = 12 - $fIni_mon; 
               $meses = $fFin_mon + $fIni_mon + (($yr_dif - 1) * 12); 
               return $meses; 
               //echo utf8_encode("la diferencia de meses con mas de un año de diferencia es -> ".$meses."<br>"); 
            } 
            else 
               echo "ERROR -> la fecha inicial es mayor a la fecha final <br>"; 
               exit(); 
         } 
      } 
   } 
 
} 
/////////////////////////////////// funciones especiales para nomina ////////////////////////////////////////////////
// Insersión en nomina
function insertar_func_nomina($ncampos,$datos,$tabla)
{
	include ('../comunes/mensajes.php');
	//Creamos la sentencia SQL y la ejecutamos
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $campos .= $datos[$contador][0].',';
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $campos .= $datos[$contador][0];
	}
	$contador = 0;
	while ($contador < $ncampos - 1) 
        { 	
	    $valores .= "'".$datos[$contador][2]."',";
	    $contador ++;	
	}
	if ($contador == $ncampos - 1)
	{
	    $valores .= "'".$datos[$contador][2]."'";
	}
	$sql = "insert into $tabla ($campos) values ($valores)";
	mysql_query($sql);	
}
// funcion para verificar vacaciones
function verifica_vaciones($mes_nom,$ano_nom,$por_nom,$cod_dep,$cod_tcar,$print,$ced_per,$cod_car,$procesar_val,$prenomina,$bonos)
{   
	if($por_nom==2 OR $por_nom==3){
		$pagar_vacaciones = "NO";
			$sql="select * from vista_cargos_per";
			if ($ced_per and $cod_car) { $sql.=" WHERE ced_per=".$ced_per." AND cod_car=".$cod_car; }
			$busq=mysql_query($sql);
			if($reg=mysql_fetch_array($busq)){
			   do{
				$ano=substr($reg['fch_asg'],0,4);
				$mes=substr($reg['fch_asg'],5,2);
				if($ano<$ano_nom AND $mes==$mes_nom){
				   if ($print=="SI") {
					echo "<tr><td class='lista_tablas'>".$reg['cod_dep']."-".$reg['num_car']."</td>
					<td class='lista_tablas'>".$reg['nom_car']."</td>
					<td class='lista_tablas'>".$reg['ced_per']."</td>
					<td class='lista_tablas'>".$reg['nom_per']."</td>
					<td class='lista_tablas'>".$reg['ape_per']."</td>
					<td class='lista_tablas'>".$reg['fch_asg']."</td></tr>";
				   }
				   $pagar_vacaciones = "SI";
				   if ($pagar_vacaciones == "SI") {
					$sql="select * from valores";
						  $res = mysql_query($sql);
						  while ($row = mysql_fetch_array($res))
							  {
						 $$row['des_val'] = $row['val_val'];
						  }
					  if ($bonos) { $bonos = $reg['mon_sue_ant'] * $PBV; redondear ($bonos,2,'','.'); }
					  $vacaciones = (((($reg['mon_sue_ant'] + $bonos) / 30) * $BV));
					  $vacaciones = redondear ($vacaciones,2,'','.');
				   }
				   if ($print=="Nomina") {
					  echo "<tr><td>Bono Vacacional</td>";
					  echo "<td align='right'>"; echo redondear ($vacaciones,2,'.',','); echo "</td><td>&nbsp;</td><td>&nbsp;</td><tr>";
					$tabla_asign = "nomina_asign";	// nombre de la tabla
					$ncampos_asign = "12";		//numero de campos del formulario
					$datos_asign[0] = crear_datos ("ano_asg","Año",$_GET['ano_nom'],"1","4","numericos");
					$datos_asign[1] = crear_datos ("mes_asg","Mes",$_GET['mes_nom'],"1","2","numericos");
					$datos_asign[2] = crear_datos ("ced_per","Cedula",$reg['ced_per'],"1","12","alfanumericos");
					$datos_asign[3] = crear_datos ("cod_car","Cargo",$reg['cod_car'],"1","11","alfanumericos");
					$datos_asign[4] = crear_datos ("por_nom","Porción",$_GET['por_nom'],"1","1","numericos");
					$datos_asign[5] = crear_datos ("cod_dep","Dependencia",$cod_dep,"1","5","alfanumericos");
					$datos_asign[6] = crear_datos ("cod_tcar","Cod tipo cargo",$cod_tcar,"1","11","alfanumericos");
					$datos_asign[7] = crear_datos ("cod_cnp","Cod Asignacion",0,"1","11","numericos");
					$datos_asign[8] = crear_datos ("con_cnp","Concepto de Asignacion","Bono Vacacional","1","100","alfanumericos");
					$datos_asign[9] = crear_datos ("cod_con","Cod Concesion",0,"1","11","numericos");
					$datos_asign[10] = crear_datos ("nom_con","Nombre Concesion","Bono Vacacional","1","50","alfanumericos");
					$datos_asign[11] = crear_datos ("mcuo_cnp","Monto Pagar",$vacaciones,"1","12","decimal");
					if ($procesar_val=="SI"){
					  if ($prenomina==1){$tabla_asign="nominapre_asign";}					
					  insertar_func_nomina($ncampos_asign,$datos_asign,$tabla_asign,"centro.php");
					}
				   }
				}
			   }while($reg=mysql_fetch_array($busq));
			}
			return $vacaciones;
	}
}
//Redondear con N decimales
function redondear ($numero,$decimales,$sep_mil,$sep_dec){
	$num_formateado = number_format($numero, $decimales, $sep_dec, $sep_mil);
	return $num_formateado;
}

//funcion para calcular numero de lunes
function calculo_lunes ($mes,$ano)
{ 
   $fecha_ini = $ano."-".$mes."-01";
   $mes_act = $mes;
   $fechaComparacion = strtotime($fecha_ini);
   $calculo = $fechaComparacion;
   $contador = 0;
   while ($mes_act == $mes)
   	{
      if (date("D",$calculo) == "Mon")  			//Verificamos si es lunes
      {
         $contador ++;
      }
	  $calculo = strtotime("+1 days", $fechaComparacion); 	//Le suamos 1 días
	  $fechaComparacion = $calculo;
	  $mes_act = date("m",$fechaComparacion);
	}
   return $contador;
}

//Calculo de los Asignaciones
function asignaciones($sueldo,$ced_per,$cod_car,$cod_dep,$cod_tcar,$factor,$procesar_val,$prenomina){
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
    $total_asig = $sueldo;
	$sql3="select a.*, c.* from asignaciones a, concesiones c WHERE a.ced_per=".$ced_per." AND a.cod_car=".$cod_car." AND a.cod_con=c.cod_con;";
	$busq3=mysql_query($sql3);
	if($reg3=mysql_fetch_array($busq3)){
		do{
			$monto_asig = 0;
			if ($reg3['por_cnp'] > 0) {
				if (($reg3['ncuo_cnp'] - $reg3['ncp_cnp']) > 0) { 
					if ($reg3['bpor_cnp'] == "B") { $monto_asig = $sueldo * ($reg3['por_cnp']/100);   } 
					if ($reg3['bpor_cnp'] == "I") {  
						$monto_asig = ((($sueldo / 30) * $BV)/12);  // calculo de cuota parte vacaciones
 						$monto_asig += ((($sueldo / 30) * $Ag)/12); // calculo de cuota parte Aguinaldos 
 						$monto_asig += $sueldo;			   // Sueldo
 						$monto_asig = $monto_asig * ($reg3['por_cnp']/100);  
					} 
					if ($reg3['bpor_cnp'] == "U") { $monto_asig = $reg3['por_cnp'] * $UT;  } 
					}
					}
					else
					{ if ($reg3['mcuo_cnp']) { if ( ($reg3['ncuo_cnp'] - $reg3['ncp_cnp']) > 0) { $monto_asig = $reg3['mcuo_cnp']; }} }
					if ($monto_asig) {
						$monto_asig = redondear ($monto_asig,2,'','.');
						//$monto_asig = $monto_asig / $factor;
						$monto_asig = redondear ($monto_asig,2,'','.');
						if ($reg3["con_cnp"]=="BONO NOCTURNO") { $total_asig2[1] = $monto_asig; }
						$total_asig += $monto_asig;
						echo "<tr><td>".$reg3['con_cnp']."</td>";
						echo "<td align='right'>"; echo redondear ($monto_asig,2,'.',','); echo "</td>";
						echo "
						<td>&nbsp;</td>
						<td>&nbsp;</td>						
						</tr>";
						$tabla_asign = "nomina_asign";	// nombre de la tabla
						$ncampos_asign = "12";		//numero de campos del formulario
						$datos_asign[0] = crear_datos ("ano_asg","Año",$_GET['ano_nom'],"1","4","numericos");
						$datos_asign[1] = crear_datos ("mes_asg","Mes",$_GET['mes_nom'],"1","2","numericos");
						$datos_asign[2] = crear_datos ("ced_per","Cedula",$reg3['ced_per'],"1","12","alfanumericos");
						$datos_asign[3] = crear_datos ("cod_car","Cargo",$reg3['cod_car'],"1","11","alfanumericos");
						$datos_asign[4] = crear_datos ("por_nom","Porción",$_GET['por_nom'],"1","1","numericos");
						$datos_asign[5] = crear_datos ("cod_dep","Dependencia",$cod_dep,"1","5","alfanumericos");
						$datos_asign[6] = crear_datos ("cod_tcar","Cod tipo cargo",$cod_tcar,"1","11","alfanumericos");
						$datos_asign[7] = crear_datos ("cod_cnp","Cod Asignacion",$reg3['cod_cnp'],"1","11","numericos");
						$datos_asign[8] = crear_datos ("con_cnp","Concepto de Asignacion",$reg3["con_cnp"],"1","100","alfanumericos");
						$datos_asign[9] = crear_datos ("cod_con","Cod Concesion",$reg3['cod_con'],"1","11","numericos");
						$datos_asign[10] = crear_datos ("nom_con","Nombre Concesion",$reg3['nom_con'],"1","50","alfanumericos");
						$datos_asign[11] = crear_datos ("mcuo_cnp","Monto Pagar",$monto_asig,"1","12","decimal");
						if ($procesar_val=="SI"){
						    if ($prenomina==1) { $tabla_asign = "nominapre_asign"; }					
						    insertar_func_nomina($ncampos_asign,$datos_asign,$tabla_asign,"centro.php");
						    if ($reg3['per_cnp']!="S") {
						    	$sql_update  = "update asignaciones set ";
						    	$sql_update .= "ncp_cnp = '".($reg3['ncp_cnp']+1)."' ";
			  			    	$sql_update .= "WHERE cod_cnp = ".$reg3['cod_cnp'];
			  			    	mysql_query ($sql_update);
			  			    }
						}					        
					}
		}while($reg3=mysql_fetch_array($busq3));
	}
	$total_asig2[0] = $total_asig;
	return $total_asig2;
}

//Calculo de los descuentos de Ley
function descuentos_ley($sueldo,$lph,$spf,$sso,$cah,$sfu,$factor,$prenomina,$bonos){
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
	if($lph=="A"){
		$monto_lph = ($sueldo + $bonos) * $LPH;  
		$monto_lph = redondear ($monto_lph,2,'','.');
		$monto_lph = $monto_lph / $factor;
		$monto_lph = redondear ($monto_lph,2,'','.');
		$total_ded = $monto_lph;
		echo '<tr><td>FAOV</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_lph,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
		</tr>';							
	}
	if($spf=="A"){
		$monto_spf = (((($sueldo + $bonos) * 12) / 52) * $SPF);
		$monto_spf = $monto_spf * calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
		$monto_spf = redondear ($monto_spf,2,'','.');
		$monto_spf = $monto_spf / $factor;
		$monto_spf = redondear ($monto_spf,2,'','.');
		$total_ded += $monto_spf; 
		echo '<tr><td>PIE</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_spf,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
		</tr>';							
	}
	if($sso=="A"){
		if ($SM && $SMMSSO) { 
		    $sueldo_max = $SM * $SMMSSO;
		    $sueldo_max = redondear($sueldo_max,2,'','.');
		     if ($sueldo >= $sueldo_max) { $sueldo =  $sueldo_max; }  
		}
		$monto_sso = (((($sueldo + $bonos) * 12) / 52) * $SSO);
		$monto_sso = $monto_sso * calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
		$monto_sso = redondear ($monto_sso,2,'','.');
		$monto_sso = $monto_sso / $factor;
		$monto_sso = redondear ($monto_sso,2,'','.');
		$total_ded += $monto_sso; 
		echo '<tr><td>Seguro social obligatorio</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_sso,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
		</tr>';							
	}
	if($cah=="A"){
		$monto_cah = ($sueldo) * $CAH;
		$monto_cah = redondear ($monto_cah,2,'','.');
		$monto_cah = $monto_cah / $factor;
		$monto_cah = redondear ($monto_cah,2,'','.');
		$total_ded += $monto_cah;
		echo '<tr><td>Caja de Ahorro</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_cah,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
		</tr>';							
	}
	if($sfu=="A"){
		$monto_sfu = $SFU;
		$monto_sfu = redondear ($monto_sfu,2,'','.');
		$monto_sfu = $monto_sfu / $factor;
		$monto_sfu = redondear ($monto_sfu,2,'','.');
		$total_ded += $monto_sfu;
		echo '<tr><td>Servicio Funerario</td>
		<td align="right">&nbsp;</td>
		<td align="right">'; echo redondear ($monto_sfu,2,".",","); echo '</td>
		<td align="right">&nbsp;</td>
		</tr>';							
	}
	$datos_ded_ley[0] = crear_datos ("total_ded","Total Deducciones",$total_ded,"1","12","decimal");
	$datos_ded_ley[1] = crear_datos ("lph","LPH",$monto_lph,"1","12","decimal");
	$datos_ded_ley[2] = crear_datos ("spf","SPF",$monto_spf,"1","12","decimal");
	$datos_ded_ley[3] = crear_datos ("sso","SSO",$monto_sso,"1","12","decimal");
	$datos_ded_ley[4] = crear_datos ("cah","CAH",$monto_cah,"1","12","decimal");
	$datos_ded_ley[5] = crear_datos ("sfu","SFU",$monto_sfu,"1","12","decimal");
	return $datos_ded_ley;
}

//Calculo de los descuentos
function descuentos($sueldo,$ced_per,$cod_car,$cod_dep,$cod_tcar,$total_ded,$procesar_val,$prenomina){
	$sql="select * from valores";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$$row['des_val'] = $row['val_val'];
	}
	$sql3="select * from deducciones d, descuentos ds WHERE ced_per=".$ced_per." AND cod_car=".$cod_car." AND d.cod_des=ds.cod_des";
	$busq3=mysql_query($sql3);
	if($reg3=mysql_fetch_array($busq3)){
		do{
			$monto_ded = 0;
			if ($reg3['por_dsp'] > 0) {
				if (($reg3['ncuo_dsp'] - $reg3['ncp_dsp']) > 0) { 
					if ($reg3['bpor_dsp'] == "B") { $monto_ded = $sueldo * ($reg3['por_dsp']/100);   } 
					if ($reg3['bpor_dsp'] == "I") {  
						$monto_ded = ((($sueldo / 30) * $BV)/12);  // calculo de cuota parte vacaciones
 						$monto_ded += ((($sueldo / 30) * $Ag)/12); // calculo de cuota parte Aguinaldos 
 						$monto_ded += $sueldo;			   // Sueldo
 						$monto_ded = $monto_ded * ($reg3['por_dsp']/100);  
					} 
					if ($reg3['bpor_dsp'] == "U") { $monto_ded = $reg3['por_dsp'] * $UT;  } 
					}
					}
					else
					{ if ($reg3['mcuo_dsp']) { if ( ($reg3['ncuo_dsp'] - $reg3['ncp_dsp']) > 0) { $monto_ded = $reg3['mcuo_dsp']; }} }
					if ($monto_ded) {
						$monto_ded = redondear ($monto_ded,2,'','.');				
						$total_ded += $monto_ded;
						echo "<tr><td>".$reg3['con_dsp']."</td>";
						echo "<td>&nbsp;</td>";
						echo "
						<td align='right'>"; echo redondear ($monto_ded,2,'.',','); echo "</td>
						<td>&nbsp;</td>						
						</tr>";
			$tabla_deduc = "nomina_deduc";	// nombre de la tabla
			$ncampos_deduc = "12";		//numero de campos del formulario
			$datos_deduc[0] = crear_datos ("ano_ded","Año",$_GET['ano_nom'],"1","4","numericos");
			$datos_deduc[1] = crear_datos ("mes_ded","Mes",$_GET['mes_nom'],"1","2","numericos");
			$datos_deduc[2] = crear_datos ("ced_per","Cedula",$reg3['ced_per'],"1","12","alfanumericos");
			$datos_deduc[3] = crear_datos ("cod_car","Cargo",$reg3['cod_car'],"1","11","alfanumericos");
			$datos_deduc[4] = crear_datos ("por_nom","Porción",$_GET['por_nom'],"1","1","numericos");
			$datos_deduc[5] = crear_datos ("cod_dep","Dependencia",$cod_dep,"1","5","alfanumericos");
			$datos_deduc[6] = crear_datos ("cod_tcar","Cod tipo cargo",$cod_tcar,"1","11","alfanumericos");
			$datos_deduc[7] = crear_datos ("cod_dsp","Cod deducciones",$reg3['cod_dsp'],"1","11","numericos");
			$datos_deduc[8] = crear_datos ("con_dsp","Concepto de deducciones",$reg3['con_dsp'],"1","100","alfanumericos");
			$datos_deduc[9] = crear_datos ("cod_des","Cod descuento",$reg3['cod_des'],"1","11","numericos");
			$datos_deduc[10] = crear_datos ("nom_des","Nombre Descuento",$reg3['nom_des'],"1","50","alfanumericos");
			$datos_deduc[11] = crear_datos ("mcuo_dsp","Monto Descontar",$monto_ded,"1","12","decimal");
			if ($procesar_val=="SI"){	
			  if ($prenomina==1){ $tabla_deduc = "nominapre_deduc"; }			
			  insertar_func_nomina($ncampos_deduc,$datos_deduc,$tabla_deduc,"centro.php");
			  if ($prenomina!=1){
			     if ($reg3['per_dsp']!="S") {
			     $sql_update  = "update deducciones set ";
			     $sql_update .= "ncp_dsp = '".($reg3['ncp_dsp']+1)."' ";
			     $sql_update .= "WHERE cod_dsp = ".$reg3['cod_dsp'];
			     mysql_query ($sql_update);
			     }
			  }
			}

					}
		}while($reg3=mysql_fetch_array($busq3));
	}
	return $total_ded;
}
/////////////////////////////////// funciones especiales para nomina ////////////////////////////////////////////////

//Consulta por fecha
function consulta_fecha ($campos,$tabla,$nom_fecha,$dia,$mes,$ano){
	$sql = "select $campos from $tabla where DAYOFMONTH($nom_fecha)=$dia and MONTH($nom_fecha)=$mes And YEAR($nom_fecha) = $ano;";

}
?>
