<?php 
////// Funciones utilizadas en la nomina
////// Esta esta destinada a efectuar calculos de la nomina
function consulta_valores($valor){
	if (!$valor){
		$sql="SELECT * FROM valores";
	}
	else{
		$sql="SELECT * FROM valores WHERE des_val = '".$valor."'";	
	}
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res))
	{
		$valores[$row['des_val']] = $row['val_val'];
	}
	return $valores;
}
function calculo_primas($ced_per,$sueldo_base,$ingreso,$cargo,$factor,$sueldob_adicional){
	$valores = consulta_valores();
	$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
			$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue = s.cod_sue";
			$res_car = mysql_fetch_array(mysql_query($sql_car));
			$horas = $res_car[hora_sue];
			// para asistencial y Medicos
			if ($horas >= 3 AND $horas <= 8){
				$horas = $horas * 6;
				switch ($horas) {
					case ($horas<36):
							$horas = 30;
						break;
					case ($horas>=36 AND $horas <= 39):
							$horas = 36;
						break;
					case ($horas >= 40):
							$horas = 40;
						break;
				}
			}

			$ant = edad($ingreso);
			$ant = $ant + $res_per[ano_adm_per];
		/// Compensacion por Evaluacion
		if ($res_per[comp_eva]>0){
			$prima[comp_eval][total] = redondear(($sueldo_base*$res_per[comp_eva]),2,'','.');
			$prima[comp_eval][total] = redondear(($prima[comp_eval][total]+$res_per[comp_eval_ant]),2,'','.');
			$prima[comp_eval][periodo] = redondear(($prima[comp_eval][total]*$factor),2,'','.');
			$prima[comp_eval][titulo] = 'Compensaci�n Salarial por Evaluaci�n y Desempe�o';
			$prima[total] += redondear($prima[comp_eval][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[comp_eval][total],2,'','.');
			if ($prima[comp_eval][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[comp_eval][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[comp_eval][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[comp_eval][periodo],2,".",",").'</td>
				 	</tr>';
			}

		}
		/// Exclusividad
		if ($res_per[exc_asg]=='A'){
			$prima[exclusividad][total] = redondear((($sueldo_base+$sueldob_adicional)*$valores['PEXC']),2,'','.');
			$prima[exclusividad][periodo] = redondear(($prima[exclusividad][total]*$factor),2,'','.');
			$prima[exclusividad][titulo] = 'Prima Exlusividad ('.($valores["PEXC"]*100).'%)';
			$prima[total] += redondear($prima[exclusividad][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[exclusividad][total],2,'','.');
			if ($prima[exclusividad][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[exclusividad][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[exclusividad][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[exclusividad][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Bono Nocturno
		if ($res_car[noc_car]=='X'){
			$prima[nocturno][total] = redondear((($sueldo_base+$sueldob_adicional)*$valores['BN']),2,'','.');
			$prima[nocturno][periodo] = redondear(($prima[nocturno][total]*$factor),2,'','.');
			$prima[nocturno][titulo] = 'Bono Nocturno';
			$prima[total] += redondear($prima[nocturno][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[nocturno][total],2,'','.');
			if ($prima[nocturno][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[nocturno][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[nocturno][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[nocturno][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Prima por Hogar
		if ($res_per[hog_asg]=='A'){
			$prima[hogar][total] = $valores['PHOG'];
			$prima[hogar][total] = redondear(($prima[hogar][total]+$res_per[hog_asg_inc]),2,'','.');
			$prima[hogar][periodo] = redondear(($prima[hogar][total]*$factor),2,'','.');
			$prima[hogar][titulo] = 'Prima Hogar';
			$prima[total] += redondear($prima[hogar][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[hogar][total],2,'','.');
			if ($prima[hogar][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[hogar][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[hogar][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[hogar][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Prima por Hijos
		if ($res_per[hij_asg]=='A'){
	        $nhijos = 0;
	        $sql="SELECT * FROM familiares WHERE ced_per='".$ced_per."' AND par_fam='H'";
		    $res = mysql_query($sql);
		    while ($row = mysql_fetch_array($res))
		    {
	            if (edad($row["fnac_fam"])<=18 || (edad($row["fnac_fam"])<=25 AND $row[est_fam] != '') || $row[dis_fam]=='S'){
	                $nhijos+=1;
	                // solo 01 hijo
	                //$nhijos = 1;
	            }
		    }
			if ($nhijos > 0 ){
				$prima[hijos][total]=redondear(($valores['PH']*$nhijos),2,'','.');
				$prima[hijos][total] = redondear(($prima[hijos][total]+$res_per[hij_asg_inc]),2,'','.');
				$prima[hijos][periodo] = redondear(($prima[hijos][total]*$factor),2,'','.');
				$prima[hijos][titulo]= 'Prima '.$nhijos.' Hijos';
				$prima[total] += redondear($prima[hijos][periodo],2,'','.');
				$prima[total_completo] += redondear($prima[hijos][total],2,'','.');
				echo '<tr align="right">
		 			<td align="left" style="padding: 3px;">'.$prima[hijos][titulo].'</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($prima[hijos][periodo],2,".",",").'</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($prima[hijos][periodo],2,".",",").'</td>
			 	</tr>';
			}
		}
		/// Prima de Transporte
		if ($res_per[tra_asg]=='A'){
			$prima[transporte][total] = $valores['PT'];
			$prima[transporte][total] = redondear(($prima[transporte][total]+$res_per[tra_asg_inc]),2,'','.');
			$prima[transporte][periodo] = redondear(($prima[transporte][total]*$factor),2,'','.');
			$prima[transporte][titulo] = 'Prima Transporte';
			$prima[total] += redondear($prima[transporte][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[transporte][total],2,'','.');
			if ($prima[transporte][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[transporte][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[transporte][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[transporte][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Prima de Medico Especialista
		if ($res_per[med_asg]=='A'){
			$prima[medesp][total] = $valores['MEDESP'];
			$prima[medesp][total] = redondear(($prima[medesp][total]+$res_per[med_asg_inc]),2,'','.');
			$prima[medesp][periodo] = redondear(($prima[medesp][total]*$factor),2,'','.');
			$prima[medesp][titulo] = 'Prima M�dico Especialista';
			$prima[total] += redondear($prima[medesp][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[medesp][total],2,'','.');
			if ($prima[medesp][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[medesp][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[medesp][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[medesp][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Prima Asistencial
		if ($res_per[asi_asg]=='A' AND $horas>0){
			switch ($horas) {
				case 30:
					$prima[asistencial][total] = $valores['PA30'];
					break;
				case 36:
					$prima[asistencial][total] = $valores['PA36'];
					break;
				case ($horas >= 40):
					$prima[asistencial][total] = $valores['PA42'];
					break;
				default:
					$prima[asistencial][total] = 0;
					break;
			}
			$prima[asistencial][total] = redondear(($prima[asistencial][total]+$res_per[asi_asg_inc]),2,'','.');
			$prima[asistencial][periodo] = redondear(($prima[asistencial][total]*$factor),2,'','.');
			$prima[asistencial][titulo] = 'Prima Dedicaci�n a la Actividad de Salud (Prima Asistencial)';
			$prima[total] += redondear($prima[asistencial][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[asistencial][total],2,'','.');
			if ($prima[asistencial][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[asistencial][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[asistencial][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[asistencial][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Prima Profesionalizacion
		if ($res_per[pro_asg]=='A'){
			$prima[profesion][base] = 0;
			$profesion = '';
			$sql_profesion = "SELECT * FROM educacion WHERE ced_per = ".$ced_per." ORDER BY niv_edu DESC LIMIT 1";
			$reg_profesion = mysql_fetch_array(mysql_query($sql_profesion));
			$niv_prof = $reg_profesion[niv_edu];
			switch ($niv_prof) {
				case 3:
					$profesion = 'T.S.U.';
					$prima[profesion][base]=$valores[PPT];
					break;
				case 4:
					$profesion = 'Universitario';
					$prima[profesion][base]=$valores[PPP];
					break;
				case ($niv_prof >=5):
					$profesion = 'POSTGRADO';
					$prima[profesion][base]=$valores[PPPO];
					break;
				default:
					$profesion = '';
					$prima[profesion][base] = 0;
					break;
			}
			$sueldo_temp = redondear((($sueldo_base+$sueldob_adicional)+$prima[comp_eval][total]+$prima[asistencial][total]+$prima[transporte][total]),2,'','.');
			$prima[profesion][total]=redondear(($sueldo_temp*$prima[profesion][base]),2,'','.');
			$prima[profesion][total] = redondear(($prima[profesion][total]+$res_per[pro_asg_inc]),2,'','.');
			$prima[profesion][periodo] = redondear(($prima[profesion][total]*$factor),2,'','.');
			$prima[profesion][titulo] = 'Prima Profesionalizaci�n ('.$profesion.')';
			if ($prima[profesion][periodo]>0){			
				$prima[total] += redondear($prima[profesion][periodo],2,'','.');
				$prima[total_completo] += redondear($prima[profesion][total],2,'','.');
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[profesion][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[profesion][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[profesion][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
		/// Prima antiguedad
		if ($res_per[ant_asg]=='A'){
			$prima[antiguedad][base]=0;
			switch ($ant) {
			    case ($ant <= 5):
					switch ($horas) {
						case 30:
							$prima[antiguedad][base]=$valores[PANT1_H30];
							break;
						case 36:
							$prima[antiguedad][base]=$valores[PANT1_H36];
							break;
						case 40:
							$prima[antiguedad][base]=$valores[PANT1_H40];
							break;
					}
			        break;
			    case ($ant <= 10):
					switch ($horas) {
						case 30:
							$prima[antiguedad][base]=$valores[PANT2_H30];
							break;
						case 36:
							$prima[antiguedad][base]=$valores[PANT2_H36];
							break;
						case 40:
							$prima[antiguedad][base]=$valores[PANT2_H40];
							break;
					}
			        break;
			    case ($ant <= 15):
					switch ($horas) {
						case 30:
							$prima[antiguedad][base]=$valores[PANT3_H30];
							break;
						case 36:
							$prima[antiguedad][base]=$valores[PANT3_H36];
							break;
						case 40:
							$prima[antiguedad][base]=$valores[PANT3_H40];
							break;
					}
			        break;
			    case ($ant <= 20):
					switch ($horas) {
						case 30:
							$prima[antiguedad][base]=$valores[PANT4_H30];
							break;
						case 36:
							$prima[antiguedad][base]=$valores[PANT4_H36];
							break;
						case 40:
							$prima[antiguedad][base]=$valores[PANT4_H40];
							break;
					}
			        break;
			    case ($ant >= 21):
					switch ($horas) {
						case 30:
							$prima[antiguedad][base]=$valores[PANT5_H30];
							break;
						case 36:
							$prima[antiguedad][base]=$valores[PANT5_H36];
							break;
						case 40:
							$prima[antiguedad][base]=$valores[PANT5_H40];
							break;
					}
			        break;
			}
			$prima[antiguedad][base];
			$sueldo_temp = redondear((($sueldo_base+$sueldob_adicional)+$prima[comp_eval][total]+$prima[asistencial][total]+$prima[transporte][total]+$prima[profesion][total]),2,'','.');
			$prima[antiguedad][total]=redondear(($sueldo_temp*$prima[antiguedad][base]),2,'','.');
			$prima[antiguedad][total] = redondear(($prima[antiguedad][total]+$res_per[ant_asg_inc]),2,'','.');
			$prima[antiguedad][periodo] = redondear(($prima[antiguedad][total]*$factor),2,'','.');
			$prima[antiguedad][titulo] = 'Prima Antiguedad ('.$ant.' a�os)';
			if($prima[antiguedad][periodo]>0){
				$prima[total] += redondear($prima[antiguedad][periodo],2,'','.');
				$prima[total_completo] += redondear($prima[antiguedad][total],2,'','.');
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[antiguedad][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[antiguedad][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[antiguedad][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}

		/// Prima Sistema Publico Nacional
		if ($res_per[spn_asg]=='A'){
			switch ($horas) {
				case 36:
					$prima[spn][total] = $valores['PSPS36'];
					break;
				case ($horas >= 40):
					$prima[spn][total] = $valores['PSPS42'];
					break;
			}
			$prima[spn][total] = redondear(($prima[spn][total]+$res_per[spn_asg_inc]),2,'','.');
			$prima[spn][periodo] = redondear(($prima[spn][total]*$factor),2,'','.');
			$prima[spn][titulo] = 'Prima Sistema P�blico Nacional de Salud';
			$prima[total] += redondear($prima[spn][periodo],2,'','.');
			$prima[total_completo] += redondear($prima[spn][total],2,'','.');
			if ($prima[spn][periodo]>0){
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$prima[spn][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[spn][periodo],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($prima[spn][periodo],2,".",",").'</td>
				 	</tr>';
			}
		}
	}
	return $prima;
}
function calculo_asignaciones($ced_per,$sueldo_base,$ingreso,$cargo,$factor,$salario){
	$valores = consulta_valores();
		$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
		$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue=s.cod_sue";
		$res_car = mysql_fetch_array(mysql_query($sql_car));
		/// consultamos las asignaciones relacionadas con el cargo
		$sql_asg = "SELECT * FROM asignaciones a, concesiones c WHERE a.cod_con=c.cod_con AND ced_per = ".$ced_per." AND cod_car = ".$cargo." AND (ncuo_cnp > ncp_cnp OR per_cnp = 'S') AND des_cnp = '".$salario."' ORDER BY fch_cnp";
		$bus_asg = mysql_query($sql_asg);
		$contador = 0;
		while ($res_asg = mysql_fetch_array($bus_asg)){
			$monto_asg = '';
			$titulo_add = '';
			$contador += 1;
			$numero_cuota = $res_asg[ncp_cnp]+1;
			/// si el calculo es en base a unidad tributario o salario base
			if ($res_asg[por_cnp]>0 AND $res_asg[bpor_cnp]!=''){
				switch ($res_asg[bpor_cnp]) {
					case 'B':
						$monto_asg_total = redondear(($res_car[mon_sue]*($res_asg[por_cnp]/100)),2,'','.');
						$monto_asg = redondear(($monto_asg_total * $factor),2,'','.');
						$titulo_add = '('.redondear($res_asg[por_cnp],2,'','.').'% Sueldo Base)';
						break;
					case 'U':
						$monto_asg_total = redondear(($valores[UT]*($res_asg[por_cnp]/100)),2,'','.');
						$monto_asg = redondear(($monto_asg_total * $factor),2,'','.');
						$titulo_add = '('.redondear(($res_asg[por_cnp]/100),2,'','.').' U.T. mensuales)';
						break;
				}
				if ($res_asg[per_cnp]=='' AND $factor==1){
						$titulo_add = '('.($res_asg[ncp_cnp]+1).' y '.($res_asg[ncp_cnp]+2).' de '.$res_asg[ncuo_cnp].')';
						$numero_cuota = $res_asg[ncp_cnp]+2;
				}
				if ($res_asg[per_cnp]=='' AND $factor!=1){
					$titulo_add = '('.($res_asg[ncp_cnp]+1).' de '.$res_asg[ncuo_cnp].')';
					$numero_cuota = $res_asg[ncp_cnp]+1;
				}

			}
			/// si el calculo es en base a un Monto
			else{			
				if ($res_asg[mcuo_cnp]>0){
					$monto_asg_total = redondear(($res_asg[mcuo_cnp]),2,'','.');
					$monto_asg = $monto_asg_total;
					/// si es el factor es 1, es decir, si se paga un mes se multiplica por 2 para pagar dos quincenas
					if ($factor == 1 AND (($res_asg[ncuo_cnp] - $res_asg[ncp_cnp])>1) || $res_asg[per_cnp]=='S'){
						$monto_asg = redondear(($monto_asg_total*2),2,'','.');
						/// si no es permanente se muestran las dos cuentas que se pagan
						if ($res_asg[per_cnp]=='' AND $factor==1){
							$titulo_add = '('.($res_asg[ncp_cnp]+1).' y '.($res_asg[ncp_cnp]+2).' de '.$res_asg[ncuo_cnp].')';
								$numero_cuota = $res_asg[ncp_cnp]+2;
						}
					}
					// si no es permanente se muesta la cuota que se paga
					if ($res_asg[per_cnp]=='' AND $factor!=1){
						$titulo_add = '('.($res_asg[ncp_cnp]+1).' de '.$res_asg[ncuo_cnp].')';
						$numero_cuota = $res_asg[ncp_cnp]+1;
					}
				}
			}
			if ($monto_asg>0){
				$asignaciones[total][total] = $monto_asg_total;
				$asignaciones[$contador][monto] = $monto_asg;
				$asignaciones[$contador][titulo] = $res_asg[con_cnp].' '.$titulo_add;
				$asignaciones[$contador][cod_con] = $res_asg[cod_con];
				$asignaciones[$contador][nom_con] = $res_asg[nom_con];
				$asignaciones[$contador][cod_cnp] = $res_asg[cod_cnp];
				if ($res_dsc[per_asg]!='S'){
					$asignaciones[$contador][ncp_cnp] = $numero_cuota;
				}
				$asignaciones[total][monto] += $monto_asg;
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$asignaciones[$contador][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($asignaciones[$contador][monto],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($asignaciones[$contador][monto],2,".",",").'</td>
				 	</tr>';
			}
		}
	}
	return $asignaciones;
}
function calculo_deducciones($ced_per,$sueldo_base,$ingreso,$cargo,$factor,$salario){
	$valores = consulta_valores();
	$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
		$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue=s.cod_sue";
		$res_car = mysql_fetch_array(mysql_query($sql_car));
		/// consultamos las deducciones relacionadas con el cargo
		$sql_dsc = "SELECT * FROM deducciones dd, descuentos ds  WHERE dd.cod_des=ds.cod_des AND ced_per = ".$ced_per." AND cod_car = ".$cargo." AND (ncuo_dsp > ncp_dsp OR per_dsp = 'S') ORDER BY fch_dsp";
		$bus_dsc = mysql_query($sql_dsc);
		$contador = 0;
		while ($res_dsc = mysql_fetch_array($bus_dsc)){
			$numero_cuota = $res_dsc[ncp_dsp]+1;
			$monto_dsc = '';
			$titulo_add = '';
			$contador += 1;
			/// si el calculo es en base a unidad tributario o salario base
			if ($res_dsc[por_dsp]>0 AND $res_dsc[bpor_dsp]!=''){
				switch ($res_dsc[bpor_dsp]) {
					case 'B':
						$monto_dsc = redondear(($res_car[mon_sue]*($res_dsc[por_dsp]/100)),2,'','.');
						$monto_dsc = redondear(($monto_dsc * $factor),2,'','.');
						$titulo_add = '('.redondear($res_dsc[por_dsp],2,'','.').'% Sueldo Base)';
						break;
					case 'U':
						$monto_dsc = redondear(($valores[UT]*($res_dsc[por_dsp]/100)),2,'','.');
						$monto_dsc = redondear(($monto_dsc * $factor),2,'','.');
						$titulo_add = '('.redondear(($res_dsc[por_dsp]/100),2,'','.').' U.T. mensuales)';
						break;
				}
				if ($res_dsc[per_dsp]=='' AND $factor==1){
					$titulo_add .= '('.($res_dsc[ncp_dsp]+1).' y '.($res_dsc[ncp_dsp]+2).' de '.$res_dsc[ncuo_dsp].')';
					$numero_cuota = $res_dsc[ncp_dsp]+1;
				}
				if ($res_dsc[per_dsp]=='' AND $factor!=1){
					$titulo_add .= '('.($res_dsc[ncp_dsp]+1).' de '.$res_dsc[ncuo_dsp].')';
					$numero_cuota = $res_dsc[ncp_dsp]+1;
				}
			}
			/// si el calculo es en base a un Monto
			else{
				if ($res_dsc[mcuo_dsp]>0){
					$monto_dsc = redondear(($res_dsc[mcuo_dsp]),2,'','.');
					/// si es el factor es 1, es decir, si se paga un mes se multiplica por 2 para pagar dos quincenas
					if ($factor == '1' AND (($res_dsc[ncuo_dsp] - $res_dsc[ncp_dsp])>1) || $res_dsc[per_dsp]=='S'){
						$monto_dsc = redondear(($monto_dsc*2),2,'','.');
						/// si no es permanente se muestran las dos cuentas que se pagan
						if ($res_dsc[per_dsp]=='' AND $factor==1){
							$titulo_add = '('.($res_dsc[ncp_dsp]+1).' y '.($res_dsc[ncp_dsp]+2).' de '.$res_dsc[ncuo_dsp].')';
							$numero_cuota = $res_dsc[ncp_dsp]+2;
						}
					}
					// si no es permanente se muesta la cuota que se paga
					if ($res_dsc[per_dsp]=='' AND $factor!=1){
						$titulo_add = '('.($res_dsc[ncp_dsp]+1).' de '.$res_dsc[ncuo_dsp].')';
						$numero_cuota = $res_dsc[ncp_dsp]+1;
					}

				}
			}
			if ($monto_dsc>0){
				$deducciones[$contador][monto] = $monto_dsc;
				$deducciones[$contador][titulo] = $res_dsc[con_dsp].' '.$titulo_add;
				$deducciones[$contador][cod_des] = $res_dsc[cod_des];
				$deducciones[$contador][nom_des] = $res_dsc[nom_des];
				$deducciones[$contador][cod_dsp] = $res_dsc[cod_dsp];
				if ($res_dsc[per_dsp]!='S'){
					$deducciones[$contador][ncp_dsp] = $numero_cuota;
				}
				$deducciones[total][monto] += $monto_dsc;
				echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$deducciones[$contador][titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($deducciones[$contador][monto],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear(($deducciones[$contador][monto]*-1),2,".",",").'</td>
				 	</tr>';
			}
		}
	}
	return $deducciones;
}
function calculo_guardias($ced_per,$sueldo_base,$ingreso,$cargo,$sueldob_addicional,$primas,$dia_adic){
	$valores = consulta_valores();
	$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
		$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue=s.cod_sue";
		$res_car = mysql_fetch_array(mysql_query($sql_car));
		/// Consultamos las guardias registradas donde sta_gua = 'A';
		$sql_guar = "SELECT * FROM guardias_feriados WHERE cod_car = ".$res_car[cod_car]." AND ced_per = ".$res_per[ced_per]." AND sta_gua ='A';";
		$bus_guar = mysql_query($sql_guar);
		$guardias[total] = '';
		while($res_guar = mysql_fetch_array($bus_guar)){
			if ($res_guar[nor_gua]>0) {
				$var = 'nor_gua';
				$var_tit = 'Guardias Nocturnas';
				/// Calculo de monto
					/// para medicos
					if ($res_car[medico]=='s'){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[asistencial][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5*$res_guar[$var]),2,'','.');
					}
					if ($res_car[cod_tcar]==1 || $res_car[cod_tcar]==2){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[spn][total] + $primas[transporte][total] + $primas[profesion][total] + $primas[antiguedad][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*($res_car[hora_sue]/5)),2,'','.');
						$monto = redondear(($monto*0.5*10),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					$guardias[$res_guar[con_gua]][$var]['monto'] = $monto;
				///
				$guardias['total']['monto'] += $guardias[$res_guar[con_gua]][$var]['monto'];
				$guardias[$res_guar[con_gua]][$var]['dias'] = $res_guar[$var];
				$guardias[$res_guar[con_gua]][$var]['titulo'] = $res_guar[con_gua].' ('.$res_guar[$var].' '.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['concepto'] = $res_guar[con_gua].' ('.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['cod_gua'] = $res_guar[cod_gua];
				echo '<tr align="right">
		 			<td align="left" style="padding: 3px;">'.$guardias[$res_guar[con_gua]][$var][titulo].'</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 	</tr>';	
			}
			if ($res_guar[fer_gua]>0) {
				$var = 'fer_gua';
				$var_tit = 'Guardias Feriados';
				/// Calculo de monto
					$monto = 0;
					/// para medicos
					if ($res_car[medico]=='s'){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $res_car[escalafon] + $primas[exclusividad][total] + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[medesp][total] + $primas[spn][total] + $primas[transporte][total] + $primas[hogar][total] + $primas[hijos][total] + $primas[profesion][total] + $primas[antiguedad][total] + $dia_adic + $valores[BALI]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto/$res_car[hora_sue]),2,'','.');
						$monto = redondear(($monto*1.5*12),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					/// para Obreros
					if ($res_car[cod_tcar]==1 || $res_car[cod_tcar]==2){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[spn][total] + $primas[transporte][total] + $primas[profesion][total] + $primas[antiguedad][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					/// para el resto
					if ($res_car[medico]!='s' AND $res_car[cod_tcar]!=1 AND $res_car[cod_tcar]!=2){ 
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[asistencial][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					$guardias[$res_guar[con_gua]][$var]['monto'] = $monto;
				///
				$guardias['total']['monto'] += $guardias[$res_guar[con_gua]][$var]['monto'];
				$guardias[$res_guar[con_gua]][$var]['dias'] = $res_guar[$var];
				$guardias[$res_guar[con_gua]][$var]['titulo'] = $res_guar[con_gua].' ('.$res_guar[$var].' '.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['concepto'] = $res_guar[con_gua].' ('.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['cod_gua'] = $res_guar[cod_gua];
				echo '<tr align="right">
		 			<td align="left" style="padding: 3px;">'.$guardias[$res_guar[con_gua]][$var][titulo].'</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 	</tr>';
			}
			if ($res_guar[dom_gua]>0) {
				$var = 'dom_gua';
				$var_tit = 'Guardias Dom�ngos';
				/// Calculo de monto
					$monto = 0;
					/// para medicos
					if ($res_car[medico]=='s'){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $res_car[escalafon] + $primas[exclusividad][total] + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[medesp][total] + $primas[spn][total] + $primas[transporte][total] + $primas[hogar][total] + $primas[hijos][total] + $primas[profesion][total] + $primas[antiguedad][total] + $dia_adic + $valores[BALI]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto/$res_car[hora_sue]),2,'','.');
						$monto = redondear(($monto*1.5*12),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					/// para Obreros
					if ($res_car[cod_tcar]==1 || $res_car[cod_tcar]==2){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[spn][total] + $primas[transporte][total] + $primas[profesion][total] + $primas[antiguedad][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					/// para el resto
					if ($res_car[medico]!='s' AND $res_car[cod_tcar]!=1 AND $res_car[cod_tcar]!=2){ 
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[asistencial][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					$guardias[$res_guar[con_gua]][$var]['monto'] = $monto;
				///
				$guardias['total']['monto'] += $guardias[$res_guar[con_gua]][$var]['monto'];
				$guardias[$res_guar[con_gua]][$var]['dias'] = $res_guar[$var];
				$guardias[$res_guar[con_gua]][$var]['titulo'] = $res_guar[con_gua].' ('.$res_guar[$var].' '.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['concepto'] = $res_guar[con_gua].' ('.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['cod_gua'] = $res_guar[cod_gua];
				echo '<tr align="right">
		 			<td align="left" style="padding: 3px;">'.$guardias[$res_guar[con_gua]][$var][titulo].'</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 	</tr>';			}
			if ($res_guar[dof_gua]>0) {
				$var = 'dof_gua';
				$var_tit = 'Guardias Dom�ngos Feriados';

				/// Calculo de monto
					$monto = 0;
					/// para medicos
					if ($res_car[medico]=='s'){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $res_car[escalafon] + $primas[exclusividad][total] + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[medesp][total] + $primas[spn][total] + $primas[transporte][total] + $primas[hogar][total] + $primas[hijos][total] + $primas[profesion][total] + $primas[antiguedad][total] + $dia_adic + $valores[BALI]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto/$res_car[hora_sue]),2,'','.');
						$monto = redondear(($monto*1.5*12),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					/// para Obreros
					if ($res_car[cod_tcar]==1 || $res_car[cod_tcar]==2){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[comp_eval][total] + $primas[asistencial][total] + $primas[spn][total] + $primas[transporte][total] + $primas[profesion][total] + $primas[antiguedad][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					/// para el resto
					if ($res_car[medico]!='s' AND $res_car[cod_tcar]!=1 AND $res_car[cod_tcar]!=2){ 
						$monto = redondear(($sueldo_base + $sueldob_addicional + $primas[asistencial][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto*1.5),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					$guardias[$res_guar[con_gua]][$var]['monto'] = $monto;
				///
				$guardias['total']['monto'] += $guardias[$res_guar[con_gua]][$var]['monto'];
				$guardias[$res_guar[con_gua]][$var]['dias'] = $res_guar[$var];
				$guardias[$res_guar[con_gua]][$var]['titulo'] = $res_guar[con_gua].' ('.$res_guar[$var].' '.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['concepto'] = $res_guar[con_gua].' ('.$var_tit.')';				
				$guardias[$res_guar[con_gua]][$var]['cod_gua'] = $res_guar[cod_gua];
				echo '<tr align="right">
		 			<td align="left" style="padding: 3px;">'.$guardias[$res_guar[con_gua]][$var][titulo].'</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 	</tr>';
			}
			if ($res_guar[dis_gua]>0) {
				$var = 'dis_gua';
				$var_tit = 'Guardias a Disponibilidad';
				/// Calculo de monto
					$monto = 0;
					/// para medicos
					if ($res_car[medico]=='s'){
						$monto = redondear(($sueldo_base + $sueldob_addicional + $res_car[escalafon] + $primas[exclusividad][total] + $primas[asistencial][total] + $primas[medesp][total] + $primas[spn][total] + $primas[profesion][total] + $primas[antiguedad][total]),2,'','.');
						$monto = redondear(($monto/30),2,'','.');
						$monto = redondear(($monto/$res_car[hora_sue]),2,'','.');
						$monto = redondear(($monto*12),2,'','.');
						$monto = redondear(($monto*$res_guar[$var]),2,'','.');
					}
					$guardias[$res_guar[con_gua]][$var]['monto'] = $monto;
				///
				$guardias['total']['monto'] += $guardias[$res_guar[con_gua]][$var]['monto'];
				$guardias[$res_guar[con_gua]][$var]['dias'] = $res_guar[$var];
				$guardias[$res_guar[con_gua]][$var]['titulo'] = $res_guar[con_gua].' ('.$res_guar[$var].' '.$var_tit.')';
				$guardias[$res_guar[con_gua]][$var]['concepto'] = $res_guar[con_gua].' ('.$var_tit.')';				
				$guardias[$res_guar[con_gua]][$var]['cod_gua'] = $res_guar[cod_gua];
				echo '<tr align="right">
		 			<td align="left" style="padding: 3px;">'.$guardias[$res_guar[con_gua]][$var][titulo].'</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
			 		<td style="padding: 3px; width: 80px;">'.redondear($guardias[$res_guar[con_gua]][$var][monto],2,".",",").'</td>
			 	</tr>';
			}
		}
	return $guardias;
	}
}
function calculo_aguinaldos($ced_per,$total_sueldo,$ingreso,$cargo,$dias_pagar){
	$valores = consulta_valores();
	$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
		$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue=s.cod_sue";
		$res_car = mysql_fetch_array(mysql_query($sql_car));
		/// Calculamos el sueldo integral
			// determinar Antiguedad
			$dias_vac = 0;
			$ant = edad($ingreso);
			$ant = $ant + $res_per[ano_adm_per];
			/// calcular d�as de bono Vacional
			/// si es asistencial
			if ($res_car[medico]=='s'){
				$grado = $res_car[grad_sue];
				$var_valor = 'BV'.$grado;
				$dias_vac = $valores[$var_valor];
				$dias_vac=54;/////////////// chicle
			}
			/// si no es asistencial
			else{
				$horas = $res_car[hora_sue];
				switch ($horas) {
					case 30:
						$dias_vac = $valores['BV30'];
						break;
					case 36:
						$dias_vac = $valores['BV36'];
						break;
					case ($horas >= 40):
						$dias_vac = $valores['BV42'];
						break;
					default:
						$dias_vac = 0;
						break;
				}
			}
			/// Calcular el monto del bono Vacacional
		    $Bono_Vac[0] = redondear(($total_sueldo),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] / 30),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] * $dias_vac),2,'','.');
		    $Bono_Vac[1] = redondear (($Bono_Vac[0] / 12),2,'','.');
		    /// Calcular el monto del Bono de Aguinaldos
		    $Bono_Agu[0] = redondear(($total_sueldo),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] + $Bono_Vac[1]),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] / 30),2,'','.');
		    $sueldo_agui = $Bono_Agu[0];
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] * $dias_pagar),2,'','.');
		    $Bono_Agu[1] = redondear (($Bono_Agu[0] / 12),2,'','.');
		    $Bono_Agu[titulo] = $dias_pagar.' d�as de Aguinaldos (Salario Integral diario: Bs. '.redondear($sueldo_agui,2,'.',',').')';
		    if ($Bono_Agu[0]>0){
		    	echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$Bono_Agu[titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($Bono_Agu[0],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($Bono_Agu[0],2,".",",").'</td>
				 	</tr>';
		    }
	}
	return $Bono_Agu;
}
function calculo_vacaciones($ced_per,$total_sueldo,$ingreso,$cargo,$dias_pagar){
	$valores = consulta_valores();
	$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
		$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue=s.cod_sue";
		$res_car = mysql_fetch_array(mysql_query($sql_car));
		/// Calculamos el sueldo integral
			// determinar Antiguedad
			$dias_vac = 0;
			$ant = edad($ingreso);
			$ant = $ant + $res_per[ano_adm_per];
			/// calcular d�as de bono Vacional
			/// si es asistencial
			if ($res_car[medico]=='s'){
				$grado = $res_car[grad_sue];
				$var_valor = 'BV'.$grado;
				$dias_vac = $valores[$var_valor];
				$dias_vac=54;/////////////// chicle
			}
			/// si no es asistencial
			else{
				$horas = $res_car[hora_sue];
				switch ($horas) {
					case 30:
						$dias_vac = $valores['BV30'];
						break;
					case 36:
						$dias_vac = $valores['BV36'];
						break;
					case ($horas >= 40):
						$dias_vac = $valores['BV42'];
						break;
					default:
						$dias_vac = 0;
						break;
				}
			}
			/// Calcular el monto del bono Vacacional
		    $Bono_Vac[0] = redondear(($total_sueldo),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] / 30),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] * $dias_vac),2,'','.');
		    $Bono_Vac[1] = redondear (($Bono_Vac[0] / 12),2,'','.');
		    $Bono_Vac[titulo] = 'Bono Vacacional';
		    if ($Bono_Vac[0]>0){
		    	echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">'.$Bono_Vac[titulo].'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($Bono_Vac[0],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($Bono_Vac[0],2,".",",").'</td>
				 	</tr>';
		    }
	}
	return $Bono_Vac;
}
function calculo_deducciones_ley($ced_per,$sueldo_base,$ingreso,$cargo,$factor,$sueldob_addicional,$primas){
	$valores = consulta_valores();
	$sql_per = "SELECT * FROM personal WHERE ced_per=".$ced_per;
	if ($res_per = mysql_fetch_array(mysql_query($sql_per))){
		/// consultamos datos del cargo
		$sql_car = "SELECT * FROM cargos c, sueldos s WHERE c.cod_car = ".$cargo." AND c.cod_sue=s.cod_sue";
		$res_car = mysql_fetch_array(mysql_query($sql_car));
		/// Calculamos el sueldo integral
			// determinar Antiguedad
			$dias_vac = 0;
			$ant = edad($ingreso);
			$ant = $ant + $res_per[ano_adm_per];
			/// calcular d�as de bono Vacional
			/// si es asistencial
			if ($res_car[medico]=='s'){
				$grado = $res_car[grad_sue];
				$var_valor = 'BV'.$grado;
				$dias_vac = $valores[$var_valor];
				$dias_vac=54;/////////////// chicle
			}
			/// si no es asistencial
			else{
				$horas = $res_car[hora_sue];
				switch ($horas) {
					case 30:
						$dias_vac = $valores['BV30'];
						break;
					case 36:
						$dias_vac = $valores['BV36'];
						break;
					case ($horas >= 40):
						$dias_vac = $valores['BV42'];
						break;
					default:
						$dias_vac = 0;
						break;
				}
			}
			/// Calcular el monto del bono Vacacional
		    $Bono_Vac[0] = redondear(($sueldo_base * $factor),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] + $sueldob_addicional),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] + $primas[total]),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] / 30),2,'','.');
		    $Bono_Vac[0] = redondear(($Bono_Vac[0] * $dias_vac),2,'','.');
		    $Bono_Vac[1] = redondear (($Bono_Vac[0] / 12),2,'','.');
		    /// Calcular el monto del Bono de Aguinaldos
		    $Bono_Agu[0] = redondear(($sueldo_base * $factor),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] + $sueldob_addicional),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] + $primas[total]),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] + $Bono_Vac[1]),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] / 30),2,'','.');
		    $Bono_Agu[0] = redondear(($Bono_Agu[0] * $valores['AGUI']),2,'','.');
		    $Bono_Agu[1] = redondear (($Bono_Agu[0] / 12),2,'','.');
		    /// Asignar Sueldo Integral 
		    $sueldo_integral = $sueldo_base + $sueldob_addicional + $primas[total] + $Bono_Vac[1] + $Bono_Agu[1];
		    $descuentos_ley[integral][monto]=$sueldo_integral;
		/// verificamos los descuentos de ley activos
			/// Fondo ahorro obligatorio para vivienda
			if ($res_per[lph_des]=='A'){
				$RTLPH = $valores['RTLPH']/100;
				$APLPH = $valores['APLPH']/100;
				$descuentos_ley[lph][apor] = redondear(($sueldo_integral * $APLPH),2,'','.');
				$descuentos_ley[lph][rete] = redondear(($sueldo_integral * $RTLPH),2,'','.');
				$descuentos_ley[lph][apor_por] = redondear(($APLPH * 100),2,'','.');
				$descuentos_ley[lph][rete_por] = redondear(($RTLPH * 100),2,'','.');
				$descuentos_ley[total][apor] = redondear(($descuentos_ley[total][apor] + $descuentos_ley[lph][apor]),2,'','.');
				$descuentos_ley[total][rete] = redondear(($descuentos_ley[total][rete] + $descuentos_ley[lph][rete]),2,'','.');
					echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">FAOV&nbsp;(Ret:'.($RTLPH * 100).'% )( Aport:'.($APLPH * 100).'% )(Salario Integral)</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[lph][rete],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[lph][apor],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear(($descuentos_ley[lph][rete]*-1),2,".",",").'</td>
				 	</tr>';
			}
			/// SSO
			if ($res_per[sso_des]=='A'){
				$RTSSO = $valores['RTSSO']/100;
				$APSSO = $valores['APSSO']/100;
				$sueldo_base_sso = redondear (($sueldo_integral * 12),2,'','.');
        		$sueldo_base_sso = redondear (($sueldo_base_sso / 52),2,'','.');
				$descuentos_ley[sso][apor] = redondear(($sueldo_base_sso * $APSSO),2,'','.');
				$descuentos_ley[sso][rete] = redondear(($sueldo_base_sso * $RTSSO),2,'','.');
				$descuentos_ley[sso][apor_por] = redondear(($APSSO * 100),2,'','.');
				$descuentos_ley[sso][rete_por] = redondear(($RTSSO * 100),2,'','.');
				$lunes = calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
				$descuentos_ley[sso][apor] = redondear(($descuentos_ley[sso][apor]  * $lunes),2,'','.');
				$descuentos_ley[sso][rete] = redondear(($descuentos_ley[sso][rete]  * $lunes),2,'','.');
				$descuentos_ley[total][apor] = redondear(($descuentos_ley[total][apor] + $descuentos_ley[sso][apor]),2,'','.');
				$descuentos_ley[total][rete] = redondear(($descuentos_ley[total][rete] + $descuentos_ley[sso][rete]),2,'','.');
					echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">S.S.O&nbsp;(Ret:'.($RTSSO * 100).'%)(Aport:'.($APSSO * 100).'%)(Salario Integral)</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[sso][rete],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[sso][apor],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear(($descuentos_ley[sso][rete]*-1),2,".",",").'</td>
				 	</tr>';
			}
			/// PIE
			if ($res_per[spf_des]=='A'){
				$RTPIE = $valores['RTPIE']/100;
				$APPIE = $valores['APPIE']/100;
				$sueldo_base_pie = redondear (($sueldo_integral * 12),2,'','.');
        		$sueldo_base_pie = redondear (($sueldo_base_pie / 52),2,'','.');
				$descuentos_ley[pie][apor] = redondear(($sueldo_base_pie * $APPIE),2,'','.');
				$descuentos_ley[pie][rete] = redondear(($sueldo_base_pie * $APPIE),2,'','.');
				$descuentos_ley[pie][apor_por] = redondear(($APPIE*100),2,'','.');
				$descuentos_ley[pie][rete_por] = redondear(($APPIE*100),2,'','.');
				$lunes = calculo_lunes ($_GET['mes_nom'],$_GET['ano_nom']);
				$descuentos_ley[pie][apor] = redondear(($descuentos_ley[pie][apor]  * $lunes),2,'','.');
				$descuentos_ley[pie][rete] = redondear(($descuentos_ley[pie][rete]  * $lunes),2,'','.');
				$descuentos_ley[total][apor] = redondear(($descuentos_ley[total][apor] + $descuentos_ley[pie][apor]),2,'','.');
				$descuentos_ley[total][rete] = redondear(($descuentos_ley[total][rete] + $descuentos_ley[pie][rete]),2,'','.');
					echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">P.I.E.&nbsp;(Ret:'.($RTPIE * 100).'%)(Aport:'.($APPIE * 100).'%)(Salario Integral)</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[pie][rete],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[pie][apor],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear(($descuentos_ley[pie][rete]*-1),2,".",",").'</td>
				 	</tr>';
			}
			/// Caja Ahorro
			if ($res_per[cah_des]!='' && $res_per[cah_des]!='Seleccione...'){
				$RTCAH = $valores['RTCAH']/100;
				$APCAH = $valores['APCAH']/100;
				$sueldo_base_cah = redondear (($sueldo_base * $factor),2,'','.');
				$sueldo_base_cah = redondear (($sueldo_base_cah + $sueldob_addicional + $primas[comp_eval][periodo]),2,'','.');
				$descuentos_ley[cah][apor] = redondear(($sueldo_base_cah * $APCAH),2,'','.');
				$descuentos_ley[cah][rete] = redondear(($sueldo_base_cah * $APCAH),2,'','.');
				$descuentos_ley[cah][apor_por] = redondear(($APCAH * 100),2,'','.');
				$descuentos_ley[cah][rete_por] = redondear(($RTCAH * 100),2,'','.');
				$descuentos_ley[cah][cah_nom] = $res_per[cah_des];
				$descuentos_ley[total][apor] = redondear(($descuentos_ley[total][apor] + $descuentos_ley[cah][apor]),2,'','.');
				$descuentos_ley[total][rete] = redondear(($descuentos_ley[total][rete] + $descuentos_ley[cah][rete]),2,'','.');
					echo '<tr align="right">
			 			<td align="left" style="padding: 3px;">Caja de Ahorro ('.$descuentos_ley[cah][cah_nom].') (Ret:'.($RTCAH * 100).'%)(Aport:'.($APCAH * 100).'%)(Salario Integral)</td>
				 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[cah][rete],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear($descuentos_ley[cah][apor],2,".",",").'</td>
				 		<td style="padding: 3px; width: 80px;">'.redondear(($descuentos_ley[cah][rete]*-1),2,".",",").'</td>
				 	</tr>';
			}
	}
	return $descuentos_ley;
}
function calculo_inasistencias($ced_per,$sueldo_base,$cargo,$desde,$hasta){
	// buscamos cuantas inasistencias para el periodo
	$sql_ina = "SELECT *, COUNT(cod_ina) as dias FROM inasistencias WHERE ced_per = ".$ced_per." AND fch_ina >= '".$desde."' AND fch_ina<= '".$hasta."' AND tip_ina ='No Remunerada' GROUP BY ced_per,cod_car";
	$res_ina = mysql_fetch_array(mysql_query($sql_ina));
	$ndias = $res_ina[dias];
	$ndias;
	// calculamos sueldo diario
	if ($ndias > 0){
		$sueldo_diario = redondear(($sueldo_base / 30),2,'','.');
		$descuento = redondear(($sueldo_diario * $ndias),2,'','.');
		$inasistencias[dias] = $ndias;
		$inasistencias[monto] = $descuento;
		echo '<tr align="right">
	 			<td align="left" style="padding: 3px;">'.$inasistencias[dias].' D�as No Laborados</td>
		 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
		 		<td style="padding: 3px; width: 80px;">'.redondear($inasistencias[monto],2,".",",").'</td>
		 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
		 		<td style="padding: 3px; width: 80px;">'.redondear(($inasistencias[monto]*-1),2,".",",").'</td>
		 	</tr>';
		return $inasistencias;
	}
}
function calculo_beca($ced_per){
	$valores = consulta_valores();	
    $sql="SELECT * FROM familiares WHERE ced_per='".$ced_per."' AND par_fam='H' AND est_fam != '' AND beca_fam='S' ";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res))
    {
        if (edad($row["fnac_fam"])<=25 || $row[dis_fam]=='S'){
        	$estudia = $row["est_fam"];
        	switch ($estudia) {
        		case 'Preescolar':
        			$becas['pre']['cantidad'] += 1;
        			$becas['pre']['titulo'] = $becas['pre']['cantidad'].' Becas Preescolar';
        			$monto = $valores['BECAPRE'];
        			$becas['pre']['monto'] += $monto;
        			break;
        		case 'Primaria':
        			$becas['pri']['cantidad'] += 1;
        			$becas['pri']['titulo'] = $becas['pri']['cantidad'].' Becas Primaria';
        			$monto = $valores['BECAPRI'];
        			$becas['pri']['monto'] += $monto;
        			break;
        		case 'Secundaria':
        			$becas['sec']['cantidad'] += 1;
        			$becas['sec']['titulo'] = $becas['sec']['cantidad'].' Becas Secundaria';
        			$monto = $valores['BECASEC'];
        			$becas['sec']['monto'] += $monto;
        			break;
        		case 'Universitaria':
        			$becas['uni']['cantidad'] += 1;
        			$becas['uni']['titulo'] = $becas['uni']['cantidad'].' Becas Universitaria';
        			$monto = $valores['BECAUNI'];
        			$becas['uni']['monto'] += $monto;
        			break;
        		default:
        			$monto = 0;
        			break;
        	}
        	$becas[total] += $monto;
        }
    }
    foreach ($becas as $key => $value) {
 		if ($key!='total' && $value['cantidad']>0){
 			echo '<tr align="right">
	 			<td align="left" style="padding: 3px;">'.$value[titulo].'</td>
		 		<td style="padding: 3px; width: 80px;">'.redondear($value[monto],2,".",",").'</td>
		 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
		 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
				<td style="padding: 3px; width: 80px;">'.redondear($value[monto],2,".",",").'</td>
		 	</tr>';
 		}
 	}
    return $becas;
}
function calculo_juguetes($ced_per){
	$valores = consulta_valores();	
    $sql="SELECT * FROM familiares WHERE ced_per='".$ced_per."' AND par_fam='H'";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res))
    {
        if (edad($row["fnac_fam"])<=12 || $row[dis_fam]=='S'){
        	$num_hijos+=1;
			$juguetes['cantidad'] += 1;
			$juguetes['titulo'] = $juguetes['cantidad'].' Juguetes';
			$monto = $valores['JUGUETE'];
			$juguetes['monto'] += $monto;
        }
    }
    if ($juguetes[monto]>0){	
		echo '<tr align="right">
			<td align="left" style="padding: 3px;">'.$juguetes[titulo].'</td>
			<td style="padding: 3px; width: 80px;">'.redondear($juguetes[monto],2,".",",").'</td>
			<td style="padding: 3px; width: 80px;">&nbsp;</td>
			<td style="padding: 3px; width: 80px;">&nbsp;</td>
		<td style="padding: 3px; width: 80px;">'.redondear($juguetes[monto],2,".",",").'</td>
		</tr>';
    }
    return $juguetes;
}

function verificando_vacaciones($mes_nom,$ano_nom,$por_nom,$ced_per,$cod_car,$print){
	$paga_vac = 'NO';
	if ($por_nom==1){ $dia_desde = 1; $dia_hasta = 15; }
	if ($por_nom==2){ $dia_desde = 15; $dia_hasta = dias_mes($mes_ano,$ano_nom); }
	if ($por_nom==3){ $dia_desde = 1; $dia_hasta = dias_mes($mes_ano,$ano_nom); }
	$sql_vac = "SELECT * FROM vista_cargos_per WHERE MONTH(fch_vac) = ".$mes_nom." AND YEAR(fch_vac) < ".$ano_nom." AND DAY(fch_vac) >= ".$dia_desde." AND DAY(fch_vac) <= ".$dia_hasta;
	if ($cod_car AND $ced_per){
		$sql_vac .= " AND ced_per = ".$ced_per." AND cod_car = ".$cod_car;
	}
	$bus_vac = mysql_query($sql_vac);
	while ($reg = mysql_fetch_array($bus_vac)){
		$paga_vac = 'SI';
		if ($print=='SI'){
			echo "<tr><td class='lista_tablas'>".$reg['cod_dep']."-".$reg['num_car']."</td>
			<td class='lista_tablas'>".$reg['nom_car']."</td>
			<td class='lista_tablas'>".$reg['ced_per']."</td>
			<td class='lista_tablas'>".$reg['nom_per']."</td>
			<td class='lista_tablas'>".$reg['ape_per']."</td>
			<td class='lista_tablas' align='center'>".ordenar_fecha($reg['fch_asg'])."</td>
			<td class='lista_tablas' align='center'>".ordenar_fecha($reg['fch_vac'])."</td></tr>";
		}
	}
	return $paga_vac;
}

?>