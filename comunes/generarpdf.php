<?php
    include('../dompdf/dompdf_config.inc.php');
    function generar_pdf($contenido,$nom_pdf_out,$papel,$orientacion,$pagina,$x,$y)
    {
        $dompdf =  new DOMPDF();
        $dompdf -> set_paper($papel,$orientacion);
        $dompdf -> load_html($contenido);
        $dompdf -> render();
        if ($pagina){
            $canvas = $dompdf->get_canvas(); 
            $font = Font_Metrics::get_font("helvetica", ""); 
            $canvas->page_text($x, $y, "Página: {PAGE_NUM} de {PAGE_COUNT}",$font, 7, array(0,0,0)); 
        }
        $dompdf->stream($nom_pdf_out); 
    }
    //generar_pdf('texto','texto.pdf','letter','portrait');
?>
