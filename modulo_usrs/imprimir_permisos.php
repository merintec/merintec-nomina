<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>

<?php $idprint=$_GET['cod_sol_perm']; 
	//consultamos los datos de la permisos
    $result=mysql_query("select * from permisos_per WHERE cod_sol_perm='$idprint'");
	if ($row=mysql_fetch_array($result))
	{
		$existe = 'SI';
		$id = $row["cod_sol_perm"];
		$fecha1 = substr($row["fch_sol_perm"], 8, 2);
		$fecha2 = substr($row["fch_sol_perm"], 5, 2);
		$fecha3 = substr($row["fch_sol_perm"], 0, 4);
		$fecha3b = substr($row["fch_sol_perm"], 2, 2);
		$fecha = $fecha1.' / '.$fecha2.' / '.$fecha3;
		$ced_per = $row["ced_per"];
		$nom_per = $row["nom_per"];
		$nom_dep = $row["nom_dep"];
		$nom_car = $row["nom_car"];
		
    	$dias_sol_perm = $row["dias_sol_perm"];
    	$mot_sol_perm = $row["mot_sol_perm"];
    	
		$ini_sol_perm1 = substr($row["ini_sol_perm"], 8, 2);
		$ini_sol_perm2 = substr($row["ini_sol_perm"], 5, 2);
		$ini_sol_perm3 = substr($row["ini_sol_perm"], 0, 4);
		$ini_sol_perm = $ini_sol_perm1.' / '.$ini_sol_perm2.' / '.$ini_sol_perm3;
		
		$fin_sol_perm1 = substr($row["fin_sol_perm"], 8, 2);
		$fin_sol_perm2 = substr($row["fin_sol_perm"], 5, 2);
		$fin_sol_perm3 = substr($row["fin_sol_perm"], 0, 4);
		$fin_sol_perm = $fin_sol_perm1.' / '.$fin_sol_perm2.' / '.$fin_sol_perm3;

		$tip_sol_perm = $row["tip_sol_perm"];

		$peri_sol_perm1 = substr($row["peri_sol_perm"], 8, 2);
		$peri_sol_perm2 = substr($row["peri_sol_perm"], 5, 2);
		$peri_sol_perm3 = substr($row["peri_sol_perm"], 0, 4);
		$peri_sol_perm = $peri_sol_perm1.' / '.$peri_sol_perm2.' / '.$peri_sol_perm3;

		$perf_sol_perm1 = substr($row["perf_sol_perm"], 8, 2);
		$perf_sol_perm2 = substr($row["perf_sol_perm"], 5, 2);
		$perf_sol_perm3 = substr($row["perf_sol_perm"], 0, 4);
		$perf_sol_perm = $perf_sol_perm1.' / '.$perf_sol_perm2.' / '.$perf_sol_perm3;

		$obs_sol_perm = $row["obs_sol_perm"];
		$apro_sol_perm = $row["apro_sol_perm"];
	}
	mysql_free_result($result);
?>

<title>Impresion de Solicitud de Permiso</title>
<div><?php include ('../comunes/pagina_encabezado.php'); ?></div>
<div><h1>Solicitud de Permiso</h1></div>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" >
    <tr>
        <td>
            &nbsp;
        </td>
        <td width="50%" align="right">
            &nbsp;<b><font size="3px">Fecha de Elaboraci�n:</b> <?php echo $fecha; ?>&nbsp;&nbsp;</font>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;<b><font size="3px">Dependencia:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nom_dep; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">Cargo:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nom_car; ?></font>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;<b><font size="3px">Nombres y Apellidos:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nom_per; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">C�dula de Identidad:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo redondear($ced_per,0,".",","); ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" >
    <tr>
        <td width="50%">
            &nbsp;<b><font size="3px">Tipo de permisos:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($tip_sol_perm=='N') { echo 'No Remunerado'; }?><?php if ($tip_sol_perm=='R') { echo 'Remunerado'; }?></font>
        </td>
        <td width="50%">
            &nbsp;<b><font size="3px">Motivo del permisos:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $mot_sol_perm; ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" >
    <tr>
        <td width="50%">
            &nbsp;<b><font size="3px">Fecha de Inicio del Permiso:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $ini_sol_perm; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">Fecha Final del Permiso:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $fin_sol_perm; ?></font>
        </td>       
        <td>
            &nbsp;<b><font size="3px">D�as:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dias_sol_perm.' d�as'; ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" >
    <tr>
        <td width="50%" height="70" valign="top">
            &nbsp;<b><font size="3px">Observaciones:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $obs_sol_perm; ?></font>
        </td>
    </tr>
</table>
<?php echo $msg_pie_reporte; ?>
<table width="95%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td width="50%">
           <br><br><br>
           
           <center><hr width="70%"><?php echo $nom_per; ?><br>SOLICITANTE</center>
        </td>      
        <td width="50%">
           <br><br><br>
           
           <center><hr width="70%"><br>DIRECTOR</center>
        </td>
    </tr>
</table>
<br>
<div><input type="button" name="bt_print" value="Imprimir Solicitud" id="bt_print" onclick="this.style.visibility='hidden'; window.print();"></div>
