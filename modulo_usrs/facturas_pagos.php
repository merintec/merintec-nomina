<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Facturas Afectadas</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
include ('../comunes/formularios_funciones.php');
$viene_val = $_GET['cod_pag'];
//// traer los datos de pago
    $sql = "SELECT * FROM pagos WHERE cod_pag=".$viene_val;
    $res = mysql_fetch_array(mysql_query($sql));
    $mon_pag = $res['mon_pag'];
////
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'facturas_pagos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"].'&cod_pag='.$_GET["cod_pag"];
$pagina2 = 'facturas_pagos.php?cod_pag='.$_GET["cod_pag"].'&seccion='.$_GET["seccion"];
$tabla = "facturas_pagos";	// nombre de la tabla
$ncampos = "8";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_fac_pag","Cod. factura  afectada",$_POST['cod_fac_pag'],"0","11","numericos");
$datos[1] = crear_datos ("cod_pag","Cod. del Pago",$_POST['cod_pag'],"1","11","numericos");
$datos[2] = crear_datos ("fch_fac_pag","Fecha de la Factura",$_POST['fch_fac_pag'],"10","10","fecha");
$datos[3] = crear_datos ("num_fac_pag","Num. de Factura",$_POST['num_fac_pag'],"1","15","alfanumericos");
$datos[4] = crear_datos ("con_fac_pag","Num. de Control de Factura",$_POST['con_fac_pag'],"1","15","alfanumericos");
$datos[5] = crear_datos ("mon_fac_pag","Monto de la Factura",$_POST['mon_fac_pag'],"1","12","decimal");
$datos[6] = crear_datos ("iva_fac_pag","Base imponible IVA en la factura",$_POST['iva_fac_pag'],"1","12","decimal");
$datos[7] = crear_datos ("isrl_fac_pag","Base imponible ISRL en la factura",$_POST['isrl_fac_pag'],"1","12","decimal");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_fac_pag = $row["cod_fac_pag"];
	    $cod_pag = $row["cod_pag"];
	    $fch_fac_pag = $row["fch_fac_pag"];
	    $num_fac_pag = $row["num_fac_pag"];
	    $con_fac_pag = $row["con_fac_pag"];
	    $mon_fac_pag = $row["mon_fac_pag"];
	    $iva_fac_pag = $row["iva_fac_pag"];
	    $isrl_fac_pag = $row["isrl_fac_pag"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	//// se verifica que el monto total de las facturas no sea mayor al monto de este pago
	if ($validacion) { 
	    if (($_POST['mon_fac_pag']!=$_POST['mon_ant']) || ($_POST['iva_fac_pag']!=$_POST['iva_ant']))
	    {
	        $sql = "select SUM(mon_fac_pag) as monto_total, SUM(iva_fac_pag) as iva_total from facturas_pagos where cod_pag='".$cod_pag."'";
    	    $res = mysql_fetch_array (mysql_query ($sql));
    	    $monto_total = $res['monto_total'];
    	    $iva_total = $res['iva_total'];
    	    $mon_act = $_POST['mon_ant'];
    	    //return;
    	    $facturado = ($monto_total) - $mon_act;
    	    $disponibles = $mon_pag - $facturado;
        	if (($_POST['mon_fac_pag']) > $disponibles){
    	        echo '<SCRIPT> alert ("'.$msg_pago_fac_mayor.'"); </SCRIPT>';
    	        $validacion = '';
    	    }
    	    else { $validacion = 1; }
	    }
	}
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_fac_pag",$_POST["cod_fac_pag"],$pagina2);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	//// se verifica que el monto total de las facturas no sea mayor al monto de este pago
	if ($validacion) { 
		$sql = "select SUM(mon_fac_pag) as monto_total, SUM(iva_fac_pag) as iva_total from facturas_pagos where cod_pag='".$cod_pag."'";
    	$res = mysql_fetch_array (mysql_query ($sql));
    	$monto_total = $res['monto_total'];
    	$iva_total = $res['iva_total'];
    	$facturado = $monto_total;
    	$disponibles = $mon_pag - $facturado;
    	if (($_POST['mon_fac_pag']) > $disponibles){
    	    echo '<SCRIPT> alert ("'.$msg_pago_fac_mayor.'"); </SCRIPT>';
    	    $validacion = '';
    	}
    	else { $validacion = 1; }
	}
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_fac_pag"],"cod_fac_pag",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_fac_pag","facturas_pagos",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Facturas Afectadas</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha:
                          </td>
		            <td><input name="cod_fac_pag" type="hidden" id="cod_fac_pag" value="<?php if (! $existe) { echo $_POST['cod_fac_pag']; } else { echo $cod_fac_pag; } ?>" title="Codigo de salida presupuestaria">
			<input name="cod_pag" type="hidden" id="cod_pag" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_pag; } ?>" title="Codigo del pago">
                        <?php escribir_campo('fch_fac_pag',$_POST["fch_fac_pag"],$fch_fac_pag,'',11,11,'Fecha de la Factura',$boton,$existe,'fecha')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� de Factura:</td>
                        <td>
                            <?php escribir_campo('num_fac_pag',$_POST["num_fac_pag"],$num_fac_pag,'',15,15,'N� de la Factura',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Control de Factura:</td>
                        <td>
                            <?php escribir_campo('con_fac_pag',$_POST["con_fac_pag"],$con_fac_pag,'',15,15,'N� de Control de la Factura',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto de Factura:</td>
                        <td>
                            <?php escribir_campo('mon_fac_pag',$_POST["mon_fac_pag"],$mon_fac_pag,'',12,15,'Monto total de la Factura',$boton,$existe,'')?>
                            <input type="hidden" name="mon_ant" id="mon_ant" value="<?php if ($existe) { echo $mon_fac_pag;} else { echo $_POST["mon_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Base del IVA:</td>
                        <td>
                            <?php escribir_campo('iva_fac_pag',$_POST["iva_fac_pag"],$iva_fac_pag,'',12,15,'Monto correspondiente a la base imponible del IVA en la factura',$boton,$existe,'')?>
                            <input type="hidden" name="iva_ant" id="iva_ant" value="<?php if ($existe) { echo $iva_fac_pag;} else { echo $_POST["iva_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Base del ISRL:</td>
                        <td>
                            <?php escribir_campo('isrl_fac_pag',$_POST["isrl_fac_pag"],$isrl_fac_pag,'',12,15,'Monto correspondiente a la base imponible del ISRL para esta factura',$boton,$existe,'')?>
                            <input type="hidden" name="isrl_ant" id="isrl_ant" value="<?php if ($existe) { echo $isrl_fac_pag;} else { echo $_POST["isrl_ant"]; }?>">
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_pagos_facturas.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
