<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Movimientos Bancarios</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_ban'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'bancos_movimientos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"].'&cod_ban='.$_GET["cod_ban"];
$tabla = "banco_movimientos";	// nombre de la tabla
$ncampos = "8";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_ban_mov","Cod. de movimiento bancario ",$_POST['cod_ban_mov'],"0","11","numericos");
$datos[1] = crear_datos ("fch_ban_mov","Fecha del Movimiento",$_POST['fch_ban_mov'],"10","10","fecha");
$datos[2] = crear_datos ("tip_ban_mov","Tipo de Movimiento",$_POST['tip_ban_mov'],"1","10","alfabeticos");
$datos[3] = crear_datos ("mon_ban_mov","Monto del Movimiento",$_POST['mon_ban_mov'],"1","12","decimal");
$datos[4] = crear_datos ("des_ban_mov","Descripci�n del Movimiento",$_POST['des_ban_mov'],"1","255","alfanumericos");
$datos[5] = crear_datos ("ref_ban_mov","Referencia",$_POST['ref_ban_mov'],"1","50","alfanumericos");
$datos[6] = crear_datos ("obs_ban_mov","Observaciones",$_POST['obs_ban_mov'],"0","255","alfanumericos");
$datos[7] = crear_datos ("cod_ban","Banco",$_POST['cod_ban'],"1","11","numericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ban_mov = $row["cod_ban_mov"];
	    $fch_ban_mov = $row["fch_ban_mov"];
	    $tip_ban_mov = $row["tip_ban_mov"];
	    $mon_ban_mov = $row["mon_ban_mov"];
	    $des_ban_mov = $row["des_ban_mov"];
	    $ref_ban_mov = $row["ref_ban_mov"];
	    $obs_ban_mov = $row["obs_ban_mov"]; 
	    $cod_ban = $row["cod_ban"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ban_mov",$_POST["cod_ban_mov"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ban_mov"],"cod_ban_mov",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_ban_mov",$tabla,$pagina);
	return;
}
/// consultamos los datos del Banco
$sql_ban = "select * from banco where cod_ban =".$viene_val;
$bus_ban = mysql_query($sql_ban);
$res_ban = mysql_fetch_array($bus_ban);
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Movimientos Bancarios</td>
                  </tr>
                  <tr>
							<td>
								<table width="100%">
                  			<tr align="center">
                  				<td class="etiquetas">Banco</td>               		
                  				<td class="etiquetas">Cuenta</td>		
                  				<td class="etiquetas">Prop�sito</td>
                  			</tr>
                  			<tr align="center">
                  				<td class="etiquetas"><?php echo $res_ban['nom_ban']; ?></td>               		
                  				<td class="etiquetas"><?php echo $res_ban['cue_ban']; ?></td>		
                  				<td class="etiquetas"><?php echo $res_ban['des_ban']; ?></td>
                  			</tr>
                  		</table>
							</td>                  
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Fecha:</td>
                        <td>
									<input name="cod_ban_mov" type="hidden" id="cod_ban_mov" value="<?php if(! $existe) { echo $_POST["cod_ban_mov"]; } else { echo $cod_ban_mov; } ?>" title="Codigo del Movimiento bancario">
									<input name="cod_ban" type="hidden" id="cod_ban" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_ban; } ?>" title="Codigo de Banco">
                        	<?php escribir_campo('fch_ban_mov',$_POST["fch_ban_mov"],$fch_ban_mov,'',10,10,'Fecha del Movimiento Bancario',$boton,$existe,'fecha'); ?>
								</td>
                      </tr>
                      <tr>
                      	<td class="etiquetas">Tipo Movimiento:</td>
                        <td><?php if ($boton != "Modificar" && $boton != "Guardar") { echo '<select name="tip_ban_mov" title="Tipo de Movimiento">
                          <option>Seleccione...</option>
                          <option value="Cr�dito" '; if ($tip_ban_mov == "Cr�dito" || $_POST['tip_ban_mov'] =="Cr�dito") { echo 'selected'; } echo '>Cr�dito</option>
                          <option value="D�bito" '; if ($tip_ban_mov == "D�bito" || $_POST['tip_ban_mov'] =="D�bito") { echo 'selected'; } echo '>D�bito</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_ban_mov" id="tip_ban_mov" value="'.$tip_ban_mov.'" >'; 
						    if ($tip_ban_mov == "Cr�dito") { echo 'Cr�dito'; } 
							if ($tip_ban_mov == "D�bito") { echo 'D�bito'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Monto:</td>
                        <td width="75%">
                        	<?php escribir_campo('mon_ban_mov',$_POST["mon_ban_mov"],$mon_ban_mov,'',12,12,'Monto del Movimiento Bancario',$boton,$existe,''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Referencia:</td>
                        <td width="75%">
                        	<?php escribir_campo('ref_ban_mov',$_POST["ref_ban_mov"],$ref_ban_mov,'',50,12,'Referencia del Movimiento Bancario',$boton,$existe,''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Descripci�n:</td>
                        <td width="75%">
                        	<?php escribir_campo('des_ban_mov',$_POST["des_ban_mov"],$des_ban_mov,'',255,40,'Descripci�n del Movimiento Bancario',$boton,$existe,''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Observaciones:</td>
                        <td width="75%">
                        	<?php escribir_campo('obs_ban_mov',$_POST["obs_ban_mov"],$obs_ban_mov,'',255,40,'Observaciones del Movimiento Bancario',$boton,$existe,''); ?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_bancos_mov.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
