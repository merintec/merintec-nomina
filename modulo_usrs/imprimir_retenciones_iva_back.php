<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">

<?php $idprint=$_GET['cod_ret_iva']; 
	//consultamos retenciones iva
	$suma_pagos = 0;
    $result=mysql_query("select * from retenciones_iva ret WHERE cod_ret_iva=".$idprint);
	if ($row=mysql_fetch_array($result))
	{
		$existe = 'SI';
	    $cod_ret_iva = $row["cod_ret_iva"];
	    $fch_ret_iva = $row["fch_ret_iva"];
	    $com_ret_iva = $row["com_ret_iva"];
	    $rif_pro = $row["rif_pro"];
	    $apl_ret_iva = $row["apl_ret_iva"];
	    $ret_ret_iva = $row["ret_ret_iva"];
	    $egr_ret_iva = $row["egr_ret_iva"];
	    $obs_ret_iva = $row["obs_ret_iva"];
		$fecha1 = substr($row["fch_ret_iva"], 8, 2);
		$fecha2 = substr($row["fch_ret_iva"], 5, 2);
		$fecha3 = substr($row["fch_ret_iva"], 0, 4);
	}
	mysql_free_result($result);
?>

<title>Impresi�n de Comprobante de Retencion de IVA: <?php  $comprobante = substr($fch_ret_iva, 0, 4).'-'.substr($fch_ret_iva, 5, 2).'-'.$com_ret_iva; echo $comprobante?></title>
<div class="rot90">
<div><?php include ('../comunes/pagina_encabezado_ret.php'); ?></div>
<table cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="0" bordercolor="#000000" align="center" class="constancias">
    <tr height="1%">
        <td>
            <div align="right"><?php echo 'Ejido, '.$fecha1.' de '.convertir_mes($fecha2).' de '.redondear($fecha3,0,".",""); ?>&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    <tr height="1%">
        <td>
            <div style="line-height:15px;">Se�or(es):<br><?php echo $des_con; ?>.<br><br></div>
        </td>
    </tr>
    <tr height="1%">
        <td>
            <div align="center"><h2>CONSTANCIA DE TRABAJO</h2></div>
        </td>
    </tr>
    <tr height="1%">
        <td>
            <div>Quien suscribe, <b><?php echo $CONTRALOR; ?></b>, titular de la c�dula de Identidad N� <b><?php echo $CONTRALOR_CI; ?></b>, Contralor Municipal del Municipio Campo El�as del Estado M�rida, mediante la presente, <b>HACE CONSTAR</b>: que
            <?php if ($sex_per == "F"){ echo ' la ciudadana, '; } ?>
            <?php if ($sex_per == "M"){ echo ' el ciudadano, '; } ?>           
            <b><?php echo $nom_per.' '.$ape_per; ?></b>, titular de la c�dula de identidad N� <b><?php echo $nac_per.'-'.redondear($ced_per,0,".",""); ?></b>, de profesi�n <b><?php echo $tit_edu; ?></b>, trabaja en este �rgano de Control Fiscal Externo desde el <b><?php echo $fecha_ing1.' de '.convertir_mes($fecha_ing2).' de '.redondear($fecha_ing3,0,".",""); ?></b>,
            hasta la presente fecha, desempe�ando actualmente el cargo de <b><?php echo $nom_car; ?></b>.<br>
            <?php 
            if ($sue_con || $prm_con || $cst_con) { ?>
            <br>
            <table cellspacing="0" cellpadding="0" width="80%" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center" >
                <tr class="etiquetas_nomina">
                    <td align="center" colspan="2">Detalles de las Remuneraciones que Percibe</td></tr>
                    <?php if ($sue_con) { $suma_pagos += $sue_mon; ?>
                        <tr><td class="etiquetas_nomina">Sueldo Mensual (Bs):</td><td align="right" class="etiquetas_nomina2"><?php echo redondear($sue_mon,2,".",","); ?>&nbsp;</td></tr>
                    <?php } ?>
                    <?php if ($prm_con) { 
								$suma_pagos += $prm_mon;
                    ?>
                        <tr><td class="etiquetas_nomina">Total Primas (Bs):</td><td align="right" class="etiquetas_nomina2"><?php echo redondear($prm_mon,2,".",","); ?>&nbsp;</td></tr>
                    <?php } ?>
                    <?php if ($cst_con) { $suma_pagos += $cst_mon; ?>
                    		
                        <tr><td class="etiquetas_nomina">Cesta Ticket (Bs):</td><td align="right" class="etiquetas_nomina2"><?php echo redondear($cst_mon,2,".",","); ?>&nbsp;</td></tr>
                    <?php } ?>
                    <tr class="etiquetas_nomina">
                        <td align="right">Total de Pagos Mensuales (Bs.)&nbsp;&nbsp;</td><td align="right"><?php echo redondear($suma_pagos,2,".",","); ?>&nbsp;</td></tr>
            </table>
            <?php } ?>

            <br>Constancia que se expide a petici�n de parte interesada a los <b><?php echo $fecha1.' d�as del mes de '.convertir_mes($fecha2).' de '.redondear($fecha3,0,".",""); ?></b>. Para efectos de <b><?php echo $mot_con; ?></b>.
            
            </div>
        </td>
    </tr>
    <tr height="1%">
        <td>
           <br><br><br>
           
           <center><hr width="300px"><?php echo $CONTRALOR; ?><BR>CONTRALOR MUNICIPAL</center>
        </td>
    </tr>
    <tr valign="bottom"><td><font size="1px"><b><u>NOTA:</u></b> La presente va sin tachaduras ni enmendaduras, debe contener firma y sello h�medo.<br>&nbsp;</font></td></tr>
</table>
<?php echo $msg_pie_carta; ?>
</div>
<div><input type="button" name="bt_print" value="Imprimir Solicitud" id="bt_print" onclick="this.style.visibility='hidden'; window.print();"></div>
