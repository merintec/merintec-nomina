<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">

<?php 
    $idprint=$_GET['fch_con'];
    $diasmes[0] = "Dom�ngo";
    $diasmes[1] = "Lunes";
    $diasmes[2] = "Martes";
    $diasmes[3] = "Mi�rcoles";
    $diasmes[4] = "Jueves";
    $diasmes[5] = "Viernes";
    $diasmes[6] = "S�bado";
?>

<title>Impresion de Reporte de Correspondencia Recibida</title>
<div><?php include ('../comunes/pagina_encabezado.php'); ?></div>
<?php if ($_POST['desde']=='' && $_POST['hasta']=='' && !$_POST['buscar_nueva']) {?>
<div align="Center"><h3><font face="arial">RELACI�N DE CORRESPONDENCIA RECIBIDA EL D�A <u>&nbsp;<?php echo $diasmes[date('w',strtotime($idprint))]; ?>&nbsp;</u><BR>FECHA: <u>&nbsp;<?php echo date('d / m / Y',strtotime($idprint)); ?>&nbsp;</u></font></h3></div>
<?php } ?>
<?php if ($_POST['buscar_nueva']=='Buscar') {
$desde = $_POST['desde']; 
$hasta=$_POST['hasta'];
$buscar_mul = 'ok';
if ($_POST['desde']=='' || $_POST['hasta']==''){ echo "<script>alert('Debe Indicar Fecha Desde y Fecha Hasta');</script>"; $desde = $idprint; $hasta=$idprint; $buscar_mul = ''; }
?>
<div align="Center"><h3><font face="arial">RELACI�N DE CORRESPONDENCIA RECIBIDA <br>DESDE: <u>&nbsp;<?php echo $diasmes[date('w',strtotime($desde))]; ?>, <?php echo date('d / m / Y',strtotime($desde)); ?>&nbsp;</u> Hasta: <u>&nbsp;<?php echo $diasmes[date('w',strtotime($hasta))]; ?>, <?php echo date('d / m / Y',strtotime($hasta)); ?>&nbsp;</u> </font></h3></div>
<?php } ?>

<div id="busqueda_nueva" align="Center">
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <form id="form1" name="form1" method="post" action="">
            <tr class="corres_etiqueta">
                <td>Rango de Busqueda:</td><td>&nbsp;Desde:</td><td><input type="text" size="6" name="desde" value="<?php echo $_POST['desde']; ?>"><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=displayCalendar(document.forms[0].desde,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>&nbsp;&nbsp;</td><td> Hasta: </td><td><input type="text" size="6" name="hasta" value="<?php echo $_POST['hasta']; ?>"><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=displayCalendar(document.forms[0].hasta,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>&nbsp;&nbsp;</td><td><input type="submit" value="Buscar" name="buscar_nueva" ></td>
            <tr>
        </form>
    </table>
</div>
<table width="80%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=0 height="650px" bordercolor="#000000" align="center">
    <tr valign="top">
        <td>
            <?php include('capa_oficios_recibidos.php'); ?>
        </td>
    </tr>
    <tr valign="top">
        <td align="left">
            <b><font face="arial">Elaborado Por:</b> Abog. Gloriana Araque Torres</font>
        </td>
    </tr>
</table>
<table width="80%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=0 bordercolor="#000000" align="center">
    <tr valign="top">
        <td>
            <?php echo $msg_pie_carta; ?>
        </td>
    </tr>
</table>
<br>
<div><input type="button" name="bt_print" value="Imprimir Reporte" id="bt_print" onclick="this.style.visibility='hidden'; busqueda_nueva.style.display='none'; window.print();"></div>
