<?php 
/// Consulta de Asistencia en el sistema BioStar
$servidor_biostar="true"; 
include('../comunes/conexion_basedatos.php');
$prm = llamar_permisos ($_GET["seccion"]);
$fch_inicio = $_GET['ano']."-".$_GET['mes']."-".$ini_rep." 06:00:00";
$fch_inicio = strtotime($fch_inicio);
// para la generación de la consulta restamos 16200 para hacer coincidir la fecha y hora con el sistema biostar
$fch_inicio_sql = $fch_inicio - 16200;
$fch_inicio += 16200;
$fch_fin = $_GET['ano']."-".$_GET['mes']."-".$fin_rep." 23:59:59";
$fch_fin = strtotime($fch_fin);
// para la generación de la consulta restamos 16200 para hacer coincidir la fecha y hora con el sistema biostar
$fch_fin_sql = $fch_fin - 16200;
$fch_fin += 16200;
$entrada = "";
$salida = "";
/*
$sql_asis = "select * from tb_event_log where nUserID >1 AND nUserID != 906078400 AND nDateTime >= $fch_inicio_sql AND nDateTime <= $fch_fin_sql Order by nUserID,nDateTime";
$res_asis = mysql_query ($sql_asis);
while ($row_asis=@mysql_fetch_array($res_asis))
{
    $tiempo = fch_unix_a_fecha($row_asis['nDateTime'],'Y');
    $fecha = $tiempo[0];
    $hora = $tiempo[1];
    $cedula = $row_asis['nUserID'];
    $hor_reg = strtotime($hora);
    echo $fecha.' - '.$hora.' - '.$cedula.'<br>';
}*/
$sql = "select * from tb_event_log where nUserID >1 AND nUserID != 906078400 AND nDateTime >= $fch_inicio_sql AND nDateTime <= $fch_fin_sql";
$res = mysql_query ($sql);
while ($row=@mysql_fetch_array($res))
{   
    $cedula = '';
    $tiempo = fch_unix_a_fecha($row['nDateTime'],'Y');
    $fecha = $tiempo[0];
    $hora = $tiempo[1];
    $cedula = $row['nUserID'];
    $hor_reg = strtotime($hora);
    
    /// determina que día de la semana es y trae el horario correspondiente y le suma la tolerancia a la hora de entrada
    $fch_dia = strtotime($fecha);  
    $fch_dia = date("w",$fch_dia);
    $hor_ent = $hor_per[$cedula][(0+$fch_dia)];
    $hor_ent = strtotime($hor_ent);
    $hor_sal = $hor_per[$cedula][(5+$fch_dia)];
    $hor_sal = strtotime($hor_sal);
    
    $asistencias[$cedula][$fecha][0] = $cedula;
    $asistencias[$cedula][$fecha][1] = $fecha;
    if ($asistencias[$cedula][$fecha][2]=="") { 
        $asistencias[$cedula][$fecha][2] = $hora;
        $entrada = 'fuera_tiempo';
        if ($hor_reg <= $hor_ent) { $entrada = 'correcto'; }
        $asistencias[$cedula][$fecha][4] = $entrada;
    }
    if ($asistencias[$cedula][$fecha][2]!=$hora) { 
        $asistencias[$cedula][$fecha][3] = $hora;
        $salida = 'fuera_tiempo';
        if ($hor_reg >= $hor_sal) { $salida = 'correcto'; }
        $asistencias[$cedula][$fecha][5] = $salida;
    }
}
?>
