<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php include ('../comunes/formularios_funciones.php'); ?>
<?php include ('../comunes/funciones_nomina.php'); ?>
<?php $procesar_val='NO'; ?>
<!DOCTYPE html>
<html>
<head>
	<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
	<title>N�mina <?php echo escribir_mes($_GET['mes_nom']).'/'.$_GET['ano_nom'].' - '; if ($_GET['por_nom']==1) { echo '1era quincena';} if ($_GET['por_nom']==2) { echo '2da quincena';} if ($_GET['por_nom']==3) { echo 'Mes';} ?></title>
</head>
<body>
<?php
	// inicializamos la variable bandera de error
	$error = '';
	// Para activar depuraci�n de codigo $debug = 1 sino $debug = ''
	$debug = '';
	// verificamos que se encuentre logueado
	if (! $_COOKIE[usnombre]) {
		echo '<b><center>'.$msg_usr_noidentificado.'</center></b>';
		echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; 
		exit; 
	}
	// obtenemos la variable prenomina para verificar si es prenomina o n�mina
	$prenomina=$_GET['prenomina'];
	if ($prenomina==1){ $
		$add_titulo = " (PRE-NOMINA)";
		$tabla_ppal = 'nominapre_pagar';
		$tabla_asig = 'nominapre_asign';
		$tabla_dedu = 'nominapre_deduc';   
	}
	else{
		$add_titulo = "";
		$tabla_ppal = 'nomina_pagar';
		$tabla_asig = 'nomina_asign';
		$tabla_dedu = 'nomina_deduc';   
	}
	// se consulta si la pren�mina ya fue procesada como n�mina
	$sql="select * from vista_nominas_pagadas where ano_nom=".$_GET['ano_nom']." AND mes_nom=".$_GET['mes_nom']." AND por_nom=".$_GET['por_nom']; 
	if ($_GET['por_nom']==3) { $sql="select * from vista_nominas_pagadas where ano_nom=".$_GET['ano_nom']." AND mes_nom=".$_GET['mes_nom']; }
	$busq=mysql_query($sql);
	if ($debug) { echo '<br>SQL: '.$sql.'<br>Error:'.mysql_error(); }
	if(mysql_fetch_array($busq)){
		$procesar_val='SI';
		echo '<SCRIPT>alert("'.$msg_nomina_ya_pagada.'");</SCRIPT>';
		echo "<SCRIPT>window.close();</SCRIPT>";			
	}
	// se se va a procesar la pre-nomina limpiar las tablas de prenomina
	if ($_POST['procesar_val']) {
		mysql_query("SET AUTOCOMMIT=0");
		mysql_query("START TRANSACTION");
		$sql_vaciar = "TRUNCATE TABLE nominapre_deduc"; 
		mysql_query($sql_vaciar); 
		if ($debug) { echo '<br>SQL: '.$sql_vaciar.'<br>Error:'.mysql_error(); }
		$sql_vaciar = "TRUNCATE TABLE nominapre_asign"; 
		mysql_query($sql_vaciar); 
		if ($debug) { echo '<br>SQL: '.$sql_vaciar.'<br>Error:'.mysql_error(); }
		$sql_vaciar = "TRUNCATE TABLE nominapre_pagar"; 
		mysql_query($sql_vaciar); 		
		if ($debug) { echo '<br>SQL: '.$sql_vaciar.'<br>Error:'.mysql_error(); }
	}
	// verificamos el tipo de nomina para determinar el factor  
	if ($_GET['por_nom']==1 || $_GET['por_nom']==2) { $factor = 0.5; }
	if ($_GET['por_nom']==3) { $factor = 1; }
	// inicializar montos totales de la nomina
	$total_ded_nomina=0;
	$total_asig_nomina=0;
	$total_aport_nomina=0;
	$total_pagar_nomina=0;

	/// definir fechas tope para pago de nomina
	$fecha_tope = $_GET['ano_nom']."-".$_GET['mes_nom']."-1";
	if ($_GET['por_nom']==1){
		$fecha_desde_salida = $_GET[ano_nom].'-'.$_GET[mes_nom].'-01';
		$fecha_hasta_salida = $_GET[ano_nom].'-'.$_GET[mes_nom].'-15';
	}
	if ($_GET['por_nom']==2){
		$fecha_desde_salida = $_GET[ano_nom].'-'.$_GET[mes_nom].'-16';
		$fecha_hasta_salida = $_GET[ano_nom].'-'.$_GET[mes_nom].'-'.dias_mes(($_GET['mes_nom']),$_GET['ano_nom']);
	}
	if ($_GET['por_nom']==3){
		$fecha_desde_salida = $_GET[ano_nom].'-'.$_GET[mes_nom].'-01';
		$fecha_hasta_salida = $_GET[ano_nom].'-'.$_GET[mes_nom].'-'.dias_mes(($_GET['mes_nom']),$_GET['ano_nom']);
	}
?>
	<!-- cabecera de la pantalla -->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	  	<tr>
		    <td width="22%"><img src="../imagenes/logo_tn_trn.png" height="80px"></td>
		    <td width="57%" align="center" class="titulo_nomina"><?php echo $titulo_paginas;  ?></td>
		    <td width="21%">
		    	<?php include('../comunes/fecha.php'); 
		    	echo '<br>'; $hora = time(); 
		    	echo '<b>Hora:</b> '.date ("h:i:s A",$hora);?>
		    </td>
	  	</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center" class="etiquetas_nomina">Sistema de N&oacute;mina<?php echo $add_titulo; ?></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="etiquetas_nomina2">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="etiquetas_nomina2">NOMINA DETALLADA DE EMPLEADOS</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="etiquetas_nomina2">CORRESPONDIENTE A: <span class="etiquetas_nomina"> 
			<?php if ($_GET['por_nom']==1) { echo '1era quincena - ';} if ($_GET['por_nom']==2) { echo '2da quincena - ';}
			  echo escribir_mes($_GET['mes_nom'])." - ".$_GET['ano_nom']; ?>
			</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
	</table>
	<!-- Inicio de la nomina -->
	<table width="99%" border="0" cellpadding="0" cellspacing="0" align="center" class="detallespago">
	<?php 
		// Consultamos las dependencias
		$sql_dep = "SELECT * FROM dependencias ORDER BY nom_dep";
		$bus_dep = mysql_query($sql_dep);
		if ($debug) { echo '<br>SQL: '.$sql_dep.'<br>Error:'.mysql_error(); }
		while($res_dep = mysql_fetch_array($bus_dep)){
			echo '<tr><td><hr></td></tr>';
			echo '<tr><td>DEPENDENCIA: <b>'.$res_dep[nom_dep].'</b><br><br></td></tr>';
			// Consultamos los cargos
			$sql_cargos = "SELECT * FROM cargos c, tipos_cargos tc WHERE c.cod_dep = '".$res_dep[cod_dep]."' AND c.cod_tcar=tc.cod_tcar AND c.fch_asg <= '".$fecha_tope."'  ORDER BY tc.nom_tcar ASC, c.nom_car ASC ";
			$sql_cargos = "SELECT *, 'sin movimiento' as condicion FROM cargos c, tipos_cargos tc WHERE c.cod_dep = '".$res_dep[cod_dep]."' AND c.cod_tcar=tc.cod_tcar AND c.fch_asg <= '".$fecha_tope."'  UNION ";
			$sql_cargos .= "SELECT *, 'con movimiento' as condicion FROM cargos c, tipos_cargos tc WHERE c.cod_dep = '".$res_dep[cod_dep]."' AND c.cod_tcar=tc.cod_tcar AND c.fch_asg != '0000-00-00' AND (c.ced_per IN (SELECT ced_per from prog_movimientos WHERE accion =2 AND YEAR(fch_asg)='".$_GET[ano_nom]."' AND MONTH(fch_asg)='".$_GET[mes_nom]."' ))  ORDER BY nom_tcar ASC, num_car ASC, nom_car ASC ";
			//echo '<br><br>';
			$bus_cargos = mysql_query($sql_cargos);
			if ($debug) { echo '<br>SQL: '.$sql_cargos.'<br>Error:'.mysql_error(); }
			while($res_cargo = mysql_fetch_array($bus_cargos)){
				$total_pagar_nomina_ind = 0;
				$total_aport_nomina_ind = 0;
				$total_asig_nomina_ind = 0;
				$total_ded_nomina_ind = 0;
				$num_dant = 0;
	 			$pag_dant = 0;
				echo '<tr><td>';
					echo '<table width="99%" border="0" cellpadding="3" cellspacing="0" align="right"  class="detallespago">';
						///// para traernos el codigo del cargo
						$sql_codcar = "SELECT num_car, CONCAT(cod_dep,'-',num_car) AS cod_cargo, hora_sue FROM cargos c, sueldos s WHERE c.cod_car = ".$res_cargo[cod_car]." AND c.cod_sue=s.cod_sue";
						$codcar = mysql_fetch_array(mysql_query($sql_codcar));
						$num_horas = $codcar[hora_sue];
						$num_car = $codcar[num_car];
						$codcar = $codcar[cod_cargo];
						echo '<tr><td><b>Tipo de Cargo: </b>'.$res_cargo[nom_tcar].' ('.$res_cargo[abr_tcar].') <b>Cargo: </b>'.$res_cargo[nom_car].' <b>C�digo:</b> '.$codcar.' <b>Horas:</b> '.$num_horas.'</td></tr>';
						// Consultamos el personal que se encuentra registrado en el cargo
						$sql_per = "SELECT *, 'activos' as condicion FROM vista_cargos_per p WHERE p.cod_car = '".$res_cargo[cod_car]."' UNION SELECT *, 'retirados' as condicion FROM vista_cargos_salida s WHERE s.cod_car = '".$res_cargo[cod_car]."' AND YEAR(fch_asg) = '".$_GET[ano_nom]."' AND MONTH(fch_asg) = '".$_GET[mes_nom]."' AND fch_asg >= '".$fecha_desde_salida."' AND fch_asg <= '".$fecha_hasta_salida."'";
						$bus_per = mysql_query($sql_per);
						while($res_per = mysql_fetch_array($bus_per)){
							echo '<tr><td>';
								echo '<table width="100%" border="1" cellpadding="3" cellspacing="0" class="detallespago" bordercolor="#000000" style="border-collapse:collapse;">';
									echo '<tr>
										<td>
											<b>Datos Personales:</b>
											<br><b>C�dula:</b> '.redondear($res_per[ced_per],0,'.','').'
											<br><b>Nombre:</b> '.ucwords($res_per[nom_per]).' '.ucwords($res_per[ape_per]);
											$adicionales = '';
											if ($res_per[condicion]=='activos'){
												echo '<br><b>Fecha Ingreso:</b> '.date('d-m-Y', strtotime($res_per[fch_asg])).'';
												$fecha_ingreso = $res_per[fch_vac];
											}
											else{
												echo '<br><b><font color="00CC00">Fecha Ingreso:</font></b> '.date('d-m-Y', strtotime($res_per[fch_vac])).'';	
												echo '<br><b><font color="00CC00">Salida del Cargo:</font></b> '.date('d-m-Y', strtotime($res_per[fch_asg])).'';
												$fecha_ingreso = $res_per[fch_vac];
												/// verificamos si vuelve a entrar
												$sql_entrar = "SELECT * FROM prog_movimientos WHERE ced_per = ".$res_per[ced_per]." AND accion = 1 AND fch_asg>='".$fecha_desde_salida."' AND fch_asg <= '".$fecha_hasta_salida."'";
												if ($res_entrar = mysql_fetch_array(mysql_query($sql_entrar))){
													$adicionales = 'NO';
												}
											}
											if ($res_per[num_cue]){
												echo '<br><b>Cuenta:</b> '.$res_per[num_cue].'
												<br><b>Banco:</b> '.ucwords($res_per[ban_cue]).'';
											}else{
												echo '<br><b><font color="red">Pago Mediante Cheque</font></b>';
											}
										echo'</td>
										<td width="75%">
											<table width="100%" border="1" cellpadding="0" cellspacing="0" class="detallespago" bordercolor="#000000" style="border-collapse:collapse;">
											 	<tr align="center" style="font-weight: bold; background-color: #000; color: #FFF;"><td colspan="5" style="padding: 3px;">DETALLES DEL PAGO</td></tr>
											 	<tr align="center" style="font-weight: bold; background-color: #bebebe; color: #000;">
											 		<td align="left" style="padding: 3px;">Conceptos</td>
											 		<td style="padding: 3px; width: 80px;">Asignaciones</td>
											 		<td style="padding: 3px; width: 80px;">Deducciones</td>
											 		<td style="padding: 3px; width: 80px;">Aportes</td>
											 		<td style="padding: 3px; width: 80px;">Total</td>
											 	</tr>';
											 	$sueldo_base = $res_per[mon_sue];
												if ($res_per[condicion]=='activos'){
												 	if ($res_cargo[condicion]=='sin movimiento'){
												 		$sueldo_pagar = redondear(($res_per[mon_sue]*$factor),2,"",".");
													 	echo '<tr align="right">
													 		<td align="left" style="padding: 3px;">Sueldo o Salario</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar,2,".",",").'</td>
													 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
													 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar,2,".",",").'</td>
													 	</tr>';
	 												 	$total_asig_nomina_ind += $sueldo_pagar;
												 	}
												 	else{
												 		/// calculamos la fracci�n por entrada con continuidad
												 		if ($_GET[por_nom]==1){
												 			if (strtotime($res_cargo[fch_asg])<=strtotime($_GET[ano_nom].'-'.$_GET[mes_nom]-'-15')){
												 				$dias_con = 16-date('d',strtotime($res_cargo[fch_asg]));
												 			}
												 			if ($dias_con<15){ $add_sueldo = 'Fraccion'; }
												 		}
												 		if ($_GET[por_nom]==2){
												 			if (strtotime($res_cargo[fch_asg])>strtotime($_GET[ano_nom].'-'.$_GET[mes_nom]-'-15')){
												 				$dias_con = 31-date('d',strtotime($res_cargo[fch_asg]));
												 			}
												 			else{
												 				$dias_con = 15;
												 			}
												 			if ($dias_con<15){ $add_sueldo = 'Fraccion'; }
												 		}
												 		if ($_GET[por_nom]==3){
											 				$dias_con = 31-date('d',strtotime($res_cargo[fch_asg]));
												 			if ($dias_con<30){ $add_sueldo = 'Fraccion'; }
												 		}
												 		$sueldo_pagar = redondear((($res_per[mon_sue]/30)*$dias_con),2,"",".");
													 	echo '<tr align="right">
													 		<td align="left" style="padding: 3px;">'.$add_sueldo.' Sueldo o Salario'; 
													 		if ($add_sueldo) { echo ' ('.$dias_con.' d�as por entrada en el cargo)'; 
													 		} echo '</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar,2,".",",").'</td>
													 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
													 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar,2,".",",").'</td>
													 	</tr>';
													 	$total_asig_nomina_ind += $sueldo_pagar;
												 	}
											 	}
											 	else{
										 			if ($_GET['por_nom']==1 || $_GET['por_nom']==3){
										 				$dias_sal = date('d',strtotime($res_per[fch_asg]));
													}
													if ($_GET['por_nom']==2){
										 				$dias_sal = date('d',strtotime($res_per[fch_asg]))-15;
													}
											 		$stemp = redondear(($res_per[mon_sue]/30),2,'','.');
											 		$stemp = redondear(($stemp*$dias_sal),2,'','.');
											 		$sueldo_pagar = $stemp;
											 		echo '<tr align="right">
												 		<td align="left" style="padding: 3px;">'.$dias_sal.' d�as de Sueldo o Salario</td>
												 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar,2,".",",").'</td>
												 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
												 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
												 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar,2,".",",").'</td>
												 	</tr>';	
 												 	$total_asig_nomina_ind += $sueldo_pagar;
											 	}
											 	// calculamos francci�n de pago por entrada de cargo 
											 	if ($_GET[por_nom]!=2){										 		
												 	if ($_GET['mes_nom']==1){
												 		$fecha_desde = strtotime(($_GET['ano_nom']-1).'-12-01');
												 		$fecha_hasta = strtotime(($_GET['ano_nom']-1).'-12-31');
												 	}
												 	else{
												 		$fecha_desde = strtotime($_GET['ano_nom'].'-'.($_GET['mes_nom']-1).'-01');
												 		$fecha_hasta = strtotime($_GET['ano_nom'].'-'.($_GET['mes_nom']-1).'-'.dias_mes(($_GET['mes_nom']-1),$_GET['ano_nom']));
												 	}
												 	if (strtotime($res_cargo[fch_asg]) >= $fecha_desde && strtotime($res_cargo[fch_asg]) <= $fecha_hasta){
													 	$dias = 31 - date('d',strtotime($res_cargo[fch_asg]));
													 	// sueldo diario
													 	$dias_monto = redondear(($res_per[mon_sue]/30),2,"",".");
													 	// sueldo de la fraccion 
													 	$dias_monto = redondear(($dias_monto*$dias),2,"",".");
													 	////// verifico si sali� en el mes anterior para no pagar por ingreso ya que se le pagar� en el mismo mes
													 	if ($_GET[mes_nom]==1){
													 		$mes_ant = 12;
													 		$ano_ant = $_GET[ano_nom]-1;
													 	}
													 	else{
														 	$mes_ant = $_GET[mes_nom]-1;
													 		$ano_ant = $_GET[ano_nom];
													 	}
													 	$sql_pagado_ant = "SELECT * FROM prog_movimientos WHERE YEAR(fch_asg)='".$ano_ant."' AND MONTH(fch_asg)='".$mes_ant."' AND ced_per =".$res_cargo[ced_per]." AND accion=2";
													 	$bus_pagado_ant = mysql_query($sql_pagado_ant);
													 	if (!$res_pagado_ant = mysql_fetch_array($bus_pagado_ant)){
													 		$sueldo_pagar2 = $dias_monto;
														 	echo '<tr align="right">
														 		<td align="left" style="padding: 3px;">'.$dias.' d�as de Salario por ingreso en cargo</td>
														 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar2,2,".",",").'</td>
														 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
														 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
														 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_pagar2,2,".",",").'</td>
														 	</tr>';
														 	$num_dant = $dias;
												 			$pag_dant = $sueldo_pagar2;
											 				$total_asig_nomina_ind += $sueldo_pagar2;
													 	}
												 	}
											 	}
											 	$total_sueldo = $sueldo_base;
											 	/// verificamos si existe inasistencia registrada para el periodo
											 		$inasistencias = calculo_inasistencias($res_per[ced_per],$sueldo_base,$res_cargo[cod_car],$fecha_desde_salida,$fecha_hasta_salida);
											 		$total_ded_nomina_ind += $inasistencias[monto];
											 	/// Verificamos asignaciones con incidencia sobre salario Base
											 	if ($adicionales!='NO'){
												 	$asignaciones_salarioB = calculo_asignaciones($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$factor,'B');
												 	//echo $asignaciones_salarioB[total][monto];
												 	$sueldob_addicional = $asignaciones_salarioB[total][monto];
												 	$total_sueldo += $asignaciones_salarioB[total][total];
												 	$total_asig_nomina_ind += $sueldob_addicional;
											 	}
											 	/// Verificamos asignaciones con incidencia sobre Salario Integral
											 	if ($adicionales!='NO'){
												 	$asignaciones_salarioI = calculo_asignaciones($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$factor,'I');
												 	//echo $asignaciones_salarioI[total][monto];
												 	$total_asig_nomina_ind += $asignaciones_salarioI[total][monto];
												 	$total_sueldo += $asignaciones_salarioI[total][total];
											 	}
											 	/// Verificar Primas
											 	if ($adicionales!='NO'){
												 	$primas = calculo_primas($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$factor,$sueldob_addicional);
												 	$total_asig_nomina_ind += $primas[total];
												 	$total_sueldo += $primas[total_completo];
											 	}
											 	/// Verificamos asignaciones sin incidencia Salarial
											 	if ($adicionales!='NO'){
												 	$asignaciones = calculo_asignaciones($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$factor,'N');
												 	//echo $asignaciones[total][monto];
												 	$total_asig_nomina_ind += $asignaciones[total][monto];
											 	}
											 	/// Verificar D�a adicional (mes de 31 dias)
											 	$sueldo_dia = 0;
											 	if ($adicionales!='NO' && $_GET['por_nom']>=2 && dias_mes($_GET['mes_nom'],$_GET['ano_nom'])==31){
											 		$num_dia = 1;
											 		$sueldo_dia = '';
											 		$sueldo_dia = redondear(($sueldo_base/30),2,'','.');
											 		echo '<tr align="right">
														 		<td align="left" style="padding: 3px;">D�a adicional</td>
														 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_dia,2,".",",").'</td>
														 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
														 		<td style="padding: 3px; width: 80px;">&nbsp;</td>
														 		<td style="padding: 3px; width: 80px;">'.redondear($sueldo_dia,2,".",",").'</td>
														 	</tr>';
													$total_asig_nomina_ind += $sueldo_dia;
											 	}
											 	/// Verificar deducciones adicionales
											 	if ($adicionales!='NO'){
												 	$deducciones = calculo_deducciones($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$factor,'N');
												 	$total_ded_nomina_ind += $deducciones[total][monto];
												}
											 	/// Verificar deducciones de Ley
											 	if ($adicionales!='NO'){
												 	$deducciones_ley = calculo_deducciones_ley($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$factor,$sueldob_addicional,$primas);
												 	$total_ded_nomina_ind += $deducciones_ley[total][rete];
												 	$total_aport_nomina_ind += $deducciones_ley[total][apor];
												}
											 	/// Verificar guardias domingos y feriados
											 	if ($adicionales!='NO'){
												 	$guardias = calculo_guardias($res_per[ced_per],$sueldo_base,$fecha_ingreso,$res_cargo[cod_car],$sueldob_addicional,$primas,$sueldo_dia);
												 	$total_asig_nomina_ind += $guardias[total][monto];
												}
												/// Verificar si hay pago de d�as de Aguinaldos
												if ($adicionales!='NO' && $_GET[aguinaldos]){
													$aguinaldos = calculo_aguinaldos($res_per[ced_per],$total_sueldo,$fecha_ingreso,$res_cargo[cod_car],$_GET[aguinaldos]);
													$total_asig_nomina_ind += $aguinaldos[0];
												}
												/// Verificar si hay pago Becas
												if ($adicionales!='NO' AND $_GET[becas]){
													$becas = calculo_beca($res_per[ced_per]);
													$total_asig_nomina_ind += $becas[total];
												}
												/// Verificar si hay pago juguetes
												if ($adicionales!='NO' AND $_GET[juguetes]){
													$juguetes = calculo_juguetes($res_per[ced_per]);
													$total_asig_nomina_ind += $juguetes[monto];
												}
												/// Verificar si corresponde pago de Vacaciones
												$vacaciones = verificando_vacaciones($_GET[mes_nom], $_GET[ano_nom], $_GET[por_nom],$res_per[ced_per],$res_cargo[cod_car]);																								
												if ($vacaciones=='SI'){
													$vacacioness = calculo_vacaciones($res_per[ced_per],$total_sueldo,$fecha_ingreso,$res_cargo[cod_car],$_GET[aguinaldos]);
													$total_asig_nomina_ind += $vacacioness[0];
												}
												$total_pagar_nomina_ind = $total_asig_nomina_ind - $total_ded_nomina_ind;
												echo '<tr align="right" style="font-weight: bold; background-color: #bebebe; color: #000;">
													 		<td style="padding: 3px;">TOTALES</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($total_asig_nomina_ind,2,'.',',').'</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($total_ded_nomina_ind,2,'.',',').'</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($total_aport_nomina_ind,2,'.',',').'</td>
													 		<td style="padding: 3px; width: 80px;">'.redondear($total_pagar_nomina_ind,2,'.',',').'</td>
													 	</tr>';
												$total_asig_nomina += $total_asig_nomina_ind;
												$total_ded_nomina += $total_ded_nomina_ind;
												$total_aport_nomina += $total_aport_nomina_ind;
												$total_pagar_nomina += $total_pagar_nomina_ind;
												if ($_POST['procesar_val']=="SI"){
													/// insertar los datos de la nomina general
													$sql_nomina_ppal = "INSERT INTO ".$tabla_ppal." (ano_nom, mes_nom, por_nom, ced_per, nac_per, nom_per, ape_per, mon_sue, mon_asi, mon_ded, mon_pag, lph_des, spf_des, sso_des, cah_des, sfu_des, cod_dep, nom_dep, cod_car, nom_car, fch_asg, cod_tcar, nom_tcar, abr_tcar, num_cue, tip_cue, ban_cue, num_dnl, mon_dnl, lph_apr, spf_apr, sso_apr, cah_apr, sfu_apr, ppo_des, pio_des, prm_hog, prm_hij, prm_ant, prm_pro, prm_hje, prm_jer, prm_otr, mon_scom, int_sue, fnj_des, fnj_apr, lph_des_por, spf_des_por, sso_des_por, cah_des_por, fnj_des_por, lph_apr_por, spf_apr_por, sso_apr_por, cah_apr_por, fnj_apr_por, prm_rie, prm_pat, prm_coo, comp_eval, bono_noct, prm_tra, prm_asis, prm_spn, cah_nom, num_dal, mon_dal, dias_agui, mont_agui, num_car, horas, prm_mesp, prm_exc, pag_dant, num_dant) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$_GET[por_nom].", ".$res_per[ced_per].", '".$res_per[nac_per]."', '".$res_per[nom_per]."', '".$res_per[ape_per]."', ".$sueldo_pagar.", ".$total_asig_nomina_ind.", ".$total_ded_nomina_ind.", ".$total_pagar_nomina_ind.", ".$deducciones_ley[lph][rete].", ".$deducciones_ley[pie][rete].", ".$deducciones_ley[sso][rete].", '".$deducciones_ley[cah][rete]."', '', '".$res_cargo[cod_dep]."', '".$res_dep[nom_dep]."', '".$res_cargo[cod_car]."', '".$res_cargo[nom_car]."', '".$res_cargo[fch_asg]."', '".$res_cargo[cod_tcar]."', '".$res_cargo[nom_tcar]."', '".$res_cargo[abr_tcar]."', '".$res_per[num_cue]."', '".$res_per[tip_cue]."', '".$res_per[ban_cue]."', '".$inasistencias[dias]."', '".$inasistencias[monto]."', '".$deducciones_ley[lph][apor]."', '".$deducciones_ley[pie][apor]."', '".$deducciones_ley[sso][apor]."', '".$deducciones_ley[cah][apor]."', '', '', '', '".$primas[hogar][periodo]."', '".$primas[hijos][periodo]."', '".$primas[antiguedad][periodo]."', '".$primas[profesion][periodo]."', '', '', '', '".$res_per[mon_sue]."', '".$deducciones_ley[integral][monto]."', '', '', '".$deducciones_ley[lph][rete_por]."', '".$deducciones_ley[pie][rete_por]."', '".$deducciones_ley[sso][rete_por]."', '".$deducciones_ley[cah][rete_por]."', '', '".$deducciones_ley[lph][apor_por]."', '".$deducciones_ley[pie][apor_por]."', '".$deducciones_ley[sso][apor_por]."', '".$deducciones_ley[cah][apor_por]."', '', '', '', '', '".$primas[comp_eval][periodo]."', '".$primas[nocturno][periodo]."', '".$primas[transporte][periodo]."', '".$primas[asistencial][periodo]."', '".$primas[spn][periodo]."', '".$deducciones_ley[cah][cah_nom]."', '".$num_dia."', '".$sueldo_dia."', '".$_GET[aguinaldos]."', '".$aguinaldos[0]."', '".$num_car."', '".$num_horas."', '".$primas[medesp][periodo]."','".$primas[exclusividad][periodo]."', '".$pag_dant."', '".$num_dant."')";
													mysql_query($sql_nomina_ppal);
													if ( mysql_error() ){
														$error = 'SI';
													}
													/// Insertar las asignaciones con incidencia sobre salario base
												 	foreach ($asignaciones_salarioB as $key => $value) {
												 		if ($key!='total'){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '".$value[cod_cnp]."', '".$value[titulo]."', '".$value[cod_con]."', '".$value[nom_con]."', '".$value[monto]."');";
															if ($value[ncp_cnp]>0 AND $prenomina!=1){
																$sql_update_cuota = "UPDATE asignaciones SET ncp_cnp = ".$value[ncp_cnp]." WHERE cod_cnp = ".$value[cod_cnp];
																mysql_query($sql_update_cuota);
																if ( mysql_error() ){
																	$error = 'SI';
																	echo mysql_error();
																}
															}
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}
												 		}
												 	}
													/// Insertar las asignaciones con incidencia sobre salario Integral
												 	foreach ($asignaciones_salarioI as $key => $value) {
												 		if ($key!='total'){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '".$value[cod_cnp]."', '".$value[titulo]."', '".$value[cod_con]."', '".$value[nom_con]."', '".$value[monto]."');";
															if ($value[ncp_cnp]>0 AND $prenomina!=1){
																$sql_update_cuota = "UPDATE asignaciones SET ncp_cnp = ".$value[ncp_cnp]." WHERE cod_cnp = ".$value[cod_cnp];
																mysql_query($sql_update_cuota);
																if ( mysql_error() ){
																	$error = 'SI';
																	echo mysql_error();
																}
															}
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}
												 		}
												 	}
													/// Insertar las asignaciones sin incidencia sobre salario
												 	foreach ($asignaciones as $key => $value) {
												 		if ($key!='total'){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '".$value[cod_cnp]."', '".$value[titulo]."', '".$value[cod_con]."', '".$value[nom_con]."', '".$value[monto]."');";
															if ($value[ncp_cnp]>0 AND $prenomina!=1){
																$sql_update_cuota = "UPDATE asignaciones SET ncp_cnp = ".$value[ncp_cnp]." WHERE cod_cnp = ".$value[cod_cnp];
																mysql_query($sql_update_cuota);
																if ( mysql_error() ){
																	$error = 'SI';
																	echo mysql_error();
																}
															}
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}
												 		}
												 	}
												 	/// Insertar las deducciones 													/// Insertar las asignaciones sin incidencia sobre salario
												 	foreach ($deducciones as $key => $value) {
												 		if ($key!='total'){
															$sql_nomina_deduc = "INSERT INTO ".$tabla_dedu." (ano_ded, mes_ded, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_dsp, con_dsp, cod_des, nom_des, mcuo_dsp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '".$value[cod_dsp]."', '".$value[titulo]."', '".$value[cod_des]."', '".$value[nom_des]."', '".$value[monto]."');"; 
															if ($value[ncp_dsp]>0 AND $prenomina!=1){
																$sql_update_cuota = "UPDATE deducciones SET ncp_dsp = ".$value[ncp_dsp]." WHERE cod_dsp = ".$value[cod_dsp];
																mysql_query($sql_update_cuota);
																if ( mysql_error() ){
																	$error = 'SI';
																	echo mysql_error();
																}
															}
															mysql_query($sql_nomina_deduc);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}
												 		}
												 	}
												 	/// Insertar guardias domingos y feriados
												 	foreach ($guardias as $key => $value) {
												 		if ($key!='total'){
													 		foreach ($value as $key2 => $value2){
													 			foreach ($value2 as $key3 => $value3) {
													 				$$key3 = $value3;
													 			}
																$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', 0, '".$titulo."', '-2', '".$concepto."', '".$monto."');";
																if ($prenomina!=1){
																	$sql_update_guar = "UPDATE guardias_feriados SET sta_gua = 'P' WHERE cod_gua = ".$cod_gua;
																	mysql_query($sql_update_guar);
																	if ( mysql_error() ){
																		$error = 'SI';
																		echo mysql_error();
																	}
																}
																mysql_query($sql_nomina_asig1);
																if ( mysql_error() ){
																	$error = 'SI';
																	//echo mysql_error();
																}
													 		}
												 		}
												 	}
													/// Insertar si hay pago de d�as de Aguinaldos
													if ($aguinaldos[0]>0){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '0', '".$aguinaldos[titulo]."', '-1', 'Aguinaldos', '".$aguinaldos[0]."');";
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}	
													}
													/// Insertar si hay pago Becas
													foreach ($becas as $key => $value) {
												 		if ($key!='total'){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '0', '".$value[titulo]."', '-3', 'Becas', '".$value[monto]."');";
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}
												 		}
												 	}
													/// Verificar si hay pago juguetes
													if ($juguetes['monto']>0){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '0', '".$juguetes[titulo]."', '-4', 'Juguetes', '".$juguetes[monto]."');";
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}	
													}
													/// Insertar si hay pago de Bono vacacional
													if ($vacacioness[0]>0){
															$sql_nomina_asig1 = "INSERT INTO ".$tabla_asig." (ano_asg, mes_asg, ced_per, cod_car, por_nom, cod_dep, cod_tcar, cod_cnp, con_cnp, cod_con, nom_con, mcuo_cnp) VALUES (".$_GET[ano_nom].", ".$_GET[mes_nom].", ".$res_per[ced_per].", '".$res_cargo[cod_car]."', ".$_GET[por_nom].", '".$res_cargo[cod_dep]."', '".$res_cargo[cod_tcar]."', '0', '".$vacacioness[titulo]."', '-5', 'Bono vacacional', '".$vacacioness[0]."');";
															mysql_query($sql_nomina_asig1);
															if ( mysql_error() ){
																$error = 'SI';
																echo mysql_error();
															}	
													}
													$procesar_val = 'SI';
												}
											echo' </table>
										</td>
									</tr>';
								echo '</table>';
							echo '</td></tr>';
						}
					echo '</table>';
					echo '</td></tr>';
			}
		}
	?>
	</table>
	<table width="99%" border="1" cellpadding="0" cellspacing="0" align="center" class="detallespago" style="border-collapse:collapse; border-color: #000000;">
		<tr align="right" style="font-weight: bold; background-color: #bebebe; color: #000;">
	 		<td style="padding: 3px;">TOTALES</td>
	 		<td style="padding: 3px; width: 80px;"><?php echo redondear($total_asig_nomina,2,'.',','); ?></td>
	 		<td style="padding: 3px; width: 80px;"><?php echo redondear($total_ded_nomina,2,'.',','); ?></td>
	 		<td style="padding: 3px; width: 80px;"><?php echo redondear($total_aport_nomina,2,'.',','); ?></td>
	 		<td style="padding: 3px; width: 80px;"><?php echo redondear($total_pagar_nomina,2,'.',','); ?></td>
	 	</tr>
	</table>	
	<br>
	<center>
		<form id="form1" name="form1" method="post" action="">
	      	<input name="procesar_val" type="hidden" id="procesar_val" value="<?php echo $_POST['procesar_val']; ?>">
	      	<?php if ($procesar_val=="NO") {
				echo '<input type="submit" name="procesar" id="procesar" value="Procesar" onclick=confirmacion_func("Procesar")>';
	       	} ?>
	      	<input type="submit" name="cancelar" id="cancelar" value="Cerrar Ventana" onclick="window.close();">
	    	<?php 
	    		/// Mostrar mensajes de exito o de error
	    		if ($procesar_val=='SI' AND $error == ''){
	   				echo '<script>alert("Se Proces� exitosamente.");</script>';
	   				mysql_query("COMMIT");
	    		}
	    		if ($procesar_val=='SI' AND $error == 'SI'){
	   				echo '<script>alert("Se produjo un error al intentar procesar.");</script>';
	   				mysql_query("ROLLBACK");
	    		}
	    	?>
	    </form>
	</center>
	<br><br>
</body>
</html>