<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Deducciones del Personal</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script src="../js/jquery.js"> </script>
<?php
$viene_val = $_GET['guar_mas'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal_guardias_feriados.php?guar_mas='.$_GET["guar_mas"].'&seccion='.$_GET["seccion"];
$pagina2 = 'personal_guardias_feriados.php?guar_mas='.$_GET["guar_mas"].'&seccion='.$_GET["seccion"];
$tabla = "guardias_feriados";	// nombre de la tabla
$ncampos = "7";			//numero de campos del formulario

$cod_gua = $_POST["cod_gua"];
$con_gua = $_POST["con_gua"];
$fec_gua = $_POST["fec_gua"];
$nor_gua = $_POST["nor_gua"];
$fer_gua = $_POST["fer_gua"];
$dom_gua = $_POST["dom_gua"];
$dof_gua = $_POST["dof_gua"];
$dis_gua = $_POST["dis_gua"];
$ced_per = $_GET["ced_per"];
$cod_car = $_POST["cod_car"];
$guar_mas = $_POST["guar_mas"];

$cod_tcar = $_POST["cod_tcar"];

$num=$_POST['num'];
$band_cedcar=false;
for($i=1;$i<$num;$i++){
	if($_POST['cedcar_'.$i]){
		$cedcar[$i]=explode(":::",$_POST['cedcar_'.$i]);
		$band_cedcar=true; 
	}
}

$datos[0] = crear_datos ("con_gua","Concepto",$_POST['con_gua'],"1","100","alfanumericos");
$datos[1] = crear_datos ("fec_gua","Fecha de Registro",$_POST['fec_gua'],"1","10","fecha");
$datos[2] = crear_datos ("nor_gua","Guardia Normal",$_POST['nor_gua'],"1","11","numericos");
$datos[3] = crear_datos ("fer_gua","Guardia Feriado",$_POST['fer_gua'],"1","11","numericos");
$datos[4] = crear_datos ("dom_gua","Guardia Domingo",$_POST['dom_gua'],"1","11","numericos");
$datos[5] = crear_datos ("dof_gua","Domingo Feriado",$_POST['dof_gua'],"1","11","numericos");
$datos[6] = crear_datos ("dis_gua","Disponibilidad",$_POST['dis_gua'],"1","11","numericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo,' LIMIT 1');
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Concepto";
		$datos[0]="con_gua";
		$parametro[1]="Cedula";
		$datos[1]="ced_per";
		$parametro[2]="Cargo";
		$datos[2]="car_per";;
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_gua");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_gua = $row["cod_gua"];
	    $con_gua = $row["con_gua"];
	    $ced_per = $row["ced_per"];
		$cod_car = $row["cod_car"];
	    $fec_gua = $row["fec_gua"];		
	    $nor_gua = $row["nor_gua"];
		$fer_gua = $row["fer_gua"];
		$dom_gua = $row["dom_gua"];
		$dof_gua = $row["dof_gua"];		
		$dis_gua = $row["dis_gua"];	
      	$guar_mas = $row["guar_mas"];
	  
		$reg_tcar=buscar_campo("cod_tcar", "cargos", "where cod_car='".$cod_car."'");
		$cod_tcar=$reg_tcar["cod_tcar"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($band_cedcar==false) { echo "<script>alert('No fue seleccionado ningun empleado de la lista');</script>";  $validacion=""; }
	if ($validacion) {
  mysql_query("delete from guardias_feriados where guar_mas='$guar_mas'");
  $consulta_deduccion=mysql_query("select max(guar_mas) as maximo  from guardias_feriados ");
  $con_dedu=mysql_fetch_assoc($consulta_deduccion);
  $maximo2=$con_dedu[maximo]+1;
  for($i=1;$i<$num;$i++){
    $ncampos = "10";
    $datos[7] = crear_datos ("ced_per","Cedula",$cedcar[$i][0],"1","12","numericos");
    $datos[8] = crear_datos ("cod_car","Cargo",$cedcar[$i][1],"1","11","numericos");
    $datos[9] = crear_datos ("guar_mas","guar_mas",$maximo2,"1","11","numericos");  
	insertar_func_nomina($ncampos,$datos,$tabla,$pagina);
    auditoria_func ('insertar', $ncampos, $datos, $tabla);
  }
    ?>
    <script type="text/javascript">
      alert("Se ha almacenado con exito....");
      location.href = "<?php echo $pagina; ?>";
    </script>
    <?php		
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($band_cedcar==false) { echo "<script>alert('No fue seleccionado ningun empleado de la lista');</script>";  $validacion=""; }
	if ($validacion) { $boton = "Guardar"; }
	//$boton=comp_exist($datos[7][0],$datos[7][2],'personal',$boton,'no','Personal');
}
if ($_POST["confirmar"]=="Guardar") 
{
  //consulta para incrementar en uno la asgnacion masiva 
  $consulta_deduccion=mysql_query("select max(guar_mas) as maximo  from guardias_feriados ");
  $con_dedu=mysql_fetch_assoc($consulta_deduccion);
  $maximo2=$con_dedu[maximo]+1;
  for($i=1;$i<$num;$i++){
    $ncampos = "10";
    $datos[7] = crear_datos ("ced_per","Cedula",$cedcar[$i][0],"1","12","numericos");
    $datos[8] = crear_datos ("cod_car","Cargo",$cedcar[$i][1],"1","11","numericos");
    $datos[9] = crear_datos ("guar_mas","guar_mas",$maximo2,"1","11","numericos");   
	insertar_func_nomina($ncampos,$datos,$tabla,$pagina);
    auditoria_func ('insertar', $ncampos, $datos, $tabla);
  }
    ?>
    <script type="text/javascript">
      alert("Se ha almacenado con exito....");
      location.href = "<?php echo $pagina; ?>";
    </script>

    <?php
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["guar_mas"],"guar_mas",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"guar_mas","guardias_feriados",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar Guardias</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">

                      <tr>
                        <td width="25%" class="etiquetas">Concepto:</td>
                        <td width="75%"><input name="guar_mas" type="hidden" id="guar_mas" value="<?php if(! $existe) { echo $_POST["guar_mas"]; } else { echo $guar_mas; } ?>" size="35" />
                        <input name="con_gua" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="con_gua" value="<?php if(! $existe) { echo $_POST["con_gua"]; } else { echo $con_gua; } ?>" size="35" title="Concepto de la Guardia">
                        <?php if ($boton=='Modificar') { echo $con_gua; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro: </td>
                        <td><input name="fec_gua" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fec_gua" value="<?php if(! $existe) { echo $_POST["fec_gua"]; } else { echo $fec_gua; } ?>" size="15" title="Fecha de Registro de la Deducción" />
                          <?php if ($boton=='Modificar') { echo $fec_gua; } ?><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fec_gua,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
					<tr><td class="etiquetas">Tipos de Personal: </td><td><?php combo('cod_tcar', $cod_tcar, 'tipos_cargos', $link, 0, 0, 1, "", 'cod_tcar', 'onchange="submit();"', $boton,''); ?>
				</td></tr>
                  <tr>
                    <td colspan="2" align="center"><?php $codig_mas='guar_mas'; include ('capa_listado_personal.php'); ?></td>
                  </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php // include ('../comunes/botonera_usr.php'); ?><input type="hidden" name="confirmar" id="confirmar" value="" ><input type="hidden" name="confirmar_val" id="confirmar_val" value="" ></td>
                  </tr>
                  <tr>
                    <td align="center"><div id="capa_guardias_personal_general"><?php include ('capa_guardias_personal_general.php'); ?></div></td>
                  </tr>
		  <tr><td align="center"><br></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
