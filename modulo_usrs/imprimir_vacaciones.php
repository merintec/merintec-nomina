<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>

<?php $idprint=$_GET['cod_sol_vac']; 
	//consultamos los datos de la vacaciones
    $result=mysql_query("select * from vacaciones_per WHERE cod_sol_vac='$idprint'");
	if ($row=mysql_fetch_array($result))
	{
		$existe = 'SI';
		$id = $row["cod_sol_vac"];
		$fecha1 = substr($row["fch_sol_vac"], 8, 2);
		$fecha2 = substr($row["fch_sol_vac"], 5, 2);
		$fecha3 = substr($row["fch_sol_vac"], 0, 4);
		$fecha3b = substr($row["fch_sol_vac"], 2, 2);
		$fecha = $fecha1.' / '.$fecha2.' / '.$fecha3;
		$ced_per = $row["ced_per"];
		$nom_per = $row["nom_per"];
		$nom_dep = $row["nom_dep"];
		$nom_car = $row["nom_car"];
		
		$fecha1_ing = substr($row["fch_ing"], 8, 2);
		$fecha2_ing = substr($row["fch_ing"], 5, 2);
		$fecha3_ing = substr($row["fch_ing"], 0, 4);
		$fch_ing = $fecha1_ing.' / '.$fecha2_ing.' / '.$fecha3_ing;
		
    	$dias_sol_vac = $row["dias_sol_vac"];
    	
		$ini_sol_vac1 = substr($row["ini_sol_vac"], 8, 2);
		$ini_sol_vac2 = substr($row["ini_sol_vac"], 5, 2);
		$ini_sol_vac3 = substr($row["ini_sol_vac"], 0, 4);
		$ini_sol_vac = $ini_sol_vac1.' / '.$ini_sol_vac2.' / '.$ini_sol_vac3;
		
		$fin_sol_vac1 = substr($row["fin_sol_vac"], 8, 2);
		$fin_sol_vac2 = substr($row["fin_sol_vac"], 5, 2);
		$fin_sol_vac3 = substr($row["fin_sol_vac"], 0, 4);
		$fin_sol_vac = $fin_sol_vac1.' / '.$fin_sol_vac2.' / '.$fin_sol_vac3;

		$tip_sol_vac = $row["tip_sol_vac"];

		$peri_sol_vac1 = substr($row["peri_sol_vac"], 8, 2);
		$peri_sol_vac2 = substr($row["peri_sol_vac"], 5, 2);
		$peri_sol_vac3 = substr($row["peri_sol_vac"], 0, 4);
		$peri_sol_vac = $peri_sol_vac1.' / '.$peri_sol_vac2.' / '.$peri_sol_vac3;

		$perf_sol_vac1 = substr($row["perf_sol_vac"], 8, 2);
		$perf_sol_vac2 = substr($row["perf_sol_vac"], 5, 2);
		$perf_sol_vac3 = substr($row["perf_sol_vac"], 0, 4);
		$perf_sol_vac = $perf_sol_vac1.' / '.$perf_sol_vac2.' / '.$perf_sol_vac3;

		$obs_sol_vac = $row["obs_sol_vac"];
		$apro_sol_vac = $row["apro_sol_vac"];
	}
	mysql_free_result($result);
?>


<?php
///// Verificar cuantos d�as de vacaciones han sido asignados
if (!$_POST['ced_per'] && !$ced_per)
{
    $ced_per_reg = $_COOKIE['uscod'];
}
else
{
    $ced_per_reg = $ced_per;
}
$sql_dias = "select sum(dias_vac) as dias_asg from vac_dias_per where ced_per = ".$ced_per_reg." GROUP BY ced_per";
$sql_res = mysql_query($sql_dias);
while ($sql_row = mysql_fetch_array($sql_res))
{
    $dias_asg = $sql_row['dias_asg'];
}

///// Verificar cuantos d�as de vacaciones han disrutado
$sql_dias = "select sum(dias_sol_vac) as dias_dis from vacaciones_per where apro_sol_vac = 'A' AND ced_per = ".$ced_per_reg." GROUP BY ced_per";
$sql_res = mysql_query($sql_dias);
while ($sql_row = mysql_fetch_array($sql_res))
{
    $dias_dis = $sql_row['dias_dis'];
}

///// Calcular cuantos d�as de vacaciones quedan por Disfrutar

    $dias_pen = $dias_asg - $dias_dis;


?>
<title>Impresion de Solicitud de Vacaciones</title>
<br><br>
<div><?php include ('../comunes/pagina_encabezado.php'); ?></div>
<div><h1>&nbsp;Solicitud de Vacaciones</h1></div>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" align="center">
    <tr>
        <td>
            &nbsp;
        </td>
        <td width="50%" align="right">
            &nbsp;<b><font size="3px">Fecha de Elaboraci�n:</b> <?php echo $fecha; ?>&nbsp;&nbsp;</font>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;<b><font size="3px">Dependencia:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nom_dep; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">Cargo:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nom_car; ?></font>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;<b><font size="3px">Nombres y Apellidos:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $nom_per; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">C�dula de Identidad:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo redondear($ced_per,0,".",","); ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000"  align="center">
    <tr>
        <td width="50%">
            &nbsp;<b><font size="3px">Fecha de Ingreso:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $fch_ing; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">D�as a Disfrutar:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dias_sol_vac.' d�as'; ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000" align="center" >
    <tr>
        <td width="50%">
            &nbsp;<b><font size="3px">Tipo de Vacaciones:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($tip_sol_vac=='A') { echo 'Atrasadas'; }?><?php if ($tip_sol_vac=='R') { echo 'Reglamentarias'; }?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">Per�odo laboral al que corresponde:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo 'Del: '.$peri_sol_vac.' Al: '.$perf_sol_vac; ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000"  align="center">
    <tr>
        <td width="50%">
            &nbsp;<b><font size="3px">Fecha de Incio del Disfrute:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $ini_sol_vac; ?></font>
        </td>
        <td>
            &nbsp;<b><font size="3px">Fecha Final del Disfrute:</b><br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $fin_sol_vac; ?></font>
        </td>
    </tr>
</table>
<br>
<table width="95%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border=1 bordercolor="#000000"  align="center">
    <tr>
        <td width="50%" height="70" valign="top">
            &nbsp;<b><font size="3px">Observaciones:</b><br>
            <?php echo  '&nbsp;Quedan '.$dias_pen.' d�as por disfrutar.<br>'; ?>
            <?php echo $obs_sol_vac; ?></font>
        </td>
    </tr>
</table>
<?php echo $msg_pie_reporte; ?>
<table width="95%" cellspacing="0" cellpadding="0" border="0"  align="center">
    <tr>
        <td width="50%">
           <br><br><br>
           
           <center><hr width="70%"><?php echo $nom_per; ?><br>SOLICITANTE</center>
        </td>      
        <td width="50%">
           <br><br><br>
           
           <center><hr width="70%"><br>DIRECTOR</center>
        </td>
    </tr>
</table>
<br>
<div><input type="button" name="bt_print" value="Imprimir Solicitud" id="bt_print" onclick="this.style.visibility='hidden'; window.print();"></div>
