<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Productos de la Compras / Servicios</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_ret_iva'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'retenciones_iva_facturas.php?cod_ret_iva='.$_GET["cod_ret_iva"].'&seccion='.$_GET["seccion"];
$pagina2 = 'retenciones_iva_facturas.php?cod_ret_iva='.$_GET["cod_ret_iva"].'&seccion='.$_GET["seccion"];
$tabla = "retenciones_iva_fact";	// nombre de la tabla
$ncampos = "8";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_fac","Cod. de factura retenida ",$_POST['cod_fac'],"0","11","numericos");
$datos[1] = crear_datos ("cod_ret_iva","Cod. comprobante de Retencion IVA",$_POST['cod_ret_iva'],"1","7","numericos");
$datos[2] = crear_datos ("fch_fac","Fecha de factura retenida",$_POST['fch_fac'],"10","10","fecha");
$datos[3] = crear_datos ("con_fac","N� Control de Factura",$_POST['con_fac'],"1","25","alfanumericos");
$datos[4] = crear_datos ("num_fac","N� de Factura",$_POST['num_fac'],"1","25","alfanumericos");
$datos[5] = crear_datos ("tot_fac","Monto Total de Factura",$_POST['tot_fac'],"1","11","decimal");
$datos[6] = crear_datos ("exc_fac","Monto Excento de Factura",$_POST['exc_fac'],"1","11","decimal");
$datos[7] = crear_datos ("imp_fac","Base Imponible de Factura",$_POST['imp_fac'],"1","11","decimal");

//// consulta los datos del comprobante
$sql_ret_iva = "Select * from retenciones_iva where cod_ret_iva=".$_GET['cod_ret_iva'];
$busca_sql_ret_iva = mysql_query($sql_ret_iva);
if ($row_ret_iva=mysql_fetch_array($busca_sql_ret_iva)) {
	    $cod_ret_iva = $row_ret_iva["cod_ret_iva"];
	    $fch_ret_iva = $row_ret_iva["fch_ret_iva"];
	    $com_ret_iva = $row_ret_iva["com_ret_iva"];
	    $rif_pro = $row_ret_iva["rif_pro"];
	    $apl_ret_iva = $row_ret_iva["apl_ret_iva"];
	    $ret_ret_iva = $row_ret_iva["ret_ret_iva"];
	    $egr_ret_iva = $row_ret_iva["egr_ret_iva"];
	    $obs_ret_iva = $row_ret_iva["obs_ret_iva"];
	    $can_ret_iva = $row_ret_iva["can_ret_iva"];
}

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row_ret_iva=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_fac = $row_ret_iva["cod_fac"];
	    $cod_ret_iva = $row_ret_iva["cod_ret_iva"];
  	    $fch_fac = $row_ret_iva["fch_fac"];
  	    $con_fac = $row_ret_iva["con_fac"];
  	    $num_fac = $row_ret_iva["num_fac"];
  	    $tot_fac = $row_ret_iva["tot_fac"];
  	    $exc_fac = $row_ret_iva["exc_fac"];
	    $imp_fac = $row_ret_iva["imp_fac"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row_ret_iva[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_fac",$_POST["cod_fac"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_fac"],"cod_fac",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_fac",$tabla,$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Facturas del Comprobante Ret. IVA: <?php echo substr($fch_ret_iva, 0, 4).'-'.substr($fch_ret_iva, 5, 2).'-'.$com_ret_iva; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td class="etiquetas">Fecha de Factura</td>
                        <td>
			<input name="cod_fac" type="hidden" id="cod_fac" value="<?php if(! $existe) { echo $_POST["cod_fac"]; } else { echo $cod_fac; } ?>" title="Codigo del producto compra">
			<input name="cod_ret_iva" type="hidden" id="cod_ret_iva" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_ret_iva; } ?>" title="Codigo de la compra">
            <?php escribir_campo('fch_fac',$_POST["fch_fac"],$fch_fac,'',10,10,'Fecha de emisi�n de Factura',$boton,$existe,'fecha')?>
			            </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� Control Factura:</td>
			            <td>
                            <?php escribir_campo('con_fac',$_POST["con_fac"],$con_fac,'',25,20,'N� de control de la factura',$boton,$existe,'')?>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� de Factura:</td>
			            <td>
                            <?php escribir_campo('num_fac',$_POST["num_fac"],$num_fac,'',25,20,'N� de la factura',$boton,$existe,'')?>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto de la Factura:</td>
			            <td>
                            <?php escribir_campo('tot_fac',$_POST["tot_fac"],$tot_fac,'',11,20,'Monto Total de la factura incluyendo IVA',$boton,$existe,'')?>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Excento:</td>
			            <td>
                            <?php escribir_campo('exc_fac',$_POST["exc_fac"],$exc_fac,'',11,20,'Monto sin derecho a credito fiscal',$boton,$existe,'')?>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Base Imponible:</td>
			            <td>
                            <?php escribir_campo('imp_fac',$_POST["imp_fac"],$imp_fac,'',11,20,'Base Imponible',$boton,$existe,'')?>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_retenciones_iva_facturas.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
