<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<title>Incidencias Salariales</title>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php include ('../comunes/formularios_funciones.php'); ?>
<?php 
$sqlv="select * from valores";
	$resv = mysql_query($sqlv);
	while ($rowv = mysql_fetch_array($resv))
	{
		$$rowv['des_val'] = $rowv['val_val'];
	}
$sql = "select * from incidencias where cod_inc = ".$_GET['cod_inc'];
$row =  mysql_fetch_array(mysql_query($sql));
$mes_inicio = substr($row['fini_inc'],5,2); 
$mes_fin = substr($row['ffin_inc'],5,2); 
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td width="22%"><img src="../imagenes/logo_tn_trn.png" width="172" height="100" /></td>
    <td width="57%" align="center" class="titulo_nomina">Institulo Aut�nomo de Policia Municipal (IAPM)</td>
    <td width="21%"><?php include('../comunes/fecha.php'); echo '<br>'; $hora = time(); echo '<b>Hora:</b> '.date ("h:i:s A",$hora); 
?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" class="etiquetas_nomina">Incidencias Salariales<br>
       Per�odo del: <?php echo $row["fini_inc"]; ?> al <?php echo $row["ffin_inc"]; 
       $nmeses = 1+meses($row["fini_inc"],$row["ffin_inc"]);
       echo ' ('; if ($nmeses < 10) { echo '0'; } echo $nmeses; if ($nmeses > 1) { echo ' Meses'; } else { echo ' Mes'; } echo ')'; ?> <br>
       Concepto: <?php echo $row['con_inc']; ?>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
<br>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<?php
$sql2="select * from dependencias ORDER BY cod_dep ASC";
$busq2=mysql_query($sql2);
while($reg2=mysql_fetch_array($busq2))
{
   echo '<tr><td colspan="9"><hr></td></tr>';
   echo '<tr><td colspan="9">'.$reg2["nom_dep"].'</td></tr>';
   echo '<tr><td colspan="9">';
   echo '<table border="1" cellpadding="1" cellspacing="0" align="center">
            <tr class="etiquetas_nomina" align="center">
               	<td  width="20px">N�</td>
		<td width="70px">Fech.Ingreso</td>
		<td width="280px">Apellidos y Nombres</td>
		<td width="60px">C�dula</td>
		<td width="280px">Cargo</td>
		<td width="70px">Sueldo<br>Acumulado</td>
		<td width="70px">Monto<br>Incidencia</td>
		<td width="50">Apor<br>SSO</td>
		<td width="50">Apor<br>PIE</td>
		<td width="50">Apor<br>CHA</td>
		<td width="50">Ret.<br>SSO</td>
		<td width="50">Ret.<br>PIE</td>
		<td width="50">Ret.<br>CHA</td>
		<td width="70px">Total<br>Incidencia</td>
            </tr>';
   $contador = 1;
   $p=array();
   //////// incio del reporte de datos ////////
   $sql_cargos = "select * from cargos c, personal p, sueldos s, incidencias_sueldos i where c.cod_dep=".$reg2["cod_dep"]." AND c.ced_per=p.ced_per AND c.cod_sue = s.cod_sue AND c.cod_sue = i.cod_sue AND i.cod_inc=".$row['cod_inc'];
   $busq_cargos = mysql_query($sql_cargos);
   while ($reg_cargos=mysql_fetch_array($busq_cargos)) {
	$busq_sueldosa = "select * from nomina_pagar where cod_car=".$reg_cargos["cod_car"]." AND ced_per=".$reg_cargos["ced_per"];
	$busq_sueldosa2 = mysql_query($busq_sueldosa);
	$sa = array();
	while ($reg_sueldosa=@mysql_fetch_array($busq_sueldosa2)) { 
		if ($reg_sueldosa['mes_nom']>=$mes_inicio&&$reg_sueldosa['mes_nom']<=$mes_fin)
		{
			$sa[0]+=$reg_sueldosa["mon_asi"];
			$sa[1]+=$reg_sueldosa["sso_des"];
			$sa[2]+=$reg_sueldosa["spf_des"];
			$sa[3]+=$reg_sueldosa["cah_des"];
		} 
	}
   echo '<tr class="etiquetas_nomina">
      <td align="right">'.$contador.'</td>
      <td align="center">'.$reg_cargos["fch_asg"].'</td>
      <td>'.$reg_cargos["ape_per"].' '.$reg_cargos["nom_per"].'</td>
      <td align="right">'.redondear($reg_cargos["ced_per"],0,'.','').'</td>
      <td>'.$reg_cargos["nom_car"].'</td>
      <td align="right">'.redondear($sa[0],2,'.',',').'</td>
      <td align="right">'; $incidencia = redondear(($sa[0]*$reg_cargos["mnt_inc"]/100),2,'','.'); echo redondear($incidencia,2,'.',',');  echo '</td>
      <td align="right">'; $ssoap = redondear(($sa[0]*$APSSO*($reg_cargos["mnt_inc"]/100)),2,'','.'); echo redondear($ssoap,2,'.',','); echo '</td>
      <td align="right">'; $spfap = redondear(($sa[0]*$APSPF*($reg_cargos["mnt_inc"]/100)),2,'','.'); echo redondear($spfap,2,'.',','); echo '</td>
      <td align="right">'; $cahap = redondear(($sa[0]*$APCAH*($reg_cargos["mnt_inc"]/100)),2,'','.'); echo redondear($cahap,2,'.',','); echo '</td>
      <td align="right">'; $ssop = redondear(($sa[1]*$reg_cargos["mnt_inc"]/100),2,'','.'); echo redondear($ssop,2,'.',','); echo '</td>
      <td align="right">'; $spfp = redondear(($sa[2]*$reg_cargos["mnt_inc"]/100),2,'','.'); echo redondear($spfp,2,'.',','); echo '</td>
      <td align="right">'; $cahp = redondear(($sa[3]*$reg_cargos["mnt_inc"]/100),2,'','.'); echo redondear($cahp,2,'.',','); echo '</td>
      <td align="right">'; $monto_incidencia = $incidencia + $ssoap + $spfap + $cahap; echo redondear($monto_incidencia,2,'.',','); echo '</td>
   </tr>';
   $p[0] += $sa[0];
   $p[1] += $incidencia;
   $p[2] += $ssoap;
   $p[3] += $spfap;
   $p[4] += $cahap;
   $p[5] += $ssop;
   $p[6] += $spfp;
   $p[7] += $cahp;
   $p[8] += $monto_incidencia;
   $contador++;
   }
   echo '<tr class="etiquetas_nomina"><td colspan="5" align="right">TOTAL</td>
	<td align="right">'.redondear($p[0],2,'.',',').'</td>
	<td align="right">'.redondear($p[1],2,'.',',').'</td>
      	<td align="right">'.redondear($p[2],2,'.',',').'</td>
     	<td align="right">'.redondear($p[3],2,'.',',').'</td>
      	<td align="right">'.redondear($p[4],2,'.',',').'</td>
    	<td align="right">'.redondear($p[5],2,'.',',').'</td>
        <td align="right">'.redondear($p[6],2,'.',',').'</td>
        <td align="right">'.redondear($p[7],2,'.',',').'</td>
	<td align="right">'.redondear($p[8],2,'.',',').'</td></tr>';
   //////////////////////////////////////////// 
   echo '</table>';
   echo '</td></tr>';
}
?>
</table>
