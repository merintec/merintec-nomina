<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'retenciones_isrl.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "retenciones_isrl";		// nombre de la tabla
$vista = "vista_retenciones_isrl";
$ncampos = "5";		            //numero de campos del formulario
$datos[0] = crear_datos ("cod_ret_isrl","C�digo",$_POST['cod_ret_isrl'],"0","11","numericos");
$datos[1] = crear_datos ("fch_ret_isrl","Fecha",$_POST['fch_ret_isrl'],"10","10","fecha");
$datos[2] = crear_datos ("com_ret_isrl","Num Comprobante",$_POST['com_ret_isrl'],"0","8","alfanumericos");
$datos[3] = crear_datos ("egr_ret_isrl","N� formulario de Orden de Servicio",$_POST['egr_ret_isrl'],"0","7","numericos");
$datos[4] = crear_datos ("obs_ret_isrl","Observaci�n",$_POST['obs_ret_isrl'],"0","255","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],$tabla,$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Num Comprobante";
		$datos[0]="com_ret_isrl";
		$parametro[1]="Fecha";
		$datos[1]="fch_ret_isrl";
		$parametro[2]="Proveedor";
		$datos[2]="nom_pro";
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_ret_isrl");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ret_isrl = $row["cod_ret_isrl"];
	    $fch_ret_isrl = $row["fch_ret_isrl"];
	    $com_ret_isrl = $row["com_ret_isrl"];
	    $egr_ret_isrl = $row["egr_ret_isrl"];
	    $obs_ret_isrl = $row["obs_ret_isrl"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ret_isrl",$_POST["cod_ret_isrl"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	$sql = "SELECT MAX(com_ret_isrl) AS com_ret_isrl FROM retenciones_isrl WHERE YEAR(fch_ret_isrl)=";
	$sql .= $_POST["fch_ret_isrl"][0].$_POST["fch_ret_isrl"][1].$_POST["fch_ret_isrl"][2].$_POST["fch_ret_isrl"][3];
    	$res = mysql_fetch_array(mysql_query($sql));
    	$com_ret_isrl = $res['com_ret_isrl'] + 1;
	$datos[2]  = crear_datos ("com_ret_isrl","Num Comprobante",$com_ret_isrl,"0","4","numericos");
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ret_isrl"],"cod_ret_isrl",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Comprobantes de Retenci�n de ISRL </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">N� de Comprobante:</td>
                        <td width="75%">
			<input name="cod_ret_isrl" type="hidden" id="cod_ret_isrl" value="<?php if(! $existe) { echo $_POST['cod_ret_isrl']; } else { echo $cod_ret_isrl; } ?>" size="35" title="Codigo de la Orden de Compra/Servicio">
			<input name="com_ret_isrl" type="hidden" id="com_ret_isrl" value="<?php if(! $existe) { echo $_POST['com_ret_isrl']; } else { echo $com_ret_isrl; } ?>" size="35" title="Num Orden">
			<?php if ($boton != "Verificar" && $boton != "Guardar") { echo $com_ret_isrl.'-'.substr($fch_ret_isrl, 0, 4); } else echo 'Por Asignar...'; ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha:</td>
                        <td><input name="fch_ret_isrl" type="<?php if ($boton!='Verificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_ret_isrl" value="<?php if(! $existe) { echo $_POST['fch_ret_isrl']; } else { echo $fch_ret_isrl; } ?>" size="20" title="Fecha de Registro de la Orden" />
                          <?php if ($boton=='Modificar') { echo $fch_ret_isrl; } ?>
			  <?php if ($boton=='Actualizar' || $boton=='Guardar') { echo $_POST['fch_ret_isrl']; } ?>
			<?php if ($boton=='Verificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_ret_isrl,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� Comprobante de Egreso:                          </td>
			            <td>
                        <input name="egr_ret_isrl" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="egr_ret_isrl" value="<?php if(! $existe) { echo $_POST['egr_ret_isrl']; } else { echo $egr_ret_isrl; } ?>" size="5" maxlength="7" title="N� del Formulario del Comprobante de Egreso">
                        <?php if ($boton=='Modificar') { echo $egr_ret_isrl; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaci&oacute;n:                          </td>
			<td>
                        <input name="obs_ret_isrl" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_ret_isrl" value="<?php if(! $existe) { echo $_POST['obs_ret_isrl']; } else { echo $obs_ret_isrl; } ?>" size="35" title="Observaciones de la Orden">
                        <?php if ($boton=='Modificar') { echo $obs_ret_isrl; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
		  <tr>
                    <td align="center"><?php if($boton=='Modificar'){ abrir_ventana('imprimir_retenciones_isrl.php','v_imprimir','Imprimir Comprobante de Retenci�n ISRL',"cod_ret_isrl=".$cod_ret_isrl); } ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =2; 
						$criterios[0] = "Comprobante"; 
						$campos[0] ="com_ret_isrl";
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[1] = "Fecha ".$adicionaL_bus; 
						$campos[1] ="fch_ret_isrl";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
				   	   }  ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
