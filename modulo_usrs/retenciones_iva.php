<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'retenciones_iva.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "retenciones_iva";		// nombre de la tabla
$ncampos = "5";		            //numero de campos del formulario
$datos[0] = crear_datos ("cod_ret_iva","C�digo",$_POST['cod_ret_iva'],"0","11","numericos");
$datos[1] = crear_datos ("fch_ret_iva","Fecha",$_POST['fch_ret_iva'],"10","10","fecha");
$datos[2] = crear_datos ("com_ret_iva","Num Comprobante",$_POST['com_ret_iva'],"0","8","alfanumericos");
$datos[3] = crear_datos ("egr_ret_iva","N� formulario del Comprobante de Egreso",$_POST['egr_ret_iva'],"0","6","numericos");
$datos[4] = crear_datos ("obs_ret_iva","Observaci�n",$_POST['obs_ret_iva'],"0","255","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Num Comprobante";
		$datos[0]="com_ret_iva";
		$parametro[1]="Fecha";
		$datos[1]="fch_ret_iva";
		$parametro[2]="RIF Proveedor";
		$datos[2]="rif_pro";
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_ret_iva");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ret_iva = $row["cod_ret_iva"];
	    $fch_ret_iva = $row["fch_ret_iva"];
	    $com_ret_iva = $row["com_ret_iva"];
	    $rif_pro = $row["rif_pro"];
	    $apl_ret_iva = $row["apl_ret_iva"];
	    $ret_ret_iva = $row["ret_ret_iva"];
	    $egr_ret_iva = $row["egr_ret_iva"];
	    $obs_ret_iva = $row["obs_ret_iva"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ret_iva",$_POST["cod_ret_iva"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	$sql = "SELECT MAX(com_ret_iva) AS com_ret_iva FROM retenciones_iva WHERE YEAR(fch_ret_iva)=";
	$sql .= $_POST["fch_ret_iva"][0].$_POST["fch_ret_iva"][1].$_POST["fch_ret_iva"][2].$_POST["fch_ret_iva"][3];
    	$res = mysql_fetch_array(mysql_query($sql));
    	$com_ret_iva = $res['com_ret_iva'] + 1;
	$datos[2]  = crear_datos ("com_ret_iva","Num Comprobante",$com_ret_iva,"0","4","numericos");
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ret_iva"],"cod_ret_iva",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Comprobantes de Retenci�n de IVA </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">N� de Comprobante:</td>
                        <td width="75%">
			<input name="cod_ret_iva" type="hidden" id="cod_ret_iva" value="<?php if(! $existe) { echo $_POST['cod_ret_iva']; } else { echo $cod_ret_iva; } ?>" size="35" title="Codigo de la Orden de Compra/Servicio">
			<input name="com_ret_iva" type="hidden" id="com_ret_iva" value="<?php if(! $existe) { echo $_POST['com_ret_iva']; } else { echo $com_ret_iva; } ?>" size="35" title="Num Orden">
			<?php if ($boton != "Verificar" && $boton != "Guardar") { echo substr($fch_ret_iva, 0, 4).'-'.substr($fch_ret_iva, 5, 2).'-'.$com_ret_iva; } else echo 'Por Asignar...'; ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha:</td>
                        <td><input name="fch_ret_iva" type="<?php if ($boton!='Verificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_ret_iva" value="<?php if(! $existe) { echo $_POST['fch_ret_iva']; } else { echo $fch_ret_iva; } ?>" size="20" title="Fecha de Registro de la Orden" />
                          <?php if ($boton=='Modificar') { echo $fch_ret_iva; } ?>
			  <?php if ($boton=='Actualizar' || $boton=='Guardar') { echo $_POST['fch_ret_iva']; } ?>
			<?php if ($boton=='Verificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_ret_iva,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Comprobante de Egreso:                          </td>
			            <td>
                        <input name="egr_ret_iva" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="egr_ret_iva" value="<?php if(! $existe) { echo $_POST['egr_ret_iva']; } else { echo $egr_ret_iva; } ?>" size="5" maxlength="7" title="N� del Formulario del Comprobante de Egreso">
                        <?php if ($boton=='Modificar') { echo $egr_ret_iva; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaci&oacute;n:                          </td>
			<td>
                        <input name="obs_ret_iva" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_ret_iva" value="<?php if(! $existe) { echo $_POST['obs_ret_iva']; } else { echo $obs_ret_iva; } ?>" size="35" title="Observaciones de la Orden">
                        <?php if ($boton=='Modificar') { echo $obs_ret_iva; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
		  <tr>
                    <td align="center"><?php if($boton=='Modificar'){ abrir_ventana('imprimir_retenciones_iva.php','v_imprimir','Imprimir Comprobante de Retenci�n IVA',"cod_ret_iva=".$cod_ret_iva); } ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =3; 
						$criterios[0] = "Comprobante"; 
						$campos[0] ="com_ret_iva";
						$criterios[1] = "A�o"; 
						$campos[1] ="YEAR(fch_ret_iva)";
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[2] = "Fecha ".$adicionaL_bus; 
						$campos[2] ="fch_ret_iva";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
				   	   }  ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
