<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Montos del Pago de Medicinas</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<script src="../js/jquery.js"> </script>
<?php
$viene_val = $_GET['cod_ree'];
$cod_ree = $_GET['cod_ree'];
$cod_tcar = $_POST['cod_tcar'];
$cod_car = $_POST['cod_car'];
$cod_cob = $_POST['cod_cob'];
$fec_fac = $_POST['fec_fac'];

include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);

$boton = "Verificar";
$existe = '';
$pagina = 'reembolsos_med.php?cod_ree='.$_GET["cod_ree"].'&seccion='.$_GET["seccion"];
$pagina2 = 'reembolsos_med.php?cod_ree='.$_GET["cod_ree"].'&seccion='.$_GET["seccion"];
$tabla = "reembolsos_med";	// nombre de la tabla
$ncampos = "17";			//numero de campos del formulario

$datos_car=buscar_campo('*', 'cargos', 'WHERE cod_car='.$cod_car);
$datos_per=buscar_campo('*', 'personal', 'WHERE ced_per='.$datos_car['ced_per']);
$datos_cue=buscar_campo('*', 'cuentas', 'WHERE ced_per='.$datos_car['ced_per']);
if(!$datos_cue['num_cue']){ $datos_cue['num_cue']="Cheque"; }

$datos[0] = crear_datos ("ced_per","Personal",$datos_car['ced_per'],"1","11","numericos");
$datos[1] = crear_datos ("cod_car","Codigo de cargo",$_POST['cod_car'],"1","11","numericos");
$datos[2] = crear_datos ("cod_cob","Codigo de cobertura",$_POST['cod_cob'],"1","11","numericos");
$datos[3] = crear_datos ("fec_reg","Fecha Registro",$_POST['fec_reg'],"10","10","fecha");
$datos[4] = crear_datos ("nom_pro","Nombre Proveedor",$_POST['nom_pro'],"1","50","alfanumericos");
$datos[5] = crear_datos ("rif_pro","Rif Proveedor",$_POST['rif_pro'],"1","11","alfanumericos");
$datos[6] = crear_datos ("tel_pro","Telefono Proveedor",$_POST['tel_pro'],"1","11","numericos");
$datos[7] = crear_datos ("nro_fac","Numero de Factura",$_POST['nro_fac'],"1","11","numericos");
$datos[8] = crear_datos ("fec_fac","Fecha Factura",$_POST['fec_fac'],"10","10","fecha");
$datos[9] = crear_datos ("mon_fac","Monto de Factura",$_POST['mon_fac'],"1","11","decimal");
$datos[10] = crear_datos ("mon_pag","Monto a Pagar",$_POST['mon_pag'],"1","11","decimal");
$datos[11] = crear_datos ("nom_per","Nombre Empleado",$datos_per['nom_per'],"1","50","alfanumericos");
$datos[12] = crear_datos ("ape_per","Apellido Empleado",$datos_per['ape_per'],"1","50","alfanumericos");
$datos[13] = crear_datos ("nom_car","Cargo",$datos_car['nom_car'],"1","100","alfanumericos");
$datos[14] = crear_datos ("num_cue","Numero de Cuenta",$datos_cue['num_cue'],"1","20","alfanumericos");
$datos[15] = crear_datos ("tip_cue","Tipo de Cuenta",$datos_cue['tip_cue'],"0","1","numericos");
$datos[16] = crear_datos ("ban_cue","Banco de Cuenta",$datos_cue['ban_cue'],"0","50","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"vista_reembolsos_per",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Cedula";
		$datos[0]="ced_per";
		$parametro[1]="Nombre";
		$datos[1]="nombre_per";
		$parametro[2]="Cargo";
		$datos[2]="nom_car";
		$parametro[3]="Factura";
		$datos[3]="nro_fac";
		$parametro[4]="Fecha";
		$datos[4]="fec_fac";
		$parametro[5]="Monto Factura";
		$datos[5]="mon_fac";		
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_ree");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ree = $row["cod_ree"];
	    $ced_per = $row["ced_per"];
	    $cod_car = $row["cod_car"];
		$cod_cob = $row["cod_cob"];
		$fec_reg = $row["fec_reg"];
		$nom_pro = $row["nom_pro"];
		$rif_pro = $row["rif_pro"];
		$tel_pro = $row["tel_pro"];
		$nro_fac = $row["nro_fac"];
		$fec_fac = $row["fec_fac"];
		$mon_fac = $row["mon_fac"];
		$mon_pag = $row["mon_pag"];
		$codigo_tcar=buscar_campo('cod_tcar', 'cargos', 'WHERE cod_car='.$cod_car);
		$cod_tcar=$codigo_tcar["cod_tcar"];
	  	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}

if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ree",$_POST["cod_ree"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ree"],"cod_ree",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_ree","reembolsos_med",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Asignar Reembolsos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Tipo de Personal:
                          </td>
		            <td><input name="cod_ree" type="hidden" id="cod_ree" value="<?php if (! $existe) { echo $_POST['cod_ree']; } else { echo $cod_ree; } ?>" title="Codigo del pago de medicinas al personal">
                        <?php combo('cod_tcar', $cod_tcar, 'tipos_cargos', $link, 0, 0, 1, "", 'cod_tcar', 'onChange="submit();"', $boton,'', ''); ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Trabajador:
                          </td>
		            <td><?php combo('cod_car', $cod_car, 'vista_cargos_per', $link, 2, 1, '9:::10', 6, 'ced_per', ' onChange="calcular_reembolso();" ', $boton, "WHERE cod_tcar='".$cod_tcar."' ORDER BY ape_per, nom_per", "");?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Tipo de Cobertura:
                          </td>
		            <td><?php combo('cod_cob', $cod_cob, 'coberturas_med', $link, 0, 0, 1, '', 'cod_cob', ' onChange="calcular_reembolso();" ', $boton, "WHERE cod_tcar='".$cod_tcar."' ORDER BY nom_cob", "");?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Registro:</td>
                        <td width="75%">
						  <input name="fec_reg" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fec_reg" value="<?php if(!$existe) { if($_POST['fec_reg']){ echo $_POST['fec_reg']; }else{ echo date('Y-m-d'); } } else { echo $fec_reg; } ?>" readonly="readonly" size="10" maxlength="10" title="Fecha de registro" ><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onClick="displayCalendar(document.forms[0].fec_reg,'yyyy-mm-dd',this)"  title="Haga click aqui para elegir una fecha"><?PHP } ?>
						  <?php if ($boton=='Modificar') { echo $fec_reg; } ?></td>
                      </tr>			
                      <tr>
                        <td width="25%" class="etiquetas">Factura:</td>
                        <td width="75%">
                        <input name="nro_fac" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nro_fac" value="<?php if(!$existe) { echo $_POST['nro_fac']; } else { echo $nro_fac; } ?>" size="10" maxlength="30" title="Numero de la factura a reembolsar">
                        <?php if ($boton=='Modificar') { echo $nro_fac; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha Factura:</td>
                        <td width="75%">
						  <input name="fec_fac" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fec_fac" value="<?php if(!$existe) { echo $_POST['fec_fac']; } else { echo $fec_fac; } ?>" readonly="readonly" size="10" maxlength="10" title="Fecha de la factura" ><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onClick="displayCalendar(document.forms[0].fec_fac,'yyyy-mm-dd',this)"  title="Haga click aqui para elegir una fecha"><?PHP } ?>
						  <?php if ($boton=='Modificar') { echo $fec_fac; } ?></td>
                      </tr>
      <script type="text/javascript">
        function calcular_reembolso(){
          var parametros = {
		  	"cod_car" : $("#cod_car").val(),
			"fec_fac" : $("#fec_fac").val(),
		    "cod_cob" : $("#cod_cob").val(),
			"mon_fac" : $("#mon_fac").val()
          }
          var url="../modulo_usrs/reembolsos_calcular.php"; 
          $.ajax
          ({
              type: "POST",
              url: url,
              data: parametros,
              beforeSend: function(){
              },
              success: function(data)
              {
			  	
                var codigo, datatemp, mensaje;
                datatemp=data;
                datatemp=datatemp.split(":::");
				$("#mon_pag").val(datatemp[1]);					 
				if(datatemp[1]<=0){
					$("#mon_pag").val(""); 
					$("#mensaje").html(datatemp[2]);
					$("#mon_fac").val("");
				}else{
					$("#mensaje").html("");
				}      
              }
          });
          return false;
        }
      </script>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Factura:</td>
                        <td width="75%">
                        <input name="mon_fac" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_fac" value="<?php if(!$existe) { echo $_POST['mon_fac']; } else { echo $mon_fac; } ?>" size="10" maxlength="30"title="Monto de la factura a reembolsar" onKeyUp="calcular_reembolso();">
                        <?php if ($boton=='Modificar') { echo $mon_fac; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto a Pagar:</td>
                        <td width="75%">
                         <input name="mon_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_pag" value="<?php if(!$existe) { echo $_POST['mon_pag']; } else { echo $mon_pag; } ?>" readonly="readonly" size="10" maxlength="30"title="Monto a pagar sobre calculo de cobertura">
                        <?php if ($boton=='Modificar') { echo $mon_pag; } ?><span id="mensaje"></span></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Proveedor/Empresa:</td>
                        <td width="75%">
                        <input name="nom_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_pro" value="<?php if(!$existe) { echo $_POST['nom_pro']; } else { echo $nom_pro; } ?>" size="35" maxlength="50" title="Nombre de empresa o proveedor de la factura">
                        <?php if ($boton=='Modificar') { echo $nom_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Rif:</td>
                        <td width="75%">
                        <input name="rif_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rif_pro" value="<?php if(!$existe) { echo $_POST['rif_pro']; } else { echo $rif_pro; } ?>" size="10" maxlength="30"title="Rif de empresa o proveedor de la factura">
                        <?php if ($boton=='Modificar') { echo $rif_pro; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Telefono:</td>
                        <td width="75%">
                        <input name="tel_pro" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tel_pro" value="<?php if(!$existe) { echo $_POST['tel_pro']; } else { echo $tel_pro; } ?>" size="10" maxlength="30"title="Telefono de empresa o proveedor de la factura">
                        <?php if ($boton=='Modificar') { echo $tel_pro; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr> 
                  <tr>
                    <td>
			<?php 
			$ncriterios =4;
			$criterios[0]="Cedula";
			$campos[0]="ced_per";
			$criterios[1]="Nombre";
			$campos[1]="nombre_per";
			$criterios[2]="Cargo";
			$campos[2]="nom_car";
			$criterios[3]="Factura";
			$campos[3]="nro_fac";
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
