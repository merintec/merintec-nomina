<?php include('../comunes/conexion_basedatos.php'); ?>
<?PHP
	include ('../comunes/formularios_funciones.php');
	
	$cod_cob=$_POST['cod_cob'];
	$mon_fac=$_POST['mon_fac'];
	$cod_car=$_POST['cod_car'];
	$fec_fac=$_POST['fec_fac'];
	
	$cedula_per=buscar_campo('ced_per', 'cargos', 'WHERE cod_car='.$cod_car);
	$ced_per=$cedula_per['ced_per'];
	
	$reg_cob=buscar_campo('*', 'coberturas_med', 'WHERE cod_cob='.$cod_cob);
	
	$fecha= explode("-",$fec_fac);
	
	// la base es un monto unico anual
	if($reg_cob['bas_cob']==0){
		if($fec_fac && $mon_fac && $cod_car && $cod_cob){
			$sum_pag_anno=buscar_campo('SUM(mon_pag) as mon_pag', 'reembolsos_med', 'WHERE cod_cob='.$cod_cob.' and ced_per='.$ced_per.' and YEAR(fec_fac)='.$fecha[0]);
			$mon_queda = ($reg_cob['mon_cob']-$sum_pag_anno['mon_pag']);
			if($mon_queda>=$mon_fac){
				$mon_pag=$mon_fac;
			}elseif($mon_queda>0){
				$mon_pag=$mon_queda;
			}else{
				$mon_pag=0;
			}
			if($mon_pag==0){
				$msj="Monto Cero o Error de calculo";
			}	
		}else{
			$msj="Faltan datos para calcular";
		}
	}
	
	// la base es un porcentaje mensual
	if($reg_cob['bas_cob']==1){
		if($mon_fac && $cod_car && $cod_cob){
			$mon_pag=($mon_fac*($reg_cob['mon_cob']/100));
		}else{
			$msj="Faltan datos para calcular";
		}
	}
	
	// la base es un monto mensual	
	if($reg_cob['bas_cob']==2){ 
		if($fec_fac && $mon_fac && $cod_car && $cod_cob){
			$sum_pag_mees=buscar_campo('SUM(mon_pag) as mon_pag', 'reembolsos_med', 'WHERE cod_cob='.$cod_cob.' and ced_per='.$ced_per.' and YEAR(fec_fac)='.$fecha[0].' and MONTH(fec_fac)='.$fecha[1]);
			$mon_queda = ($reg_cob['mon_cob']-$sum_pag_mees['mon_pag']);
			if($mon_queda>=$mon_fac){
				$mon_pag=$mon_fac;
			}elseif($mon_queda>0){
				$mon_pag=$mon_queda;
			}else{
				$mon_pag=0;
			}
			if($mon_pag==0){
				$msj="Monto Cero o Error de calculo";
			}
		}else{
			$msj="Faltan datos para calcular";
		}		
	}	
	
	
	echo ":::".number_format($mon_pag,2,".","").":::".$msj;
?>
