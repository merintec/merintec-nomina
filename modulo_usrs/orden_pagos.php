<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'orden_pagos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "pagos";		// nombre de la tabla
$ncampos = "10";		    //numero de campos del formulario
$datos[0]  = crear_datos ("cod_pag","C�digo",$_POST['cod_pag'],"0","11","numericos");
$datos[1]  = crear_datos ("fch_pag","Fecha",$_POST['fch_pag'],"10","10","fecha");
$datos[2]  = crear_datos ("nor_pag","Num Orden",$_POST['nor_pag'],"0","4","numericos");
$datos[3]  = crear_datos ("frm_pag","Num Formulario",$_POST['frm_pag'],"0","6","numericos");
$datos[4]  = crear_datos ("frm_com","Num Formulario",$_POST['frm_com'],"0","6","numericos");
$datos[5] = crear_datos ("mon_pag","Monto Total",$_POST['mon_pag'],"1","12","decimal");
$datos[6] = crear_datos ("ela_pag","Elaborado",$_POST['ela_pag'],"1","50","alfanumericos");
$datos[7] = crear_datos ("rev_pag","Revisado",$_POST['rev_pag'],"1","50","alfanumericos");
$datos[8] = crear_datos ("apr_pag","Aprobado",$_POST['apr_pag'],"1","50","alfanumericos");
$datos[9] = crear_datos ("obs_pag","Observaci�n",$_POST['obs_pag'],"0","255","alfanumericos");

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Num Formulario";
		$datos[0]="frm_pag";
		$parametro[1]="Num Compromiso";
		$datos[1]="frm_com";
		$parametro[2]="Fecha";
		$datos[2]="fch_pag";
		$parametro[3]="Monto";
		$datos[3]="mon_pag";
		busqueda_varios(6,$buscando,$datos,$parametro,"cod_pag");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pag = $row["cod_pag"];
	    $fch_pag = $row["fch_pag"];
	    $nor_pag = $row["nor_pag"];
   	    $frm_pag = $row["frm_pag"];
   	    $frm_com = $row["frm_com"];
	    $mon_pag = $row["mon_pag"];
	    $ela_pag = $row["ela_pag"];
	    $rev_pag = $row["rev_pag"];
	    $apr_pag = $row["apr_pag"];
	    $obs_pag = $row["obs_pag"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	    ///  verificamos si ya existen comprobante de egreso para el presente pago
	    $sql_verifica = "select * from pagos where frm_pag=".$frm_pag." AND frm_egr!='000000'";
        $sql_bus_verifica = mysql_query($sql_verifica);
        if ($res_verifica=mysql_fetch_array($sql_bus_verifica)){
            $permitir_modificar='NO';
        }

        /// verificamos si tiene facturas asociadas
		$sql_rela = "SELECT * FROM facturas_pagos WHERE cod_pag=".$cod_pag;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}

        /// verificamos si tiene partidas asociadas
		$sql_rela = "SELECT * FROM partidas_pagos WHERE cod_pag=".$cod_pag ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}

	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	//// Validanos que el monto total de las ordenes de pago relacionadas con el compromiso no sea mayor al monto comprometido
	//// 1.- traemos el monto del compromiso 
	if ($validacion) { 
	    $sql_verifica="SELECT * FROM compras WHERE frm_com=".$_POST['frm_com'];
	    $sql_bus_ver= mysql_fetch_array(mysql_query($sql_verifica));
		$compromiso = $sql_bus_ver['mon_com'];
		///// 2.- Traemos el monto total de los pagos relacionados con el compromiso
		$sql_verifica="SELECT sum(mon_pag) as pagos FROM pagos WHERE frm_com=".$_POST['frm_com'];
	    $sql_bus_ver= mysql_fetch_array(mysql_query($sql_verifica));
		$pagos = $sql_bus_ver['pagos'];
		///// 3.- Calculamos el monto restante por pagar
        $restar_actual=0;
	    if ($_POST["frm_com"]==$_POST["com_ant"])
	    {
	        $restar_actual = $_POST["mon_ant"]; 
	    }
		$disponible = $compromiso - $pagos + $restar_actual;
		///// 4.- Verificamos que el monto a pagar no sea mayor al disponible
		if ($_POST['mon_pag'] > $disponible) {
		    echo '<SCRIPT>alert("'.$msg_pago_mayor.' (Solo restan Bs. '.$disponible.' por pagar)'.$msg_partidas_mayor2.'");</SCRIPT>';
		    $validacion = '';
		}
		else { $validacion = 1; }
	}
	///// Si se cambia el numero del formulario se verifica que sea el siguiente numero disponible (esto para casos de reimpresi�n del documento)
	if ($validacion)
	{
	    if ($_POST['frm_pag'] != $_POST['pag_ant'])
	    {
	        $sql = "SELECT MAX(frm_pag) AS frm_pag FROM pagos";
            $res = mysql_fetch_array(mysql_query($sql));
            $frm_next = $res['frm_pag'] + 1;
	        if ($frm_next != $_POST['frm_pag'])
            {
                echo '<SCRIPT>alert("'.$msg_pago_cambiofrm.' ('.$frm_next.')");</SCRIPT>';
		        $validacion = '';                
            }
            else { $validacion = 1; }
	    }
	}
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pag",$_POST["cod_pag"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$sql_verifica = "select * from pagos where frm_pag =".$frm_pag." AND frm_egr!='000000'";
    $sql_bus_verifica = mysql_query($sql_verifica);
    if ($res_verifica=mysql_fetch_array($sql_bus_verifica)){
        echo '<SCRIPT> alert ("'.$msg_pago_modificar_no.'"); </SCRIPT>';
        $boton = "Modificar";
        $permitir_modificar='NO';
    }
    else {
        $boton = "Actualizar";
    }
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	//// Validanos que el monto total de las ordenes de pago relacionadas con el compromiso no sea mayor al monto comprometido
	//// 1.- traemos el monto del compromiso 
	if ($validacion) { 
	    $sql_verifica="SELECT * FROM compras WHERE frm_com=".$_POST['frm_com'];
	    $sql_bus_ver= mysql_fetch_array(mysql_query($sql_verifica));
		$compromiso = $sql_bus_ver['mon_com'];
		///// 2.- Traemos el monto total de los pagos relacionados con el compromiso
		$sql_verifica="SELECT sum(mon_pag) as pagos FROM pagos WHERE frm_com=".$_POST['frm_com'];
	    $sql_bus_ver= mysql_fetch_array(mysql_query($sql_verifica));
		$pagos = $sql_bus_ver['pagos'];
		///// 3.- Calculamos el monto restante por pagar
		$disponible = $compromiso - $pagos;
		///// 4.- Verificamos que el monto a pagar no sea mayor al disponible
		if ($_POST['mon_pag'] > $disponible) {
		    echo '<SCRIPT>alert("'.$msg_pago_mayor.' (Solo restan Bs. '.$disponible.' por pagar)'.$msg_partidas_mayor2.'");</SCRIPT>';
		    $validacion = '';
		}
		else { $validacion = 1; }
	}
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
    //// Generar numero de control
	$sql = "SELECT MAX(nor_pag) AS nor_pag FROM pagos WHERE YEAR(fch_pag)=";
	$sql .= $_POST["fch_pag"][0].$_POST["fch_pag"][1].$_POST["fch_pag"][2].$_POST["fch_pag"][3];
    	$res = mysql_fetch_array(mysql_query($sql));
    	$nor_pag = $res['nor_pag'] + 1;
	$datos[2]  = crear_datos ("nor_pag","Num Orden",$nor_pag,"0","4","numericos");
	
    //// Generar numero de formulario
	$sql = "SELECT MAX(frm_pag) AS frm_pag FROM pagos";
    $res = mysql_fetch_array(mysql_query($sql));
    $frm_pag = $res['frm_pag'] + 1;
	$datos[3]  = crear_datos ("frm_pag","Num Formulario",$frm_pag,"0","6","numericos");
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	$sql_verifica = "select * from pagos where frm_pag =".$frm_pag." AND frm_egr!='000000'";
    $sql_bus_verifica = mysql_query($sql_verifica);
    if ($res_verifica=mysql_fetch_array($sql_bus_verifica)){
        echo '<SCRIPT> alert ("'.$msg_pago_eliminar_no.'"); </SCRIPT>';
        $boton = "Modificar";
        $permitir_modificar='NO';
    }
    else {
    	eliminar_func($_POST["cod_pag"],"cod_pag",$tabla,$pagina);
    	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
    	return;
    }
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
        /// verificamos si tiene facturas asociadas
		$sql_rela = "SELECT * FROM facturas_pagos WHERE cod_pag=". $cod_pag ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}

        /// verificamos si tiene partidas asociadas
		$sql_rela = "SELECT * FROM partidas_pagos WHERE cod_pag=". $cod_pag ;
           if (mysql_fetch_array(mysql_query($sql_rela))){
			$prm[3]='';
		}
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de �rdenes de Pago</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">N&uacute;mero de Control:</td>
                        <td width="75%">
			<input name="cod_pag" type="hidden" id="cod_pag" value="<?php if(! $existe) { echo $_POST['cod_pag']; } else { echo $cod_pag; } ?>" size="35" title="Codigo de la Orden de Pago">
			<input name="nor_pag" type="hidden" id="nor_pag" value="<?php if(! $existe) { echo $_POST['nor_pag']; } else { echo $nor_pag; } ?>" size="35" title="Num Orden"><?php if ($boton != "Verificar" && $boton != "Guardar") { echo substr($fch_pag, 5, 2).substr($fch_pag, 2, 2).$nor_pag; } else echo 'Por Asignar...'; ?>                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">N� Formulario:</td>
                        <td width="75%">
			                <input name="frm_pag" type="<?php if ($boton == 'Verificar' || $boton=='Modificar' || $boton=='Guardar'){ echo 'hidden';} else {echo 'text';} ?>" id="frm_pag" value="<?php if(! $existe) { echo $_POST['frm_pag']; } else { echo $frm_pag; } ?>" size="15" title="Numero de Formulario"><?php if ($boton == "Modificar") { echo $frm_pag; } if ($boton == "Verificar" || $boton == "Guardar") { echo 'Por Asignar...'; }?>
                            <input type="hidden" name="pag_ant" id="pag_ant" value="<?php if ($existe) { echo $frm_pag;} else { echo $_POST["pag_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N� Compromiso:                          </td>
			            <td>
                            <input name="frm_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="frm_com" value="<?php if(! $existe) { echo $_POST['frm_com']; } else { echo $frm_com; } ?>" size="15" maxlength="6" title="Num de Compromiso">
                            <?php if ($boton=='Modificar') { echo $frm_com; } ?>
                            <input type="hidden" name="com_ant" id="com_ant" value="<?php if ($existe) { echo $frm_com;} else { echo $_POST["com_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha:</td>
                        <td><input name="fch_pag" type="<?php if ($boton!='Verificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_pag" value="<?php if(! $existe) { echo $_POST['fch_pag']; } else { echo $fch_pag; } ?>" size="20" title="Fecha de Registro de la Orden" />
                          <?php if ($boton=='Modificar') { echo $fch_pag; } ?>
			  <?php if ($boton=='Actualizar' || $boton=='Guardar') { echo $_POST['fch_pag']; } ?>
			<?php if ($boton=='Verificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_pag,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto Total:                          </td>
			            <td>
                            <input name="mon_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_pag" value="<?php if(! $existe) { echo $_POST['mon_pag']; } else { echo $mon_pag; } ?>" size="15" title="Monto Total de la transacci�n">
                            <?php if ($boton=='Modificar') { echo $mon_pag.' Bs.'; } ?>
                            <input type="hidden" name="mon_ant" id="mon_ant" value="<?php if ($existe) { echo $mon_pag;} else { echo $_POST["mon_ant"]; }?>">
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Elaborado:</td>
			<td>
                        <input name="ela_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ela_pag" value="<?php if(! $existe) { echo $_POST['ela_pag']; } else { echo $ela_pag; } ?>" size="35" title="Persona que Elabora la Orden">
                        <?php if ($boton=='Modificar') { echo $ela_pag; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Revisado:</td>
			<td>
                        <input name="rev_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="rev_pag" value="<?php if(! $existe) { echo $_POST['rev_pag']; } else { echo $rev_pag; } ?>" size="35" title="Persona que Revisa la Orden">
                        <?php if ($boton=='Modificar') { echo $rev_pag; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Aprobado:</td>
			<td>
                        <input name="apr_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="apr_pag" value="<?php if(! $existe) { echo $_POST['apr_pag']; } else { echo $apr_pag; } ?>" size="35" title="Persona que Aprueba la Orden">
                        <?php if ($boton=='Modificar') { echo $apr_pag; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaci&oacute;n:                          </td>
			<td>
                        <input name="obs_pag" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="obs_pag" value="<?php if(! $existe) { echo $_POST['obs_pag']; } else { echo $obs_pag; } ?>" size="35" title="Observaciones de la Orden">
                        <?php if ($boton=='Modificar') { echo $obs_pag; } ?></td>
                      </tr>

                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  </tr>
                   <tr>
                    <td align="center"><?php if($boton=='Modificar'){ abrir_ventana('imprimir_pago.php','v_imprimir','Imprimir Orden',"cod_pag=".$cod_pag); } ?></td>
                  </tr>
		  <?php if ($boton=="Modificar") { echo '<tr><td><hr></td></tr>'; 
		  echo '<tr><td> 
			<table width="100%" align="center" cellspacing="10">';
			?>
			<tr><td width="<?php echo (100/1).'%'; ?>" align="center">
			<?php if ($permitir_modificar=='NO'){ echo "<b>Facturas relacionados al Pago</b>"; }?>
			            <?php if($boton=='Modificar' && $permitir_modificar!='NO'){ abrir_ventana('facturas_pagos.php','v_facturas','Facturas Afectadas',"cod_pag=".$cod_pag."&seccion=".$_GET['seccion']); } ?>
			</td></tr>
		  <tr>
		    <td>
			<?php include('capa_pagos_facturas.php'); ?>
		    </td>
		  </tr>
		  <?php echo '<tr><td></td></tr>'; 
		  echo '<tr><td> 
			<table width="100%" align="center" cellspacing="10">';
			?>
			<tr><td width="<?php echo (100/1).'%'; ?>" align="center">
			
			<?php if ($permitir_modificar=='NO'){ echo "<b>Codificaci�n Presupuestaria</b>"; }?>
			            <?php if($boton=='Modificar' && $permitir_modificar!='NO'){ abrir_ventana('partidas_pagos.php','v_Productos','Codificaci�n Presupuestaria',"cod_pag=".$cod_pag."&frm_com=".$frm_com."&frm_pag=".$frm_pag."&seccion=".$_GET['seccion']); } ?>            
			    </td></tr>
		  <tr>
		    <td>
			<?php include('capa_pagos_partidas.php'); ?>
		    </td>
		  </tr>
			<?php
			echo '</table>
		        </td></tr>';
		  } ?>
                  <tr>
                    <td>
					<?php 
						$ncriterios =3; 
						$criterios[0] = "N� Formulario"; 
						$campos[0] ="frm_pag"; 
						$criterios[1] = "N� Compromiso"; 
						$campos[1] ="frm_com";
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=criterio1.checked=true;displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[2] = "Fecha ".$adicionaL_bus; 
						$campos[2] ="fch_pag";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             
				   	   }  ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
