<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
?>
<?php $qcna = 'Mes de '; ?>

<title>Resumen de pago de ISRL. <?php echo $qcna; ?> de <?php echo escribir_mes($_GET['mes']); ?> del <?php echo $_GET['ano']; ?></title>
<br>
<?php include('../comunes/pagina_encabezado.php'); ?>
<table width="95%" align="center" border="0" cellspacing="4" cellpadding="0">
    <tr>
        <td align="right"><font size="1">Fecha de Impresi�n: <?php echo date(d." / ".m." / ".Y); ?></font></td>
    </tr>
</table>
<br>
<table width="650" align="center" border="0" cellspacing="4" cellpadding="0">
    <tr>
        <td class="titulo">Resumen de pago de ISRL. <?php echo $qcna; ?> de <?php echo escribir_mes($_GET['mes']); ?> del <?php echo $_GET['ano']; ?></td>
    </tr>
</table>
<br>
<table width="95%" align="center" border="1" bordercolor="#000000" cellspacing="0" cellpadding="0">
    <tr align="center" class='etiquetas'>
        <td width="1" rowspan="2">N�</td>
        <td colspan="3">Comprobante de Egreso</td>
        <td colspan="5">Factura</td>
        <td colspan="3">Retenci�n</td>
    </tr>
    <tr align="center" class="detallespago2">
        <td width="40">Fecha</td>
        <td width="40">Form. N�</td>
        <td>Concepto del Egreso</td>
        <td width="40">Fecha</td>
        <td width="40">N�mero</td>
        <td width="40">Control</td>
        <td width="50">Monto</td>
        <td width="50">Base Imp.</td>
        <td width="30">Coprobante</td>
        <td width="30">%</td>
        <td width="60">Monto</td>
    </tr>
    <?php 
        $contador = 1;
        $tot_isrl = 0;
        $sql_isrl = "SELECT e.fch_egr,e.fac_egr,e.nor_egr,e.con_egr,e.mon_egr,e.ret_isrl_egr, (SELECT SUM(mon_pro_egr) FROM productos_egresos WHERE cod_egr=e.cod_egr) as monto_base FROM egresos e, productos_egresos pe WHERE YEAR(e.fch_egr)=".$_GET['ano']." AND MONTH(e.fch_egr)=".$_GET['mes']." AND e.ret_isrl_egr >= 0 AND e.cod_egr=pe.cod_egr And e.tip_egr = 'Servicio' GROUP BY pe.cod_egr";
        $sql_isrl = "SELECT e.fch_egr,e.frm_egr,e.con_egr,fp.fch_fac_pag,fp.num_fac_pag,fp.con_fac_pag,fp.mon_fac_pag,fp.isrl_fac_pag,e.ret_isrl_egr,rt.com_ret_isrl FROM egresos e, pagos p, facturas_pagos fp, compras c, retenciones_isrl rt WHERE e.ret_isrl_egr > 0 AND e.frm_egr=p.frm_egr AND p.cod_pag = fp.cod_pag AND p.frm_com=c.frm_com AND rt.egr_ret_isrl=e.frm_egr AND (YEAR(rt.fch_ret_isrl)=".$_GET['ano']." AND MONTH(rt.fch_ret_isrl)=".$_GET['mes'].")";
        $res_isrl = mysql_query($sql_isrl);
        while ($row_isrl = mysql_fetch_array($res_isrl)) {
       		$fecha1 = substr($row_isrl["fch_egr"], 8, 2);
    		$fecha2 = substr($row_isrl["fch_egr"], 5, 2);
    		$fecha3 = substr($row_isrl["fch_egr"], 0, 4);
    		$fecha3b = substr($row_isrl["fch_egr"], 2, 2);
    		$frm_egr=$row_isrl['frm_egr'];
    		$con_egr= ucwords(strtolower($row_isrl['con_egr']));
       		$fecha1_fac = substr($row_isrl["fch_fac_pag"], 8, 2);
    		$fecha2_fac = substr($row_isrl["fch_fac_pag"], 5, 2);
    		$fecha3_fac = substr($row_isrl["fch_fac_pag"], 0, 4);
    		$num_fac_pag = $row_isrl["num_fac_pag"];
    		$con_fac_pag = $row_isrl["con_fac_pag"];
    		$mon_fac_pag = $row_isrl["mon_fac_pag"];
    		$iva_com = $row_isrl["iva_com"];
    		$isrl_fac_pag = $row_isrl["isrl_fac_pag"];
    		$ret_isrl_egr = $row_isrl["ret_isrl_egr"];
    		$com_ret_isrl = $row_isrl["com_ret_isrl"];
    		$monto_ret = redondear(($isrl_fac_pag*$ret_isrl_egr),2,"",".");
    		$monto_ret = redondear(($monto_ret/100),2,"",".");
    		$tot_isrl = redondear(($tot_isrl+$monto_ret),2,"",".");
        ?>
        <tr  class="detallespago2">
            <td align="right"><?php echo $contador; ?>&nbsp;</td>
            <td align="center"><?php echo $fecha1.'-'.$fecha2.'-'.$fecha3; ?></td>
            <td align="center"><?php echo $frm_egr; ?></td>
            <td><?php echo $con_egr; ?></td>
            <td align="center"><?php echo $fecha1_fac.'-'.$fecha2_fac.'-'.$fecha3_fac; ?></td>
            <td align="right"><?php echo $num_fac_pag; ?></td>
            <td align="right"><?php echo $con_fac_pag; ?></td>
            <td align="right"><?php echo redondear($mon_fac_pag,2,'.',','); ?>&nbsp;</td>
            <td align="right"><?php echo redondear($isrl_fac_pag,2,'.',','); ?>&nbsp;</td>
            <td align="center">...<?php echo $com_ret_isrl;?></td>
            <td align="center"><?php echo $ret_isrl_egr;?>%</td>
            <td align="right"><?php echo redondear($monto_ret,2,'.',','); ?>&nbsp;</td>
        </tr>
        <?php $contador++; } ?>
        <tr class="tabla_total">
            <td colspan="11" align="right"><b><font size="-1">T O T A L    de    Retenciones ISRL <?php echo $qcna;?> <?php echo escribir_mes($_GET['mes']); ?> del <?php echo $_GET['ano'];?>&nbsp;</b></td>
            <td align="right"><b><font size="-1"><?php echo redondear($tot_isrl,2,'.',','); ?></font></b></td>
        </tr>
        <?php
    ?>
</table>
<br><br>
<center>
<input type="button" name="bt_print" value="Imprimir reporte ISRL" id="bt_print" onclick="this.style.visibility='hidden'; window.print();">
</center>
<br><br>
<?php echo $msg_pie_administrador; ?>
