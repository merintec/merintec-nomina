<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'presup_partidas.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "part_presup";	    // nombre de la tabla
$ncampos = "11";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_par","C�digo",$_POST['cod_par'],"0","3","numericos");
$datos[1] = crear_datos ("sec_par","Sector",$_POST['sec_par'],"0","2","numericos");
$datos[2] = crear_datos ("pro_par","Programa",$_POST['pro_par'],"0","2","numericos");
$datos[3] = crear_datos ("act_par","Actividad",$_POST['act_par'],"0","2","numericos");
$datos[4] = crear_datos ("ram_par","Ramo",$_POST['ram_par'],"0","2","numericos");
$datos[5] = crear_datos ("par_par","Partida",$_POST['par_par'],"1","4","decimal");
$datos[6] = crear_datos ("gen_par","Generica",$_POST['gen_par'],"1","2","numericos");
$datos[7] = crear_datos ("esp_par","Especifica",$_POST['esp_par'],"1","2","numericos");
$datos[8] = crear_datos ("sub_par","SubEspecifica",$_POST['sub_par'],"1","2","numericos");
$datos[9] = crear_datos ("des_par","Descripcion",$_POST['des_par'],"1","255","alfanumericos");
$datos[10] = crear_datos ("obs_par","Observacion",$_POST['obs_par'],"0","255","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Partida";
		$datos[0]="par_par";
		$parametro[1]="Gen�rica";
		$datos[1]="gen_par";
		$parametro[2]="Espec�fica";
		$datos[2]="esp_par";
		$parametro[3]="Sub_Espec�fica";
		$datos[3]="sub_par";
		$parametro[4]="Concepto";
		$datos[4]="des_par";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_par");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_par = $row['cod_par'];
	    $sec_par = $row['sec_par'];
	    $pro_par = $row['pro_par'];
	    $act_par = $row['act_par'];
	    $ram_par = $row['ram_par'];
	    $par_par = $row['par_par'];
	    $gen_par = $row['gen_par'];
	    $esp_par = $row['esp_par'];
	    $sub_par = $row['sub_par'];
	    $des_par = $row['des_par'];
	    $obs_par = $row['obs_par'];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_par",$_POST["cod_par"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[9][0],$datos[9][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_par"],"cod_par",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de Partida Presupuestaria</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">C&oacute;digo:</td>
                        <td width="75%">
                        <?php escribir_campo('cod_par',$_POST["cod_par"],$cod_par,'readonly',11,35,'C�digo de Partida',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Sector:</td>
			            <td>
                        <?php escribir_campo('sec_par',$_POST["sec_par"],$sec_par,'',2,35,'Sector',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Programa:</td>
			            <td>
                        <?php escribir_campo('pro_par',$_POST["pro_par"],$pro_par,'',2,35,'Programa',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Actividad:</td>
			            <td>
                        <?php escribir_campo('act_par',$_POST["act_par"],$act_par,'',2,35,'Actividad',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Ramo:</td>
			            <td>
                        <?php escribir_campo('ram_par',$_POST["ram_par"],$ram_par,'',2,35,'Ramo',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Partida:</td>
			            <td>
                        <?php escribir_campo('par_par',$_POST["par_par"],$par_par,'',4,35,'Partida',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Gen�rica:</td>
			            <td>
                        <?php escribir_campo('gen_par',$_POST["gen_par"],$gen_par,'',2,35,'Gen�rica',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Espec�fica:</td>
			            <td>
                        <?php escribir_campo('esp_par',$_POST["esp_par"],$esp_par,'',2,35,'Espec�fica',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Sub-Espec�fica:</td>
			            <td>
                        <?php escribir_campo('sub_par',$_POST["sub_par"],$sub_par,'',2,35,'Sub-Espec�fica',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci�n:</td>
			            <td>
                        <?php escribir_campo('des_par',$_POST["des_par"],$des_par,'',255,35,'Descripci�n',$boton,$existe,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
			            <td>
                        <?php escribir_campo('obs_par',$_POST["obs_par"],$obs_par,'',255,35,'Observaciones',$boton,$existe,'')?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$ncriterios =2; 
						$criterios[0] = "Descripci�n"; 
						$campos[0] ="des_par";
						$criterios[1] = "Partida"; 
						$campos[1] ="par_par";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
