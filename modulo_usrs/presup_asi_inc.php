<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php
  if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'presup_asi_inc.php?opt='.$_GET["opt"].'&seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "part_presup_mov";		// nombre de la tabla
$ncampos = "8";		//numero de campos del formulario
$datos[0]  = crear_datos ("cod_par_mov","C�digo",$_POST['cod_par_mov'],"0","11","numericos");
$datos[1]  = crear_datos ("tip_par_mov","Tipo de Movimiento",$_POST['tip_par_mov'],"0","15","alfanumericos");
$datos[2]  = crear_datos ("fch_par_mov","Fecha",$_POST['fch_par_mov'],"10","10","fecha");
$datos[3]  = crear_datos ("ano_par_mov","A�o del Presupuesto Destino",$_POST['ano_par_mov'],"4","10","alfanumericos");
$datos[4]  = crear_datos ("idn_par_mov","Identificador del Movimiento",$_POST['idn_par_mov'],"1","50","alfanumericos");
$datos[5]  = crear_datos ("con_par_mov","Descipcion del Movimiento",$_POST['con_par_mov'],"1","255","alfanumericos");
$datos[6]  = crear_datos ("mon_par_mov","Monto Total del Movimiento",$_POST['mon_par_mov'],"1","12","decimal");
$datos[7]  = crear_datos ("fin_par_mov","Fecha del Cierre",$_POST['fin_par_mov'],"0","10","fecha");


if ($_GET['opt']=="asi") { $opt = "Asignaci�n"; }
if ($_GET['opt']=="inc") { $opt = "Incremento"; }
if ($_GET['opt']=="trs") { $opt = "Traslado"; }
if ($_GET['opt']=="reint") { $opt = "Reintegro"; }

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"]."%' And tip_par_mov LIKE '%".$opt,$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Tipo";
		$datos[0]="tip_par_mov";
		$parametro[1]="Fecha";
		$datos[1]="fch_par_mov";
		$parametro[2]="A�o";
		$datos[2]="ano_par_mov";
		$parametro[3]="Descripci�n";
		$datos[3]="con_par_mov";
		$parametro[4]="Identificador";
		$datos[4]="idn_par_mov";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_par_mov");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_par_mov = $row["cod_par_mov"];
	    $tip_par_mov = $row["tip_par_mov"];
	    $fch_par_mov = $row["fch_par_mov"];
	    $ano_par_mov = $row["ano_par_mov"];
	    $idn_par_mov = $row["idn_par_mov"];
	    $con_par_mov = $row["con_par_mov"];
	    $mon_par_mov = $row["mon_par_mov"];
	    $fin_par_mov = $row["fin_par_mov"];
	    if ($fin_par_mov=='0000-00-00'){ $fin_par_mov=''; }
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		if ($_POST['tip_par_mov']=="Asignaci�n"){
			$sql_buscar = "SELECT * FROM ".$tabla." WHERE cod_par_mov = ".$_POST['cod_par_mov'];
			$res_buscar = mysql_fetch_array(mysql_query($sql_buscar));
			$sql_update = "UPDATE ".$tabla." set ano_par_mov='".$_POST['ano_par_mov']."', idn_par_mov ='".$_POST['idn_par_mov']."' WHERE ano_par_mov = '".$res_buscar[ano_par_mov]."' AND idn_par_mov = '".$res_buscar[idn_par_mov]."'";
			mysql_query($sql_update);
		}
		modificar_func($ncampos,$datos,$tabla,"cod_par_mov",$_POST["cod_par_mov"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_par_mov"],"cod_par_mov",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de <?php echo $opt; ?></td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Tipo de Movimiento:</td>
                        <td width="75%">
                        <input name="cod_par_mov" type="hidden" id="cod_par_mov" value="<?php if(! $existe) { echo $_POST['cod_par_mov']; } else { echo $cod_par_mov; } ?>" title="C�digo del Movimiento">
                        <?php   $boton2 = $boton; 
                                if ($boton != 'Modificar') { $boton='Actualizar'; }
                                $existe2 = $existe; 
                                $existe='si'; 
                                $tip_par_mov = $opt; 
                        ?>
                        <?php escribir_campo('tip_par_mov',$_POST["tip_par_mov"],$tip_par_mov,'readonly',11,15,'Tipo de Movimiento',$boton,$existe,'','','')?>
                        <?php $existe = $existe2; $boton = $boton2; ?> 
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha del Movimiento:</td>
                        <td width="75%">
                        <?php escribir_campo('fch_par_mov',$_POST["fch_par_mov"],$fch_par_mov,'',11,7,'Fecha de Movimiento',$boton,$existe,'fecha','','')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">A�o del Presupuesto:</td>
                        <td width="75%">
                        <?php 
			if ($opt != "Asignaci�n"){ $add_ano = ' OnChange="submit();" '; }
			escribir_campo('ano_par_mov',$_POST["ano_par_mov"],$ano_par_mov,'',11,7,'A�o del Presupuesto Destino',$boton,$existe,'',$add_ano,'')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Identificador del Presupuesto:</td>
                        <td width="75%">
                        <?php
			    if ($opt == "Asignaci�n" || $boton=="Modificar"){ 
				escribir_campo('idn_par_mov',$_POST["idn_par_mov"],$idn_par_mov,' ',50,15,'Identificador del Presupuesto Destino',$boton,$existe,'','','');
			    }
			    else {
				echo '<select name="idn_par_mov">';
				echo '<option>Seleccione...</option>';
				$sql_idn = "SELECT * FROM part_presup_mov WHERE ano_par_mov = '".$ano_par_mov."' AND tip_par_mov = 'Asignaci�n' ";
				$bus_idn = mysql_query($sql_idn);
				while ($res_idn = mysql_fetch_array($bus_idn)) {
					$add_select = '';
                                        if ($res_idn["idn_par_mov"]==$idn_par_mov){ $add_select = 'selected=selected'; }
					echo '<option '.$add_select.' >'.$res_idn["idn_par_mov"].'</option>';
				}
				echo '</select>';
			    }
			?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Descripci�n o Concepto del Movimiento:</td>
                        <td width="75%">
                        <?php escribir_campo('con_par_mov',$_POST["con_par_mov"],$con_par_mov,'',255,50,'Descripci�n del Movimiento',$boton,$existe,'','','')?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto total del Movimiento:</td>
                        <td width="75%">
                        <?php escribir_campo('mon_par_mov',$_POST["mon_par_mov"],$mon_par_mov,'',12,10,'Monto total del Movimiento',$boton,$existe,'','','')?>
                        </td>
                      </tr>
			<?PHP  if ($opt == "Asignaci�n"){ ?>
                      <tr>
                        <td width="25%" class="etiquetas">Fecha del Cierre:</td>
                        <td width="75%">
                        <?php escribir_campo('fin_par_mov',$_POST["fin_par_mov"],$fin_par_mov,'',11,7,'Fecha de Cierre del Presupuesto',$boton,$existe,'fecha','','')?>
                        </td>
                      </tr>
			<?PHP } ?>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
		  <?php if ($boton=="Modificar") { echo '<tr><td><hr></td></tr>'; 
		  echo '<tr><td> 
			<table width="100%" align="center" cellspacing="10">';
			?>
			<tr><td width="<?php echo (100/1).'%'; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('presup_part_mov.php','v_Pardias','Partidas Afectadas',"&opt=".$_GET['opt']."&cod_par_mov=".$cod_par_mov."&seccion=".$_GET['seccion']); } ?></td></tr>
		  <tr>
		    <td>
			<?php include('capa_presup_partidas.php'); ?>
		    </td>
		  </tr>
			<?php
			echo '</table>
		        </td></tr>';
		  } ?>
                  <tr>
                    <td>
					<?php 
						$ncriterios =3; 
						$criterios[0] = "A�o"; 
						$campos[0] ="ano_par_mov";
						$adicionaL_bus = '<img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick=displayCalendar(document.forms[0].buscar_a,"yyyy-mm-dd",this);  title="Haga click aqui para elegir una fecha"/>'; 
						$criterios[1] = "Fecha ".$adicionaL_bus; 
						$campos[1] ="fch_par_mov";
						$criterios[2] = "Identificador "; 
						$campos[2] ="idn_par_mov";

					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					     crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); 
                                             echo '</center>'; 
				   	   }  ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
