<?php     
    /// Definici�n del ancho del reporte
    $ancho_rep="700px";
    $ancho_rep_imp="500px";
    include('../comunes/conexion_basedatos.php'); 
    include ('../comunes/formularios_funciones.php'); 
    include ('../comunes/titulos.php'); 
    include ('../comunes/mensajes.php'); 
    include('../comunes/numerosaletras.php'); 
    $viene_val = $_GET['cod_egr'];
    $chequeprint=$_GET['cheque'];
    $paga_imp=$_GET['paga_imp'];
    //// obteniendo los datos
    $total_chq = 0;
    $npagos = 0;
    $base_iva = 0;
    $base_isrl = 0;
    $total_pagos = 0;
    $total_ded = 0;
    $total_isrl_ret = 0;
    $total_iva_ret = 0;
    $base_iva = 0;
    $base_isrl = 0;
    $sql="SELECT * FROM egresos e, proveedores prv, banco bn WHERE cod_egr=".$viene_val." AND e.rif_pro=prv.rif_pro AND e.cod_ban=bn.cod_ban";
    $res = mysql_query($sql);
	while ($reg = mysql_fetch_array ($res))
	{
        $fch_egr = $reg['fch_egr'];
        $frm_egr = $reg['frm_egr'];
        $chq_egr = $reg['chq_egr'];
        $rif_pro = $reg['rif_pro'];
        $nom_pro = $reg['nom_pro'];
        $nom_ban = $reg['nom_ban'];
        $cue_ban = $reg['cue_ban'];
        $con_egr = $reg['con_egr'];
        $rev_egr = $reg['rev_egr'];
        $apr_egr = $reg['apr_egr'];
        $obs_egr = $reg['obs_egr'];
        $afavor = $reg['nom_pro'];
        $sin_par_egr = $reg['sin_par_egr'];
        $ded_egr = $reg['ded_egr'];
        $monto_pago = $reg['mon_pag'];
        $monto_pago_letras = convertir_a_letras($monto_pago);
        $monto_pago_letras = ucwords(strtolower($monto_pago_letras));
        //// Manipulando un poco los datos para presentarlos
        $fecha1 = substr($reg["fch_egr"], 8, 2);
	    $fecha2 = substr($reg["fch_egr"], 5, 2);
	    $fecha3 = substr($reg["fch_egr"], 0, 4);
	    $fecha3b = substr($reg["fch_egr"], 2, 2);
        $fecha = $fecha1.'/'.$fecha2.'/'.$fecha3;
        $fecha_presup = substr($reg["fch_com"], 0, 4);
	    $norden=$fecha2.$fecha3b.$reg[nor_egr];
	    $npagos++;	
	}
	if ($paga_imp=='') {
    $sql="SELECT * FROM compras, proveedores, pagos, egresos, banco WHERE cod_egr=".$viene_val." AND pagos.frm_com=compras.frm_com AND compras.rif_pro=proveedores.rif_pro AND egresos.frm_egr=pagos.frm_egr AND egresos.cod_ban=banco.cod_ban";
	}
	if ($paga_imp=='iva') {
     $sql="SELECT *, (SELECT ret_iva_egr FROM egresos WHERE frm_egr = pagos.frm_egr) as iva_retenido FROM compras, proveedores, pagos, egresos, banco WHERE egresos.cod_egr=".$viene_val." AND pagos.frm_com=compras.frm_com AND proveedores.rif_pro='".$rif_pro ."' AND egresos.frm_egr=pagos.frm_egr_".$paga_imp." AND egresos.cod_ban=banco.cod_ban";
	}
	$res = mysql_query($sql);
	while ($reg = mysql_fetch_array ($res))
	{
	    ///// info de los pagos
	    if ($npagos>0){ $pagos_nrs .= ', '; }
	    $pagos_nrs .= $reg['frm_pag'];
        $total_pagos += $reg['mon_pag'];
        $cod_pag=$reg['cod_pag'];
        $iva_aplicado = $reg['iva_com'];
            ///// facturas asociadas a los pagos
            $sql_fac = "SELECT SUM(iva_fac_pag) AS iva_fact,SUM(isrl_fac_pag) AS isrl_fact FROM facturas_pagos WHERE cod_pag=".$reg['cod_pag'];
            $res_fac = mysql_fetch_array(mysql_query($sql_fac));
            ///// calculamos IVA
            $mon_iva_ret = redondear(($res_fac['iva_fact']*$iva_aplicado),2,"",".");
            $mon_iva_ret = redondear(($mon_iva_ret/100),2,"",".");
            $base_iva += $mon_iva_ret;
		if ($paga_imp==''){
            $mon_iva_ret = redondear(($mon_iva_ret*$reg['ret_iva_egr']),2,"",".");
		}
		if ($paga_imp=='iva'){
            $mon_iva_ret = redondear(($mon_iva_ret*$reg['iva_retenido']),2,"",".");
		}
            $mon_iva_ret = redondear(($mon_iva_ret/100),2,"",".");
            $total_iva_ret = redondear(($total_iva_ret+$mon_iva_ret),2,"",".");
            ///// calculamos ISRL
            $mon_isrl_ret = redondear(($res_fac['isrl_fact']*$reg['ret_isrl_egr']),2,"",".");
            $mon_isrl_ret = redondear(($mon_isrl_ret/100),2,"",".");
            ///// si la retencion de ISRL es 1%
            if ($reg['ret_isrl_egr']==1){
               /// cargamos el valor del sustraendo cuando el porcentaje de ISRL sea 1% 
                $sql_val="select * from valores WHERE des_val='SUSTRAC_ISRL'";
          	    $res_val = mysql_query($sql_val);
           	    while ($row_val = mysql_fetch_array($res_val))
                {
                   $$row_val['des_val'] = $row_val['val_val'];
                }
                $mon_isrl_ret = redondear(($mon_isrl_ret-$SUSTRAC_ISRL),2,"",".");    
            }
            //// si el monto de la retencion es menor a 0 la retencion isrl es 0
            if ($mon_isrl_ret<0){
                $mon_isrl_ret=0;
            }
            else {
                $total_isrl_ret = redondear(($total_isrl_ret+$mon_isrl_ret),2,"","."); 
                $base_isrl += $res_fac['isrl_fact'];
            }      
            /////
	    /////
    	$cod_com = $reg['cod_com'];
        $fch_egr = $reg['fch_egr'];
        $frm_egr = $reg['frm_egr'];
        $chq_egr = $reg['chq_egr'];
        $fre_com = $reg['fre_com'];
        $ret_iva_egr = $reg['ret_iva_egr'];
        $ret_isrl_egr = $reg['ret_isrl_egr'];
        $rif_pro = $reg['rif_pro'];
        $nom_pro = $reg['nom_pro'];
        $nom_ban = $reg['nom_ban'];
        $cue_ban = $reg['cue_ban'];
        $con_egr = $reg['con_egr'];
        $rev_egr = $reg['rev_egr'];
        $apr_egr = $reg['apr_egr'];
        $obs_egr = $reg['obs_egr'];
        $afavor = $reg['nom_pro'];
        $sin_par_egr = $reg['sin_par_egr'];
        $ded_egr = $reg['ded_egr'];
        $monto_pago = $reg['mon_pag'];
        $monto_pago_letras = convertir_a_letras($monto_pago);
        $monto_pago_letras = ucwords(strtolower($monto_pago_letras));
        //// Manipulando un poco los datos para presentarlos
        $fecha1 = substr($reg["fch_egr"], 8, 2);
	    $fecha2 = substr($reg["fch_egr"], 5, 2);
	    $fecha3 = substr($reg["fch_egr"], 0, 4);
	    $fecha3b = substr($reg["fch_egr"], 2, 2);
        $fecha = $fecha1.'/'.$fecha2.'/'.$fecha3;
        $fecha_presup = substr($reg["fch_com"], 0, 4);
	    $norden=$fecha2.$fecha3b.$reg[nor_egr];
	    $npagos++;
    }
    ////// Calculamos el monto total a pagar_cheque
    $total_pagos = redondear(($total_pagos + $sin_par_egr),2,"",".");
    $total_chq = redondear(($total_pagos - $total_iva_ret),2,"",".");
    $total_chq = redondear(($total_chq - $total_isrl_ret),2,"",".");
    $total_chq = redondear(($total_chq - $ded_egr),2,"",".");
    $monto_chq_letras = convertir_a_letras($total_chq);
    $monto_chq_letras = ucwords(strtolower($monto_chq_letras));
	    /// cuando es pago de impuesto IVA //////
	    if ($paga_imp=='iva'){
    $total_pagos = redondear($total_iva_ret,2,"",".");
    $total_chq = redondear($total_iva_ret,2,"",".");
    $monto_chq_letras = convertir_a_letras($total_chq);
    $monto_chq_letras = ucwords(strtolower($monto_chq_letras));
    $base_iva = 0;
    $total_iva_ret = 0;
		}
?>
<html>
    <head>
        <title>Imprimir Comprobante de Egreso</title>
        <style type="text/css">
        <!--
            body,td,th {
            	font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            .resp_datos {	
                font-size: 12px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            .montos_totales {	
                font-size: 14px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #formulario {	
                color: RED;
                font-size: 25px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #control {	
                font-size: 15px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #fecha {	
                font-size: 15px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #titulo {	
                font-size: 18px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #titulos_seq {	
                font-size: 12px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #total_pago {	
                font-size: 25px;
                font-weight: bold;
                color: #FF0000;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            .titulos_cel {	
                font-size: 9px;
                font-weight: bold;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            .datos_cel {	
                font-size: 9px;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            .tabla_borde 
            {
                border:solid 1px #000000;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            #printing {	
            	position:absolute;
            	z-index:1;
            	align: right;
            	top: 10px;
                border: 0px solid #000000;
                width: <?php echo $ancho_rep_imp; ?>;
                text-align: right;
            } 
            #pie_merintec {	
                font-size: 6pt;            
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #FFFFFF;';
                }
                ?>
            }
            /* para el cheque */
            #cheque {	
            	align: center;
                border:solid 1px 
                <?php 
			  if ($chequeprint!='') {
                  echo '#FFFFFF;';
                }
                else { echo '#000000;'; }
                if ($chequeprint!='') {
                     echo 'margin-top: 100px;';
                }
			?>
                width: 670px;
                height: 305px;                  
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>  
            }
            #chq_monto {
                position: relative;
                top: 22px;
                float: right;
                text-align: right;
                width: 150px;
                margin-right: 50px;
                border: 1px solid #FFF;
                font-size: 15px;                  
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>            
            }
            #chq_beneficiario {    
                position: relative;
                top: 70px;
                float: right;
                text-align: center;
                width: 550px;
                margin-right: 20px;
                border: 1px solid #FFF;
                font-size: 15px;                      
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>          
            }
            #chq_monto_enletras {    
                position: relative;
                top: 75px;
                float: right;
                text-align: left;
                width: 580px;
                height: 45px;
                margin-right: 20px;
                border: 1px solid #FFF;
                font-size: 15px;
                line-height: 22px;                    
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>            
            }
            #chq_fecha {    
                position: relative;
                float: left;
                top: 75px;
                margin-left: 55px;
                text-align: left;
                width: 200px;
                border: 1px solid #FFF;
                font-size: 15px;                         
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>       
            }
            #chq_fecha_ano {    
                position: relative;
                float: left;
                top: 75px;
                margin-left: 35px;
                text-align: left;
                width: 200px;
                border: 1px solid #FFF;
                font-size: 15px;                      
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>          
            }
            #chq_endoso {
                position: relative;
                top: 200px;
                float: right;
                text-align: right;
                width: 150px;
                margin-right: 20px;
                border: 1px solid #FFF;
                font-size: 15px;                  
                <?php 
                if ($chequeprint!='') {
                   echo 'color: #000000;';
                }
                ?>            
            }
}
        -->
        </style>
    </head>
    <body>
<?php if ($chequeprint=='') { ?>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr>
                <td width="1px"><img src="../imagenes/logo_tn_trn.png" alt="Logo <?php echo $organizacion;  ?>" height="72px" title="Logo <?php echo $organizacion;  ?>"/></td>
                <td width="260px" align="center">REP�BLICA BOLIVARIANA DE VENEZUELA<BR>CONTRALOR�A DEL MUNICIPIO JAUREGUI<BR>LA GRITA - ESTADO T�CHIRA<BR>RIF: <?php echo $organizacion_rif; ?></td>
                <td width="1px"><img src="../imagenes/logo_contraloria.png" alt="Logo <?php echo $organizacion;  ?>" height="72px" title="Logo <?php echo $organizacion;  ?>"/></td>
                <td align="right">
                    <span id="formulario">N� <?php echo $frm_egr; ?></span><br>
                    N� de Control: <span id="control"><u><?php echo $norden; ?></u></span><br>
                    Fecha: <span id="fecha"><u><?php echo $fecha; ?></u></span><br>
                </td>
            </tr>
        </table>
<?php } ?>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulo">
                    <?php if ($chequeprint=='') { echo 'COMPROBANTE DE EGRESO'; } ?>
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0" height="320px">
            <tr align="center">
                <td id="titulo">
                    <div id="cheque">
                        <div id="chq_monto">
                            <?php echo redondear($total_chq,2,".",","); ?>
                        </div>
                        <div id="chq_beneficiario">
                            <?php echo $nom_pro; ?>
                        </div>
                        <div id="chq_monto_enletras">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $monto_chq_letras; ?>
                        </div>
                        <div id="chq_fecha">
                            <?php echo 'La Grita,&nbsp;&nbsp;&nbsp;&nbsp;'.$fecha1.'&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;'.$fecha2; ?>
                        </div>
                        <div id="chq_fecha_ano">
                            <?php echo $fecha3b; ?>
                        </div>
                        <div id="chq_endoso" onclick="this.style.visibility='hidden'">
                            NO ENDOSABLE
                        </div>
                    </div>
                </td>
            </tr>
        </table>
<?php if ($chequeprint=='') { ?>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr>
                <td width="33.33%">
                    Ordene(s) de Pago:
                </td>
                <td width="33.33%">
                    Presupuesto:
                </td>
                <td width="33.33%">
                    Monto total de Pago(s):
                </td>
            </tr>
            <tr>
                <td width="33.33%" align="center">
                    <?php echo $pagos_nrs; ?>
                </td>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo $fecha_presup; ?></span>
                </td>
                <td width="33.33%" align="right">
                    <span class="resp_datos"><?php echo redondear($total_pagos,2,".",","); ?></span>
                </td>
            <tr>
                <td width="33.33%">
                    Monto del IVA:
                </td>
                <td width="33.33%">
                    Base imponible ISRL:
                </td>
                <td width="33.33%">
                    Otras deducciones:
                </td>
            </tr>
            <tr>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo redondear($base_iva,2,".",","); ?></span>
                </td>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo redondear($base_isrl,2,".",","); ?></span>
                </td>
                <td width="33.33%" align="right">
                    <span class="resp_datos"><?php echo redondear($ded_egr,2,".",","); ?></span>
                </td>
            </tr>
            <tr>
                <td width="33.33%">
                    % Retenci�n IVA:
                </td>
                <td width="33.33%">
                    % Retenci�n ISRL:
                </td>
                <td width="33.33%">
                    Neto a pagar:
                </td>
            </tr>
            <tr>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo $ret_iva_egr; ?>%</span>
                </td>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo $ret_isrl_egr; ?>%</span>
                </td>
                <td width="33.33%" align="right" rowspan="3">
                    <span id="total_pago"><?php echo redondear($total_chq,2,".",","); ?></span>
                </td>
            </tr>
            <tr>
                <td width="33.33%">
                    Monto Retenci�n IVA:
                </td>
                <td width="33.33%">
                    Monto Retenci�n ISRL:
                </td>
            </tr>
            <tr>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo redondear($total_iva_ret,2,".",","); ?></span>
                </td>
                <td width="33.33%" align="center">
                    <span class="resp_datos"><?php echo redondear($total_isrl_ret,2,".",","); ?></span>
                </td>
            </tr>
            <tr height="40px">
                <td colspan="3" valign="top">
                    Concepto: <span class="resp_datos"><?php echo $con_egr; ?></span>
                </td>
            </tr>
            
        </table>         
        <table width="<?php echo $ancho_rep; ?>" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#<?php if ($chequeprint=='') { echo '000000'; } else { echo 'ffffff'; }?>">
            <tr align="center" height="1" align="center">
                <td id="titulos_seq" width="33.33%">
                    Entidad Bancaria: 
                </td>
                <td id="titulos_seq" width="33.33%">
                    N� de Cuenta: 
                </td>
                <td id="titulos_seq" width="33.33%">
                    Cheque / Transferencia: 
                </td>
            </tr>
            <tr align="center">
                <td>
                    <span class="resp_datos"><?php echo $nom_ban; ?></span>
                </td>
                <td>    
                    <span class="resp_datos"><?php echo $cue_ban; ?></span>
                </td>
                <td>
                    <span class="resp_datos"><?php echo $chq_egr; ?></span>
                </td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulos_seq">
                    CUENTADANTES
                </td>
            </tr>
        </table>
         <table width="<?php echo $ancho_rep; ?>" height="120px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#<?php if ($chequeprint=='') { echo '000000'; } else { echo 'ffffff'; }?>">
            <tr align="center" height="1">
                <td id="titulos_seq" align="left" width="33.33%">
                    &nbsp;Administraci�n: <?php echo $rev_egr; ?>
                </td>
                <td id="titulos_seq" align="left" width="33.33%">
                    &nbsp;Contralor(a): <?php echo $apr_egr; ?>
                </td>
            </tr>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" height="1">
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
                <td id="titulos_seq" align="left">
                    &nbsp;Fecha:
                </td>
            </tr>
        </table>    
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td id="titulos_seq">
                    RECIBIDO POR EL BENEFICIARIO
                </td>
            </tr>
        </table>     
        <table width="<?php echo $ancho_rep; ?>" height="100px" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="1" bordercolor="#<?php if ($chequeprint=='') { echo '000000'; } else { echo 'ffffff'; }?>">
            <tr align="center" height="1" align="center">
                <td id="titulos_seq" width="28%">
                    Nombre y Apellido: 
                </td>
                <td id="titulos_seq" width="20%">
                    C.I. N�: 
                </td>
                <td id="titulos_seq" width="36%">
                    Firma y Sello: 
                </td>
                <td id="titulos_seq">
                    Fecha y Hora: 
                </td>
            </tr>
            <tr align="center">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="<?php echo $ancho_rep; ?>" border="0" cellspacing="0" cellmargin="0">
            <tr align="center">
                <td>
                    <?php 
        if ($chequeprint=='') {
            echo $pie_ord; 
        }
			?>
                </td>
            </tr>
        </table>

<?php } ?>
    </body>
</html>
<div id="printing">
    <input type="image" src="../imagenes/imprimir.png" id="imprimir" name="imprimir" value="imprimir" title="<?php echo $msg_pago_imprimir; ?>" onclick="this.style.visibility='hidden'; window.print();">
</div>
