<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'anular_formulario.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "anular_form";	// nombre de la tabla
$ncampos = "5";		//numero de campos del formulario
$datos[0] = crear_datos ("cod_anl","C�digo",$_POST['cod_anl'],"0","11","alfanumericos");
$datos[1] = crear_datos ("fch_anl","Fecha de Anulacion",$_POST['fch_anl'],"10","10","fecha");
$datos[2] = crear_datos ("tip_anl","Tipo de Formulario",$_POST['tip_anl'],"1","1","alfabeticos");
$datos[3] = crear_datos ("num_anl","N del Formulario",$_POST['num_anl'],"1","5","numericos");
$datos[4] = crear_datos ("mot_anl","Motivo de la Anulacion",$_POST['mot_anl'],"1","255","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Fecha";
		$datos[0]="fch_anl";
		$parametro[1]="Tipo de Formulario";
		$datos[1]="tip_anl";
		$parametro[2]="Motivo";
		$datos[2]="mot_anl";
		$parametro[3]="N�mero";
		$datos[3]="num_anl";
		busqueda_varios(6,$buscando,$datos,$parametro,"cod_anl");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_anl = $row["cod_anl"];
	    $fch_anl = $row["fch_anl"];
	    $tip_anl = $row["tip_anl"];
	    $num_anl = $row["num_anl"];
	    $mot_anl = $row["mot_anl"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_anl",$_POST["cod_anl"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[3][0],$datos[3][2]."' AND ".$datos[2][0]." = '".$datos[2][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_anl"],"cod_anl",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Anular Formulario</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Fecha de la Anulaci�n: </td>
                        <td width="75%">
			<input name="cod_anl" type="hidden" id="cod_anl" value="<?php if(! $existe) { echo $_POST['cod_anl']; } else { echo $cod_anl; } ?>" size="35" title="Codigo de la Anulaci�n">
			<input name="fch_anl" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_anl" value="<?php if(! $existe) { echo $_POST["fch_anl"]; } else { echo $fch_anl; } ?>" size="20" title="Fecha de Anulaci�n del Formulario" />
                          <?php if ($boton=='Modificar') { echo $fch_anl; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fch_anl,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tipo de Formulario:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_anl" title="Tipo de Formulario que desea Anular">
                          <option>Seleccione...</option>
                          <option value="C" '; if ($tip_anl == "C" || $_POST['tip_anl'] =="C") { echo 'selected'; } echo '>Orden de Compromiso</option>
                          <option value="P" '; if ($tip_anl == "P" || $_POST['tip_anl'] =="P") { echo 'selected'; } echo '>Orden de Pago</option>
                          <option value="E" '; if ($tip_anl == "E" || $_POST['tip_anl'] =="E") { echo 'selected'; } echo '>Comprobante de Egreso</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_anl" id="tip_anl" value="'.$tip_anl.'" >'; 
						    if ($tip_anl == "C") { echo 'Orden de Compromiso'; } 
						    if ($tip_anl == "P") { echo 'Orden de Pago'; } 
							if ($tip_anl == "E") { echo 'Comprobante de Egreso'; }
						}?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">N�mero de Formulario:
                          </td>
			<td>
                        <input name="num_anl" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="num_anl" value="<?php if(! $existe) { echo $_POST["num_anl"]; } else { echo $num_anl; } ?>" size="10" maxlength="5" title="N�mero de Formulario que se anular�">
                        <?php if ($boton=='Modificar') { echo $num_anl; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Motivo de la Anulaci�n:
                          </td>
			<td>
                        <input name="mot_anl" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mot_anl" value="<?php if(! $existe) { echo $_POST['mot_anl']; } else { echo $mot_anl; } ?>" size="35" title="Raz�n por la cual se anular�">
                        <?php if ($boton=='Modificar') { echo $mot_anl; } ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
					<?php 
						$adicional = "<select name='rif_pro2' id='rif_pro2' onchange=";
						$adicional .= '"valor_acampo(this.value, ';
						$adicional .= "'buscar_a')";
						$adicional .= '"; ><option value="">Seleccione...</option><option value="C">Orden de Compromiso</option><option value="P">Orden de Pago</option><option value="E">Comprobante de Egreso</option></select>';
						$ncriterios =2; 
						$criterios[1] = "N�mero"; 
						$campos[1] ="num_anl";
						$criterios[0] = $adicional;
						$campos[0] = "tip_anl";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
