<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$sql="select * from valores";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res))
{
    $$row['des_val'] = $row['val_val'];
}
$boton = "Verificar";
$existe = '';
$pagina = 'coberturas_med.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "coberturas_med";	// nombre de la tabla
$ncampos = "4";		    //numero de campos del formulario
$datos[0] = crear_datos ("nom_cob","Cobertura",$_POST['nom_cob'],"0","255","alfanumericos");
$datos[1] = crear_datos ("mon_cob","Monto",$_POST['mon_cob'],"1","11","decimal");
$datos[2] = crear_datos ("bas_cob","Base de la cobertura",$_POST['bas_cob'],"1","11","numericos");
$datos[3] = crear_datos ("cod_tcar","Aplicable a",$_POST['cod_tcar'],"1","11","numericos");

$bas_cob=$_POST['bas_cob'];
$cod_tcar=$_POST['cod_tcar'];

if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) 
	{ 
	    $tipo = "general";
	    $criterio_buscar = $_POST["criterio"];
	    $valor_buscar = $_POST["buscar_a"];
	    $buscando = busqueda_func($valor_buscar,$criterio_buscar,"$tabla",$pagina,$tipo);
	}
	elseif ($_POST["BuscarInd"]) { 
	$tipo = "individual"; 
	$buscando = busqueda_func($_POST["buscar_a"],"cod_cob","$tabla",$pagina,$tipo);
	} 
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Cobertura";
		$datos[0]="nom_cob";		
		busqueda_varios(3,$buscando,$datos,$parametro,"cod_cob");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_cob = $row["cod_cob"];
	    $nom_cob = $row["nom_cob"];
	    $mon_cob = $row["mon_cob"];
	    $bas_cob = $row["bas_cob"];
	    $cod_tcar = $row["cod_tcar"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_cob",$_POST["cod_cob"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{  
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_cob"],"cod_cob",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Tipos de Reembolsos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		              <tr>
                        <td class="etiquetas">Cobertura: </td>
                        <td>
                            <input name="cod_cob" type="hidden" id="cod_cob" value="<?php if(! $existe) { echo $_POST['cod_cob']; } else { echo $cod_cob; } ?>" size="35" title="Codigo de pago de medicinas">
							<?php escribir_campo('nom_cob',$_POST["nom_cob"],$nom_cob,'',50,35,'Nombre de cobertura',$boton,$existe,''); ?>
                            
                          
			            </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td>
                            <?php escribir_campo('mon_cob',$_POST["mon_cob"],$mon_cob,'',12,15,'Monto de la cobertura',$boton,$existe,''); ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Base de Monto:</td>
                        <td>
							<?PHP if($boton=="Modificar"){
								echo '<input name="bas_cob" type="hidden" id="bas_cob" value="'.$bas_cob.'" >';
								if($bas_cob==0){ echo 'Monto Unico Anual'; }
								if($bas_cob==1){ echo '% Mensual'; }
								if($bas_cob==2){ echo 'Monto Mensual'; }
							}else{  ?>
							
							<select name="bas_cob">
								<option value="">Seleccione...</option>
								<option value="0" <?PHP if($bas_cob==0){ echo 'selected="selected"'; } ?> >Monto Unico Anual</option>
								<option value="1" <?PHP if($bas_cob==1){ echo 'selected="selected"'; } ?> >% Mensual</option>
								<option value="2" <?PHP if($bas_cob==2){ echo 'selected="selected"'; } ?> >Monto Mensual</option>
							</select>
							<?PHP } ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Aplicable a:</td>
                        <td>
                            <?php combo('cod_tcar', $cod_tcar, 'tipos_cargos', $link, 0, 0, 1, "", 'cod_tcar', '', $boton,''); ?>
						</td>
                      </tr>

         </table></td>
                  </tr>
                   <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td>
			<?php 
			$ncriterios =1;
			$criterios[0] = "Nombre Cobertura";
			$campos[0] = "nom_cob";
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
