<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "personal";	// nombre de la tabla
$ncampos = "46";	//numero de campos del formulario
$ced_per = $_POST["ced_per"];
	    $nac_per = $_POST["nac_per"];
	    $nom_per = $_POST["nom_per"];
	    $ape_per = $_POST["ape_per"];
	    $sex_per = $_POST["sex_per"];
	    $fnac_per = $_POST["fnac_per"];
	    $lnac_per = $_POST["lnac_per"];
	    $cor_per = $_POST["cor_per"];
	    $pro_per = $_POST["pro_per"];
	    $abr_per = $_POST["abr_per"];
	    $dir_per = $_POST["dir_per"];
	    $tel_per = $_POST["tel_per"];
	    $cel_per = $_POST["cel_per"];
		$fch_reg = $_POST["fch_reg"];
		$lph_des = $_POST["lph_des"];
		$spf_des = $_POST["spf_des"];
		$sso_des = $_POST["sso_des"];
		$cah_des = $_POST["cah_des"];
		$sfu_des = $_POST["sfu_des"];
		$hog_asg = $_POST["hog_asg"];
		$hij_asg = $_POST["hij_asg"];
		$ant_asg = $_POST["ant_asg"];
		$pro_asg = $_POST["pro_asg"];
		$hje_asg = $_POST["hje_asg"];
		$jer_asg = $_POST["jer_asg"];
		$otr_asg = $_POST["otr_asg"];
		$fnj_des = $_POST["fnj_des"];
		/// nuevas HULA
		$med_asg = $_POST["med_asg"];
		$tra_asg = $_POST["tra_asg"];
		$asi_asg = $_POST["asi_asg"];
		$spn_asg = $_POST["spn_asg"];
		$ano_adm_per=$_POST["ano_adm_per"];

		//bonos para hijos por utiles y juguetes
		$bono_util = $_POST["bono_util"];
		$bono_juge = $_POST["bono_juge"];
		$comp_eva = $_POST["comp_eva"];
		$comp_eval_ant = $_POST["comp_eval_ant"];

		$hog_asg_inc = $_POST["hog_asg_inc"];
		$hij_asg_inc = $_POST["hij_asg_inc"];
		$ant_asg_inc = $_POST["ant_asg_inc"];
		$pro_asg_inc = $_POST["pro_asg_inc"];
		$med_asg_inc = $_POST["med_asg_inc"];
		$tra_asg_inc = $_POST["tra_asg_inc"];
		$asi_asg_inc = $_POST["asi_asg_inc"];
		$spn_asg_inc = $_POST["spn_asg_inc"];
		$exc_asg = $_POST["exc_asg"];
		$exc_asg_inc = $_POST["exc_asg_inc"];

$datos[0] = crear_datos ("ced_per","C�dula",$_POST['ced_per'],"1","12","numericos");
$datos[1] = crear_datos ("nac_per","Nacionalidad",$_POST['nac_per'],"1","1","alfabeticos");
$datos[2] = crear_datos ("nom_per","Nombre",$_POST['nom_per'],"1","50","alfabeticos");
$datos[3] = crear_datos ("ape_per","Apellido",$_POST['ape_per'],"1","50","alfabeticos");
$datos[4] = crear_datos ("sex_per","Sexo",$_POST['sex_per'],"0","1","alfabeticos");
$datos[5] = crear_datos ("fnac_per","Fecha de Nacimiento",$_POST['fnac_per'],"1","10","fecha");
$datos[6] = crear_datos ("lnac_per","Lugar de Nacimiento",$_POST['lnac_per'],"0","50","alfabeticos");
$datos[7] = crear_datos ("cor_per","Correo",$_POST['cor_per'],"0","50","email");
$datos[8] = crear_datos ("pro_per","Profesi�n",$_POST['pro_per'],"0","50","alfabeticos");
$datos[9] = crear_datos ("abr_per","Abreviatura de Profesi�n",$_POST['abr_per'],"0","10","alfabeticos");
$datos[10] = crear_datos ("dir_per","Direcci�n",$_POST['dir_per'],"0","255","alfanumericos");
$datos[11] = crear_datos ("tel_per","Tel�fono",$_POST['tel_per'],"0","12","numericos");
$datos[12] = crear_datos ("cel_per","Celular",$_POST['cel_per'],"0","12","numericos");
$datos[13] = crear_datos ("fch_reg","Fecha de Registro",$_POST['fch_reg'],"1","11","fecha");
$datos[14] = crear_datos ("lph_des","Ley de Pol�tica Habitacional",$_POST['lph_des'],"0","1","alfabeticos");
$datos[15] = crear_datos ("spf_des","Seguro de Paro Forzoso",$_POST['spf_des'],"0","1","alfabeticos");
$datos[16] = crear_datos ("sso_des","Seguro Social Obligatorio",$_POST['sso_des'],"0","1","alfabeticos");
$datos[17] = crear_datos ("cah_des","Caja de Ahorro",$_POST['cah_des'],"0","1","alfabeticos");
$datos[18] = crear_datos ("sfu_des","Servicio Funerario",$_POST['sfu_des'],"0","1","alfabeticos");
$datos[19] = crear_datos ("hog_asg","Prima por Hogar",$_POST['hog_asg'],"0","1","alfabeticos");
$datos[20] = crear_datos ("hij_asg","Prima por Hijos",$_POST['hij_asg'],"0","1","alfabeticos");
$datos[21] = crear_datos ("ant_asg","Prima por Antiguedad",$_POST['ant_asg'],"0","1","alfabeticos");
$datos[22] = crear_datos ("pro_asg","Prima por Profesionalizaci�n",$_POST['pro_asg'],"0","1","alfabeticos");
$datos[23] = crear_datos ("hje_asg","Prima por Hijos Excepcionales",$_POST['hje_asg'],"0","1","alfabeticos");
$datos[24] = crear_datos ("jer_asg","Prima por Jerarqu�a",$_POST['jer_asg'],"0","1","alfabeticos");
$datos[25] = crear_datos ("otr_asg","Otras Primas",$_POST['otr_asg'],"0","1","alfabeticos");
$datos[26] = crear_datos ("fnj_des","Fondo de Jubilaciones",$_POST['fnj_des'],"0","1","alfabeticos");
////// HULA
$datos[27] = crear_datos ("ano_adm_per","A�os de Servicio en la Adm Publica",$_POST['ano_adm_per'],"1","10","numericos");
$datos[28] = crear_datos ("med_asg","Prima Medico Especialista",$_POST['med_asg'],"0","1","alfabeticos");
$datos[29] = crear_datos ("tra_asg","Prima Transporte",$_POST['tra_asg'],"0","1","alfabeticos");
$datos[30] = crear_datos ("asi_asg","Prima Transporte",$_POST['asi_asg'],"0","1","alfabeticos");
$datos[31] = crear_datos ("spn_asg","Prima Transporte",$_POST['spn_asg'],"0","1","alfabeticos");

		//bonos para hijos por utiles y juguetes
$datos[32] = crear_datos ("bono_util","Bono de Utiles",$_POST['bono_util'],"0","1","alfabeticos");
$datos[33] = crear_datos ("bono_juge","Bono Juguetes",$_POST['bono_juge'],"0","1","alfabeticos");
$datos[34] = crear_datos ("comp_eva","Compensacion Evaluacion",$_POST['comp_eva'],"0","5","decimal");
$datos[35] = crear_datos ("comp_eval_ant","Compensacion Evaluacion",$_POST['comp_eval_ant'],"0","11","decimal");

$datos[36] = crear_datos ("hog_asg_inc","Incremento Prima por Hogar",$_POST['hog_asg_inc'],"0","9","decimal");
$datos[37] = crear_datos ("hij_asg_inc","Incremento Prima por Hijos",$_POST['hij_asg_inc'],"0","9","decimal");
$datos[38] = crear_datos ("ant_asg_inc","Incremento Prima por Antiguedad",$_POST['ant_asg_inc'],"0","9","decimal");
$datos[39] = crear_datos ("pro_asg_inc","Incremento Prima por Profesionalizaci�n",$_POST['pro_asg_inc'],"0","9","decimal");
$datos[40] = crear_datos ("med_asg_inc","Incremento Prima Medico Especialista",$_POST['med_asg_inc'],"0","9","decimal");
$datos[41] = crear_datos ("tra_asg_inc","Incremento Prima Transporte",$_POST['tra_asg_inc'],"0","9","decimal");
$datos[42] = crear_datos ("asi_asg_inc","Incremento Prima Transporte",$_POST['asi_asg_inc'],"0","9","decimal");
$datos[43] = crear_datos ("spn_asg_inc","Incremento Prima Transporte",$_POST['spn_asg_inc'],"0","9","decimal");
$datos[44] = crear_datos ("exc_asg","Prima Exclusividad",$_POST['exc_asg'],"0","9","alfabeticos");
$datos[45] = crear_datos ("exc_asg_inc","Incremento Prima Exclusividad",$_POST['exc_asg_inc'],"0","9","decimal");

if ($_POST["Buscar"]||$_POST["BuscarInd"]||$_COOKIE['uspriv']>=3) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscar_txt = $_POST["buscar_a"];
	$criterio_txt = $_POST["criterio"];
	if ($_COOKIE['uspriv']>=3) 
	{
	   $tipo = "individual";
	   $buscar_txt = $_COOKIE['uscod'];
	   $criterio_txt = 'ced_per';
	}
	$buscando = busqueda_func($buscar_txt,$criterio_txt,$tabla,$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="C�dula";
		$datos[0]="ced_per";
		$parametro[1]="Nombre";
		$datos[1]="nom_per";
		$parametro[2]="Apellido";
		$datos[2]="ape_per";
		busqueda_varios(5,$buscando,$datos,$parametro,"ced_per");
		return;
	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $ced_per = $row["ced_per"];
	    $nac_per = $row["nac_per"];
	    $nom_per = $row["nom_per"];
	    $ape_per = $row["ape_per"];
	    $sex_per = $row["sex_per"];
	    $fnac_per = $row["fnac_per"];
	    $lnac_per = $row["lnac_per"];
	    $cor_per = $row["cor_per"];
	    $pro_per = $row["pro_per"];
	    $abr_per = $row["abr_per"];
	    $dir_per = $row["dir_per"];
	    $tel_per = $row["tel_per"];
	    $cel_per = $row["cel_per"];
		$fch_reg = $row["fch_reg"];
		$lph_des = $row["lph_des"];
		$spf_des = $row["spf_des"];
		$sso_des = $row["sso_des"];
		$cah_des = $row["cah_des"];
		$sfu_des = $row["sfu_des"];
		$hog_asg = $row["hog_asg"];
		$hij_asg = $row["hij_asg"];
		$ant_asg = $row["ant_asg"];
		$pro_asg = $row["pro_asg"];
		$hje_asg = $row["hje_asg"];
		$jer_asg = $row["jer_asg"];
		$otr_asg = $row["otr_asg"];
		$fnj_des = $row["fnj_des"];
		$comp_eva = $row["comp_eva"];

		/// nuevas policia Municipal
		$med_asg = $row["med_asg"];
		$tra_asg = $row["tra_asg"];
		$asi_asg = $row["asi_asg"];
		$spn_asg = $row["spn_asg"];
		$ano_adm_per=$row["ano_adm_per"];

		/// nuevas bonos
		$bono_juge = $row["bono_juge"];
		$bono_util = $row["bono_util"];
		$comp_eval_ant = $row["comp_eval_ant"];


		$hog_asg_inc = $row["hog_asg_inc"];
		$hij_asg_inc = $row["hij_asg_inc"];
		$ant_asg_inc = $row["ant_asg_inc"];
		$pro_asg_inc = $row["pro_asg_inc"];
		$med_asg_inc = $row["med_asg_inc"];
		$tra_asg_inc = $row["tra_asg_inc"];
		$asi_asg_inc = $row["asi_asg_inc"];
		$spn_asg_inc = $row["spn_asg_inc"];
		$exc_asg = $row["exc_asg"];
		$exc_asg_inc = $row["exc_asg_inc"];


	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	//$validacion = validando_campos ($ncampos,$datos);
	$validacion=1;
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"ced_per",$_POST["ced_per"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	//$validacion = validando_campos ($ncampos,$datos);
	//if ($validacion) { $boton = "Guardar"; }
	$boton ="Guardar";
	//$boton=comp_exist($datos[0][0],$datos[0][2],$tabla,$boton,'si',$_GET["nom_sec"]);
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	$tabla2 = "cargos";
	$boton = "eliminando";
	$boton=comp_exist($datos[0][0],$datos[0][2],$tabla2,$boton,'si','Cargos de Personal');
	if ($boton != "Verificar"){
	eliminar_func($_POST["ced_per"],"ced_per",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	}
	$boton = "Modificar";
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	modificar_func($ncampos2,$datos2,$tabla2,"cod_car",$_POST['confirmar_val'],$pagina);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}


?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos del Personal</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td width="25%" class="etiquetas">C&eacute;dula:</td>
                        <td width="75%">
			<?php if ($boton != "Modificar") { echo '<select name="nac_per" title="Nacionalidad">
                          <option>-</option>
                          <option value="V" '; if ($nac_per == "V" || $_POST['nac_per'] =="V") { echo 'selected'; } echo '>V-</option>
                          <option value="E" '; if ($nac_per == "E" || $_POST['nac_per'] =="E") { echo 'selected'; } echo '>E-</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="nac_per" id="nac_per" value="'.$nac_per.'" >'; 
						    if ($nac_per == "V") { echo 'V-'; } 
							if ($nac_per == "E") { echo 'E-'; }
						}?>

                        <input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" value="<?php if(! $existe) { echo $_POST["ced_per"]; } else { echo $ced_per; } ?>" <?php if ($boton=='Actualizar') { echo "readonly"; } ?> size="29" title="C�dula del Personal">
                        <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>

                      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td><input name="nom_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_per" value="<?php if(! $existe) { echo $_POST["nom_per"]; } else { echo $nom_per; } ?>" size="35" title="Nombre del Personal">
                        <?php if ($boton=='Modificar') { echo $nom_per; } ?></td>
                      </tr>
                      <tr>
                        <td width="25%" class="etiquetas">Apellido:</td>
                        <td width="75%">
                        <input name="ape_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ape_per" value="<?php if(! $existe) { echo $_POST["ape_per"]; } else { echo $ape_per; } ?>" size="35" title="Apellido del Personal">
                        <?php if ($boton=='Modificar') { echo $ape_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Sexo:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="sex_per" title="Sexo">
                          <option>Seleccione...</option>
                          <option value="M" '; if ($sex_per == "M" || $_POST['sex_per'] =="M") { echo 'selected'; } echo '>Masculino</option>
                          <option value="F" '; if ($sex_per == "F" || $_POST['sex_per'] =="F") { echo 'selected'; } echo '>Femenino</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="sex_per" id="sex_per" value="'.$sex_per.'" >'; 
						    if ($sex_per == "M") { echo 'Masculino'; } 
							if ($sex_per == "F") { echo 'Femenino'; }
						}?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Nacimiento: </td>
                        <td><input name="fnac_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fnac_per" value="<?php if(! $existe) { echo $_POST["fnac_per"]; } else { echo $fnac_per; } ?>" size="20" title="Fecha de Nacimiento" />
                          <?php if ($boton=='Modificar') { echo $fnac_per; } ?>
			<?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].fnac_per,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Lugar de Nacimiento: </td>
                        <td><input name="lnac_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="lnac_per" value="<?php if(! $existe) { echo $_POST["lnac_per"]; } else { echo $lnac_per; } ?>" size="35" title="Lugar de Nacimiento" />
                          <?php if ($boton=='Modificar') { echo $lnac_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Correo:</td>
                        <td><input name="cor_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cor_per" value="<?php if(! $existe) { echo $_POST["cor_per"]; } else { echo $cor_per; } ?>" size="35" title="Correo Electronico" />
                          <?php if ($boton=='Modificar') { echo $cor_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Profesi&oacute;n:</td>
                        <td><input name="pro_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="pro_per" value="<?php if(! $existe) { echo $_POST["pro_per"]; } else { echo $pro_per; } ?>" size="35" title="Profesi�n" />
                          <?php if ($boton=='Modificar') { echo $pro_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Abreviatura de Profesi&oacute;n:</td>
                        <td><input name="abr_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="abr_per" value="<?php if(! $existe) { echo $_POST["abr_per"]; } else { echo $abr_per; } ?>" size="35" title="Profesi�n" />
                          <?php if ($boton=='Modificar') { echo $abr_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Direccion:</td>
                        <td><?php if ($boton!='Modificar') { echo '<textarea name="dir_per" cols="32" rows="2" id="dir_per" title="Direcci�n del Personal">';  if(! $existe) { echo $_POST["dir_per"]; } else { echo $dir_per; } echo '</textarea>'; } ?>
                          <?php  if ($boton=='Modificar') { echo '<input name="dir_per" type="hidden" id="dir_per" value="'; if(!$existe) { echo $_POST['dir_per']; } else { echo $dir_per; } echo '">'.$dir_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Tel�fono:</td>
                        <td><input name="tel_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tel_per" value="<?php if(! $existe) { echo $_POST["tel_per"]; } else { echo $tel_per; } ?>" size="20" title="Telefono" />
                          <?php if ($boton=='Modificar') { echo $tel_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Celular:</td>
                        <td><input name="cel_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="cel_per" value="<?php if(! $existe) { echo $_POST['cel_per']; } else { echo $cel_per; } ?>" size="20" title="Celular">
                          <?php if ($boton=='Modificar') { echo $cel_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Registro: </td>
                        <td><input name="fch_reg" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" readonly id="fch_reg" value="<?php if(! $fch_reg) { echo date('Y-m-d'); } else { echo $fch_reg; } ?>" size="20" title="Fecha de Registro" />
						  <?php if ($boton=='Modificar') { echo $fch_reg; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Porcentaje&nbsp;Compensaci�n&nbsp;por&nbsp;Evaluaci�n:</td>
                        <td><input name="comp_eva" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="comp_eva" value="<?php if(! $existe) { echo $_POST['comp_eva']; } else { echo $comp_eva; } ?>" size="5" title="Porcentaje Compensaci�n Salarial por Evaluaci�n y Desempe�o. Ejm. 7.5/100 = 0.075">
                          <?php if ($boton=='Modificar') { echo $comp_eva; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Monto&nbsp;Acumulado de Compensaci�n&nbsp;:</td>
                        <td><input name="comp_eval_ant" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="comp_eval_ant" value="<?php if(! $existe) { echo $_POST['comp_eval_ant']; } else { echo $comp_eval_ant; } ?>" size="5" title="Monto en Bs. Acumulado Compensaci�n Salarial ">
                          <?php if ($boton=='Modificar') { echo $comp_eval_ant; } ?></td>
                      </tr>
                       <tr>
                        <td class="etiquetas">A�os en la Administraci�n Publica:</td>
                        <td><input name="ano_adm_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ano_adm_per" value="<?php if(! $existe) { echo $_POST['ano_adm_per']; } else { echo $ano_adm_per; } ?>" size="5" title="A�os en la Administraci�n Publica">
                          <?php if ($boton=='Modificar') { echo $ano_adm_per; } ?></td>
                      </tr>
		      <tr>
			<td class="etiquetas" valign="top">Descuentos:</td>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr><td><input name="lph_des" id="lph_des" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$lph_des.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["lph_des"]=='A' || $lph_des == 'A') { echo 'checked'; } ?> title="Fondo de Ahorro Obligatorio para Vivienda">FAOV</td><td>
                          <?php if ($boton=='Modificar') { if ($lph_des=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr>
			<tr><td><input name="spf_des" id="spf_des" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$spf_des.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["spf_des"]=='A' || $spf_des == 'A') { echo 'checked'; } ?> title="Seguro de Perdida Involuntaria de Empleo">PIE</td><td>
                          <?php if ($boton=='Modificar') { if ($spf_des=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr>
			<tr><td><input name="sso_des" id="sso_des" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$sso_des.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["sso_des"]=='A' || $sso_des == 'A') { echo 'checked'; } ?> title="Seguro Social Obligatorio">SSO</td><td>
                          <?php if ($boton=='Modificar') { if ($sso_des=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr>
			<!-- <tr><td><input name="fnj_des" id="fnj_des" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$fnj_des.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["fnj_des"]=='A' || $fnj_des == 'A') { echo 'checked'; } ?> title="Fondo de Jubilaciones">Fondo de Jubilaciones</td><td>
                          <?php if ($boton=='Modificar') { if ($fnj_des=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr> -->
			<!--<tr><td><input name="cah_des" id="cah_des" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$cah_des.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["cah_des"]=='A' || $cah_des == 'A') { echo 'checked'; } ?> title="Caja de Ahorro">Caja de Ahorro</td><td>
                          <?php if ($boton=='Modificar') { if ($cah_des=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr>-->
			<tr>
                <td><?php if ($boton != "Modificar") { echo '<select name="cah_des" title="Sexo">
                  <option value="">Seleccione...</option>
                  <option value="CATEEM" '; if ($cah_des == "CATEEM" || $_POST['cah_des'] =="CATEEM") { echo 'selected'; } echo '>CATEEM</option>
                  <option value="STHCYS" '; if ($cah_des == "STHCYS" || $_POST['cah_des'] =="STHCYS") { echo 'selected'; } echo '>STHCYS</option>
                </select>'; } 
				else 
				{ 
				    echo '<input type="hidden" name="cah_des" id="cah_des" value="'.$cah_des.'" >'; 
				    if ($cah_des == "CATEEM") { echo 'CATEEM'; }
				    if ($cah_des == "STHCYS") { echo 'STHCYS'; } 
				}?></td>
           	</tr>		
</table>			</td>
		      </tr>
		      <tr>
			<td class="etiquetas" valign="top">Primas:</td> 
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> <td class="etiquetas"> &nbsp;</td> <td class="etiquetas"> &nbsp;</td>  <td class="etiquetas"> Incremento</td> </tr>
			<tr><td><input name="hog_asg" id="hog_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$hog_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["hog_asg"]=='A' || $hog_asg == 'A') { echo 'checked'; } ?> title="Prima por Hogar">Hogar</td><td>
                          <?php if ($boton=='Modificar') { if ($hog_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="hog_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="hog_asg_inc"  placeholder="Bs."value="<?php if(! $existe) { echo $_POST['hog_asg_inc']; } else { echo $hog_asg_inc; } ?>" size="5" title="Incremento de Prima de Hogar ">
                          <?php if ($boton=='Modificar') { echo $hog_asg_inc; } ?></td>

			</tr>
			<tr><td><input name="hij_asg" id="hij_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$hij_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["hij_asg"]=='A' || $hij_asg == 'A') { echo 'checked'; } ?> title="Prima por Hijo(s)">Hijos</td><td>
                          <?php if ($boton=='Modificar') { if ($hij_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="hij_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="hij_asg_inc"  placeholder="Bs."value="<?php if(! $existe) { echo $_POST['hij_asg_inc']; } else { echo $hij_asg_inc; } ?>" size="5" title="Incremento de Prima de Hijos ">
                          <?php if ($boton=='Modificar') { echo $hij_asg_inc; } ?></td>
			</tr>
			<tr><td><input name="ant_asg" id="ant_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$ant_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["ant_asg"]=='A' || $ant_asg == 'A') { echo 'checked'; } ?> title="Prima por Antiguedad">Antiguedad</td><td>
                          <?php if ($boton=='Modificar') { if ($ant_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="ant_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ant_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['ant_asg_inc']; } else { echo $ant_asg_inc; } ?>" size="5" title="Incremento de Prima de Antiguedad ">
                          <?php if ($boton=='Modificar') { echo $ant_asg_inc; } ?></td>
			</tr>
			<tr><td><input name="pro_asg" id="pro_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$pro_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["pro_asg"]=='A' || $pro_asg == 'A') { echo 'checked'; } ?> title="Prima por Profesionalizacion">Profesionalizaci�n</td><td>
                          <?php if ($boton=='Modificar') { if ($pro_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="pro_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="pro_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['pro_asg_inc']; } else { echo $pro_asg_inc; } ?>" size="5" title="Incremento de Prima de Profesionalizaci�n ">
                          <?php if ($boton=='Modificar') { echo $pro_asg_inc; } ?></td>
			</tr>
			<!-- Nuevas para hula -->
			<tr><td><input name="tra_asg" id="tra_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$tra_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["tra_asg"]=='A' || $tra_asg == 'A') { echo 'checked'; } ?> title="Prima de Transporte">Transporte</td><td>
                          <?php if ($boton=='Modificar') { if ($tra_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="tra_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="tra_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['tra_asg_inc']; } else { echo $tra_asg_inc; } ?>" size="5" title="Incremento de Prima Transporte ">
                          <?php if ($boton=='Modificar') { echo $tra_asg_inc; } ?></td>
			</tr>
			<tr><td><input name="asi_asg" id="asi_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$asi_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["asi_asg"]=='A' || $asi_asg == 'A') { echo 'checked'; } ?> title="Prima Dedicaci�n a la Actividad de Salud">Dedicaci�n a la Actividad de Salud</td><td>
                          <?php if ($boton=='Modificar') { if ($asi_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="asi_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="asi_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['asi_asg_inc']; } else { echo $asi_asg_inc; } ?>" size="5" title="Incremento de Prima Asistencial ">
                          <?php if ($boton=='Modificar') { echo $asi_asg_inc; } ?></asi

			</tr>
			<tr><td><input name="spn_asg" id="spn_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$spn_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["spn_asg"]=='A' || $spn_asg == 'A') { echo 'checked'; } ?> title="Prima Sistema Publico Nacional de Salud">Sistema Publico Nacional de Salud</td><td>
                          <?php if ($boton=='Modificar') { if ($spn_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="spn_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="spn_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['spn_asg_inc']; } else { echo $spn_asg_inc; } ?>" size="5" title="Incremento de Prima Sistema Publico Nacional de Salud ">
                          <?php if ($boton=='Modificar') { echo $spn_asg_inc; } ?></spn
			</tr>
			<tr><td><input name="med_asg" id="med_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$med_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["med_asg"]=='A' || $med_asg == 'A') { echo 'checked'; } ?> title="Prima Medico Especialista">M�dico Especialista</td><td>
                          <?php if ($boton=='Modificar') { if ($med_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="med_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="med_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['med_asg_inc']; } else { echo $med_asg_inc; } ?>" size="5" title="Incremento de Prima Medico Especialista ">
                          <?php if ($boton=='Modificar') { echo $med_asg_inc; } ?></med
			</tr>
			<tr><td><input name="exc_asg" id="exc_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$exc_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["exc_asg"]=='A' || $exc_asg == 'A') { echo 'checked'; } ?> title="Prima Exclusividad"> Exclusividad</td><td>
                          <?php if ($boton=='Modificar') { if ($exc_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td>
			<td><input name="exc_asg_inc" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="exc_asg_inc" placeholder="Bs." value="<?php if(! $existe) { echo $_POST['exc_asg_inc']; } else { echo $exc_asg_inc; } ?>" size="5" title="Incremento de Prima Exclusividad ">
                          <?php if ($boton=='Modificar') { echo $exc_asg_inc; } ?></med
			</tr>
			<!-- <tr><td><input name="hje_asg" id="hje_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$hje_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["hje_asg"]=='A' || $hje_asg == 'A') { echo 'checked'; } ?> title="Prima por Hijos Escepcionales">Hijos Excepcionales</td><td>
                          <?php if ($boton=='Modificar') { if ($hje_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr> 
			<tr><td><input name="jer_asg" id="jer_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$jer_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["jer_asg"]=='A' || $jer_asg == 'A') { echo 'checked'; } ?> title="Prima por Jerarqu�a">Jerarqu�a</td><td>
                          <?php if ($boton=='Modificar') { if ($jer_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?> -->
			</td></tr>
			<!--<tr><td><input name="otr_asg" id="otr_asg" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$otr_asg.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["otr_asg"]=='A' || $otr_asg == 'A') { echo 'checked'; } ?> title="Otras Primas">Otras Primas</td><td>
                          <?php if ($boton=='Modificar') { if ($otr_asg=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr> -->
</table>			</td>
		      </tr>

		     <tr>
			<td class="etiquetas" valign="top">Bonos:</td>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr><td width="40%"><input name="bono_util" id="bono_util" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$bono_util.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["bono_util"]=='A' || $bono_util == 'A') { echo 'checked'; } ?> title="Bono Utiles Escolares">Utiles Escolares</td><td>
            <?php if ($boton=='Modificar') { if ($bono_util=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr>
			<tr><td width="40%"><input name="bono_juge" id="bono_juge" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$bono_juge.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["bono_juge"]=='A' || $bono_juge == 'A') { echo 'checked'; } ?> title="Bono Juguetes">Juguetes</td><td>
            <?php if ($boton=='Modificar') { if ($bono_juge=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr>
			</table>			
			</td>
		      </tr>
		   <!--   <tr>
			<td class="etiquetas" valign="top">Otros Aportes:</td>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr><td width="40%"><input name="sfu_des" id="sfu_des" <?php if ($boton=='Modificar') { echo 'type="hidden" value="'.$sfu_des.'" '; } else { echo 'type="checkbox" value="A"'; } if($_POST["sfu_des"]=='A' || $sfu_des == 'A') { echo 'checked'; } ?> title="Servicio Funerario">SFU (SOVENPFA)</td><td>
                          <?php if ($boton=='Modificar') { if ($sfu_des=='A') {echo ': SI'; } else { echo ': NO'; } } ?>
			</td></tr> -->
</table>			</td>
		      </tr>
              <tr><td colspan="2"><?php include ('../comunes/botonera_usr.php'); ?></td></tr>
		      <?php if ($boton == 'Modificar') { echo '<tr><td colspan="2" align="center"><hr></td></tr>'; ?>
		      <tr>
		        <td height="45" colspan="2" align="center" valign="top" class="etiquetas"><table width="100%" align="center" cellspacing="10">
                  <tr>
		    <td width="<?PHP echo (100/6)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('personal_familiares.php','v_familiares','Familiares',"ced_per=".$ced_per."&seccion=".$_GET['seccion']); } ?></td>
                    <td width="<?PHP echo (100/6)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('personal_cargos.php','v_cargos','Cargos',"ced_per=".$ced_per."&seccion=".$_GET['seccion']); } ?></td>
                    <td width="<?PHP echo (100/6)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('personal_cuentas.php','v_cuentas','Cuentas',"ced_per=".$ced_per."&seccion=".$_GET['seccion']); } ?></td>
					<td width="<?PHP echo (100/6)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('personal_asignaciones.php','v_asignaciones','Asignaciones',"ced_per=".$ced_per."&seccion=".$_GET['seccion']);  } ?></td>

<td width="<?PHP echo (100/6)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('personal_educacion.php','v_educacion','Educaci�n',"ced_per=".$ced_per."&seccion=".$_GET['seccion']);  } ?></td>


					<td width="<?PHP echo (100/6)."%"; ?>" align="center"><?php if($boton=='Modificar'){ abrir_ventana('personal_deducciones.php','v_deducciones','Deducciones',"ced_per=".$ced_per."&seccion=".$_GET['seccion']);  } ?></td>
                  </tr>
                </table>		          
                    <?php 	if($prm[1]=='A'){ 
								if($boton=='Modificar'){
									echo '<table width="100%" align="center" cellspacing="1">
									<tr>
									  <td width="50%" height="23" align="center">&nbsp;</td>
									</tr>';
									echo '<tr><td>'; include ('capa_cargos_personal.php'); echo '</tr></td>'; 
									echo '</table>';
								}
							} ?>
				</td>
		        </tr>
                    </table></td>
                  </tr>
		  <?php } ?> 
                  <tr>
                    <td colspan="2" align="center">
					<?php 
						$ncriterios =3; 
						$criterios[0] = "C�dula"; 
						$campos[0] ="ced_per";
						$criterios[1] = "Nombre";
						$campos[1] = "nom_per";
						$criterios[2] = "Apellido";
						$campos[2] = "ape_per";						
						if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                  
                  <tr>
                    <td></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<form action="http://www.ivss.gob.ve:8080/CuentaIndividualIntranet/CtaIndividual_PortalCTRL" method="post" id="myForm" name="frm_cta_individual" target="IVSS">
	<input type="hidden" name="nacionalidad_aseg" id="nacionalidad_aseg" value="<?php echo $nac_per; ?>">
	<input type="hidden" name="cedula_aseg" id="cedula_aseg" maxlength="9" value="<?php echo $ced_per; ?>">
	<?php
	$dia_nac=substr($fnac_per, 8, 2);
	$mes_nac=substr($fnac_per, 5, 2);
	$anno_nac=substr($fnac_per, 0, 4);
	?>
	<input type="hidden" name="y" id="y" value="<?php echo $anno_nac; ?>">
	<input type="hidden" name="m" id="m" value="<?php echo $mes_nac; ?>">
	<input type="hidden" name="d" id="d" value="<?php echo $dia_nac; ?>">						
</form>
