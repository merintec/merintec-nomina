<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<title>Generar Fideicomiso</title>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php include ('../comunes/formularios_funciones.php'); 
$prm = llamar_permisos ($_GET["seccion"]);
$mes_fide = $_POST[mes_fide];
$ano_fide = $_POST[ano_fide];

if($_POST['confirmar']=='Generar'){
	if($mes_fide==0 OR $ano_fide==0){
		echo '<SCRIPT>alert("'.$msg_falt_dat_fide.''.$ano_fide.'");</SCRIPT>';
	}else{ 
		$sql_pagado = "SELECT * FROM fideicomiso WHERE ano_fide = $ano_fide AND mes_fide = $mes_fide GROUP BY ano_fide, mes_fide";
		if($res = mysql_fetch_array(mysql_query($sql_pagado))){
			echo '<SCRIPT>alert("'.$msg_fide_ya_pagado.'");</SCRIPT>';
		}
		else {
			$generar_fide = 'SI';
		}
	}
}

if($_POST['confirmar']=='Consultar'){
	if($mes_fide==0 OR $ano_fide==0){
		echo '<SCRIPT>alert("'.$msg_falt_dat_fide.''.$ano_fide.'");</SCRIPT>';
	}else{ 
		$sql_pagado = "SELECT * FROM fideicomiso WHERE ano_fide = $ano_fide AND mes_fide = $mes_fide GROUP BY ano_fide, mes_fide";
		if($res = mysql_fetch_array(mysql_query($sql_pagado))){
			$consultar_fide = 'SI';
		}
		else {
			echo '<SCRIPT>alert("'.$msg_fide_no_pagado.'");</SCRIPT>';
		}
	}
}
?>
<form method="POST" action="" name="form1">
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
	    	<td>
	    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		        	<tr>
		            	<td>&nbsp;</td>
		          	</tr>
		          	<tr>
		            	<td>
		                	<table width="550" border="0" cellspacing="4" cellpadding="0" align="center">
		                  		<tr>
		                    		<td class="titulo">Generar Fideicomiso</td>
		                  		</tr>
		                  		<tr>
		                    		<td width="526"><table width="100%" border="0" cellspacing="8" cellpadding="0">
		                      	<tr>
		                        	<td width="33%" class="etiquetas_centradas">Mes</td>
		                        	<td  width="33%"  class="etiquetas_centradas">A&ntilde;o</td>
		                      	</tr>
		                      	<tr>
		                        	<td align="center"><label>
									<?php if (! $_POST['mes_fide']) { $mes_fide=date("m"); } else {$mes_fide = $_POST['mes_fide'];} ?>
		                        		<select name="mes_fide" title="Mes a procesar nomina" onchange="submit();">
				                          <option value="0" <?php if($mes_fide==0){echo 'selected';} ?>>Seleccione...</option>
				                          <option value="1" <?php if($mes_fide==1){echo 'selected';} ?>>Enero</option>
				                          <option value="2" <?php if($mes_fide==2){echo 'selected';} ?>>Febrero</option>
				                          <option value="3" <?php if($mes_fide==3){echo 'selected';} ?>>Marzo</option>
				                          <option value="4" <?php if($mes_fide==4){echo 'selected';} ?>>Abril</option>
				                          <option value="5" <?php if($mes_fide==5){echo 'selected';} ?>>Mayo</option>
				                          <option value="6" <?php if($mes_fide==6){echo 'selected';} ?>>Junio</option>
				                          <option value="7" <?php if($mes_fide==7){echo 'selected';} ?>>Julio</option>
				                          <option value="8" <?php if($mes_fide==8){echo 'selected';} ?>>Agosto</option>
				                          <option value="9" <?php if($mes_fide==9){echo 'selected';} ?>>Septiembre</option>
				                          <option value="10" <?php if($mes_fide==10){echo 'selected';} ?>>Octubre</option>
				                          <option value="11" <?php if($mes_fide==11){echo 'selected';} ?>>Noviembre</option>
				                          <option value="12" <?php if($mes_fide==12){echo 'selected';} ?>>Diciembre</option>
		                        		</select></label>
		                        	</td>
		                        	<td align="center">
		                          		<select name="ano_fide"  title="A&ntilde;o a procesar nomina" onchange="submit();">
								  			<?php
											$ano_act=date("Y");
											echo "<option value='$ano_act'>A�o: $ano_act</option>";
											for($i=2010;$i<$ano_act;$i++){
												echo"<option value='$i'";
													if($i==$ano_fide){ echo "selected";}
												echo">A�o: $i</option>";
											} ?>
		                          		</select>
		                          	</td>
		                      	</tr>
		                    </table>
		                </td>
	            	</tr>
		            <tr>
		            	<td align="center">
		            		<table width="350" border="0" cellspacing="5" cellpadding="0">
		            			<tr align="center">
		                        	<td><?php 
			                            if ($prm[0] == 'A' && $prm[1] == 'A') {
			                                echo '<input name="confirmar" type="submit" value="Generar">'; 
			                            }     
			                            ?>
		                        	</td>
		                        	<td>
		                        		<?php echo '<input name="confirmar" type="submit" value="Consultar">';  ?>
		                        	</td>
		                      	</tr>
		                    </table>
		                </td>
		            </tr>
		            <?php if ($generar_fide == 'SI'){ ?>
		            <tr align="center">
		            	<td>
		            		<?php abrir_ventana('fideicomiso_preliminar.php','v_nomina','Vista Preliminar',"ano_fide=".$_POST['ano_fide']."&mes_fide=".$_POST['mes_fide']); ?>
		            	</td>
		            </tr>	
		            <?php } ?>
		            <?php if ($consultar_fide == 'SI'){ ?>
		            <tr align="center">
		            	<td>
		            		<?php abrir_ventana('fideicomiso_visualizar.php','v_nomina','Visualizar',"ano_fide=".$_POST['ano_fide']."&mes_fide=".$_POST['mes_fide']); ?>
		            	</td>
		            </tr>	
		            <?php } ?>
	       		</table>
	       	</td>
	    </tr>
	</table>
</form>