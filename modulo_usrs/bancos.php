<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'bancos.php?seccion='.$_GET["seccion"];
$tabla = "banco";	// nombre de la tabla
$ncampos = "3";		//numero de campos del formulario
$datos[0] = crear_datos ("nom_ban","Banco",$_POST['nom_ban'],"1","50","alfabeticos");
$datos[1] = crear_datos ("cue_ban","Num Cuenta",$_POST['cue_ban'],"20","20","numericos");
$datos[2] = crear_datos ("des_ban","Destino de Cuenta la cuenta",$_POST['des_ban'],"1","50","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Banco";
		$datos[0]="nom_ban";
		$parametro[1]="N� de Cuenta";
		$datos[1]="cue_ban";
		$parametro[2]="Destino/Prop�sito";
		$datos[2]="des_ban";
		busqueda_varios(5,$buscando,$datos,$parametro,"cod_ban");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_ban = $row["cod_ban"];
	    $nom_ban = $row["nom_ban"];
	    $cue_ban = $row["cue_ban"];
	    $des_ban = $row["des_ban"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{	$boton = "Actualizar";
	$boton = comp_exist($datos[1][0],$datos[1][2]."' AND cod_ban <> '".$_POST['cod_ban'],$tabla,$boton,'si',"Bancos.");
	if ($boton == "Actualizar")
	{ $validacion = validando_campos ($ncampos,$datos); }
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_ban",$_POST["cod_ban"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
	$boton=comp_exist($datos[1][0],$datos[1][2],$tabla,$boton,'si',"Bancos");
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_ban"],"cod_ban",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="actualizar padre") 
{
	$boton = "Modificar";
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Datos de Bancos</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		      <tr>
                        <td class="etiquetas">Banco:</td>
                        <td width="75%">
									<input name="cod_ban" type="hidden" id="cod_ban" value="<?php if(! $existe) { echo $_POST["cod_ban"]; } else { echo $cod_ban; } ?>" size="35" title="Codigo de Banco">
                       		<?php escribir_campo('nom_ban',$_POST["nom_ban"],$nom_ban,'',50,20,'Nombre de la entidad Bancaria',$boton,$existe,''); ?>
                        </td>
                      </tr>
		      <tr>
                        <td class="etiquetas">N�m. de Cuenta:</td>
                        <td width="75%">
                        	<?php escribir_campo('cue_ban',$_POST["cue_ban"],$cue_ban,'',20,20,'N�mero de cuenta bancaria',$boton,$existe,''); ?>
                        </td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Destino de Cuenta:</td>
                        <td width="75%">
                        	<?php escribir_campo('des_ban',$_POST["des_ban"],$des_ban,'',50,20,'Destino de los fondos en la cuenta bancaria',$boton,$existe,''); ?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center">
                    		<?php if($boton=='Modificar'){ abrir_ventana('bancos_movimientos.php','v_movimientos','Movimientos',"cod_ban=".$cod_ban."&seccion=".$_GET['seccion']); } ?>
                   	 	<?php if($boton=='Modificar'){ abrir_ventana('bancos_libro.php','v_libro','Libro de Banco',"cod_ban=".$cod_ban."&seccion=".$_GET['seccion']); } ?>
                    </td>
                  </tr>
                  <tr>
                    <td>
			<?php 
			$ncriterios =2;
			$criterios[0] = "Banco";
			$campos[0] = "nom_ban";
			$criterios[1] = "Cuenta"; 
			$campos[1] ="cue_ban";
			if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {					
			crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
