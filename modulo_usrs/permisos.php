<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php 
include ('../comunes/formularios_funciones.php');
$post_dias_sol_perm = $_POST["dias_sol_perm"];
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'permisos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$tabla = "permisos_per";	// nombre de la tabla
$ncampos = "13";		//numero de campos del formulario
$datos[0] = crear_datos ("cod_sol_perm","Codigo de Solicitud",$_POST['cod_sol_perm'],"0","11","numericos");
$datos[1] = crear_datos ("fch_sol_perm","Fecha de Registro de Solicitud",$_POST['fch_sol_perm'],"1","10","fecha");
$datos[2] = crear_datos ("ced_per","C�dula del Solicitante",$_POST['ced_per'],"1","11","numericos");
$datos[3] = crear_datos ("nom_per","Nombre del Solicitante",$_POST['nom_per'],"1","100","alfabeticos");
$datos[4] = crear_datos ("nom_dep","Departamento",$_POST['nom_dep'],"1","100","alfanumericos");
$datos[5] = crear_datos ("nom_car","Cargo",$_POST['nom_car'],"0","100","alfabeticos");
$datos[6] = crear_datos ("dias_sol_perm","D�as del Permiso",$_POST['dias_sol_perm'],"1","2","numericos");
$datos[7] = crear_datos ("ini_sol_perm","Periodo de Permiso - Fecha de Inicio",$_POST['ini_sol_perm'],"1","10","fecha");
$datos[8] = crear_datos ("fin_sol_perm","Periodo de Permiso - Fecha de Fin",$_POST['fin_sol_perm'],"0","10","fecha");
$datos[9] = crear_datos ("mot_sol_perm","Motivo del Permiso",$_POST['mot_sol_perm'],"1","25","alfanumericos");
$datos[10] = crear_datos ("tip_sol_perm","Tipo de Permiso",$_POST['tip_sol_perm'],"0","20","alfanumericos");
$datos[11] = crear_datos ("obs_sol_perm","Observaci�n",$_POST['obs_sol_perm'],"0","255","alfanumericos");
$datos[12] = crear_datos ("apro_sol_perm","Estado de la solicitud",$_POST['apro_sol_perm'],"0","2","alfanumericos");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) 
	{ 
	    $tipo = "general";
	    $criterio_buscar = $_POST["criterio"];
	    $valor_buscar = $_POST["buscar_a"];
	    $buscando = busqueda_func($valor_buscar,$criterio_buscar,"$tabla",$pagina,$tipo);
	}
	elseif ($_POST["BuscarInd"]) { 
	$tipo = "individual"; 
	$buscando = busqueda_func($_POST["buscar_a"],"cod_sol_perm","$tabla",$pagina,$tipo);
	} 
	if (mysql_num_rows($buscando) > 1)
	{
		include ('../comunes/busqueda_varios.php');
		$parametro[0]="Nombre de Solicitante";
		$datos[0]="nom_per";	
		$parametro[1]="D�as";
		$datos[1]="dias_sol_perm";	
		$parametro[2]="Motivo";
		$datos[2]="mot_sol_perm";	
		$parametro[3]="Desde";
		$datos[3]="ini_sol_perm";
		$parametro[4]="Hasta";
		$datos[4]="fin_sol_perm";
		busqueda_varios(7,$buscando,$datos,$parametro,"cod_sol_perm");
		return;	}
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_sol_perm = $row["cod_sol_perm"];
	    $fch_sol_perm = $row["fch_sol_perm"];
	    $ced_per = $row["ced_per"];
	    $nom_per = $row["nom_per"];
	    $nom_dep = $row["nom_dep"];
	    $nom_car = $row["nom_car"];
	    $dias_sol_perm = $row["dias_sol_perm"];
	    $ini_sol_perm = $row["ini_sol_perm"];
	    $fin_sol_perm = $row["fin_sol_perm"];
   	    $mot_sol_perm = $row["mot_sol_perm"];
	    $tip_sol_perm = $row["tip_sol_perm"];
	    $obs_sol_perm = $row["obs_sol_perm"];
	    $apro_sol_perm = $row["apro_sol_perm"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++) 
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Motivo_Verificar" || $_POST["confirmar"]=="Motivo_Actualizar") 
{
    $boton = str_replace("Motivo_","",$_POST["confirmar"]);
}
if ($_POST["confirmar"]=="Fecha_tope_Verificar" || $_POST["confirmar"]=="Fecha_tope_Actualizar") 
{
    if ($_POST['dias_sol_perm']=='Seleccione...'){ 
        echo '<SCRIPT> alert ("Debe Indicar D�as del Permiso"); </SCRIPT>';
        $post_fin_sol_perm ="";
    }
    else {
        if ($_POST['mot_sol_perm']=="Salud" || $_POST['mot_sol_perm']=="Licencia Paternidad" || $_POST['mot_sol_perm']=="Muerte de Familiar" || $_POST['mot_sol_perm']=="Matrimonio" || $_POST['mot_sol_perm']=="Pre/Post Natal" || $_POST['mot_sol_perm']=="Enfermedad de Hijo(a)") { 
            $post_fin_sol_perm = calculo_fecha_continuos ($_POST['ini_sol_perm'],"+",$_POST['dias_sol_perm']-1);
        }
        else { 
    	    $post_fin_sol_perm = calculo_fecha ($_POST['ini_sol_perm'],"+",$_POST['dias_sol_perm']-1);
    	}   	
    	if ($post_fin_sol_perm=='1969-12-31'){ $post_fin_sol_perm = $_POST['ini_sol_perm']; }
	    if (! $_POST['ini_sol_perm']) { $post_fin_sol_perm =""; }
	}
	$boton = str_replace("Fecha_tope_","",$_POST["confirmar"]);
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_sol_perm",$_POST["cod_sol_perm"],$pagina);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
    $post_fin_sol_perm = $_POST["fin_sol_perm"];
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
    $post_fin_sol_perm = $_POST["fin_sol_perm"];
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { $boton = "Guardar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{  
     $post_fin_sol_perm = $_POST["fin_sol_perm"];
	insertar_func($ncampos,$datos,$tabla,$pagina);
	auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_sol_perm"],"cod_sol_perm",$tabla,$pagina);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_sol_perm","Permisos Solicitados",$pagina);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Solicitud de Permiso</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		              <tr>
                        <td class="etiquetas">Fecha: </td>
                        <td>
                            <input name="cod_sol_perm" type="hidden" id="cod_sol_perm" value="<?php if(! $existe) { echo $_POST['cod_sol_perm']; } else { echo $cod_sol_perm; } ?>" size="35" title="Codigo de la solicitud">
                            <input name="fch_sol_perm" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_sol_perm" readonly value="<?php if(! $existe) { if (! $_POST['fch_sol_perm']) { echo date('Y-m-d'); } else { echo $_POST['fch_sol_perm']; } } else { echo $fch_sol_perm; } ?>" size="20" title="Fecha de Solicitud" />
                          <?php if ($boton=='Modificar') { echo $fch_sol_perm; } ?>
			</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Departamento:</td>
                        <td width="75%">
			<?php if ($nom_dep == "" && $_POST["nom_dep"] == "") { 




			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_dep = $row2['nom_dep'];	
			} ?>
			<input name="nom_dep" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_dep" readonly value="<?php echo $nom_dep; ?>" size="35" title="Departamento">
                        <?php if ($boton=='Modificar') { echo $nom_dep; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td width="75%">
			<?php if ($nom_car == "" && $_POST["nom_car"] == "") { 
			$sql2 = "SELECT c.fch_asg, c.nom_car, d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_car = $row2['nom_car'];			
			$fch_ing = $row2['fch_asg'];		
			} ?>
			<input name="nom_car" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_car" readonly value="<?php echo $nom_car; ?>" size="35" title="Cargo que desempe�a">
                        <?php if ($boton=='Modificar') { echo $nom_car; } ?></td>
                      </tr>    

		      <tr>
                        <td class="etiquetas">C�dula:</td>
                        <td width="75%">
			<?php if ($ced_per == "" && $_POST["ced_per"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$ced_per = $_COOKIE['uscod'];
			} ?>
			<input name="ced_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ced_per" readonly value="<?php echo $ced_per; ?>" size="35" title="C�dula del solicitante">
                        <?php if ($boton=='Modificar') { echo $ced_per; } ?></td>
                      </tr>
                      

		      <tr>
                        <td class="etiquetas">Nombre:</td>
                        <td width="75%">
			<?php if ($nom_per == "" && $_POST["nom_per"] == "") { 
			$sql2 = "SELECT d.nom_dep, d.cod_dep FROM cargos c, dependencias d WHERE c.ced_per = ".$_COOKIE['uscod']." AND c.cod_dep = d.cod_dep";
			$row2 = mysql_fetch_array(mysql_query ($sql2));
			$nom_per = $_COOKIE['usnombre'];
			} ?>
			<input name="nom_per" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="nom_per" readonly value="<?php echo $nom_per; ?>" size="35" title="Nombre y Apellido del solicitante">
                        <?php if ($boton=='Modificar') { echo $nom_per; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Motivos de Permiso:</td>
                        <?php 
                        $add_mot = 'onchange="confirmacion_func(';
                        $add_mot .= "'Motivo_".$boton."')";
                        $add_mot .= '"';
                        ?>
                        <td><?php if ($boton != "Modificar") { echo '<select '.$add_mot.' name="mot_sol_perm" title="Tipo de Permiso" >
                          <option>Seleccione...</option>
                          <option value="Personales" '; if ($mot_sol_perm == "Personales" || $_POST['mot_sol_perm'] =="Personales") { echo 'selected'; } echo '>Personales</option>
                          <option value="Salud" '; if ($mot_sol_perm == "Salud" || $_POST['mot_sol_perm'] =="Salud") { echo 'selected'; } echo '>Salud</option>
                          <option value="Matrimonio" '; if ($mot_sol_perm == "Matrimonio" || $_POST['mot_sol_perm'] =="Matrimonio") { echo 'selected'; } echo '>Matrimonio</option>
                          <option value="Estudios" '; if ($mot_sol_perm == "Estudios" || $_POST['mot_sol_perm'] =="Estudios") { echo 'selected'; } echo '>Estudios</option>
                          <option value="Estudios de Hijo/Hija" '; if ($mot_sol_perm == "Estudios de Hijo/Hija" || $_POST['mot_sol_perm'] =="Estudios de Hijo/Hija") { echo 'selected'; } echo '>Estudios de Hijo/Hija</option>
                          <option value="Act. Hijo/Hija" '; if ($mot_sol_perm == "Act. Hijo/Hija" || $_POST['mot_sol_perm'] =="Act. Hijo/Hija") { echo 'selected'; } echo '>Act. Hijo/Hija</option>                          
                          <option value="Pre/Post Natal" '; if ($mot_sol_perm == "Pre/Post Natal" || $_POST['mot_sol_perm'] =="Pre/Post Natal") { echo 'selected'; } echo '>Pre/Post Natal</option>
                          <option value="Licencia Paternidad" '; if ($mot_sol_perm == "Licencia Paternidad" || $_POST['mot_sol_perm'] =="Licencia Paternidad") { echo 'selected'; } echo '>Licencia Paternidad</option>
                          <option value="Enfermedad de Hijo(a)" '; if ($mot_sol_perm == "Enfermedad de Hijo(a)" || $_POST['mot_sol_perm'] =="Enfermedad de Hijo(a)") { echo 'selected'; } echo '>Enfermedad de Hijo(a)</option>
                          <option value="Muerte de Familiar" '; if ($mot_sol_perm == "Muerte de Familiar" || $_POST['mot_sol_perm'] =="Muerte de Familiar") { echo 'selected'; } echo '>Muerte de Familiar</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="mot_sol_perm" id="mot_sol_perm" value="'.$mot_sol_perm.'" >'; 
						    if ($mot_sol_perm == "Personales") { echo 'Personales'; } 
						    if ($mot_sol_perm == "Salud") { echo 'Salud'; } 
						    if ($mot_sol_perm == "Matrimonio") { echo 'Matrimonio'; } 
						    if ($mot_sol_perm == "Estudios") { echo 'Estudios'; } 
						    if ($mot_sol_perm == "Estudios de Hijo/Hija") { echo 'Estudios de Hijo/Hija'; } 
						    if ($mot_sol_perm == "Act. Hijo/Hija") { echo 'Act. Hijo/Hija'; } 
						    if ($mot_sol_perm == "Pre/Post Natal") { echo 'Pre/Post Natal'; } 
						    if ($mot_sol_perm == "Licencia Paternidad") { echo 'Licencia Paternidad'; } 
						    if ($mot_sol_perm == "Enfermedad de Hijo(a)") { echo 'Enfermedad de Hijo(a)'; } 
						    if ($mot_sol_perm == "Muerte de Familiar") { echo 'Muerte de Familiar'; } 
						}?></td>
                      <tr>
                        <td class="etiquetas">D�as del Permiso:</td>
                        <td><?php if ($boton != "Modificar" && $boton != "Guardar") { 
                        $add_ini = 'onchange="confirmacion_func(';
                        $add_ini .= "'Fecha_tope_".$boton."')";
                        $add_ini .= '"';
                        echo '<select name="dias_sol_perm" title="D�as a disfrutar en el per�odo de vaciones" '.$add_ini.'>
                          <option>Seleccione...</option>';
            $i_ini=1; $i_fin=0;
            if ($mot_sol_perm == "Personales") { $i_ini=1; $i_fin=3; } 
			if ($mot_sol_perm == "Salud") { $i_ini=1; $i_fin=60; } 
			if ($mot_sol_perm == "Matrimonio") { $i_ini=10; $i_fin=10; } 
			if ($mot_sol_perm == "Estudios") { $i_ini=1; $i_fin=15; } 
			if ($mot_sol_perm == "Estudios de Hijo/Hija") { $i_ini=1; $i_fin=1; } 
			if ($mot_sol_perm == "Act. Hijo/Hija") { $i_ini=1; $i_fin=5; } 
			if ($mot_sol_perm == "Pre/Post Natal") { $i_ini=84; $i_fin=84; } 
			if ($mot_sol_perm == "Licencia Paternidad") { $i_ini=14; $i_fin=14; } 
			if ($mot_sol_perm == "Enfermedad de Hijo(a)") { $i_ini=1; $i_fin=30; } 
			if ($mot_sol_perm == "Muerte de Familiar") { $i_ini=9; $i_fin=9; } 
			for($i=$i_ini;$i<=$i_fin;$i++){
			echo '<option value="'.$i.'"'; if ($dias_sol_perm == $i || $_POST['dias_sol_perm'] == $i) { echo 'selected'; } echo '>'.$i.' d�as </option>';
			}
			echo '</select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="dias_sol_perm" id="dias_sol_perm" value="'.$dias_sol_perm.'" > '.$dias_sol_perm.' d&iacute;as'; 
						}?>
						
						</td>
                      </tr>
		      <tr>
                        <td class="etiquetas">Per�odo del Permiso: </td>
			<?php if ($ini_sol_perm == "0000-00-00") { $ini_sol_perm="";} ?>
			<?php if ($fin_sol_perm == "0000-00-00") { $fin_sol_perm="";} ?>			
                        <td>del: <input name="ini_sol_perm" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ini_sol_perm" value="<?php if(! $existe) { echo $_POST['ini_sol_perm']; } else { echo $ini_sol_perm; } ?>" size="8" title="Fecha de inicio del per�odo de Permiso" onchange="confirmacion_func('Fecha_tope_<?php echo $boton; ?>')"><?php if ($boton=='Modificar') { echo $ini_sol_perm; } ?><?php if ($boton!='Modificar') { ?><img src="../imagenes/imagenes_cal/cal.gif" width="20" height="17" onclick="displayCalendar(document.forms[0].ini_sol_perm,'yyyy-mm-dd',this)" title="Haga click aqui para elegir una fecha"/><?php } ?>
&nbsp;&nbsp;&nbsp;al: <input name="fin_sol_perm" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fin_sol_perm" readonly value="<?php if(! $existe) { echo $post_fin_sol_perm; } else { echo $fin_sol_perm; } ?>" size="8" title="Fecha de fin del per�odo de Permiso" />
                          <?php if ($boton=='Modificar') { echo $fin_sol_perm; } ?>
			             </td>
                      </tr> 
                      <tr>
                        <td width="25%" class="etiquetas">Observaciones:</td>
                        <td>
                            <?php escribir_campo('obs_sol_perm',$_POST["obs_sol_perm"],$obs_sol_perm,'',255,35,'Observaciones o Informaci�n adicional',$boton,$existe,'')?>
                        </td>
                      </tr>
		      <?php if ($boton!='Verificar' && $boton!='Guardar') { ?>
                      <tr>
                        <td class="etiquetas">Tipo:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="tip_sol_perm" title="Tipo de Permiso">
                          <option>Seleccione...</option>

                          <option value="N" '; if ($tip_sol_perm == "N" || $_POST['tip_sol_perm'] =="N") { echo 'selected'; } echo '>No Remunerado</option>
                          <option value="R" '; if ($tip_sol_perm == "R" || $_POST['tip_sol_perm'] =="R") { echo 'selected'; } echo '>Remunerado</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="tip_sol_perm" id="tip_sol_perm" value="'.$tip_sol_perm.'" >'; 
						    	if (!$tip_sol_perm) { echo 'Por Determinar'; } 
    					    	if ($tip_sol_perm == "N") { echo 'No Remunerado'; } 
    							if ($tip_sol_perm == "R") { echo 'Remunerado'; }
						}?></td>
                      </tr>
		      <?php }?>
		      <?php if ($boton!='Verificar' && $boton!='Guardar') { ?>
                      <tr>
                        <td class="etiquetas">Estado:</td>
                        <td><?php if ($boton != "Modificar") { echo '<select name="apro_sol_perm" title="Estado">
                          <option value="N" '; if ($apro_sol_perm == "N" || $_POST['apro_sol_perm'] =="N" || $apro_sol_perm == "" || $_POST['apro_sol_perm'] =="") { echo 'selected'; } echo '>Seleccione...</option>
                          <option value="A" '; if ($apro_sol_perm == "A" || $_POST['apro_sol_perm'] =="A") { echo 'selected'; } echo '>Aprobada</option>
                          <option value="R" '; if ($apro_sol_perm == "R" || $_POST['apro_sol_perm'] =="R") { echo 'selected'; } echo '>Rechazada</option>
                        </select>'; } 
						else 
						{ 
						    echo '<input type="hidden" name="apro_sol_perm" id="apro_sol_perm" value="'.$apro_sol_perm.'" >'; 
						    	if (!$apro_sol_perm) { echo 'Por Aprobar'; } 
    					    	if ($apro_sol_perm == "A") { echo 'Aprobada'; } 
    							if ($apro_sol_perm == "R") { echo 'Rechazada'; }
						}?></td>
                      </tr>
		      <?php }?>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php if($boton=='Modificar' && $apro_sol_perm == 'A'){ abrir_ventana('imprimir_permisos.php','v_imprimir','Imprimir Solicitud',"cod_sol_perm=".$cod_sol_perm); } ?></td>
                  </tr>
                  <tr>
                    <td>
                        <hr>
                        <center><b>Solicitud de Permisos Efectuados</b></center>
                        <?php include('capa_permisos.php'); ?>
                    </td>
                  </tr>
                  <?php if ($_COOKIE['uspriv']==2) { ?> 
                  <tr>
                    <td>
					<?php 
						$ncriterios =1; 
						$criterios[0] = "C�dula"; 
						$campos[0] ="ced_per";
					  if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
					  crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } 
					     $funcion_combo = '"valor_acampo(this.value, ';
					     $funcion_combo .= "'buscar_a')";
					     $funcion_combo .= '";';
                                             echo '<center>Buscar C�dula: '; 

                                             combo('ced_per2', $ced_per3, 'vista_personal', $link, 0, 1, 0, '', 'ced_per', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nombre");?></td>
                  </tr>
                  <?php } ?>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
