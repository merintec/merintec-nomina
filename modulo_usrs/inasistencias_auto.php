<table><td>&nbsp;</td><td>
<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php include ('../comunes/formularios_funciones.php');
/// Preparando las sentencias SQL para insertar en la base de datos la inasistencias o las deducciones por retardo 
$sql_cont = 0;
$sql_querys = array();
/// Verifica cuantos dias tiene el mes
$dias_del_mes = dias_mes ($_GET['mes'],$_GET['ano']); 
$por_mes = $_GET['por_mes'];
if ($por_mes==1) { 
    $titulo_add = '1era Quincena'; 
    $ini_rep = 1;
    $fin_rep = 15;
}
if ($por_mes==2) { 
    $titulo_add = '2da Quincena';
    $ini_rep = 16;
    $fin_rep = $dias_del_mes; 
}
if ($por_mes==3) {
    $titulo_add = '';
    $ini_rep = 1;
    $fin_rep = $dias_del_mes; 
}
$titulo = "Resgistro de Asistencia ".$titulo_add." del Mes de ".convertir_mes($_GET['mes'])." de ".$_GET['ano']; 
echo '<title>'.$titulo.'</title>';
/// Matriz con los d�as de la Semana
$diasmes[0] = "Dom";
$diasmes[1] = "Lun";
$diasmes[2] = "Mar";
$diasmes[3] = "Mi�";
$diasmes[4] = "Jue";
$diasmes[5] = "Vie";
$diasmes[6] = "S�b";
/// Colsulta de personal
$i=0;
$sql_per = "Select * From personal, cargos, horarios where personal.ced_per=cargos.ced_per AND cargos.cod_hor=horarios.cod_hor Order By personal.ape_per,personal.nom_per";
$bus_per = mysql_query($sql_per);
while ($row_per=@mysql_fetch_array($bus_per)){
    $datos[$i][0] = $row_per['ced_per'];
    $datos[$i][1] = $row_per['ape_per']." ".$row_per['nom_per'];
    $i = $i+1;
}
/// Consulta de D�as Feriados
$sql_fer = "Select * From feriados where MONTH(fch_frd)=".$_GET['mes']." And YEAR(fch_frd)=".$_GET['ano'];
$bus_fer = mysql_query($sql_fer);
$f=1;
while ($row_fer=@mysql_fetch_array($bus_fer)){
    $feriados[$row_fer['fch_frd']][0] = $row_fer['fch_frd'];
    $feriados[$row_fer['fch_frd']][1] = $row_fer['des_frd'];
    $feriados[$row_fer['fch_frd']][2] = $row_fer['tip_frd'];
}
/// Consulta de vacaciones
$sql_vac = "Select * From vacaciones_per where apro_sol_vac='A' AND ((MONTH(ini_sol_vac)=".$_GET['mes']." And YEAR(ini_sol_vac)=".$_GET['ano'].") OR (MONTH(fin_sol_vac)=".$_GET['mes']." And YEAR(fin_sol_vac)=".$_GET['ano']."))";
$bus_vac = mysql_query($sql_vac);
while ($row_vac=@mysql_fetch_array($bus_vac)){
    $fecha_actual = $row_vac['ini_sol_vac'];
    while ($fecha_actual<=$row_vac['fin_sol_vac']){
        $vacaciones[$row_vac['ced_per']][0] = $row_vac['ced_per'];
        $vacaciones[$row_vac['ced_per']][$fecha_actual] = $fecha_actual;
        $fecha_actual = strtotime($fecha_actual);
        $calculo = strtotime("+1 days", $fecha_actual); //Le suamos 1 d�as
        $fecha_actual = date("Y-m-d", $calculo);
    }
}
/// Consulta de Permisos
$sql_perm = "Select * From permisos_per p, cargos c, sueldos s where apro_sol_perm='A' AND ((MONTH(ini_sol_perm)<=".$_GET['mes']." And YEAR(ini_sol_perm)<=".$_GET['ano'].") OR (MONTH(fin_sol_perm)=".$_GET['mes']." And YEAR(fin_sol_perm)=".$_GET['ano'].")) AND p.ced_per=c.ced_per AND c.cod_sue=s.cod_sue";
$bus_perm = mysql_query($sql_perm);
while ($row_perm=@mysql_fetch_array($bus_perm)){
    $fecha_actual = $row_perm['ini_sol_perm'];
    while ($fecha_actual<=$row_perm['fin_sol_perm']){
        $permisos[$row_perm['ced_per']][$fecha_actual][0] = $row_perm['ced_per'];
        $permisos[$row_perm['ced_per']][$fecha_actual][1] = $fecha_actual;
        $permisos[$row_perm['ced_per']][$fecha_actual][2] = $row_perm['mot_sol_perm'];
        $permisos[$row_perm['ced_per']][$fecha_actual][3] = $row_perm['tip_sol_perm'];
        $fecha_actual = strtotime($fecha_actual);
        if ($row_perm['tip_sol_perm']=='N'){
            $descontar = redondear (($row_perm['mon_sue']/30),2,'','.');
            $sql_querys[$sql_cont] = "INSERT INTO deducciones (con_dsp, ncuo_dsp, mcuo_dsp, cod_des, ced_per, cod_car, fch_dsp, ncp_dsp) values ";
            $sql_querys[$sql_cont].= "('Permiso No Remunerado ".date("d-m-Y", $fecha_actual)."', 1, $descontar, 7, ".$row_perm['ced_per'].", ".$row_perm['cod_car'].", '".date("Y-m-d", $fecha_actual)."', 0)";
            $sql_cont = $sql_cont+1;      
        }
        $calculo = strtotime("+1 days", $fecha_actual); //Le suamos 1 d�as
        $fecha_actual = date("Y-m-d", $calculo);
    }
}
/// Consulta de justificaciones
$sql_jus = "Select * From justificativos_per where apro_sol_jus='A' AND ((MONTH(ini_sol_jus)=".$_GET['mes']." And YEAR(ini_sol_jus)=".$_GET['ano'].") OR (MONTH(fin_sol_jus)=".$_GET['mes']." And YEAR(fin_sol_jus)=".$_GET['ano']."))";
$bus_jus = mysql_query($sql_jus);
while ($row_jus=@mysql_fetch_array($bus_jus)){
    $fecha_actual = $row_jus['ini_sol_jus'];
    while ($fecha_actual<=$row_jus['fin_sol_jus']){
        $justificativos[$row_jus['ced_per']][$fecha_actual][0] = $row_jus['ced_per'];
        $justificativos[$row_jus['ced_per']][$fecha_actual][1] = $fecha_actual;
        $justificativos[$row_jus['ced_per']][$fecha_actual][2] = $row_jus['mot_sol_jus'];
        $justificativos[$row_jus['ced_per']][$fecha_actual][3] = $row_jus['obs_sol_jus'];
        $fecha_actual = strtotime($fecha_actual);
        $calculo = strtotime("+1 days", $fecha_actual); //Le suamos 1 d�as
        $fecha_actual = date("Y-m-d", $calculo);
    }
}
/// Consulta de Horarios
$sql_hor = "select c.*, h.* from cargos c, horarios h where c.cod_hor = h.cod_hor";
$res_hor = mysql_query ($sql_hor);
while ($row_hor=@mysql_fetch_array($res_hor)) {
    $hor_per[$row_hor['ced_per']][0]=$row_hor['ced_per'];
    $hor_per[$row_hor['ced_per']][1]=$row_hor['lun_ini_hor'];
    $hor_per[$row_hor['ced_per']][6]=$row_hor['lun_fin_hor'];
    $hor_per[$row_hor['ced_per']][2]=$row_hor['mar_ini_hor'];
    $hor_per[$row_hor['ced_per']][7]=$row_hor['mar_fin_hor'];
    $hor_per[$row_hor['ced_per']][3]=$row_hor['mie_ini_hor'];
    $hor_per[$row_hor['ced_per']][8]=$row_hor['mie_fin_hor'];
    $hor_per[$row_hor['ced_per']][4]=$row_hor['jue_ini_hor'];
    $hor_per[$row_hor['ced_per']][9]=$row_hor['jue_fin_hor'];
    $hor_per[$row_hor['ced_per']][5]=$row_hor['vie_ini_hor'];
    $hor_per[$row_hor['ced_per']][10]=$row_hor['vie_fin_hor']; 
    $hor_per[$row_hor['ced_per']][11]=$row_hor['tol_hor'];  
    $hor_per[$row_hor['ced_per']][21]=$row_hor['lun_ini_hor2'];
    $hor_per[$row_hor['ced_per']][26]=$row_hor['lun_fin_hor2'];
    $hor_per[$row_hor['ced_per']][22]=$row_hor['mar_ini_hor2'];
    $hor_per[$row_hor['ced_per']][27]=$row_hor['mar_fin_hor2'];
    $hor_per[$row_hor['ced_per']][23]=$row_hor['mie_ini_hor2'];
    $hor_per[$row_hor['ced_per']][28]=$row_hor['mie_fin_hor2'];
    $hor_per[$row_hor['ced_per']][24]=$row_hor['jue_ini_hor2'];
    $hor_per[$row_hor['ced_per']][29]=$row_hor['jue_fin_hor2'];
    $hor_per[$row_hor['ced_per']][25]=$row_hor['vie_ini_hor2'];
    $hor_per[$row_hor['ced_per']][30]=$row_hor['vie_fin_hor2'];
}
// consulta biostar
include('inasistencias_auto_biostar.php');
?>
<?php echo '<h1><center>'.$organizacion.'<br>'.$titulo.'</center></h1>'; ?>
<table border="1" <?php if ($por_mes==3){ echo 'width="1360px"'; }?> bordercolor="#000000" align="center" cellpadding="0" cellspacing="0">
<?php /// Para el Encabezado de la tabla?>
    <tr align="center">
        <td><b>N�</b></td>
        <td width="500px"><b>Apellidos y Nombre</b></td>
        <?php for ($j=$ini_rep;$j<=$fin_rep;$j++)
              {
                $fch_num = $_GET['ano'].'-'.$_GET['mes'].'-';
                if ($j<10) { $fch_num .= '0'; }
                $fch_num .= $j;
                $fch_num = strtotime($fch_num);  
                $dia_num = date("w",$fch_num);
                // para incluir solo de lunes a viernes
                if ($dia_num!=0 && $dia_num!=6) {
                    echo '<td colspan="2">';
                    echo $diasmes[$dia_num].'<br>';
                    if ($j<10){ echo '<b>0</b>'; }
                    echo '<b>'.$j.'</b>'; 
                    echo '</td>';
                }
              } ?>
     </tr>
     <?php
     /// Para mostrar los Datos
        for($i2=0;$i2<$i;$i2++) {
           echo '<tr>';
           echo '<td align="right"><b>'.($i2+1).'</b>&nbsp;</td>';
           echo '<td>&nbsp;'.$datos[$i2][1].'</td>'; 
           for ($j=$ini_rep;$j<=$fin_rep;$j++) {
                $fch_num = $_GET['ano'].'/'.$_GET['mes'].'/';
                if ($j<10) { $fch_num .= '0'; }
                $fch_num .= $j;
                $fch_num = strtotime($fch_num);  
                $dia_num = date("w",$fch_num);
                // para incluir solo de lunes a viernes
                if ($dia_num!=0 && $dia_num!=6) {
                    /// Verificar si hay un feriado
                    $verificador = $_GET['ano']."-".$_GET['mes']."-";
                    if ($j<10) { $verificador .= '0'; }
                    $verificador .= $j;
                    if ($feriados[$verificador][1]!="") {
                        echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/'.$feriados[$verificador][2].'.png" title="'.$feriados[$verificador][1].'"></td>';
                    }
                    /// Verificar si est� de vacaciones
                    elseif ($vacaciones[($datos[$i2][0])][$verificador]!=""){
                        echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/vacaciones.png" title="En Vacaciones"></td>';
                    }
                    /// Verificar si est� de Permiso
                    elseif ($permisos[($datos[$i2][0])][$verificador][0]!=""){
                        $des_mot = $permisos[($datos[$i2][0])][$verificador][2];
                        $des_tip = $permisos[($datos[$i2][0])][$verificador][3];
                        if ($des_tip=='N'){ $img_add = '_n'; $tit_add = 'No Remunerado'; }
                        else { $img_add = '_p'; $tit_add = 'Remunerado'; }
                        if ($des_mot=='Salud') { $img_add = '_salud'; $tit_add = 'Permiso M�dico'; }
                        if ($des_mot=='Pre/Post Natal') { $img_add = '_natal'; $tit_add = 'Permiso Pre/Post Natal'; }
                        if ($des_mot=='Licencia Paternidad') { $img_add = '_natal'; $tit_add = 'Licencia Paternidad'; }
                        echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/permiso'.$img_add.'.png" title="En Permiso '.$tit_add.' - '.$des_mot.'"></td>';
                    }
                    /// Verificar si existe una justificaci�n
                    elseif ($justificativos[($datos[$i2][0])][$verificador][0]!=""){
                        $des_mot = $justificativos[($datos[$i2][0])][$verificador][2];
                        $des_tip = $justificativos[($datos[$i2][0])][$verificador][3];
                        if ($des_mot=='fuera'){ $img_add = 'fuera'; $tit_add = 'Trabajo Fuera de Oficina - '.$des_tip; }
                        else { $img_add = 'justificado'; $tit_add = 'Situaci�n Justificada - '.$des_tip; }
                        
                        echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/'.$img_add.'.png" title="'.$tit_add.'"></td>';
                    }
                    
                    /// si es el contralor
                    elseif ($datos[$i2][0]=="8023210"){
                        echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/correcto.png" title="Contralor"></td>';
                    }
                    /// si es 05 o 06  y es carlos puentes
                    elseif (($verificador=="2012-01-05" || $verificador=="2012-01-06") && $datos[$i2][0] ==4264264 ) {
                            echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/errado.png" title="Contralor"></td>';
                    }
                    /// si nueva contralora
                    elseif (($verificador>="2012-07-19") && $datos[$i2][0] ==5204088 ) {
                            echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/correcto.png" title="Contralor"></td>';
                    }
                    /// si es 05 o 06
                    elseif ($verificador=="2012-01-05" || $verificador=="2012-01-06") {
                            echo '<td colspan="2" align="center"><img src="../imagenes/ico_ina/correcto.png" title="Contralor"></td>';
                    }
                    
                    else{
                        // muestra la asistencia
                        $verificador2 = str_replace("-","/",$verificador); 
                        $entrada_status = $asistencias[($datos[$i2][0])][$verificador2][4];
                        $entrada_hora = $asistencias[($datos[$i2][0])][$verificador2][2];
                        if (!$entrada_status) { 
                        $entrada_status = 'errado';
                        $entrada_hora = 'Huella no registrada';
                        $sql_querys[$sql_cont] = "INSERT INTO inasistencias (ced_per, fch_ina, tip_ina, des_ina) values (".$datos[$i2][0].",'".$verificador2."','No Remunerada','NO ASISTI�');";
                        $sql_cont = $sql_cont+1;
                        }
                        $salida_status = $asistencias[($datos[$i2][0])][$verificador2][5];
                        $salida_hora = $asistencias[($datos[$i2][0])][$verificador2][3];
                        if (!$salida_status) { 
                        $salida_status = 'errado';
                        $salida_hora = 'Huella no registrada';
                        }
                        echo '<td>';
                        echo '<img src="../imagenes/ico_ina/'.$entrada_status.'.png" title="'.$entrada_hora.'">';
                        echo '</td>';
                        echo '<td>';
                        echo '<img src="../imagenes/ico_ina/'.$salida_status.'.png" title="'.$salida_hora.'">';
                        echo '</td>';                   
                    }

                }
              }
            echo '</tr>';
        }?>
</table>
<br>
<table border="1" bordercolor="#000000" cellpadding="0" cellspacing="0">
    <tr><td colspan="2"><b>Descripci�n de Iconos</b></td></tr>
    <tr><td><img src="../imagenes/ico_ina/correcto.png" title="Ingreso/Salida a Tiempo"></td><td>&nbsp;Ingreso/Salida a Tiempo</td></tr>
    <tr><td><img src="../imagenes/ico_ina/fuera_tiempo.png" title="Ingreso/Salida fuera de Tiempo"></td></td><td>&nbsp;Ingreso/Salida fuera de Tiempo</td></tr>
    <tr><td><img src="../imagenes/ico_ina/errado.png" title="No Registrado"></td></td><td>&nbsp;No Registrado</td></tr>
    <tr><td><img src="../imagenes/ico_ina/feriado.png" title="Feriado"></td></td><td>&nbsp;Feriado</td></tr>
    <tr><td><img src="../imagenes/ico_ina/municipal.png" title="Jubilo/Feriado Municipal"></td></td><td>&nbsp;Jubilo/Feriado Municipal</td></tr>
    <tr><td><img src="../imagenes/ico_ina/fiesta_nacional.png" title="Fiesta Nacional"></td></td><td>&nbsp;Fiesta Nacional</td></tr>
    <tr><td><img src="../imagenes/ico_ina/no_laboral.png" title="No Loborable"></td></td><td>&nbsp;No Loborable</td></tr>
    <tr><td><img src="../imagenes/ico_ina/fuera.png" title="Trabajo Fuera de Oficina"></td></td><td>&nbsp;Trabajo Fuera de Oficina</td></tr>
    <tr><td><img src="../imagenes/ico_ina/vacaciones.png" title="En Vacaciones"></td></td><td>&nbsp;En Vacaciones</td></tr>
    <tr><td><img src="../imagenes/ico_ina/justificado.png" title="Situaci�n Justificada"></td></td><td>&nbsp;Situaci�n Justificada</td></tr>
    <tr><td><img src="../imagenes/ico_ina/permiso_n.png" title="Permiso No Remunerado Solicitado"></td></td><td>&nbsp;Permiso No Remunerado Solicitado </td></tr>
    <tr><td><img src="../imagenes/ico_ina/permiso_p.png" title="Permiso Remunerado Solicitado"></td></td><td>&nbsp;Permiso Remunerado Solicitado </td></tr>
    <tr><td><img src="../imagenes/ico_ina/permiso_salud.png" title="Permiso M�dico"></td></td><td>&nbsp;Permiso M�dico</td></tr>
    <tr><td><img src="../imagenes/ico_ina/permiso_natal.png" title="Permiso Pre/Post Natal - Licencia por Paternidad"></td></td><td>&nbsp;Permiso Pre/Post Natal - Licencia por Paternidad</td></tr>
</table>
<br>
</td>
<td>&nbsp;</td>
</table>
<form id="form1" name="form1" method="post" action="">
<table width="100%">
    <tr>
        <td align="center">
            <input name="procesar_val" type="hidden" id="procesar_val" value="<?php echo $_POST['procesar_val']; ?>">
            <?php
            if ($_POST['procesar_val']!='SI'){
                echo '<input type="submit" name="procesar" id="procesar" value="Procesar" onclick=confirmacion_func("Procesar")>';
            }
            ?>
        </td>
    </tr>
</table>
</form>
<?php
if ($_POST['procesar_val']=='SI'){
    $servidor_biostar="false";
    include('../comunes/conexion_basedatos.php');
        for($j=0;$j<$sql_cont;$j++){
            mysql_query($sql_querys[$j]);
        }
}
?>
