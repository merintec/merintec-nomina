<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Codificaci&oacute;n Presupuestaria</title>
<html>
<body onLoad="actualizar_padre();">
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link type="text/css" rel="stylesheet" href="../comunes/calendar.css?" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../comunes/calendar.js?"></script>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['cod_com'];
// traer el idn de la compra
  $sql_idn ="SELECT * FROM compras WHERE cod_com=".$viene_val; 
  $res_idn = mysql_fetch_array(mysql_query($sql_idn));
  $idn_com = $res_idn['idn_par_mov'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'partidas_compras.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"].'&year='.$_GET["year"].'&cod_com='.$_GET["cod_com"];
$pagina2 = 'partidas_compras.php?cod_com='.$_GET["cod_com"].'&seccion='.$_GET["seccion"].'&year='.$_GET["year"];
$tabla = "partidas_compras";	// nombre de la tabla
$ncampos = "4";			//numero de campos del formulario
$datos[0] = crear_datos ("cod_pro_com","Cod. Presupuestario  ",$_POST['cod_pro_com'],"0","11","numericos");
$datos[1] = crear_datos ("cod_com","Cod. Compra",$_POST['cod_com'],"1","11","numericos");
$datos[2] = crear_datos ("cod_par","Cod. Partidas",$_POST['cod_par'],"1","11","numericos");
$datos[3] = crear_datos ("mon_pro_com","Monto del compromiso por la Partida",$_POST['mon_pro_com'],"1","12","decimal");
if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
	if ($_POST["Buscar"]) { $tipo = "general"; }
	if ($_POST["BuscarInd"]) { $tipo = "individual"; }
	$buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
	while ($row=@mysql_fetch_array($buscando))
	{
	    $existe = 'SI';
	    $cod_pro_com = $row["cod_pro_com"];
	    $cod_com = $row["cod_com"];
	    $cod_par = $row["cod_par"];
	    $isrl_pro_com = $row["isrl_pro_com"];
	    $mon_pro_com = $row["mon_pro_com"];
	    $boton = "Modificar";
	    // No modificar, datos necesarios para auditoria
	    $n_ant = mysql_num_fields($buscando);
	    for ($i = 0; $i < $n_ant; $i++)
	    { 
	        $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
	    }
	    ///
	}
}
if ($_POST["confirmar"]=="Actualizar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	/// Verificamos que la suma de las partidas no sea mayor al monto registrado como total del compromiso
	if ($validacion) {
		$sql_verifica = "Select SUM(mon_pro_com) as comprometido from partidas_compras where cod_com=".$viene_val." AND cod_pro_com!=".$cod_pro_com." GROUP BY cod_com";
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$comprometido = $sql_bus_ver['comprometido'];
		$sql_verifica = "Select mon_com from compras where cod_com=".$viene_val;
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$mon_ini = $sql_bus_ver['mon_com'];
		$mon_pend = $mon_ini - $comprometido;  
		$mon_pend = redondear($mon_pend,2,'','.');
		$mon_act = $_POST['mon_pro_com'];
		if ($mon_act > $mon_pend){
			echo '<SCRIPT>alert("'.$msg_partidas_mayor.'(Solo restan Bs. '.$mon_pend.' por comprometer)'.$msg_partidas_mayor2.'");</SCRIPT>';
			$validacion = '';
		}
		else { $validacion = 1; }
	}
	// consultamos ingresos presupuestarios
	$sql_disp = "Select pm.ano_par_mov,ppm.cod_par,sum(ppm.mon_pro_par_mov) as monto";
    $sql_disp .=" from part_presup_mov pm, productos_part_mov ppm";
    $sql_disp .=" where ppm.tip_pro_par_mov='ingreso' AND pm.cod_par_mov=ppm.cod_par_mov";
    $sql_disp .=" AND pm.ano_par_mov = ".$_GET[year]." AND pm.idn_par_mov='".$idn_com."' AND ppm.cod_par=".$_POST[cod_par]." GROUP BY ppm.cod_par";
    $disp_bus = mysql_query($sql_disp);
    if ($disp_res=mysql_fetch_array($disp_bus)){
    $ingreso=$disp_res[monto];
    }
    // consultamos egresos presupuestarios
	$sql_disp = "Select pm.ano_par_mov,ppm.cod_par,sum(ppm.mon_pro_par_mov) as monto";
    $sql_disp .=" from part_presup_mov pm, productos_part_mov ppm";
    $sql_disp .=" where ppm.tip_pro_par_mov='egreso' AND pm.cod_par_mov=ppm.cod_par_mov";
    $sql_disp .=" AND pm.ano_par_mov = ".$_GET[year]." AND pm.idn_par_mov='".$idn_com."' AND ppm.cod_par=".$_POST[cod_par]." GROUP BY ppm.cod_par";
    $disp_bus = mysql_query($sql_disp);
    if ($disp_res=mysql_fetch_array($disp_bus)){
    $egreso=$disp_res[monto];
    }
  	// consultamos compromisos
	$sql_disp = "SELECT pcom.cod_par, sum(mon_pro_com) AS monto";
	$sql_disp .=" FROM compras com, partidas_compras pcom";
	$sql_disp .=" WHERE com.cod_com = pcom.cod_com AND YEAR(fch_com)=".$_GET[year]." AND com.idn_par_mov='".$idn_com."' AND pcom.cod_par=".$_POST[cod_par];
	$sql_disp .=" GROUP BY pcom.cod_par";
    $disp_bus = mysql_query($sql_disp);
    if ($disp_res=mysql_fetch_array($disp_bus)){
    $compromiso=$disp_res[monto];
    }
    // calculamos la disponibilidad
    $restar_actual=0;
	if ($_POST["cod_par"]==$_POST["par_ant"])
	{
	    $restar_actual = $_POST["mon_ant"]; 
	}
//    echo $ingreso; echo $egreso; echo $compromiso; echo $restar_actual;
    echo $disponible=$ingreso-$egreso-$compromiso+$restar_actual;
    echo $disponible=redondear($disponible,2,'','.');
    if ($disponible<$_POST[mon_pro_com]){
        $msg_err=$msg_partida_nodisp.': Bs. '.redondear($disponible,2,'.',',');
        echo '<SCRIPT> alert ("'.$msg_err.'"); </SCRIPT>';
	    $validacion="";
    }
    if ($validacion) {
		modificar_func($ncampos,$datos,$tabla,"cod_pro_com",$_POST["cod_pro_com"],$pagina2);
		auditoria_func ('modificar', '', $_POST["ant"], $tabla);
		return;			
	}else{
		$boton = "Actualizar";
	}
}
if ($_POST["confirmar"]=="Modificar") 
{
	$boton = "Actualizar";
}
if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	/// Verificamos que la suma de las partidas no sea mayor al monto registrado como total del compromiso
	if ($validacion) {
		$sql_verifica = "Select SUM(mon_pro_com) as comprometido from partidas_compras where cod_com=".$viene_val." GROUP BY cod_com";
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$comprometido = $sql_bus_ver['comprometido'];
		$sql_verifica = "Select mon_com from compras where cod_com=".$viene_val;
		$sql_bus_ver = mysql_fetch_array(mysql_query($sql_verifica));
		$mon_ini = $sql_bus_ver['mon_com'];
		$mon_pend = $mon_ini - $comprometido; 
		$mon_pend = redondear($mon_pend,2,'','.');
		$mon_act = $_POST['mon_pro_com'];
		if ($mon_act > $mon_pend){
			echo '<SCRIPT>alert("'.$msg_partidas_mayor.'(Solo restan Bs. '.$mon_pend.' por comprometer)'.$msg_partidas_mayor2.'");</SCRIPT>';
			$validacion = '';
		}
		else { $validacion = 1; }
	}
	// consultamos ingresos presupuestarios
	$sql_disp = "Select pm.ano_par_mov,ppm.cod_par,sum(ppm.mon_pro_par_mov) as monto";
    $sql_disp .=" from part_presup_mov pm, productos_part_mov ppm";
    $sql_disp .=" where (ppm.tip_pro_par_mov='ingreso' OR ppm.tip_pro_par_mov='reintegro') AND pm.cod_par_mov=ppm.cod_par_mov";
    $sql_disp .=" AND pm.ano_par_mov = ".$_GET[year]." AND pm.idn_par_mov='".$idn_com."' AND ppm.cod_par=".$_POST[cod_par]." GROUP BY ppm.cod_par";
    $disp_bus = mysql_query($sql_disp);
    if ($disp_res=mysql_fetch_array($disp_bus)){
    $ingreso=$disp_res[monto];
    }
    // consultamos egresos presupuestarios
	$sql_disp = "Select pm.ano_par_mov,ppm.cod_par,sum(ppm.mon_pro_par_mov) as monto";
    $sql_disp .=" from part_presup_mov pm, productos_part_mov ppm";
    $sql_disp .=" where ppm.tip_pro_par_mov='egreso' AND pm.cod_par_mov=ppm.cod_par_mov";
    $sql_disp .=" AND pm.ano_par_mov = ".$_GET[year]." AND pm.idn_par_mov='".$idn_com."' AND ppm.cod_par=".$_POST[cod_par]." GROUP BY ppm.cod_par";
    $disp_bus = mysql_query($sql_disp);
    if ($disp_res=mysql_fetch_array($disp_bus)){
    $egreso=$disp_res[monto];
    }
  	// consultamos compromisos
	$sql_disp = "SELECT pcom.cod_par, sum( mon_pro_com ) AS monto";
	$sql_disp .=" FROM compras com, partidas_compras pcom";
	$sql_disp .=" WHERE com.cod_com = pcom.cod_com AND YEAR(fch_com)=".$_GET[year]." AND com.idn_par_mov='".$idn_com."' AND pcom.cod_par=".$_POST[cod_par];
	$sql_disp .=" GROUP BY pcom.cod_par";
    $disp_bus = mysql_query($sql_disp);
    if ($disp_res=mysql_fetch_array($disp_bus)){
    $compromiso=$disp_res[monto];
    }
    // calculamos la disponibilidad
    $disponible=$ingreso-$egreso-$compromiso;
    $disponible=redondear($disponible,2,'','.');
    if ($disponible<$_POST[mon_pro_com]){
        $msg_err=$msg_partida_nodisp.': Bs. '.redondear($disponible,2,'.',',');
        echo '<SCRIPT> alert ("'.$msg_err.'"); </SCRIPT>';
	    $validacion="";
    }
	if ($validacion) { 
		$boton = "Guardar"; 
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	//auditoria_func ('insertar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
	eliminar_func($_POST["cod_pro_com"],"cod_pro_com",$tabla,$pagina2);
	auditoria_func ('eliminar', $ncampos, $datos, $tabla);
	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{

	eliminar_func($_POST['confirmar_val'],"cod_pro_com","partidas_compras",$pagina2);
	return;
}
?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Codificaci&oacute;n Presupuestaria</td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <tr>
                        <td width="25%" class="etiquetas">Partida:
                          </td>
		            <td><input name="cod_pro_com" type="hidden" id="cod_pro_com" value="<?php if (! $existe) { echo $_POST['cod_pro_com']; } else { echo $cod_pro_com; } ?>" title="Codigo de salida presupuestaria">
			<input name="cod_com" type="hidden" id="cod_com" value="<?php if(! $existe) { echo $viene_val; } else { echo $cod_com; } ?>" title="Codigo del Egreso">
                        <?php
			   combo('cod_par', $cod_par, 'vista_partidas_presupuesto', $link, 0, 5, 6, 9, 'cod_par', "", $boton, "where idn_par_mov = '".$idn_com."' AND ano_par_mov=".$_GET[year]." GROUP BY cod_par ORDER BY par_par,gen_par,esp_par,sub_par",''); ?><input type="hidden" name="par_ant" id="par_ant" value="<?php if ($existe) { echo $cod_par;} else { echo $_POST["par_ant"]; }?>"></td>
                      </tr>
                       <tr>
                        <td width="25%" class="etiquetas">Monto:</td>
                        <td width="75%">
                        <input name="mon_pro_com" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mon_pro_com" value="<?php if(! $existe) { echo $_POST['mon_pro_com']; } else { echo $mon_pro_com; } ?>" size="10" maxlength="12" title="Monto del Compromiso de Partida">
                        <?php if ($boton=='Modificar') { echo $mon_pro_com; } ?>
                        <input type="hidden" name="mon_ant" id="mon_ant" value="<?php if ($existe) { echo $mon_pro_com;} else { echo $_POST["mon_ant"]; }?>"></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_compras_partidas.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
</body>
</html>
