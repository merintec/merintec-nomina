<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php include ('../comunes/formularios_funciones.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
	<title>N�mina de Reembolsos</title>
</head>
<body>
<?php
	// Para activar depuraci�n de codigo $debug = 1 sino $debug = ''
	$debug = '';
	$procesar=false;
	// verificamos que se encuentre logueado
	if (! $_COOKIE[usnombre]) {
		echo '<b><center>'.$msg_usr_noidentificado.'</center></b>';
		echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; 
		exit; 
	}
	// obtenemos la variable prenomina para verificar si es prenomina o n�mina
	$boton=$_POST['boton'];
	if($_POST['prenomina']){ $prenomina=$_POST['prenomina']; }else{ $prenomina=$_GET['prenomina']; }
	if($_POST['ano_nom']){ $ano_nom=$_POST['ano_nom']; }else{ $ano_nom=$_GET['ano_nom']; }
	if($_GET['fec_pag']){ $fec_pag="fec_pag='".$_GET['fec_pag']."'"; $fec_nom="fec_nom='0000-00-00'"; }
	if($_GET['fec_nom']){ $fec_nom="fec_nom='".$_GET['fec_nom']."'"; }
	if ($prenomina==1){ $add_titulo = " (PRE-NOMINA)"; }

	// inicializar montos totales de la nomina
	$total_ded_nomina=0;
	$total_asig_nomina=0;
	$total_aport_nomina=0;
	$total_pagar_nomina=0;

?>
	<!-- cabecera de la pantalla -->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	  	<tr>
		    <td width="22%"><img src="../imagenes/logo_tn_trn.png" height="80px"></td>
		    <td width="57%" align="center" class="titulo_nomina"><?php echo $titulo_paginas;  ?></td>
		    <td width="21%">
		    	<?php include('../comunes/fecha.php'); 
		    	echo '<br>'; $hora = time(); 
		    	echo '<b>Hora:</b> '.date ("h:i:s A",$hora);?>
		    </td>
	  	</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center" class="etiquetas_nomina">Sistema de N&oacute;mina Reembolsos<?php echo $add_titulo; ?></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="etiquetas_nomina2">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="etiquetas_nomina2">NOMINA DETALLADA DE EMPLEADOS</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="etiquetas_nomina2">CORRESPONDIENTE A: <span class="etiquetas_nomina"> 
			<?php  echo $ano_nom; ?>
			</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
	</table>
	<!-- Inicio de la nomina -->
	<table width="99%" border="0" cellpadding="0" cellspacing="0" align="center" class="detallespago">
	<?php 
		// Consultamos las dependencias
		$sql_dep = "SELECT * FROM dependencias ORDER BY nom_dep";
		$bus_dep = mysql_query($sql_dep);
		if ($debug) { echo '<br>SQL: '.$sql_dep.'<br>Error:'.mysql_error(); }
		while($res_dep = mysql_fetch_array($bus_dep)){
			echo '<tr><td><hr></td></tr>';
			echo '<tr><td>DEPENDENCIA: <b>'.$res_dep[nom_dep].'</b><br><br></td></tr>';
			// Consultamos los cargos
			$sql_cargos = "SELECT * FROM cargos c, tipos_cargos tc WHERE c.cod_dep = '".$res_dep[cod_dep]."' AND c.cod_tcar=tc.cod_tcar AND c.ced_per!=''  ORDER BY tc.nom_tcar ASC, c.nom_car ASC ";
			$bus_cargos = mysql_query($sql_cargos);

			if ($debug) { echo '<br>SQL: '.$sql_cargos.'<br>Error:'.mysql_error(); }
			while($res_cargo = mysql_fetch_array($bus_cargos)){
				echo '<tr><td>';
					echo '<table width="99%" border="0" cellpadding="3" cellspacing="0" align="right"  class="detallespago">';
						echo '<tr><td><b>Tipo de Cargo: </b>'.$res_cargo[nom_tcar].' ('.$res_cargo[abr_tcar].') <b>Cargo: </b>'.$res_cargo[nom_car].'</td></tr>';
						// Consultamos el personal que se encuentra registrado en el cargo
						$sql_per = "SELECT *, 'activos' as condicion FROM vista_cargos_per p WHERE p.cod_car = '".$res_cargo[cod_car]."' UNION SELECT *, 'retirados' as condicion FROM vista_cargos_salida s WHERE s.cod_car = '".$res_cargo[cod_car]."' AND YEAR(fch_asg) = '".$_GET[ano_nom]."' AND MONTH(fch_asg) = '".$_GET[mes_nom]."' AND fch_asg >= '".$fecha_desde_salida."' AND fch_asg <= '".$fecha_hasta_salida."'";
						$bus_per = mysql_query($sql_per);
						$i=0;
						while($res_per = mysql_fetch_array($bus_per)){
							$i++;
							if(!in_array($res_per[ced_per],$vec_ced)){
							
								$sql_reem="select * from vista_reembolsos_per where YEAR(fec_fac)='".$ano_nom."' and ".$fec_pag." and ".$fec_nom." and ced_per=".$res_per[ced_per];	
								$busq_reem=mysql_query($sql_reem);
								if($reg_reem=mysql_fetch_array($busq_reem)){
									echo '<tr><td>';
										echo '<table width="100%" border="1" cellpadding="3" cellspacing="0" class="detallespago" bordercolor="#000000" style="border-collapse:collapse;">';
											echo '<tr>
												<td>
													<b>Datos Personales:</b>
													<br><b>C�dula:</b> '.redondear($res_per[ced_per],0,'.','').'
													<br><b>Nombre:</b> '.ucwords($res_per[nom_per]." ".$res_per[ape_per]);
													if ($res_per[condicion]=='activos'){
														echo '<br><b>Fecha Ingreso:</b> '.date('d-m-Y', strtotime($res_per[fch_asg])).'';
													}
													else{
														echo '<br><b><font color="00CC00">Fecha Ingreso:</font></b> '.date('d-m-Y', strtotime($res_per[fch_vac])).'';	
														echo '<br><b><font color="00CC00">Salida del Cargo:</font></b> '.date('d-m-Y', strtotime($res_per[fch_asg])).'';	
													}
													if ($res_per[num_cue]){
														echo '<br><b>Cuenta:</b> '.$res_per[num_cue].'
														<br><b>Banco:</b> '.ucwords($res_per[ban_cue]).'';
													}else{
														echo '<br><b><font color="red">Pago Mediante Cheque</font></b>';
													}
												echo'</td>
												<td width="75%">
													<table width="100%" border="1" cellpadding="0" cellspacing="0" class="detallespago" bordercolor="#000000" style="border-collapse:collapse;">
														<tr align="center" style="font-weight: bold; background-color: #000; color: #FFF;"><td colspan="5" style="padding: 3px;">DETALLES DEL PAGO</td></tr>
														<tr align="center" style="font-weight: bold; background-color: #bebebe; color: #000;">
															<td align="left" style="padding: 3px;">Conceptos</td>
															<td style="padding: 3px; width: 80px;">Asignaciones</td>
															<td style="padding: 3px; width: 80px;">Total</td>
														</tr>';
														$tot_pag=0;
														$vec_ced[$i]=$res_per[ced_per];
														do{
															$tot_pag+=$reg_reem[mon_pag];
															$tot_tot+=$reg_reem[mon_pag];
															$datos_cob=buscar_campo('*', 'coberturas_med', 'WHERE cod_cob='.$reg_reem[cod_cob]);
															echo '<tr align="right">
																	<td align="left" style="padding: 3px;">'.$datos_cob[nom_cob].'</td>
																	<td style="padding: 3px; width: 80px;">'.redondear($reg_reem[mon_pag],2,".",",").'</td>
																	<td style="padding: 3px; width: 80px;">&nbsp;</td>
																</tr>';
														}while($reg_reem=mysql_fetch_array($busq_reem));										
																echo '<tr align="right">
																	<td align="left" style="padding: 3px;">Total por pago de reembolsos medicos</td>
																	<td style="padding: 3px; width: 80px;">&nbsp;</td>
																	<td style="padding: 3px; width: 80px;">'.redondear($tot_pag,2,".",",").'</td>
																</tr>';
		
													echo' </table>
												</td>
											</tr>';
										echo '</table>';
									echo '</td></tr>';
								}
							}
						}
					echo '</table>';
					echo '</td></tr>';

			}
		}
		echo '<tr><td><hr>';
					echo '<table width="99%" border="0" cellpadding="3" cellspacing="0" align="right"  class="detallespago">
				<table width="99%" border="1" cellpadding="0" cellspacing="0" class="detallespago" bordercolor="#000000" style="border-collapse:collapse;">
				<tr align="center" style="font-weight: bold; background-color: #000; color: #FFF;">
					<td align="left" style="padding: 3px;"><font color="FFFFFF"><b>Concepto</b></font></td>
					<td align="left" style="padding: 3px;"><font color="FFFFFF"><b>Total</b></font></td>
				</tr>
				<tr>
					<td align="left" style="padding: 3px;"><b>TOTAL NOMINA REEMBOLSOS</b></td>
					<td align="right" style="padding: 3px;">'; echo redondear ($tot_tot,2,".",","); echo '</b></td>
				</tr>
				</table><br><br><br></td></tr>';
	?>
	</table>

</body>
</html>
