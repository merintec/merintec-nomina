<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Registro de Vacaciones</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'registro_vacaciones.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
//$pagina2 = 'personal_cargos.php?ced_per='.$viene_val.'&seccion='.$_GET["seccion"].'';
$tabla = "vacaciones";	// nombre de la tabla
$ncampos = "7";		//numero de campos del formulario
$ced_per=$_POST['ced_per'];
$fech_inic=$_POST['fech_inic'];
$fech_fin=$_POST['fech_fin'];
$ano_vaca=$_POST['ano_vaca'];
$mes_pag=$_POST['mes_pag'];
$ano_pag=$_POST['ano_pag'];
$por_nom=$_POST['por_nom'];

$id_vaca = $_POST['id_vaca'];

//$tip_ces=$_POST['tip_ces'];
$datos[0] = crear_datos ("ced_per","Cédula",$_POST['ced_per'],"1","11","numericos");
$datos[1] = crear_datos ("fech_inic","Fecha de Inicio",$_POST['fech_inic'],"1","10","fecha");
$datos[2] = crear_datos ("fech_fin","Fecha Fin",$_POST['fech_fin'],"1","10","fecha");
$datos[3] = crear_datos ("ano_vaca","Año Vacaciones",$_POST['ano_vaca'],"1","4","numericos");
$datos[4] = crear_datos ("mes_pag","Mes a Pagar",$_POST['mes_pag'],"1","11","numericos");
$datos[5] = crear_datos ("ano_pag","Año a Pagar",$_POST['ano_pag'],"1","11","numericos");
$datos[6] = crear_datos ("por_nom","Porción de Nomina",$_POST['por_nom'],"1","11","numericos");




if ($_POST["Buscar"]||$_POST["BuscarInd"]) 
{
  if ($_POST["Buscar"]) { $tipo = "general"; }
  if ($_POST["BuscarInd"]) { $tipo = "individual"; }
  $buscando = busqueda_func($_POST["buscar_a"],$_POST["criterio"],"$tabla",$pagina,$tipo);
  if (mysql_num_rows($buscando) > 1)
  {
    include ('../comunes/busqueda_varios.php');
    $parametro[0]="Cedula";
    $datos[0]="ced_per";
    $parametro[1]="Fecha Inicio";
    $datos[1]="fech_inic";
    $parametro[2]="Fecha Fin";
    $datos[2]="fech_fin";
    $parametro[3]="A&ntilde;o Vacaciones";
    $datos[3]="ano_vaca";


    busqueda_varios(8,$buscando,$datos,$parametro,"id_vaca");
    return;
  }
  while ($row=@mysql_fetch_array($buscando))
  {
      $existe = 'SI';
      $id_vaca = $row['id_vaca'];
      $ced_per = $row['ced_per'];
      $fech_inic=$row['fech_inic'];
      $fech_fin = $row['fech_fin'];
      $ano_vaca = $row['ano_vaca'];
      $mes_pag = $row['mes_pag'];
      $ano_pag = $row['ano_pag'];
      $por_nom = $row['por_nom'];
      $boton = "Modificar";
      // No modificar, datos necesarios para auditoria
      $n_ant = mysql_num_fields($buscando);
      for ($i = 0; $i < $n_ant; $i++) 
      { 
          $ant .= mysql_field_name($buscando, $i).'='.$row[$i].'; ';
      }
      ///
  }
}

if ($_POST["confirmar"]=="Actualizar") 
{
  $validacion = validando_campos ($ncampos,$datos);
  if ($validacion) {
    modificar_func($ncampos,$datos,$tabla,"id_vaca",$_POST["id_vaca"],$pagina);
    auditoria_func ('modificar', '', $_POST["ant"], $tabla);
    return;     
  }else{
    $boton = "Actualizar";
  }
}

if ($_POST["confirmar"]=="Modificar") 
{
  $boton = "Actualizar";
}

if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos,$datos);
	if ($validacion) { 
		$boton = "Guardar"; 
		
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	insertar_func($ncampos,$datos,$tabla,$pagina);
	return;
}
if ($_POST["confirmar"]=="Eliminar") 
{
  eliminar_func($_POST["id_vaca"],"id_vaca",$tabla,$pagina);
  auditoria_func ('eliminar', $ncampos, $datos, $tabla);
  return;
}

?>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Registro de Vacaciones </td>
                  </tr>
                  <tr>
                      <input name="id_vaca"  type="hidden" id="id_vaca"  value="<?php echo $id_vaca; ?>"  title="Mes en el que se pagara en la nomina" />

                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <td width="25%" class="etiquetas">Seleccionar Funcionario:</td>
                        <td width="75%">
                           <?php  combo('ced_per', $ced_per, 'vista_personal', $link, 0, 1, 0, '', 'ced_per', 'onchange='.$funcion_combo, 'Verificar', "ORDER BY nombre"); ?>

                        </td>
                      </tr>
                     
                      <tr>
                        <td class="etiquetas">Fecha Inicio:</td>
                        <td><input name="fech_inic" maxlength="10" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fech_inic" value="<?php if(! $fech_inic) { echo date('Y-m-d'); } else { echo $fech_inic; } ?>" size="20" title="Fecha de Inicio de Vacaciones" />
						  <?php if ($boton=='Modificar') { echo $fech_inic; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha Fin:</td>
                        <td><input name="fech_fin" maxlength="10" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fech_fin" value="<?php if(! $fech_fin) { echo date('Y-m-d'); } else { echo $fech_fin; } ?>" size="20" title="Fecha de Inicio de Vacaciones" />
              <?php if ($boton=='Modificar') { echo $fech_fin; } ?></td>
                      </tr>
                      

                      <tr>
                        <td class="etiquetas">A&ntilde;o Vacaciones:</td>
                        <td><input name="ano_vaca" maxlength="4" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ano_vaca" value="<?php if(! $ano_vaca) { } else { echo $ano_vaca; } ?>" size="4" title="Año a que pertenece las Vacaciones" />
						  <?php if ($boton=='Modificar') { echo $ano_vaca; } ?></td>
                      </tr>
                      <tr>
                        <td  colspan="2" class="titulo">Datos para el Pago</td>
                        
                      </tr>
                      <tr>
                        <td class="etiquetas">Mes Nomina:</td>
                        <td>
                        <input name="mes_pag" maxlength="2" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="mes_pag" value="<?php if(! $mes_pag) { } else { echo $mes_pag; } ?>" size="4" title="Mes en el que se pagara en la nomina" />
                        <?php if ($boton=='Modificar') { echo $mes_pag; } ?>
                          
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">A&ntilde;o Nomina:</td>
                        <td>
                        <input name="ano_pag" maxlength="4" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="ano_pag" value="<?php if(! $ano_pag) { } else { echo $ano_pag; } ?>" size="4" title="Año en el que se pagara en la nomina" />
                        <?php if ($boton=='Modificar') { echo $ano_pag; } ?>
                          
                        </td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Quincena Nomina:</td>
                        <td>
                        <input name="por_nom" maxlength="1" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="por_nom" value="<?php if(! $por_nom) { } else { echo $por_nom; } ?>" size="4" title="Quincena que se pagara en la nomina" />
                        <?php if ($boton=='Modificar') { echo $por_nom; } ?>
                          
                        </td>
                      </tr>

                  <tr>
                    <td colspan="2"><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>

                    <tr>
                    <td colspan="2" align="center">
          <?php 
            $ncriterios =1; 
            $criterios[0] = "C&eacute;dula"; 
            $campos[0] ="ced_per";
            
            if ($prm[1]=='A' || $prm[2]=='A' || $prm[3]=='A') {
            crear_busqueda_func ($ncriterios,$criterios,$campos,$boton); } ?></td>
                  </tr>
                  

                  </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
