<?php include('../comunes/conexion_basedatos.php'); 
include ('../comunes/formularios_funciones.php');
include ('../comunes/comprobar_inactividad_capa.php');
include ('../comunes/mensajes.php');
include ('../comunes/titulos.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">

<?php $idprint=$_GET['cod_con']; 
	//consultamos los datos de la permisos
	$suma_pagos = 0;
    $result=mysql_query("select cper.*,per.nom_per,per.ape_per,per.nac_per,per.sex_per,per.pro_per from constancias_per cper, personal per WHERE cper.cod_con='$idprint' AND cper.ced_per = per.ced_per");
	if ($row=mysql_fetch_array($result))
	{
		$existe = 'SI';
		$cod = $row["cod_con"];
		$fecha1 = substr($row["fch_con"], 8, 2);
		$fecha2 = substr($row["fch_con"], 5, 2);
		$fecha3 = substr($row["fch_con"], 0, 4);
		$fecha = $fecha1.' / '.$fecha2.' / '.$fecha3;
		$ced_per = $row["ced_per"];
		$pro_per = $row["pro_per"];
		$nac_per = $row["nac_per"];
		$nom_per = $row["nom_per"];
		$ape_per = $row["ape_per"];
		$nom_dep = $row["nom_dep"];
		$nom_car = $row["nom_car"];
      $fecha_ing1 = substr($row["fch_ing"], 8, 2);
		$fecha_ing2 = substr($row["fch_ing"], 5, 2);
		$fecha_ing3 = substr($row["fch_ing"], 0, 4);
		$fecha_ing = $fecha_ing1.' / '.$fecha_ing2.' / '.$fecha_ing3;
		$des_con = $row["des_con"];
		$sue_con = $row["sue_con"];
		$prm_con = $row["prm_con"];
		$cst_con = $row["cst_con"];
		$sue_mon = $row["sue_mon"];
		$prm_mon = $row["prm_mon"];
		$cst_mon = $row["cst_mon"];
		$mot_con = $row["mot_con"];
		$sex_per = $row["sex_per"];
		$tit_edu = $row["tit_edu"];		
	}
	mysql_free_result($result);
?>

<title>Impresi�n de Constancias</title>

<div><?php include ('../comunes/pagina_encabezado.php'); ?></div>
<table cellspacing="0" cellpadding="0" style="border-collapse:collapse;" border="0" bordercolor="#000000" align="center" class="constancias">
    <tr height="1%">
        <td>
            <div align="right"><?php echo 'Ejido, '.$fecha1.' de '.convertir_mes($fecha2).' de '.redondear($fecha3,0,".",""); ?>&nbsp;&nbsp;<BR><BR></div>
        </td>
    </tr>
    <tr height="1%">
        <td>
            <div style="line-height:15px;">Se�or(es):<br><?php echo $des_con; ?>.<br><br></div>
        </td>
    </tr>
    <tr height="1%">
        <td>
            <div align="center"><h2>CONSTANCIA DE TRABAJO</h2></div>
        </td>
    </tr>
    <tr height="1%">
        <td>
            <div>Quien suscribe, <b><?php echo $ADMIN; ?></b>, titular de la c�dula de Identidad N� <b><?php echo $ADMIN_CI; ?></b>, Directora de Recursos Humanos del  <?php echo $organizacion; ?> del Municipio Campo El�as del Estado M�rida, mediante la presente, <b>HACE CONSTAR</b>: que
            <?php if ($sex_per == "F"){ echo ' la ciudadana, '; } ?>
            <?php if ($sex_per == "M"){ echo ' el ciudadano, '; } ?>           
            <b><?php echo $nom_per.' '.$ape_per; ?></b>, titular de la c�dula de identidad N� <b><?php echo $nac_per.'-'.redondear($ced_per,0,".",""); ?></b>; trabaja en esta Instituci�n desde el <b><?php echo $fecha_ing1.' de '.convertir_mes($fecha_ing2).' de '.redondear($fecha_ing3,0,".",""); ?></b>,
            hasta la presente fecha, desempe�ando actualmente el cargo de <b><?php echo $nom_car; ?></b>.<br>
            <?php 
            if ($sue_con || $prm_con || $cst_con) { ?>
            <br>
            <table cellspacing="0" cellpadding="0" width="80%" style="border-collapse:collapse;" border="1" bordercolor="#000000" align="center" >
                <tr class="etiquetas_nomina">
                    <td align="center" colspan="2">Detalles de las Remuneraciones que Percibe</td></tr>
                    <?php if ($sue_con) { $suma_pagos += $sue_mon; ?>
                        <tr><td class="etiquetas_nomina">Sueldo Mensual (Bs):</td><td align="right" class="etiquetas_nomina2"><?php echo redondear($sue_mon,2,".",","); ?>&nbsp;</td></tr>
                    <?php } ?>
                    <?php if ($prm_con) { 
								$suma_pagos += $prm_mon;
                    ?>
                        <tr><td class="etiquetas_nomina">Total Primas (Bs):</td><td align="right" class="etiquetas_nomina2"><?php echo redondear($prm_mon,2,".",","); ?>&nbsp;</td></tr>
                    <?php } ?>
                    <?php if ($cst_con) { $suma_pagos += $cst_mon; ?>
                    		
                        <tr><td class="etiquetas_nomina">Cesta Ticket (Bs):</td><td align="right" class="etiquetas_nomina2"><?php echo redondear($cst_mon,2,".",","); ?>&nbsp;</td></tr>
                    <?php } ?>
                    <tr class="etiquetas_nomina">
                        <td align="right">Total de Pagos Mensuales (Bs.)&nbsp;&nbsp;</td><td align="right"><?php echo redondear($suma_pagos,2,".",","); ?>&nbsp;</td></tr>
            </table>
            <?php } ?>

            <br>Constancia que se expide a petici�n de parte interesada a los <b><?php echo $fecha1.' d�as del mes de '.convertir_mes($fecha2).' de '.redondear($fecha3,0,".",""); ?></b>. Para efectos de <b><?php echo $mot_con; ?></b>.
            
            </div>
        </td>
    </tr>
    <tr height="1%">
        <td>
           <br><br><br>
           
           <center><hr width="300px"><?php echo $ADMIN; ?><BR><?php echo $ADMIN_CI; ?>  </center>
        </td>
    </tr>
</table>
<?php echo $msg_pie_carta; ?>
<div><input type="button" name="bt_print" value="Imprimir Solicitud" id="bt_print" onclick="this.style.visibility='hidden'; window.print();"></div>
