<?php include('../comunes/conexion_basedatos.php'); ?>
<?php include ('../comunes/comprobar_inactividad_capa.php'); ?>
<title>Administrar Cargos del Personal</title>
<?php include ('../comunes/titulos.php'); ?>
<?php include ('../comunes/mensajes.php'); ?>
<?php if (! $_COOKIE[usnombre]) { echo '<b><center>'.$msg_usr_noidentificado.'</center></b>'; 
  echo '<SCRIPT> alert ("'.$msg_usr_noidentificado_alert.'"); </SCRIPT>'; exit; } ?>
<link href="../comunes/estilo.css" rel="stylesheet" type="text/css">
<?php
$viene_val = $_GET['ced_per'];
include ('../comunes/formularios_funciones.php');
$prm = llamar_permisos ($_GET["seccion"]);
$boton = "Verificar";
$existe = '';
$pagina = 'personal_cargos.php?seccion='.$_GET["seccion"].'&nom_sec='.$_GET["nom_sec"];
$pagina2 = 'personal_cargos.php?ced_per='.$viene_val.'&seccion='.$_GET["seccion"].'';
$tabla = "cargos";	// nombre de la tabla
$ncampos = "6";		//numero de campos del formulario
$ced_per=$_GET['ced_per'];
$cod_car=$_POST['cod_car'];
$cod_dep=$_POST['cod_dep'];
$fch_vac=$_POST['fch_vac'];
$fch_asg=$_POST['fch_asg'];
$cod_car2=$_POST['cod_car2'];
$cod_dep2=$_POST['cod_dep2'];
$escalafon=$_POST['escalafon'];

//$tip_ces=$_POST['tip_ces'];
$datos[0] = crear_datos ("ced_per","C�dula",$_POST['ced_per'],"1","11","numericos");
$datos[1] = crear_datos ("est_car","Estado del Cargo","A","1","1","alfabeticos");
$datos[2] = crear_datos ("fch_asg","Fecha de Asignacion",$_POST['fch_asg'],"1","10","fecha");
$datos[3] = crear_datos ("cod_dep","Dependencia",$_POST['cod_dep'],"1","5","alfanumericos");
$datos[4] = crear_datos ("cod_car","Cargo",$_POST['cod_car'],"1","5","numericos");
$datos[5] = crear_datos ("cod_car","Cargo",$_POST['cod_car2'],"1","5","numericos");
$datos[6] = crear_datos ("escalafon","Escalaf�n",$_POST['escalafon'],"0","12","decimal");




$tabla2 = "cargos";
$ncampos2 = "3";
$datos2[0] = crear_datos ("ced_per","C�dula","","1","12","numericos");
$datos2[1] = crear_datos ("est_car","Estado","D","1","1","alfabeticos");
$datos2[2] = crear_datos ("fch_asg","Fecha de Asignacion del Cargo","0000-00-00","1","10","fecha");
//datos para programacion de movimientos
$tabla3 = "prog_movimientos";	// nombre de la tabla
$ncampos3 = "8";		//numero de campos del formulario
$datos3[0] = crear_datos ("ced_per","C�dula",$_POST['ced_per'],"1","11","numericos");
$datos3[1] = crear_datos ("cod_car","Cargo",$_POST['cod_car'],"1","5","numericos");
$datos3[2] = crear_datos ("accion","Tipo de Movimiento",$_POST['accion'],"1","1","numericos");
$datos3[3] = crear_datos ("estado","Estado","1","1","1","numericos");
$datos3[4] = crear_datos ("fch_asg","Fecha de Asignacion de Cargo",$_POST['fch_asg'],"1","10","fecha");
$datos3[5] = crear_datos ("fch_vac","Fecha de ingreso a Instituci�n",$_POST['fch_vac'],"1","10","fecha");
$datos3[6] = crear_datos ("pagado","pagado en nomina la fraccion","N","0","10","alfabeticos");
$datos3[7] = crear_datos ("escalafon","Escalaf�n",$_POST['escalafon'],"0","12","decimal");


if ($_POST["confirmar"]=="Verificar") 
{
	$validacion = validando_campos ($ncampos3,$datos3);
	if ($validacion) { 
		$boton = "Guardar"; 
		$boton = comp_exist($datos[4][0],$datos[4][2]."' AND est_car = 'A' AND ced_per <> '".$datos3[0][2]."",$tabla,$boton,'si',"Cargos de Personal, ese cargo esta asignado a otra persona");
		$boton = comp_exist($datos3[1][0],$datos3[1][2]."' AND estado = '1",$tabla3,$boton,'si',"Movimientos ya esta programado un cambio para ese cargo");
		if($_POST['accion']==2){
			$boton = comp_exist($datos3[0][0],$datos3[0][2]."' AND cod_car = '".$datos3[1][2]."",$tabla,$boton,'no',"Cargos no existe ese cargo para esta cedula");
		}
		if($_POST['accion']==1){
			$boton = comp_exist($datos3[0][0],$datos3[0][2]."' AND cod_car = '".$datos3[1][2]."",$tabla,$boton,'si',"Cargos ya existe ese cargo para esta cedula");
			$boton = comp_num_exist($datos[0][0],$datos[0][2],$tabla2,$boton, 2,"Cargos de Personal");
			$boton = comp_num_exist($datos3[0][0],$datos3[0][2]."' AND (estado <> '0' AND estado <> '2') AND accion = '1",$tabla3,$boton, 1,"Movimientos qw");
		}
    if ($_POST['accion']==3)
    {

            $boton = comp_exist($datos3[0][0],$datos3[0][2]."' AND cod_car = '".$datos3[1][2]."",$tabla,$boton,'no',"Cargos no existe ese cargo para esta cedula");
            $boton = comp_exist($datos[5][0],$datos[5][2]."' AND est_car = 'A' AND ced_per <> '".$datos3[0][2]."",$tabla,$boton,'si',"Cargos de Personal, ese cargo esta asignado a otra persona");


    }
	}
	else { $boton = "Verificar"; }
}
if ($_POST["confirmar"]=="Guardar") 
{
	//modificar_func(3,$datos,$tabla,"cod_car",$_POST["cod_car"],$pagina);
  if ($_POST['accion']==3)
  {

      $datos3[2] = crear_datos ("accion","Tipo de Movimiento",2,"1","1","numericos");
      insertar_func_nomina($ncampos3,$datos3,$tabla3,$pagina);
      $datos3[2] = crear_datos ("accion","Tipo de Movimiento",1,"1","1","numericos");
      $fecha_asig=strtotime($_POST['fch_asg']);
      $fecha_asig=strtotime("+1 days",$fecha_asig);
      $fecha_asig=date("Y-m-d",$fecha_asig);
      $datos3[1] = crear_datos ("cod_car","Cargo",$_POST['cod_car2'],"1","5","numericos");
      $datos3[4] = crear_datos ("fch_asg","Fecha de Asignacion de Cargo",$fecha_asig,"1","10","fecha");
      insertar_func_nomina($ncampos3,$datos3,$tabla3,$pagina);
      $actualizar="UPDATE deducciones SET cod_car='$_POST[cod_car2]' WHERE ced_per='$_POST[ced_per]' and cod_car='$_POST[cod_car]' ";
      $actualizar=mysql_query($actualizar);
      $actualizar1="UPDATE asignaciones SET cod_car='$_POST[cod_car2]' WHERE ced_per='$_POST[ced_per]' and cod_car='$_POST[cod_car]' ";
      $actualizar1=mysql_query($actualizar1);

      echo '<SCRIPT>alert("'.$msg_ins_exito.'");</SCRIPT>';
      echo  '<SCRIPT LANGUAGE="javascript">location.href = "'.$pagina2.'";</SCRIPT>';  



  }

  else
  {
      insertar_func($ncampos3,$datos3,$tabla3,$pagina2);

  }

	return;
}
if ($_POST["confirmar"]=="Eliminar de la lista") 
{
	eliminar_func($_POST['confirmar_val'],"cod_mov","prog_movimientos",$pagina2);
	return;
}

include ('nomina_movimientos.php');
?>

<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="center"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center">
                <table width="550" border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td class="titulo">Administrar cargos de Personal C.I: <?php echo $viene_val; ?> </td>
                  </tr>
                  <tr>
                    <td width="526"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
                      <td width="25%" class="etiquetas">Dependencia:</td>
                        <td width="75%"><input name="ced_per" type="hidden" id="ced_per" value="<?php echo $viene_val; ?>" />
                                              <?php
                    			$funcion_combo = 'onchange="mostrar_opcion(';
                    			$funcion_combo .= "'cod_car', this.value)";
                    			$funcion_combo .= '"'; 
                    			combo('cod_dep', $cod_dep, 'dependencias', $link, 0, 0, 1, "", 'cod_dep', $funcion_combo, $boton,''); ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Cargo:</td>
                        <td><?php combo('cod_car', $cod_car, 'cargos', $link, 0, 1, 2, 4, 'cod_dep', "", $boton, ""); ?>			</td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha de Movimiento:</td>
                        <td><input name="fch_asg" maxlength="10" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_asg" value="<?php if(! $fch_asg) { echo date('Y-m-d'); } else { echo $fch_asg; } ?>" size="20" title="Fecha de realizacion del Movimiento" />
						  <?php if ($boton=='Modificar') { echo $fch_asg; } ?></td>
                      </tr>
                      <tr>
                        <td class="etiquetas">Fecha Ingreso a Instituci�n:</td>
                        <td><input name="fch_vac" maxlength="10" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="fch_vac" value="<?php if(! $fch_vac) { } else { echo $fch_vac; } ?>" size="20" title="Fecha en que ingres� en la instituci�n (Continuidad en la Instituci�n)" />
						  <?php if ($boton=='Modificar') { echo $fch_asg; } ?></td>
                      </tr>

                        <tr>
                        <td class="etiquetas">Escalaf�n:</td>
                        <td><input name="escalafon" maxlength="11" type="<?php if ($boton=='Modificar') { echo 'hidden'; } else { echo 'text'; } ?>" id="escalafon" value="<?php if(! $escalafon) { } else { echo $escalafon; } ?>" size="20" title="Ingrese escalafon si lo posee)" />
                        <?php if ($boton=='Modificar') { echo $escalafon; } ?></td>
                      </tr>
                  
                      <tr>
                        <td class="etiquetas">Tipo de Movimiento:</td>
                        <td>  <label>
							  <input type="radio" name="accion" value="1" onclick="otro_cargo.style.display='none'; info.style.display='none';  otro_cargo2.style.display='none'" <?php  if($_POST["accion"]==1 || $accion == 1) { echo 'checked'; } ?> title="Marcar si ingresa al cargo"/>
								Entrada a Cargo</label>
                <br>
							  <label>
							  <input type="radio" name="accion" value="2" onclick="otro_cargo.style.display='none'; info.style.display='inline'; otro_cargo2.style.display='none'; otro_cargo3.style.display='none'" <?php  if($_POST["accion"]==2 || $accion == 2) { echo 'checked'; } ?> title="Marcar si sale del cargo"/>
								Salida de Cargo</label>              <span id='info' style=" display: none; font-size: 0.8em; color: #cf0000;"> <br>  Recuerde Revisar las Deducciones antes de efectuar el movimiento!!! </span>

                <br>
                <input type="radio" name="accion" value="3" onclick="otro_cargo.style.display='table-row'; info.style.display='none';  otro_cargo2.style.display='table-row' ; otro_cargo3.style.display='table-row' " <?php  if($_POST["accion"]==3 || $accion == 3) { echo 'checked'; } ?> title="Cambio de cargo"/>
                Cambio de Cargo</label>
                </td>
                </tr>


                <tr id="otro_cargo3" class="titulo" <?php if ($_POST[accion]!='3'){ echo 'style="display: none;"'; } ?> > <td colspan="2">Nuevo Cargo </td> </tr>
              
                        <tr id="otro_cargo" <?php if ($_POST[accion]!='3'){ echo 'style="display: none;"'; } ?>>
                          <td width="25%" class="etiquetas">Dependencia:</td>
                            <td width="75%">
                              <?php
                        $funcion_combo = 'onchange="mostrar_opcion(';
                        $funcion_combo .= "'cod_car2', this.value)";
                        $funcion_combo .= '"'; 
                        combo('cod_dep2', $cod_dep2, 'dependencias', $link, 0, 0, 1, "", 'cod_dep', $funcion_combo, $boton,''); ?></td>
                          </tr>
                          <tr id="otro_cargo2" <?php if ($_POST[accion]!='3'){ echo 'style="display: none;"'; } ?>>
                            <td class="etiquetas">Cargo:</td>
                            <td><?php combo('cod_car2', $cod_car2, 'cargos', $link, 0, 1, 2, 4, 'cod_dep', "", $boton, ""); ?>      </td>
                          </tr>
                

                    </table></td>
                  </tr>


                  <tr>
                    <td><?php include ('../comunes/botonera_usr.php'); ?></td>
                  </tr>
                  <tr>
                    <td class="etiquetas_centradas">Cargo(s) Asociado(s)</td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_cargos_personal.php'); ?></td>
                  </tr>
                  <tr>
                    <td align="center"></td>
                  </tr>
                  <tr>
                    <td class="etiquetas_centradas">Movimientos por Realizar</td>
                  </tr>
                  <tr>
                    <td align="center"><?php include ('capa_movimientos.php'); ?></td>
                  </tr>
		  <tr><td align="center"><br><input type="button" name="Submit" value="Cerrar Ventana" onclick="window.close();" title="<?php echo $msg_btn_cerrarV; ?>"></td></tr>
                </table>
            </div></td>
          </tr>
      </table></td>
    </tr>
  </table>

</form>
