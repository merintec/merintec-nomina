-- phpMyAdmin SQL Dump
-- version 2.9.0.2
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 29-04-2013 a las 16:02:52
-- Versión del servidor: 5.0.24
-- Versión de PHP: 5.1.6
-- 
-- Base de datos: `zuly-1406`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `actividades`
-- 

CREATE TABLE `actividades` (
  `cod_act` varchar(5) NOT NULL COMMENT 'codigo de la Actividad',
  `nom_act` varchar(150) NOT NULL COMMENT 'nombre de la actividad',
  PRIMARY KEY  (`cod_act`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `actividades`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `adt_ing_cat`
-- 

CREATE TABLE `adt_ing_cat` (
  `cod_cat` int(11) NOT NULL auto_increment COMMENT 'Codigo de Categoría',
  `des_cat` varchar(50) NOT NULL COMMENT 'Descripción de Categoría',
  PRIMARY KEY  (`cod_cat`),
  UNIQUE KEY `des_cat` (`des_cat`),
  UNIQUE KEY `des_cat_2` (`des_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las Categorias de los recibos para Auditorias' AUTO_INCREMENT=31 ;

-- 
-- Volcar la base de datos para la tabla `adt_ing_cat`
-- 

INSERT INTO `adt_ing_cat` VALUES (13, 'Activid. Económ. (Moroso)');
INSERT INTO `adt_ing_cat` VALUES (5, 'Actividades Económicas');
INSERT INTO `adt_ing_cat` VALUES (7, 'Ajuste Fiscal');
INSERT INTO `adt_ing_cat` VALUES (11, 'Apuestas Lícitas');
INSERT INTO `adt_ing_cat` VALUES (16, 'Aseo Domiciliario');
INSERT INTO `adt_ing_cat` VALUES (6, 'Cementerio');
INSERT INTO `adt_ing_cat` VALUES (3, 'Certificación y Solvencia');
INSERT INTO `adt_ing_cat` VALUES (10, 'Espectáculos Públicos');
INSERT INTO `adt_ing_cat` VALUES (28, 'Fondo de Comp. Interritorial');
INSERT INTO `adt_ing_cat` VALUES (4, 'Ingresos Varios');
INSERT INTO `adt_ing_cat` VALUES (12, 'Inmueb. Urb (Moroso)');
INSERT INTO `adt_ing_cat` VALUES (8, 'Inmuebles Urbanos');
INSERT INTO `adt_ing_cat` VALUES (30, 'Inspección');
INSERT INTO `adt_ing_cat` VALUES (24, 'Intereses de Aportes FIDES');
INSERT INTO `adt_ing_cat` VALUES (25, 'Intereses de Aportes LAEE');
INSERT INTO `adt_ing_cat` VALUES (26, 'Intereses Dinero en DEP');
INSERT INTO `adt_ing_cat` VALUES (21, 'Intereses Mora y Recargo');
INSERT INTO `adt_ing_cat` VALUES (19, 'Matadero');
INSERT INTO `adt_ing_cat` VALUES (20, 'Mercado');
INSERT INTO `adt_ing_cat` VALUES (18, 'Mercado (Moroso)');
INSERT INTO `adt_ing_cat` VALUES (23, 'Multas y Recargos');
INSERT INTO `adt_ing_cat` VALUES (14, 'Otros Impuestos Indirectos');
INSERT INTO `adt_ing_cat` VALUES (29, 'Otros Ingresos Extraordinarios');
INSERT INTO `adt_ing_cat` VALUES (15, 'Permisos Municipales');
INSERT INTO `adt_ing_cat` VALUES (17, 'Propag. Comer. (Moroso)');
INSERT INTO `adt_ing_cat` VALUES (9, 'Propaganda Comercial');
INSERT INTO `adt_ing_cat` VALUES (22, 'Reintegros');
INSERT INTO `adt_ing_cat` VALUES (27, 'Situado Municipal');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `adt_ing_pro`
-- 

CREATE TABLE `adt_ing_pro` (
  `serie_id` int(11) NOT NULL,
  `recibo` int(11) NOT NULL,
  `monto` decimal(9,2) NOT NULL,
  `cod_cat` int(2) NOT NULL,
  `con_esp` varchar(50) NOT NULL COMMENT 'Condición Especial',
  UNIQUE KEY `serie_id` (`serie_id`,`recibo`,`monto`,`cod_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `adt_ing_pro`
-- 

INSERT INTO `adt_ing_pro` VALUES (1, 15901, 76.00, 0, '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `anular_form`
-- 

CREATE TABLE `anular_form` (
  `cod_anl` int(11) NOT NULL auto_increment,
  `fch_anl` date NOT NULL,
  `tip_anl` varchar(1) NOT NULL,
  `num_anl` int(5) unsigned zerofill NOT NULL,
  `mot_anl` varchar(255) NOT NULL,
  PRIMARY KEY  (`cod_anl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los Formularios Anulados' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `anular_form`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `asignaciones`
-- 

CREATE TABLE `asignaciones` (
  `cod_cnp` int(11) NOT NULL auto_increment COMMENT 'Codigo de la asignacion',
  `con_cnp` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `ncuo_cnp` int(3) NOT NULL COMMENT 'Numero de cuotas de la asignacion',
  `per_cnp` varchar(1) NOT NULL COMMENT 'Permanencia de la asignacion',
  `mcuo_cnp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la asignacion',
  `por_cnp` int(3) NOT NULL COMMENT 'Porcentaje de asignacion',
  `bpor_cnp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_con` int(11) NOT NULL COMMENT 'Codigo del tipo de concesion',
  `ced_per` varchar(12) NOT NULL COMMENT 'La cedula de la persona',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_cnp` date NOT NULL COMMENT 'Fecha de Registro de la asignacion',
  `ncp_cnp` int(3) NOT NULL COMMENT 'N�mero de cuotas pagadas',
  `des_cnp` varchar(1) NOT NULL COMMENT 'Aplica para Descuentos de ley',
  PRIMARY KEY  (`cod_cnp`),
  KEY `cod_con` (`cod_con`),
  KEY `ced_per` (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- 
-- Volcar la base de datos para la tabla `asignaciones`
-- 

INSERT INTO `asignaciones` VALUES (1, 'Cuatro dias acumulativos según L.O.T art. 108 ', 1, '', 465.32, 0, '', 3, '15621190', 12, '2011-08-24', 1, '');
INSERT INTO `asignaciones` VALUES (2, 'Cancelación de 18 días acumulativos art. 108 LOT  ', 1, '', 2750.94, 0, '', 3, '10715628', 7, '2011-10-26', 1, '');
INSERT INTO `asignaciones` VALUES (3, 'Cancelación de 22 días acumulativos art. 108 LOT', 1, '', 2871.44, 0, '', 3, '11960597', 9, '2011-10-26', 1, '');
INSERT INTO `asignaciones` VALUES (4, '(01) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 380.00, 0, '', 4, '10715628', 7, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (5, '(02) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 760.00, 0, '', 4, '10109577', 8, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (6, '(02) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 760.00, 0, '', 4, '11960597', 9, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (7, '(01) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 380.00, 0, '', 4, '8019652', 11, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (8, '(02) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 760.00, 0, '', 4, '15621190', 12, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (9, '(02) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 760.00, 0, '', 4, '4264264', 6, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (10, '(01) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 380.00, 0, '', 4, '8036119', 5, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (11, '(01) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 380.00, 0, '', 4, '14304657', 13, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (12, '(01) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 380.00, 0, '', 4, '12346028', 14, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (13, '(01) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 380.00, 0, '', 4, '16933715', 15, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (14, '(03) Aporte para adquisición de Juguetes a niños menores según Resolución Nº 0106-2011 literal K', 1, '', 1140.00, 0, '', 4, '15031097', 17, '2011-11-10', 1, '');
INSERT INTO `asignaciones` VALUES (15, 'Ajuste de sueldo por incremento de salario según resolución  0153-2012', 1, '', 402.54, 0, '', 5, '4264264', 6, '2012-02-28', 1, '');
INSERT INTO `asignaciones` VALUES (16, 'Ajuste de sueldo por incremento de salario según resolución  0152-2012', 1, '', 402.54, 0, '', 5, '8036119', 5, '2012-02-28', 1, '');
INSERT INTO `asignaciones` VALUES (17, 'Ajuste de sueldo por incremento de salario según resolución  0155-2012', 1, '', 30.97, 0, '', 5, '6343267', 3, '2012-02-28', 1, '');
INSERT INTO `asignaciones` VALUES (18, 'Ajuste de sueldo por incremento de salario según resolución  0154-2012', 1, '', 402.54, 0, '', 5, '3766280', 19, '2012-02-28', 1, '');
INSERT INTO `asignaciones` VALUES (19, 'Diferencia prima por hijos abril a julio', 1, '', 180.00, 0, '', 6, '14304657', 13, '2012-08-29', 1, '');
INSERT INTO `asignaciones` VALUES (20, 'Diferencia prima por hijos mayo a julio', 1, '', 135.00, 0, '', 6, '11960597', 9, '2012-08-29', 1, '');
INSERT INTO `asignaciones` VALUES (21, 'Bono vacacional', 1, '', 3986.80, 0, '', 8, '17130852', 20, '2012-09-27', 1, '');
INSERT INTO `asignaciones` VALUES (22, 'Bono vacacional', 1, '', 7409.20, 0, '', 8, '3766280', 19, '2012-09-27', 1, '');
INSERT INTO `asignaciones` VALUES (23, 'Bono de Compensación por Experiencia', 1, '', 38.70, 0, '', 7, '15621190', 12, '2012-10-23', 1, 'S');
INSERT INTO `asignaciones` VALUES (24, 'Bono de Compensación por Experiencia no cancelado en mes de Septiembre', 1, '', 38.70, 0, '', 7, '15621190', 12, '2012-10-23', 1, '');
INSERT INTO `asignaciones` VALUES (25, 'Bono de Compensación por Experiencia', 1, 'S', 71.04, 0, '', 7, '3766280', 19, '2012-10-23', 1, 'S');
INSERT INTO `asignaciones` VALUES (26, 'Bono de Compensación por Experiencia no cancelado en mes de Septiembre', 1, '', 71.04, 0, '', 7, '3766280', 19, '2012-10-23', 1, '');
INSERT INTO `asignaciones` VALUES (27, 'Bono de Compensación por Experiencia', 1, 'S', 36.38, 0, '', 7, '17130852', 20, '2012-10-23', 1, 'S');
INSERT INTO `asignaciones` VALUES (28, 'Bono de Compensación por Experiencia no cancelado en mes de Septiembre', 1, '', 36.38, 0, '', 7, '17130852', 20, '2012-10-23', 1, '');
INSERT INTO `asignaciones` VALUES (29, '21 días por ingreso el día 25/10/2012', 1, '', 1806.27, 0, '', 10, '17830393', 15, '2012-11-27', 1, 'S');
INSERT INTO `asignaciones` VALUES (30, 'Pago de 10 días laborados en el periodo por entrada en el cargo el día 21/11/2012', 1, '', 808.50, 0, '', 11, '18208772', 26, '2012-11-27', 1, 'S');
INSERT INTO `asignaciones` VALUES (31, '24 días por ingreso el día 22/10/2012', 1, '', 3256.80, 0, '', 10, '8021566', 27, '2012-11-27', 1, 'S');
INSERT INTO `asignaciones` VALUES (32, 'Bono de Compensación por Experiencia', 1, 'S', 71.04, 0, '', 7, '3766280', 19, '2012-11-27', 0, '');
INSERT INTO `asignaciones` VALUES (33, 'Bono de Compensación por Experiencia', 1, 'S', 36.38, 0, '', 7, '17130852', 20, '2012-11-27', 0, '');
INSERT INTO `asignaciones` VALUES (34, 'Bono de Compensación por Experiencia', 1, 'S', 38.70, 0, '', 7, '15621190', 12, '2012-11-27', 0, '');
INSERT INTO `asignaciones` VALUES (35, 'Bono de Compensación por Experiencia', 1, 'S', 144.87, 0, '', 7, '10715628', 7, '2012-11-27', 0, '');
INSERT INTO `asignaciones` VALUES (36, 'Bono de Compensación por Experiencia', 1, 'S', 128.96, 0, '', 7, '11960597', 9, '2012-11-27', 0, '');
INSERT INTO `asignaciones` VALUES (37, 'pago de Prima por Antiguedad meses Septiembre, Octubre y Noviembre de 2012', 1, '', 135.00, 0, '', 12, '16657924', 16, '2012-12-10', 1, '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `auditoria`
-- 

CREATE TABLE `auditoria` (
  `cod_aud` int(11) NOT NULL auto_increment COMMENT 'C�digo de Auditor�a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C�digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci�n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci�n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu� la acci�n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci�n',
  `hora` time NOT NULL COMMENT 'hora de la acci�n',
  PRIMARY KEY  (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `auditoria`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `auditoria_backup`
-- 

CREATE TABLE `auditoria_backup` (
  `cod_aud` int(11) NOT NULL auto_increment COMMENT 'C�digo de Auditor�a',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C�digo del usuario',
  `accion` varchar(15) NOT NULL COMMENT 'Acci�n Realizada',
  `detalle` longtext NOT NULL COMMENT 'Detalles de la Acci�n',
  `tabla` varchar(50) NOT NULL COMMENT 'Tabla sobre la que se efectu� la acci�n',
  `fecha` date NOT NULL COMMENT 'fecha de la acci�n',
  `hora` time NOT NULL COMMENT 'hora de la acci�n',
  PRIMARY KEY  (`cod_aud`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `auditoria_backup`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `banco`
-- 

CREATE TABLE `banco` (
  `cod_ban` int(11) NOT NULL auto_increment,
  `nom_ban` varchar(50) default NULL,
  `cue_ban` varchar(20) default NULL,
  `des_ban` varchar(50) NOT NULL COMMENT 'Destino de la Cuenta',
  PRIMARY KEY  (`cod_ban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `banco`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `banco_conciliacion`
-- 

CREATE TABLE `banco_conciliacion` (
  `cod_ban_conc` int(11) NOT NULL auto_increment,
  `cod_ban` int(11) NOT NULL,
  `mes_ban_conc` int(11) NOT NULL,
  `ano_ban_conc` int(4) NOT NULL,
  `sal_ban_conc` decimal(9,2) NOT NULL,
  `obs_ban_conc` varchar(255) NOT NULL,
  PRIMARY KEY  (`cod_ban_conc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las concialiaciones bancarias' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `banco_conciliacion`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `banco_movimientos`
-- 

CREATE TABLE `banco_movimientos` (
  `cod_ban_mov` int(11) NOT NULL auto_increment,
  `fch_ban_mov` date NOT NULL,
  `tip_ban_mov` varchar(10) NOT NULL,
  `mon_ban_mov` double(9,2) NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `des_ban_mov` varchar(255) NOT NULL,
  `obs_ban_mov` varchar(255) NOT NULL,
  `ref_ban_mov` varchar(50) NOT NULL,
  `sta_ban_mov` int(11) NOT NULL,
  PRIMARY KEY  (`cod_ban_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de los movimientos bancarios' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `banco_movimientos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cargos`
-- 

CREATE TABLE `cargos` (
  `cod_car` int(11) NOT NULL auto_increment COMMENT 'codigo del cargo',
  `num_car` int(3) unsigned zerofill NOT NULL COMMENT 'numero del cargo',
  `nom_car` varchar(100) NOT NULL COMMENT 'nombre del cargo',
  `cod_sue` int(2) NOT NULL COMMENT 'codigo del sueldo',
  `est_car` varchar(1) NOT NULL COMMENT 'estado del cargo',
  `cod_tcar` int(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `ced_per` varchar(12) NOT NULL,
  `fch_asg` date NOT NULL COMMENT 'Fecha de asignacion  del cargo',
  `des_car` varchar(1) NOT NULL COMMENT 'Se efectuan los descuentos de ley por este Cargo',
  `fch_vac` date NOT NULL,
  `cod_hor` int(11) NOT NULL,
  PRIMARY KEY  (`cod_car`),
  UNIQUE KEY `num_car` (`num_car`,`cod_dep`),
  KEY `cod_sue` (`cod_sue`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `cargos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `compras`
-- 

CREATE TABLE `compras` (
  `cod_com` int(11) NOT NULL auto_increment,
  `fch_com` date NOT NULL,
  `nor_com` int(4) unsigned zerofill NOT NULL,
  `frm_com` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `sol_com` varchar(20) default NULL,
  `tip_com` varchar(15) NOT NULL,
  `for_com` varchar(15) default NULL,
  `adl_com` double(9,2) default NULL,
  `fre_com` varchar(255) default NULL,
  `mon_com` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `rif_pro` varchar(10) NOT NULL,
  `npr_com` int(2) NOT NULL,
  `iva_com` int(2) NOT NULL,
  `ela_com` varchar(50) NOT NULL,
  `rev_com` varchar(50) NOT NULL,
  `obs_com` varchar(255) default NULL,
  KEY `id` (`cod_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `compras`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `concesiones`
-- 

CREATE TABLE `concesiones` (
  `cod_con` int(11) NOT NULL auto_increment COMMENT 'Codigo del tipo de concesion',
  `nom_con` varchar(60) character set latin1 collate latin1_spanish_ci NOT NULL COMMENT 'Nombre del tipo de concesion',
  `des_con` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de concesion',
  PRIMARY KEY  (`cod_con`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- 
-- Volcar la base de datos para la tabla `concesiones`
-- 

INSERT INTO `concesiones` VALUES (1, 'Bono de Productividad', 'Bono que se cancela de acuerdo con la evaluación de desempeño');
INSERT INTO `concesiones` VALUES (2, 'Devolución', 'Devolución por cobro indebido');
INSERT INTO `concesiones` VALUES (3, 'Dias acumulativos fideicomiso', 'Art. 108 L.O.T.');
INSERT INTO `concesiones` VALUES (4, 'Aprt. Juguetes a Menores<br>Res. 0106-2011 Lit. K', 'Aporte para adquisición de juguetes a hijos menores según resolución N 0106-2011 literal K');
INSERT INTO `concesiones` VALUES (5, 'Ajuste de sueldo por pagar', 'Ajuste de sueldo por pagar');
INSERT INTO `concesiones` VALUES (6, 'Diferencia prima por pagar', 'Diferencia prima por pagar');
INSERT INTO `concesiones` VALUES (7, 'Bono de Compensación por Experiencia', 'Bono de Compensación por Experiencia');
INSERT INTO `concesiones` VALUES (8, 'Bono vacacional', 'Bono vacacional');
INSERT INTO `concesiones` VALUES (9, 'Bono de Compensación por Experiencia no cancelado mes de Sep', 'Bono de Compensación por Experiencia no cancelado mes de Sep.');
INSERT INTO `concesiones` VALUES (10, 'Días laborados por pagar por entrada en el cargo', 'Días del período anterior no cancelados por motivo de la entrada en el cargo');
INSERT INTO `concesiones` VALUES (11, 'Fración de salario por días laborados', 'Fración de salario por días laborados en el período por entrada en el cargo');
INSERT INTO `concesiones` VALUES (12, 'Retroactivo por Prima Antiguedad', 'Cancelación de retroactivo de Prima por antiguedad no cancelada');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `constancias_per`
-- 

CREATE TABLE `constancias_per` (
  `cod_con` int(11) NOT NULL auto_increment,
  `fch_con` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `nom_dep` varchar(100) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `des_con` varchar(50) NOT NULL,
  `sue_con` varchar(1) NOT NULL,
  `prm_con` varchar(1) NOT NULL,
  `cst_con` varchar(1) NOT NULL,
  `sue_mon` decimal(9,2) NOT NULL,
  `prm_mon` decimal(9,2) NOT NULL,
  `cst_mon` decimal(9,2) NOT NULL,
  `mot_con` varchar(120) NOT NULL,
  PRIMARY KEY  (`cod_con`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Constancias Solicitadas' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `constancias_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cst_tk_pagada`
-- 

CREATE TABLE `cst_tk_pagada` (
  `cod_cst` int(11) NOT NULL auto_increment COMMENT 'Código de Cesta Ticket Pagada',
  `mes_cst` int(11) NOT NULL COMMENT 'Mes de la Cesta Ticket pagada',
  `ano_cst` int(11) NOT NULL COMMENT 'Año de de la Cesta Ticket pagada',
  `ced_per` int(11) NOT NULL COMMENT 'Cédula del Personal de la Cesta Ticket pagada',
  `dias_cst` int(11) NOT NULL COMMENT 'Total de días del mes de la Cesta Ticket pagada',
  `dias_cst_pag` int(11) NOT NULL COMMENT 'Total de días pagados por mes de la Cesta Ticket pagada',
  `ut_cst` int(11) NOT NULL COMMENT 'Bs. por día de la Cesta Ticket pagada',
  `mnt_cst` int(11) NOT NULL COMMENT 'Total Bs. por personal de la Cesta Ticket pagada',
  PRIMARY KEY  (`cod_cst`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de la Cesta Ticket pagada' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `cst_tk_pagada`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cuentas`
-- 

CREATE TABLE `cuentas` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `num_cue` varchar(20) NOT NULL COMMENT 'numero de cuenta de la persona',
  `tip_cue` varchar(1) NOT NULL COMMENT 'tipo de la cuenta de la persona',
  `fcam_cue` date NOT NULL COMMENT 'fecha de cambio del numero de cuenta',
  `ban_cue` varchar(50) NOT NULL,
  PRIMARY KEY  (`ced_per`),
  UNIQUE KEY `num_cue` (`num_cue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `cuentas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `deducciones`
-- 

CREATE TABLE `deducciones` (
  `cod_dsp` int(11) NOT NULL auto_increment COMMENT 'Codigo de la Deduccion',
  `con_dsp` varchar(100) NOT NULL COMMENT 'Concepto de la Deduccion',
  `ncuo_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas de la deduccion',
  `per_dsp` varchar(1) NOT NULL COMMENT 'Permanencia de la Deduccion',
  `mcuo_dsp` double(9,2) NOT NULL COMMENT 'Monto de las cuotas de la deduccion',
  `por_dsp` int(3) NOT NULL COMMENT 'Porcentaje de deduccion',
  `bpor_dsp` varchar(1) NOT NULL COMMENT 'Base del porcentaje',
  `cod_des` int(11) NOT NULL COMMENT 'Codigo del Descuento',
  `ced_per` varchar(12) NOT NULL COMMENT 'Cedula del Personal',
  `cod_car` int(11) NOT NULL COMMENT 'Codigo de cargo',
  `fch_dsp` date NOT NULL COMMENT 'Fecha de Registro de la Deduccion',
  `ncp_dsp` int(3) NOT NULL COMMENT 'Numero de cuotas pagadas',
  PRIMARY KEY  (`cod_dsp`),
  KEY `cod_des` (`cod_des`),
  KEY `cod_per` (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `deducciones`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dependencias`
-- 

CREATE TABLE `dependencias` (
  `cod_dep` varchar(5) NOT NULL COMMENT 'codigo de la dependencia',
  `nom_dep` varchar(150) NOT NULL COMMENT 'nombre de la dependencia',
  `cod_act` varchar(5) NOT NULL COMMENT 'actividad de la dependencia',
  `cod_pro` varchar(5) NOT NULL COMMENT 'programa de la dependencia',
  PRIMARY KEY  (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `dependencias`
-- 

INSERT INTO `dependencias` VALUES ('CBN', 'Dpto.  de Control de Bienes y Servicios Públicos', '', '');
INSERT INTO `dependencias` VALUES ('CTR', 'Despacho del Contralor', '', '');
INSERT INTO `dependencias` VALUES ('DAC', 'Departamento de Atención al Ciudadano', '', '');
INSERT INTO `dependencias` VALUES ('DAP', 'Dirección de Administración y Presupuesto', '', '');
INSERT INTO `dependencias` VALUES ('DCP', 'Dirección de Auditoría y Control Posterior', '', '');
INSERT INTO `dependencias` VALUES ('DSJ', 'Dirección de Serv. Jur. Potest. Inves. y Aten al Ciudano', '', '');
INSERT INTO `dependencias` VALUES ('DST', 'Dirección de Sala Técnica y Control de Obras Públicas', '', '');
INSERT INTO `dependencias` VALUES ('DTR', 'Determinación de Responsabilidades', '', '');
INSERT INTO `dependencias` VALUES ('SCR', 'Secretaría', '', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `descuentos`
-- 

CREATE TABLE `descuentos` (
  `cod_des` int(11) NOT NULL auto_increment COMMENT 'Codigo del tipo de descuento',
  `nom_des` varchar(50) NOT NULL COMMENT 'Nombre del tipo de descuento',
  `des_des` varchar(255) NOT NULL COMMENT 'Descripcion del tipo de descuento',
  PRIMARY KEY  (`cod_des`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Volcar la base de datos para la tabla `descuentos`
-- 

INSERT INTO `descuentos` VALUES (2, 'Monto Pendiente', 'Monto Pendiente por descontar');
INSERT INTO `descuentos` VALUES (4, 'Reintegro', 'Reintegro por Error');
INSERT INTO `descuentos` VALUES (5, 'Día no laborado', 'Día no laborado pendiente por descontar mes de diciembre dos mil once');
INSERT INTO `descuentos` VALUES (7, 'Descuento Permiso No remunerado', 'Descuento por día de permiso no remunerado');
INSERT INTO `descuentos` VALUES (9, 'Días  no laborados por renuncia', 'Despuento de Días no laborados por salida del cargo por renuncia. ');
INSERT INTO `descuentos` VALUES (10, 'Días no laborados por Salida del Cargo', 'Días no laborados por renuncia');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `educacion`
-- 

CREATE TABLE `educacion` (
  `cod_edu` int(11) NOT NULL auto_increment,
  `ced_per` varchar(12) NOT NULL,
  `tit_edu` varchar(50) NOT NULL,
  `niv_edu` varchar(50) NOT NULL,
  `ins_edu` varchar(100) NOT NULL,
  `fch_edu` date NOT NULL,
  `reg_edu` varchar(1) NOT NULL,
  PRIMARY KEY  (`cod_edu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Estudios Realizados' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `educacion`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `egresos`
-- 

CREATE TABLE `egresos` (
  `cod_egr` int(11) NOT NULL auto_increment,
  `fch_egr` date NOT NULL,
  `nor_egr` int(4) unsigned zerofill NOT NULL,
  `cod_ban` int(11) NOT NULL,
  `chq_egr` varchar(25) NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `con_egr` varchar(255) NOT NULL,
  `ret_iva_egr` int(3) NOT NULL,
  `ret_isrl_egr` int(2) NOT NULL,
  `ela_egr` varchar(50) NOT NULL,
  `rev_egr` varchar(50) NOT NULL,
  `apr_egr` varchar(50) NOT NULL,
  `cont_egr` varchar(50) NOT NULL,
  `obs_egr` varchar(255) default NULL,
  `ded_egr` double(9,2) NOT NULL COMMENT 'Otras Deducciones',
  `sin_par_egr` decimal(9,2) NOT NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  `sta_ban_egr` int(11) NOT NULL,
  KEY `id` (`cod_egr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `egresos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `entradas`
-- 

CREATE TABLE `entradas` (
  `cod_ntr` int(11) NOT NULL auto_increment COMMENT 'C�digo de la Entrada',
  `cod_usr` varchar(12) NOT NULL COMMENT 'C�digo del usuario',
  `fecha_ntr` date NOT NULL COMMENT 'fecha de la entrada',
  `hora_ntr` char(15) NOT NULL COMMENT 'Hora de la Entrada',
  PRIMARY KEY  (`cod_ntr`),
  KEY `cod_usr` (`cod_usr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Entradas de los Usuarios al Sistema' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `entradas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `facturas_pagos`
-- 

CREATE TABLE `facturas_pagos` (
  `cod_fac_pag` int(11) NOT NULL auto_increment,
  `cod_pag` int(11) NOT NULL,
  `fch_fac_pag` date NOT NULL,
  `num_fac_pag` varchar(15) NOT NULL,
  `con_fac_pag` varchar(15) NOT NULL,
  `mon_fac_pag` double(9,2) NOT NULL,
  `iva_fac_pag` double(9,2) NOT NULL,
  `isrl_fac_pag` double(9,2) NOT NULL,
  `iva_pag_pag` date NOT NULL,
  `iva_pag_mes` int(2) NOT NULL,
  `iva_pag_ano` int(4) NOT NULL,
  PRIMARY KEY  (`cod_fac_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='información de las facturas pagadas' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `facturas_pagos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `familiares`
-- 

CREATE TABLE `familiares` (
  `cod_fam` int(2) NOT NULL auto_increment COMMENT 'C�digo del Familiar',
  `ced_per` varchar(12) collate latin1_spanish_ci NOT NULL COMMENT 'C�dula del Personal',
  `ced_fam` varchar(12) collate latin1_spanish_ci NOT NULL COMMENT 'C�dula del  del Familiar',
  `nom_fam` varchar(50) collate latin1_spanish_ci NOT NULL COMMENT 'Nombre  del Familiar',
  `ape_fam` varchar(50) collate latin1_spanish_ci NOT NULL COMMENT 'Apellido  del Familiar',
  `sex_fam` varchar(1) collate latin1_spanish_ci NOT NULL COMMENT 'Sexo del Familiar',
  `fnac_fam` date NOT NULL COMMENT 'Fecha de Nacimiento del Familiar',
  `par_fam` varchar(25) collate latin1_spanish_ci NOT NULL COMMENT 'Parentesco  del Familiar',
  `est_fam` varchar(1) collate latin1_spanish_ci NOT NULL COMMENT 'Estudia el Familiar?',
  `obs_fam` varchar(255) collate latin1_spanish_ci NOT NULL COMMENT 'Observaciones acerca del Familiar',
  PRIMARY KEY  (`cod_fam`),
  UNIQUE KEY `ced_per` (`ced_per`,`nom_fam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Datos de los Familiares del Personal' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `familiares`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `feriados`
-- 

CREATE TABLE `feriados` (
  `fch_frd` date NOT NULL,
  `des_frd` varchar(50) NOT NULL,
  `tip_frd` varchar(25) NOT NULL,
  `tck_frd` varchar(2) NOT NULL,
  PRIMARY KEY  (`fch_frd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de dias Feridos';

-- 
-- Volcar la base de datos para la tabla `feriados`
-- 

INSERT INTO `feriados` VALUES ('2011-03-07', 'Carnaval', '', '');
INSERT INTO `feriados` VALUES ('2011-03-08', 'Carnaval', '', '');
INSERT INTO `feriados` VALUES ('2011-04-19', 'Firma Acta de Independencia', '', '');
INSERT INTO `feriados` VALUES ('2011-04-21', 'Semana Santa', '', '');
INSERT INTO `feriados` VALUES ('2011-04-22', 'Semana Santa', '', '');
INSERT INTO `feriados` VALUES ('2011-06-24', 'Dia de San Juan / Batalla de Carabobo', '', '');
INSERT INTO `feriados` VALUES ('2011-07-05', 'Día de la Independencia de Venezuela', '', '');
INSERT INTO `feriados` VALUES ('2011-07-24', 'Día de la Muerte del Libertador Simón Bolívar', '', '');
INSERT INTO `feriados` VALUES ('2011-10-12', 'Día de la Resistencia Indigena', '', '');
INSERT INTO `feriados` VALUES ('2012-01-02', 'Fumigación de Oficina', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-01-03', 'Fumigación de Oficina', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-01-04', 'Fumigación de Oficina', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-01-18', 'Día elevación a ciudad de Ejido', 'municipal', 'NO');
INSERT INTO `feriados` VALUES ('2012-02-17', 'Feriado', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-02-20', 'Lunes de Carnaval', 'feriado', 'NO');
INSERT INTO `feriados` VALUES ('2012-02-21', 'Martes de Carnaval', 'feriado', 'NO');
INSERT INTO `feriados` VALUES ('2012-04-04', 'Día Otorgado por contralor encargado Lic. RamóLobo', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-04-05', 'Jueves Santo', 'feriado', 'SI');
INSERT INTO `feriados` VALUES ('2012-04-06', 'Viernes Santo', 'feriado', 'SI');
INSERT INTO `feriados` VALUES ('2012-04-19', 'Firma de Acta de Independencia', 'fiesta_nacional', 'SI');
INSERT INTO `feriados` VALUES ('2012-05-01', 'Día Internacional del Trabajador', 'feriado', 'SI');
INSERT INTO `feriados` VALUES ('2012-07-05', 'Día de la Independencia de Venezuela', 'fiesta_nacional', 'SI');
INSERT INTO `feriados` VALUES ('2012-07-24', 'Natalicio de Simón  Bolívar', 'fiesta_nacional', 'SI');
INSERT INTO `feriados` VALUES ('2012-09-07', 'Dia funcionario publico', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-10-08', 'Día por votaciones', 'no_laboral', 'NO');
INSERT INTO `feriados` VALUES ('2012-10-12', 'Día de la Resistencia Indigena', 'fiesta_nacional', 'SI');
INSERT INTO `feriados` VALUES ('2012-12-24', 'Navidad', 'feriado', 'SI');
INSERT INTO `feriados` VALUES ('2012-12-25', 'Navidad', 'feriado', 'SI');
INSERT INTO `feriados` VALUES ('2012-12-26', 'Navidad', 'feriado', 'NO');
INSERT INTO `feriados` VALUES ('2012-12-27', 'Navidad', 'feriado', 'NO');
INSERT INTO `feriados` VALUES ('2012-12-28', 'Navidad', 'feriado', 'NO');
INSERT INTO `feriados` VALUES ('2012-12-31', 'Navidad', 'feriado', 'SI');
INSERT INTO `feriados` VALUES ('2013-01-01', 'Navidad', 'feriado', 'NO');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `grupos`
-- 

CREATE TABLE `grupos` (
  `cod_grp` int(2) NOT NULL auto_increment COMMENT 'codigo del grupo',
  `nom_grp` varchar(20) NOT NULL COMMENT 'nombre del grupo',
  `des_grp` varchar(255) NOT NULL COMMENT 'descripcion del grupo',
  PRIMARY KEY  (`cod_grp`),
  UNIQUE KEY `nom_grp` (`nom_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- 
-- Volcar la base de datos para la tabla `grupos`
-- 

INSERT INTO `grupos` VALUES (1, 'Administradores', 'Con todos los privilegios');
INSERT INTO `grupos` VALUES (2, 'Administración', 'Grupo con todos los privilegios del área de Usuarios');
INSERT INTO `grupos` VALUES (3, 'Personal', 'Grupo para el personal adscrito a la institución');
INSERT INTO `grupos` VALUES (4, 'Pasantes', 'Privilegios Mínimos');
INSERT INTO `grupos` VALUES (5, 'BienesP', 'Grupo de Pasantes');
INSERT INTO `grupos` VALUES (6, 'Secretaria', 'Secretaria de la Contraloría');
INSERT INTO `grupos` VALUES (7, 'Asist Administración', 'Asistentes Asignados en el Area de Administración');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `horarios`
-- 

CREATE TABLE `horarios` (
  `cod_hor` int(11) NOT NULL auto_increment,
  `nom_hor` varchar(25) NOT NULL,
  `fch_dsd` date NOT NULL,
  `fch_hst` date NOT NULL,
  `lun_ini_hor` time NOT NULL,
  `lun_fin_hor` time NOT NULL,
  `lun_ini_hor2` time NOT NULL,
  `lun_fin_hor2` time NOT NULL,
  `mar_ini_hor` time NOT NULL,
  `mar_fin_hor` time NOT NULL,
  `mar_ini_hor2` time NOT NULL,
  `mar_fin_hor2` time NOT NULL,
  `mie_ini_hor` time NOT NULL,
  `mie_fin_hor` time NOT NULL,
  `mie_ini_hor2` time NOT NULL,
  `mie_fin_hor2` time NOT NULL,
  `jue_ini_hor` time NOT NULL,
  `jue_fin_hor` time NOT NULL,
  `jue_ini_hor2` time NOT NULL,
  `jue_fin_hor2` time NOT NULL,
  `vie_ini_hor` time NOT NULL,
  `vie_fin_hor` time NOT NULL,
  `vie_ini_hor2` time NOT NULL,
  `vie_fin_hor2` time NOT NULL,
  `sab_ini_hor` time NOT NULL,
  `sab_fin_hor` time NOT NULL,
  `dom_ini_hor` time NOT NULL,
  `dom_fin_hor` time NOT NULL,
  `tol_hor` int(11) NOT NULL,
  `obs_hor` longtext NOT NULL,
  PRIMARY KEY  (`cod_hor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `horarios`
-- 

INSERT INTO `horarios` VALUES (1, 'Horario Normal', '2012-01-01', '2012-07-19', '08:15:00', '11:55:00', '14:40:00', '00:00:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '08:15:00', '11:55:00', '14:40:00', '16:55:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 15, '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ina_pru`
-- 

CREATE TABLE `ina_pru` (
  `cod_ina` int(11) NOT NULL auto_increment,
  `ced_per` varchar(12) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL,
  PRIMARY KEY  (`cod_ina`),
  UNIQUE KEY `ced_per` (`ced_per`,`fch_ina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ina_pru`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `inasistencias`
-- 

CREATE TABLE `inasistencias` (
  `cod_ina` int(11) NOT NULL auto_increment,
  `ced_per` varchar(12) NOT NULL,
  `fch_ina` date NOT NULL,
  `tip_ina` varchar(30) NOT NULL,
  `des_ina` varchar(50) NOT NULL,
  PRIMARY KEY  (`cod_ina`),
  UNIQUE KEY `ced_per` (`ced_per`,`fch_ina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Inasistencias del personal' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `inasistencias`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `incidencias`
-- 

CREATE TABLE `incidencias` (
  `cod_inc` int(11) NOT NULL auto_increment COMMENT 'Codigo de la asignacion',
  `con_inc` varchar(100) NOT NULL COMMENT 'Concepto de la asignacion',
  `mes_inc` int(2) NOT NULL COMMENT 'Mes en que se paga la incidencia',
  `ano_inc` int(4) NOT NULL COMMENT 'Ano en que se paga la incidencia',
  `fini_inc` date NOT NULL COMMENT 'Fecha de inicio del Periodo de Inicidencia',
  `ffin_inc` date NOT NULL COMMENT 'Fecha de fin del Periodo de Inicidencia',
  PRIMARY KEY  (`cod_inc`),
  UNIQUE KEY `mes_inc` (`mes_inc`,`ano_inc`,`fini_inc`,`ffin_inc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `incidencias`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `incidencias_sueldos`
-- 

CREATE TABLE `incidencias_sueldos` (
  `cod_inc` int(11) NOT NULL COMMENT 'Codigo de incidencias',
  `cod_sue` int(2) NOT NULL COMMENT 'Codigo del sueldo',
  `mnt_inc` double(9,2) NOT NULL COMMENT 'Monto o porcentaje de la incidencia',
  `bas_inc` varchar(1) NOT NULL COMMENT 'Base para el calculo de la Incidencia',
  UNIQUE KEY `cod_inc` (`cod_inc`,`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de inicidencias para cada sueldo';

-- 
-- Volcar la base de datos para la tabla `incidencias_sueldos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ing_pat_vhi`
-- 

CREATE TABLE `ing_pat_vhi` (
  `id` int(11) NOT NULL auto_increment,
  `fecha` date NOT NULL,
  `serie` int(3) NOT NULL,
  `inicio` int(8) NOT NULL,
  `fin` int(8) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de los ingresos por Patente Vehicular' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ing_pat_vhi`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `justificativos_per`
-- 

CREATE TABLE `justificativos_per` (
  `cod_sol_jus` int(11) NOT NULL auto_increment,
  `fch_sol_jus` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_jus` int(2) NOT NULL,
  `ini_sol_jus` date NOT NULL,
  `fin_sol_jus` date NOT NULL,
  `obs_sol_jus` longtext NOT NULL,
  `mot_sol_jus` varchar(25) NOT NULL,
  `apro_sol_jus` varchar(2) NOT NULL,
  PRIMARY KEY  (`cod_sol_jus`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `justificativos_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `medicinas`
-- 

CREATE TABLE `medicinas` (
  `cod_med` int(11) NOT NULL auto_increment COMMENT 'codigo de pago de medicinas',
  `fch_med` date NOT NULL COMMENT 'fecha de registro del pago de medicinas',
  `fch_pag_med` date NOT NULL COMMENT 'fecha del pago de las medicinas',
  `mnt_med` decimal(9,2) NOT NULL COMMENT 'monto anual',
  `obs_med` text NOT NULL COMMENT 'observaciones, comentarios o información adicional',
  PRIMARY KEY  (`cod_med`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las medicinas pagadas a los Funcionarios' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `medicinas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `medicinas_per`
-- 

CREATE TABLE `medicinas_per` (
  `cod_med_per` int(11) NOT NULL auto_increment,
  `cod_med` int(11) NOT NULL,
  `ced_per` int(11) NOT NULL,
  `mnt_med_per` decimal(9,2) NOT NULL,
  PRIMARY KEY  (`cod_med_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `medicinas_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nomina_asign`
-- 

CREATE TABLE `nomina_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(60) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY  (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nomina_asign`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nomina_deduc`
-- 

CREATE TABLE `nomina_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY  (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nomina_deduc`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nomina_incidencias`
-- 

CREATE TABLE `nomina_incidencias` (
  `ano_inc` int(4) NOT NULL,
  `mes_inc` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `mnt_inc` double(9,2) NOT NULL,
  `con_inc` varchar(255) NOT NULL,
  `ivss_inc` double(9,2) NOT NULL,
  `spf_inc` double(9,2) NOT NULL,
  `cah_inc` double(9,2) NOT NULL,
  `bnoc_inc` double(9,2) NOT NULL,
  `bvac_inc` double(9,2) NOT NULL,
  PRIMARY KEY  (`ano_inc`,`mes_inc`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nomina_incidencias`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nomina_pagar`
-- 

CREATE TABLE `nomina_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  PRIMARY KEY  (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nomina_pagar`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nominapre_asign`
-- 

CREATE TABLE `nominapre_asign` (
  `ano_asg` int(4) NOT NULL,
  `mes_asg` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_cnp` int(11) NOT NULL,
  `con_cnp` varchar(100) NOT NULL,
  `cod_con` int(11) NOT NULL,
  `nom_con` varchar(50) NOT NULL,
  `mcuo_cnp` double(9,2) NOT NULL,
  PRIMARY KEY  (`ano_asg`,`mes_asg`,`cod_car`,`por_nom`,`cod_cnp`,`con_cnp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nominapre_asign`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nominapre_deduc`
-- 

CREATE TABLE `nominapre_deduc` (
  `ano_ded` int(4) NOT NULL,
  `mes_ded` int(2) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` varchar(11) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `cod_dsp` int(11) NOT NULL,
  `con_dsp` varchar(100) NOT NULL,
  `cod_des` int(11) NOT NULL,
  `nom_des` varchar(50) NOT NULL,
  `mcuo_dsp` double(9,2) NOT NULL,
  PRIMARY KEY  (`ano_ded`,`mes_ded`,`cod_car`,`por_nom`,`cod_dsp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nominapre_deduc`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nominapre_pagar`
-- 

CREATE TABLE `nominapre_pagar` (
  `ano_nom` int(4) NOT NULL,
  `mes_nom` int(2) NOT NULL,
  `por_nom` int(1) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nac_per` varchar(1) NOT NULL,
  `nom_per` varchar(50) NOT NULL,
  `ape_per` varchar(50) NOT NULL,
  `mon_sue` double(9,2) NOT NULL,
  `mon_asi` double(9,2) NOT NULL,
  `mon_ded` double(9,2) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `lph_des` double(9,2) NOT NULL,
  `spf_des` double(9,2) NOT NULL,
  `sso_des` double(9,2) NOT NULL,
  `cah_des` double(9,2) NOT NULL,
  `sfu_des` double(9,2) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_asg` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  `num_cue` varchar(20) NOT NULL,
  `tip_cue` varchar(1) NOT NULL,
  `ban_cue` varchar(50) NOT NULL,
  `num_dnl` int(4) NOT NULL COMMENT 'Número de Días no laborados',
  `mon_dnl` float(9,2) NOT NULL COMMENT 'Monto por Días No Laborados',
  `lph_apr` double(9,2) NOT NULL COMMENT 'Aportes de LPH',
  `spf_apr` double(9,2) NOT NULL COMMENT 'Aportes de SPF',
  `sso_apr` double(9,2) NOT NULL COMMENT 'Aportes de SSO',
  `cah_apr` double(9,2) NOT NULL COMMENT 'Aportes de CAH',
  `sfu_apr` double(9,2) NOT NULL COMMENT 'Aportes de SFU',
  `ppo_des` double(9,3) NOT NULL,
  `pio_des` double(9,2) NOT NULL,
  `prm_hog` double(9,2) NOT NULL COMMENT 'Primas por Hogar',
  `prm_hij` double(9,2) NOT NULL COMMENT 'Primas por Hijos',
  `prm_ant` double(9,2) NOT NULL COMMENT 'Primas por Antiguedad',
  `prm_pro` double(9,2) NOT NULL COMMENT 'Primas por Profesion',
  `prm_hje` double(9,2) NOT NULL COMMENT 'Primas por Hijos Excepcionales',
  `prm_jer` double(9,2) NOT NULL COMMENT 'Primas por Jerarquia',
  `prm_otr` double(9,2) NOT NULL COMMENT 'Otras Primas',
  `mon_scom` double(9,2) NOT NULL COMMENT 'Monto del sueldo Completo',
  PRIMARY KEY  (`ano_nom`,`mes_nom`,`por_nom`,`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `nominapre_pagar`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `oficios_enviados`
-- 

CREATE TABLE `oficios_enviados` (
  `cod_ofi` int(11) NOT NULL auto_increment,
  `tip_ofi` varchar(10) NOT NULL,
  `num_ofi` int(4) unsigned zerofill NOT NULL,
  `fch_ofi` date NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `rdp_ofi` varchar(75) NOT NULL,
  `rpr_ofi` varchar(75) NOT NULL,
  `ddp_ofi` varchar(75) NOT NULL,
  `dpr_ofi` varchar(75) NOT NULL,
  `des_ofi` varchar(255) NOT NULL,
  `nds_ofi` int(2) NOT NULL,
  `fen_ofi` date default NULL,
  `ftr_ofi` date default NULL,
  `frs_ofi` date default NULL,
  `ref_ofi` varchar(7) NOT NULL,
  `obs_ofi` varchar(255) NOT NULL,
  `est_ofi` varchar(1) NOT NULL COMMENT 'Estado del Oficio',
  PRIMARY KEY  (`cod_ofi`),
  UNIQUE KEY `tip_ofi` (`tip_ofi`,`num_ofi`,`fch_ofi`,`rdp_ofi`),
  KEY `cod_dep` (`cod_dep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Oficios Enviados' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `oficios_enviados`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `oficios_recibidos`
-- 

CREATE TABLE `oficios_recibidos` (
  `cod_ofr` int(11) NOT NULL auto_increment,
  `fch_ofr` date NOT NULL,
  `fch_emi_ofr` date NOT NULL,
  `num_ofr` varchar(60) NOT NULL,
  `rem_ofr` varchar(60) NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `con_ofr` varchar(150) NOT NULL,
  `ane_ofr` varchar(2) NOT NULL,
  `obs_ofr` varchar(255) NOT NULL,
  `prc_ofr` varchar(255) NOT NULL,
  PRIMARY KEY  (`cod_ofr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Oficios Recibidos' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `oficios_recibidos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `pagados`
-- 

CREATE TABLE `pagados` (
  `ced_per` varchar(12) NOT NULL,
  PRIMARY KEY  (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `pagados`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `pagados_frc`
-- 

CREATE TABLE `pagados_frc` (
  `cod_car` varchar(12) NOT NULL,
  PRIMARY KEY  (`cod_car`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `pagados_frc`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `pagos`
-- 

CREATE TABLE `pagos` (
  `cod_pag` int(11) NOT NULL auto_increment,
  `fch_pag` date NOT NULL,
  `nor_pag` int(4) unsigned zerofill NOT NULL,
  `frm_pag` int(6) unsigned zerofill NOT NULL COMMENT 'Nº de formulario',
  `frm_com` int(6) unsigned zerofill NOT NULL,
  `mon_pag` double(9,2) NOT NULL COMMENT 'Monto total de la transacción',
  `ela_pag` varchar(50) NOT NULL,
  `rev_pag` varchar(50) NOT NULL,
  `apr_pag` varchar(50) NOT NULL,
  `obs_pag` varchar(255) default NULL,
  `frm_egr` int(6) unsigned zerofill NOT NULL,
  KEY `id` (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `pagos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `part_presup`
-- 

CREATE TABLE `part_presup` (
  `cod_par` int(11) NOT NULL auto_increment,
  `sec_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `pro_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `act_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `ram_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `par_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `gen_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `esp_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `sub_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  `des_par` varchar(255) character set latin1 collate latin1_spanish_ci default NULL,
  `obs_par` varchar(11) character set latin1 collate latin1_spanish_ci default NULL,
  PRIMARY KEY  (`cod_par`),
  UNIQUE KEY `sector` (`par_par`,`gen_par`,`esp_par`,`sub_par`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=137 ;

-- 
-- Volcar la base de datos para la tabla `part_presup`
-- 

INSERT INTO `part_presup` VALUES (1, '', '', '', '', '4.01', '01', '01', '00', 'Sueldos básicos personal fijo a tiempo completo', '');
INSERT INTO `part_presup` VALUES (2, '', '', '', '', '4.01', '01', '03', '00', 'Suplencias a empleados', '');
INSERT INTO `part_presup` VALUES (3, NULL, NULL, NULL, NULL, '4.01', '01', '18', '00', 'Remuneración al Personal Contratado', NULL);
INSERT INTO `part_presup` VALUES (4, '', '', '', '', '4.01', '01', '19', '00', 'Retribuc. por becas-salarios, bolsas de trabajo, pasantías y similares', '');
INSERT INTO `part_presup` VALUES (5, NULL, NULL, NULL, NULL, '4.01', '03', '03', '00', 'Primas por Hogar', NULL);
INSERT INTO `part_presup` VALUES (6, NULL, NULL, NULL, NULL, '4.01', '03', '04', '00', 'Prima por hijos a Empleados', NULL);
INSERT INTO `part_presup` VALUES (7, NULL, NULL, NULL, NULL, '4.01', '03', '08', '00', 'Prima de Profesionalización a Empleados', NULL);
INSERT INTO `part_presup` VALUES (8, NULL, NULL, NULL, NULL, '4.01', '03', '09', '00', 'Prima por antiguedad a Empelados', NULL);
INSERT INTO `part_presup` VALUES (9, NULL, NULL, NULL, NULL, '4.01', '03', '10', '00', 'Prima por jerarquía o responsabilidad en el cargo', NULL);
INSERT INTO `part_presup` VALUES (10, NULL, NULL, NULL, NULL, '4.01', '03', '97', '00', 'Otras primas', NULL);
INSERT INTO `part_presup` VALUES (11, NULL, NULL, NULL, NULL, '4.01', '04', '05', '00', 'Complemento a empleados por gastos de representación', NULL);
INSERT INTO `part_presup` VALUES (12, NULL, NULL, NULL, NULL, '4.01', '04', '06', '00', 'Complemento a empleados por comisión de servicio', NULL);
INSERT INTO `part_presup` VALUES (13, NULL, NULL, NULL, NULL, '4.01', '04', '08', '00', 'Bono Compensatorio de Alimentación a Empleados', NULL);
INSERT INTO `part_presup` VALUES (14, NULL, NULL, NULL, NULL, '4.01', '04', '26', '00', 'Bono Compensatorio de Alimentación a pesonal contratado', NULL);
INSERT INTO `part_presup` VALUES (15, NULL, NULL, NULL, NULL, '4.01', '05', '01', '00', 'Aguinaldos a Empleados', NULL);
INSERT INTO `part_presup` VALUES (16, NULL, NULL, NULL, NULL, '4.01', '05', '03', '00', 'Bono Vacacional a Empleados', NULL);
INSERT INTO `part_presup` VALUES (17, NULL, NULL, NULL, NULL, '4.01', '05', '07', '00', 'Aguinaldos a personal contratado', NULL);
INSERT INTO `part_presup` VALUES (18, NULL, NULL, NULL, NULL, '4.01', '05', '08', '00', 'Bono Vacacional a personal contratado', NULL);
INSERT INTO `part_presup` VALUES (19, NULL, NULL, NULL, NULL, '4.01', '06', '01', '00', 'Aporte Patronal al IVSS por empleados', NULL);
INSERT INTO `part_presup` VALUES (20, NULL, NULL, NULL, NULL, '4.01', '06', '04', '00', 'Aporte Patronal al Fondo Seg. de Paro Fzoso Empleados', NULL);
INSERT INTO `part_presup` VALUES (21, NULL, NULL, NULL, NULL, '4.01', '06', '05', '00', 'Aporte Patronal Fondo de Ahorro Hab Empleados', NULL);
INSERT INTO `part_presup` VALUES (22, '', '', '', '', '4.01', '07', '02', '00', 'Becas a Empleados', '');
INSERT INTO `part_presup` VALUES (23, NULL, NULL, NULL, NULL, '4.01', '07', '06', '00', 'Ayud. p/ medicin. Gtos Med. Odont y Hospit Empleados', NULL);
INSERT INTO `part_presup` VALUES (24, NULL, NULL, NULL, NULL, '4.01', '07', '07', '00', 'Aporte patronal a cajas de ahorro empleados', NULL);
INSERT INTO `part_presup` VALUES (25, NULL, NULL, NULL, NULL, '4.01', '07', '09', '00', 'Ayudas a empleados para adquisición de uniformes y utiles escolares de sus hijos', NULL);
INSERT INTO `part_presup` VALUES (26, NULL, NULL, NULL, NULL, '4.01', '07', '10', '00', 'Dotación de Uniformes a Empleados', NULL);
INSERT INTO `part_presup` VALUES (27, NULL, NULL, NULL, NULL, '4.01', '07', '12', '00', 'Aporte Patronal p la adqui de juguetes p hijos de Empleados', NULL);
INSERT INTO `part_presup` VALUES (28, NULL, NULL, NULL, NULL, '4.01', '08', '01', '00', 'Prestaciones Sociales e Indemnizaciones a empleados', NULL);
INSERT INTO `part_presup` VALUES (29, NULL, NULL, NULL, NULL, '4.01', '08', '03', '00', 'Prestaciones Sociales e indemnizaciones a personal contratado', NULL);
INSERT INTO `part_presup` VALUES (30, NULL, NULL, NULL, NULL, '4.02', '01', '01', '00', 'Alimentos y Bebidas para personas', NULL);
INSERT INTO `part_presup` VALUES (31, NULL, NULL, NULL, NULL, '4.02', '04', '03', '00', 'Cauchos y tripas para vehiculos', NULL);
INSERT INTO `part_presup` VALUES (32, NULL, NULL, NULL, NULL, '4.02', '05', '01', '00', 'Pulpa de madera, papel y cartón', NULL);
INSERT INTO `part_presup` VALUES (33, NULL, NULL, NULL, NULL, '4.02', '05', '03', '00', 'Productos de papel y cartón para oficina', NULL);
INSERT INTO `part_presup` VALUES (34, NULL, NULL, NULL, NULL, '4.02', '05', '04', '00', 'Libros, revistas y periódicos', NULL);
INSERT INTO `part_presup` VALUES (35, NULL, NULL, NULL, NULL, '4.02', '06', '03', '00', 'Tintas, pinturas y colorantes', NULL);
INSERT INTO `part_presup` VALUES (36, NULL, NULL, NULL, NULL, '4.02', '06', '06', '00', 'Combustible y lubricantes', NULL);
INSERT INTO `part_presup` VALUES (37, NULL, NULL, NULL, NULL, '4.02', '08', '07', '00', 'Material de señalamiento', NULL);
INSERT INTO `part_presup` VALUES (38, NULL, NULL, NULL, NULL, '4.02', '08', '09', '00', 'Repuestos y Accesorios p/ equipos de transporte', NULL);
INSERT INTO `part_presup` VALUES (39, NULL, NULL, NULL, NULL, '4.02', '10', '02', '00', 'Materiales y útiles de limpieza y aseo', NULL);
INSERT INTO `part_presup` VALUES (40, NULL, NULL, NULL, NULL, '4.02', '10', '05', '00', 'Útiles Escritorio, Ofic. y materiales instrucción', NULL);
INSERT INTO `part_presup` VALUES (41, NULL, NULL, NULL, NULL, '4.02', '10', '06', '00', 'Condecoraciones ofrendas y similares', NULL);
INSERT INTO `part_presup` VALUES (42, NULL, NULL, NULL, NULL, '4.02', '10', '07', '00', 'Productos de seguridad en el trabajo', NULL);
INSERT INTO `part_presup` VALUES (43, '', '', '', '', '4.02', '10', '08', '00', 'Materiales p/Equipos de Computación', '');
INSERT INTO `part_presup` VALUES (44, NULL, NULL, NULL, NULL, '4.02', '10', '09', '00', 'Especies timbradas y valores', NULL);
INSERT INTO `part_presup` VALUES (45, NULL, NULL, NULL, NULL, '4.02', '10', '11', '00', 'Materiales Eléctricos', NULL);
INSERT INTO `part_presup` VALUES (46, NULL, NULL, NULL, NULL, '4.02', '10', '99', '00', 'Otros productos y útiles diversos', NULL);
INSERT INTO `part_presup` VALUES (47, NULL, NULL, NULL, NULL, '4.02', '99', '01', '00', 'Otros materiales y Suministros', NULL);
INSERT INTO `part_presup` VALUES (49, NULL, NULL, NULL, NULL, '4.03', '04', '01', '00', 'Electricidad', NULL);
INSERT INTO `part_presup` VALUES (50, NULL, NULL, NULL, NULL, '4.03', '04', '03', '00', 'Agua', NULL);
INSERT INTO `part_presup` VALUES (51, NULL, NULL, NULL, NULL, '4.03', '04', '04', '00', 'Teléfono', NULL);
INSERT INTO `part_presup` VALUES (52, NULL, NULL, NULL, NULL, '4.03', '04', '05', '00', 'Servicio de comunicaciones', NULL);
INSERT INTO `part_presup` VALUES (53, NULL, NULL, NULL, NULL, '4.03', '04', '06', '00', 'Servicio de Aseo Urbano y Dominciliario', NULL);
INSERT INTO `part_presup` VALUES (54, NULL, NULL, NULL, NULL, '4.03', '04', '07', '00', 'Servicio de Condominio', NULL);
INSERT INTO `part_presup` VALUES (55, NULL, NULL, NULL, NULL, '4.03', '06', '01', '00', 'Fletes y embalajes', NULL);
INSERT INTO `part_presup` VALUES (56, NULL, NULL, NULL, NULL, '4.03', '07', '01', '00', 'Publicidad y propaganda', NULL);
INSERT INTO `part_presup` VALUES (57, NULL, NULL, NULL, NULL, '4.03', '07', '02', '00', 'Imprenta y Reproducción', NULL);
INSERT INTO `part_presup` VALUES (58, NULL, NULL, NULL, NULL, '4.03', '07', '03', '00', 'Relaciones Sociales', NULL);
INSERT INTO `part_presup` VALUES (59, NULL, NULL, NULL, NULL, '4.03', '07', '04', '00', 'Avisos', NULL);
INSERT INTO `part_presup` VALUES (60, NULL, NULL, NULL, NULL, '4.03', '08', '01', '00', 'Primas y gastos de Seguros', NULL);
INSERT INTO `part_presup` VALUES (61, NULL, NULL, NULL, NULL, '4.03', '08', '02', '00', 'Comisiones y Gastos Bancarios', NULL);
INSERT INTO `part_presup` VALUES (62, NULL, NULL, NULL, NULL, '4.03', '09', '01', '00', 'Viaticos y pasajes dentro del país', NULL);
INSERT INTO `part_presup` VALUES (63, NULL, NULL, NULL, NULL, '4.03', '10', '01', '00', 'Servicios Jurídicos', NULL);
INSERT INTO `part_presup` VALUES (64, NULL, NULL, NULL, NULL, '4.03', '10', '02', '00', 'Servicios de Contabilidad y Auditoría', NULL);
INSERT INTO `part_presup` VALUES (65, NULL, NULL, NULL, NULL, '4.03', '10', '07', '00', 'Servicios de Capacitación y Adiestramiento', NULL);
INSERT INTO `part_presup` VALUES (66, NULL, NULL, NULL, NULL, '4.03', '11', '02', '00', 'Conserv. y reparac. menores equipo de transp. trac y elev', NULL);
INSERT INTO `part_presup` VALUES (67, NULL, NULL, NULL, NULL, '4.03', '11', '03', '00', 'Conserv.  y rep. Men. de equipos de comunic y señalamiento', NULL);
INSERT INTO `part_presup` VALUES (68, NULL, NULL, NULL, NULL, '4.03', '11', '07', '00', 'Conserv. y reparac. Men. de máq. muebles y demás equipos', NULL);
INSERT INTO `part_presup` VALUES (69, NULL, NULL, NULL, NULL, '4.03', '11', '99', '00', 'Conserv.  y rep. Men. de otras maq. y equipos', NULL);
INSERT INTO `part_presup` VALUES (70, NULL, NULL, NULL, NULL, '4.03', '15', '02', '00', 'Tasas y otros derechos obligatorios', NULL);
INSERT INTO `part_presup` VALUES (71, NULL, NULL, NULL, NULL, '4.03', '17', '01', '00', 'Serv. de gestión adm. prestados por org. asist. tecnica', NULL);
INSERT INTO `part_presup` VALUES (72, NULL, NULL, NULL, NULL, '4.03', '18', '01', '00', 'Impuesto al Valor Agregado', NULL);
INSERT INTO `part_presup` VALUES (73, '', '', '', '', '4.03', '99', '01', '00', 'Otros Servicios no personales', '');
INSERT INTO `part_presup` VALUES (74, NULL, NULL, NULL, NULL, '4.04', '01', '01', '02', 'Repuestos mayores p/ Equipos Transp. Tracc y elevación', NULL);
INSERT INTO `part_presup` VALUES (75, NULL, NULL, NULL, NULL, '4.04', '01', '02', '02', 'Reparaciones Mayores p/ Equipos Transp. Trac. y Elevación', NULL);
INSERT INTO `part_presup` VALUES (76, NULL, NULL, NULL, NULL, '4.04', '02', '01', '00', 'Conserv. Ampliac. y Mejoras mayores de Obras en Bienes Domin Privado', NULL);
INSERT INTO `part_presup` VALUES (77, NULL, NULL, NULL, NULL, '4.04', '04', '01', '00', 'Vehículos automotores terrestres', NULL);
INSERT INTO `part_presup` VALUES (78, NULL, NULL, NULL, NULL, '4.04', '05', '01', '00', 'Equipos de Telecomunicaciones', NULL);
INSERT INTO `part_presup` VALUES (79, NULL, NULL, NULL, NULL, '4.04', '09', '01', '00', 'Mobiliario y equipos de oficina', NULL);
INSERT INTO `part_presup` VALUES (80, NULL, NULL, NULL, NULL, '4.04', '09', '02', '00', 'Equipos de Computación', NULL);
INSERT INTO `part_presup` VALUES (81, NULL, NULL, NULL, NULL, '4.04', '09', '03', '00', 'Mobiliario y equipos de Alojamiento', NULL);
INSERT INTO `part_presup` VALUES (82, NULL, NULL, NULL, NULL, '4.04', '12', '04', '00', 'Paquetes y programas de computación', NULL);
INSERT INTO `part_presup` VALUES (83, NULL, NULL, NULL, NULL, '4.04', '99', '01', '00', 'Otros Activos Reales', NULL);
INSERT INTO `part_presup` VALUES (84, NULL, NULL, NULL, NULL, '4.03', '06', '03', '00', 'Estacionamiento', NULL);
INSERT INTO `part_presup` VALUES (85, '', '', '', '', '4.02', '10', '01', '00', 'Artículos de Deporte, Recreación y Juguetes', '');
INSERT INTO `part_presup` VALUES (86, NULL, NULL, NULL, NULL, '4.02', '06', '08', '00', 'Productos plásticos', NULL);
INSERT INTO `part_presup` VALUES (87, NULL, NULL, NULL, NULL, '4.02', '08', '03', '00', 'Herramientas menores, cuchillería y artículos generales de ferretería', NULL);
INSERT INTO `part_presup` VALUES (88, '', '', '', '', '4.01', '07', '08', '00', 'Aporte Patronal a los Servicios de salud, accidentes personales, hospitalización, cirugía, Materinidad y Gastos Funerarios', '');
INSERT INTO `part_presup` VALUES (90, '', '', '', '', '4.01', '07', '05', '00', 'Ayudas por defunción a empleados', '');
INSERT INTO `part_presup` VALUES (91, '', '', '', '', '4.01', '07', '04', '00', 'Ayudas por nacimiento de hijos a empleados', '');
INSERT INTO `part_presup` VALUES (92, '', '', '', '', '4.03', '10', '99', '00', 'Otros servicios profesionales y técnicos', '');
INSERT INTO `part_presup` VALUES (93, '', '', '', '', '4.02', '10', '03', '00', 'Utensilios de cocina y comedor', '');
INSERT INTO `part_presup` VALUES (94, '', '', '', '', '4.02', '10', '12', '00', 'Materiales para instalaciones sanitarias', '');
INSERT INTO `part_presup` VALUES (95, '', '', '', '', '4.04', '03', '04', '00', 'Maquinaria y equipos de artes gráficas y reproducción', '');
INSERT INTO `part_presup` VALUES (96, '', '', '', '', '4.04', '07', '03', '00', 'Obras de arte', '');
INSERT INTO `part_presup` VALUES (97, '', '', '', '', '4.04', '11', '02', '00', 'Adquisición de edificaciones e instalaciones', '');
INSERT INTO `part_presup` VALUES (98, '', '', '', '', '4.01', '07', '96', '00', 'Otras subvenciones a empleados', '');
INSERT INTO `part_presup` VALUES (99, '', '', '', '', '4.01', '07', '97', '00', 'Otras subvenciones a obreros', '');
INSERT INTO `part_presup` VALUES (100, '', '', '', '', '4.01', '01', '10', '00', 'Salarios a obreros en puestos permanentes a tiempo completo', '');
INSERT INTO `part_presup` VALUES (101, '', '', '', '', '4.01', '01', '36', '00', 'Sueldo básico del personal de alto nivel y de dirección', '');
INSERT INTO `part_presup` VALUES (102, '', '', '', '', '4.01', '03', '18', '00', 'Primas por hogar a Obreros', '');
INSERT INTO `part_presup` VALUES (103, '', '', '', '', '4.01', '04', '18', '00', 'Bono compensatorio de Alimentación a obreros', '');
INSERT INTO `part_presup` VALUES (104, '', '', '', '', '4.01', '04', '51', '00', 'Bono compensatorio de Alimentación al personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (105, '', '', '', '', '4.01', '05', '04', '00', 'Aguinaldos a Obreros', '');
INSERT INTO `part_presup` VALUES (106, '', '', '', '', '4.01', '05', '06', '00', 'Bono Vacacional a Obreros', '');
INSERT INTO `part_presup` VALUES (107, '', '', '', '', '4.01', '05', '16', '00', 'Aguinaldos al Personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (108, '', '', '', '', '4.01', '05', '18', '00', 'Bono Vacacional al Personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (109, '', '', '', '', '4.01', '06', '10', '00', 'Aporte patronal al I.V.S.S. por Obreros', '');
INSERT INTO `part_presup` VALUES (110, '', '', '', '', '4.01', '06', '12', '00', 'Aporte Patronal al Fondo Seg. de Paro Fzoso Obreros', '');
INSERT INTO `part_presup` VALUES (111, '', '', '', '', '4.01', '06', '13', '00', 'Aporte Patronal Fondo de Ahorro Hab Obreros', '');
INSERT INTO `part_presup` VALUES (112, '', '', '', '', '4.01', '06', '25', '00', 'Aporte legal al I.V.S.S. por personal contratado', '');
INSERT INTO `part_presup` VALUES (113, '', '', '', '', '4.01', '06', '26', '00', 'Aporte legal al Fondo de Ahorro Hab por personal contratado', '');
INSERT INTO `part_presup` VALUES (114, '', '', '', '', '4.01', '06', '39', '00', 'Aporte patronal al I.V.S.S. por personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (115, '', '', '', '', '4.01', '06', '42', '00', 'Aporte patronal al Fondo de Ahorro Hab por personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (116, '', '', '', '', '4.01', '07', '18', '00', 'Becas a Obreros', '');
INSERT INTO `part_presup` VALUES (117, '', '', '', '', '4.01', '07', '22', '00', 'Ayud. p/ medicin. Gtos Med. Odont y Hospit a obreros', '');
INSERT INTO `part_presup` VALUES (118, '', '', '', '', '4.01', '07', '23', '00', 'Aporte Patronal a Cajas de Ahorro por Obreros', '');
INSERT INTO `part_presup` VALUES (119, '', '', '', '', '4.01', '07', '26', '00', 'Dotación de uniformes a obreros', '');
INSERT INTO `part_presup` VALUES (120, '', '', '', '', '4.01', '07', '67', '00', 'Ayud. p/ medicin. Gtos Med. Odont y Hospit a personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (121, '', '', '', '', '4.01', '07', '68', '00', 'Aporte Patronal a Cajas de Ahorro por personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (122, '', '', '', '', '4.01', '07', '69', '00', 'Aporte Patronal a los servicios de salud, accidentes personales y gastos funerarios por Personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (123, '', '', '', '', '4.01', '07', '95', '00', 'Otras subvenciones al personal de Alto Nivel y de Dirección', '');
INSERT INTO `part_presup` VALUES (125, '', '', '', '', '4.01', '08', '02', '00', 'Prestaciones Sociales e Indemnizaciones a Obreros', '');
INSERT INTO `part_presup` VALUES (126, '', '', '', '', '4.01', '08', '07', '00', 'Prestaciones Sociales e Indemnizaciones a personal de Alto Nivel y de Direccióno', '');
INSERT INTO `part_presup` VALUES (127, '', '', '', '', '4.02', '06', '04', '00', 'Productos farmacéuticos y medicamentos', '');
INSERT INTO `part_presup` VALUES (128, '', '', '', '', '4.01', '07', '33', '00', 'Asistencia socio-económica al pers. contratado ', '');
INSERT INTO `part_presup` VALUES (129, '', '', '', '', '4.01', '03', '38', '00', 'Primas por hogar al personal contratado', '');
INSERT INTO `part_presup` VALUES (130, '', '', '', '', '4.01', '03', '39', '00', 'Primas por hijos al personal contratado', '');
INSERT INTO `part_presup` VALUES (131, '', '', '', '', '4.01', '03', '40', '00', 'Primas por profesionalizacion al personal contratado', '');
INSERT INTO `part_presup` VALUES (132, '', '', '', '', '4.01', '03', '41', '00', 'Primas por antiguedad al personal contratado', '');
INSERT INTO `part_presup` VALUES (133, '', '', '', '', '4.01', '01', '99', '00', 'Otras retribuciones', '');
INSERT INTO `part_presup` VALUES (134, '', '', '', '', '4.01', '03', '21', '00', 'Prima por antiguedad a obreros', '');
INSERT INTO `part_presup` VALUES (135, '', '', '', '', '4.01', '07', '01', '00', 'Capacitación y Adiestramiento a empleados', 'Creada segu');
INSERT INTO `part_presup` VALUES (136, '', '', '', '', '4.01', '07', '24', '00', 'Aporte patronal a los servicios de salud, accidentes personales y gastos funerarios por obreros', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `part_presup_mov`
-- 

CREATE TABLE `part_presup_mov` (
  `cod_par_mov` int(11) NOT NULL auto_increment,
  `tip_par_mov` varchar(15) NOT NULL,
  `fch_par_mov` date NOT NULL,
  `ano_par_mov` int(4) NOT NULL,
  `con_par_mov` longtext NOT NULL,
  `mon_par_mov` double(9,2) NOT NULL,
  PRIMARY KEY  (`cod_par_mov`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `part_presup_mov`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `partidas_compras`
-- 

CREATE TABLE `partidas_compras` (
  `cod_pro_com` int(11) NOT NULL auto_increment,
  `cod_com` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_com` double(9,2) NOT NULL,
  PRIMARY KEY  (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `partidas_compras`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `partidas_pagos`
-- 

CREATE TABLE `partidas_pagos` (
  `cod_pro_pag` int(11) NOT NULL auto_increment,
  `cod_pag` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `isrl_pro_pag` varchar(2) default NULL,
  `mon_pro_pag` double(9,2) NOT NULL,
  PRIMARY KEY  (`cod_pro_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `partidas_pagos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `permisos`
-- 

CREATE TABLE `permisos` (
  `ing_prm` varchar(1) NOT NULL COMMENT 'permisos para ingresar',
  `mod_prm` varchar(1) NOT NULL COMMENT 'permisos para modificar',
  `con_prm` varchar(1) NOT NULL COMMENT 'permisos para consultar',
  `eli_prm` varchar(1) NOT NULL COMMENT 'permisos para eliminar',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  `cod_sec` int(2) NOT NULL COMMENT 'codigo de la seccion',
  KEY `cod_grp` (`cod_grp`),
  KEY `cod_sec` (`cod_sec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permisologia para los Usuarios del Sistema';

-- 
-- Volcar la base de datos para la tabla `permisos`
-- 

INSERT INTO `permisos` VALUES ('', '', '', '', 5, 11);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 2);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 8);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 3);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 7);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 6);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 1);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 14);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 15);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 13);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 12);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 5);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 16);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 10);
INSERT INTO `permisos` VALUES ('', '', '', '', 5, 9);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 5, 17);
INSERT INTO `permisos` VALUES ('', 'A', 'A', 'A', 1, 18);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 1, 19);
INSERT INTO `permisos` VALUES ('', '', '', '', 1, 11);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 2);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 8);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 3);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 7);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 6);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 1);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 14);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 1, 20);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 15);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 13);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 12);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 1, 5);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 16);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 10);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 1, 9);
INSERT INTO `permisos` VALUES ('', '', '', '', 1, 17);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 18);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 19);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 11);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 2);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 8);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 3);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 7);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 6);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 1);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 14);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 23);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 20);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 21);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 22);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 15);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 13);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 12);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 5);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 16);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 10);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 9);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 17);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 4, 24);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 25);
INSERT INTO `permisos` VALUES ('', '', '', '', 4, 26);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 18);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 19);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 11);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 2);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 8);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 3);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 7);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 6);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 6, 1);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 14);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 23);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 6, 20);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 6, 21);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 6, 22);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 15);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 6, 13);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 12);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 6, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 5);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 16);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 10);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 6, 9);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 6, 27);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 17);
INSERT INTO `permisos` VALUES ('', '', '', '', 6, 24);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 6, 25);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 6, 26);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 18);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 7, 19);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 11);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 2);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 8);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 3);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 7);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 6);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', '', 7, 1);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 14);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 23);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 7, 20);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 7, 21);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 7, 22);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 15);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 7, 13);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 12);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 7, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 5);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', '', 7, 16);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', '', 7, 10);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 9);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 27);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 17);
INSERT INTO `permisos` VALUES ('', '', '', '', 7, 24);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 7, 25);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 7, 26);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 18);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 19);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 11);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 2);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 8);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 3);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 7);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 6);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 3, 1);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 14);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 23);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 3, 20);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 3, 21);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 3, 22);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 15);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 3, 13);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 12);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 3, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 5);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 16);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 10);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 3, 9);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 27);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 17);
INSERT INTO `permisos` VALUES ('', '', '', '', 3, 24);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 3, 25);
INSERT INTO `permisos` VALUES ('A', '', 'A', '', 3, 26);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 18);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 28);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 19);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 11);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 2);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 8);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 3);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 7);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 6);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 1);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 14);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 23);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 20);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 21);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 22);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 15);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 13);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 12);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 4);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 5);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 16);
INSERT INTO `permisos` VALUES ('A', 'A', 'A', 'A', 2, 10);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 9);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 27);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 24);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 17);
INSERT INTO `permisos` VALUES ('', '', 'A', '', 2, 25);
INSERT INTO `permisos` VALUES ('', '', '', '', 2, 26);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `permisos_per`
-- 

CREATE TABLE `permisos_per` (
  `cod_sol_perm` int(11) NOT NULL auto_increment,
  `fch_sol_perm` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `dias_sol_perm` int(2) NOT NULL,
  `ini_sol_perm` date NOT NULL,
  `fin_sol_perm` date NOT NULL,
  `tip_sol_perm` varchar(20) NOT NULL,
  `obs_sol_perm` longtext NOT NULL,
  `mot_sol_perm` varchar(25) NOT NULL,
  `apro_sol_perm` varchar(2) NOT NULL,
  PRIMARY KEY  (`cod_sol_perm`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `permisos_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `personal`
-- 

CREATE TABLE `personal` (
  `ced_per` varchar(12) NOT NULL COMMENT 'cedula de la persona',
  `nac_per` varchar(1) NOT NULL COMMENT 'nacionalidad de la persona',
  `nom_per` varchar(50) NOT NULL COMMENT 'nombre de la persona',
  `ape_per` varchar(50) NOT NULL COMMENT 'apellido de la persona',
  `sex_per` varchar(1) NOT NULL COMMENT 'sexo de la persona',
  `fnac_per` date NOT NULL COMMENT 'fecha de nacimiento de la persona',
  `lnac_per` varchar(50) NOT NULL COMMENT 'lugar de nacimiento de la persona',
  `cor_per` varchar(50) NOT NULL COMMENT 'correo de la persona',
  `pro_per` varchar(50) NOT NULL COMMENT 'profesion de la persona',
  `abr_per` varchar(10) NOT NULL COMMENT 'Abreviatura de Profesión',
  `dir_per` varchar(255) NOT NULL COMMENT 'direccion de la persona',
  `tel_per` varchar(12) NOT NULL COMMENT 'telefono de la persona',
  `cel_per` varchar(12) NOT NULL COMMENT 'celular de la persona',
  `lph_des` varchar(1) NOT NULL COMMENT 'Descuento de ley politica habitacional',
  `spf_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro de paro forzoso',
  `sso_des` varchar(1) NOT NULL COMMENT 'Descuento de seguro social obligatorio',
  `cah_des` varchar(1) NOT NULL COMMENT 'Descuento de caja de ahorro',
  `sfu_des` varchar(1) character set latin1 collate latin1_spanish_ci NOT NULL COMMENT 'Servicio funerario',
  `hog_asg` varchar(1) NOT NULL COMMENT 'Prima por hogar',
  `hij_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos',
  `ant_asg` varchar(1) NOT NULL COMMENT 'Prima por antiguedad',
  `pro_asg` varchar(1) NOT NULL COMMENT 'Prima por profesionalizaci�n',
  `hje_asg` varchar(1) NOT NULL COMMENT 'Prima por hijos excepcionales',
  `jer_asg` varchar(1) NOT NULL COMMENT 'Prima por Jerarquia',
  `otr_asg` varchar(1) NOT NULL COMMENT 'Prima por Postgrado',
  `fch_reg` date NOT NULL COMMENT 'Fecha de registro en el personal',
  PRIMARY KEY  (`ced_per`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `personal`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `productos_compras`
-- 

CREATE TABLE `productos_compras` (
  `cod_pro_com` int(11) NOT NULL auto_increment,
  `cod_com` int(11) NOT NULL,
  `cnt_pro_com` int(4) NOT NULL,
  `uni_pro_com` int(4) NOT NULL,
  `con_pro_com` varchar(255) NOT NULL,
  `pun_pro_com` double(9,2) NOT NULL,
  `exc_pro_com` varchar(2) default NULL,
  KEY `id` (`cod_pro_com`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `productos_compras`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `productos_part_mov`
-- 

CREATE TABLE `productos_part_mov` (
  `cod_pro_par_mov` int(11) NOT NULL auto_increment,
  `cod_par_mov` int(11) NOT NULL,
  `cod_par` int(11) NOT NULL,
  `mon_pro_par_mov` double(9,2) NOT NULL,
  `tip_pro_par_mov` varchar(10) NOT NULL default 'Ingreso',
  PRIMARY KEY  (`cod_pro_par_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='partidas que pertenecen a un movimiento presupuestario' AUTO_INCREMENT=11 ;

-- 
-- Volcar la base de datos para la tabla `productos_part_mov`
-- 

INSERT INTO `productos_part_mov` VALUES (1, 1, 1, 1000.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (2, 1, 5, 2000.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (3, 1, 6, 3000.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (4, 2, 1, 3000.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (5, 3, 1, 500.00, 'Egreso');
INSERT INTO `productos_part_mov` VALUES (6, 3, 5, 500.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (7, 4, 72, 2000.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (8, 4, 80, 3000.00, 'Ingreso');
INSERT INTO `productos_part_mov` VALUES (9, 5, 1, 100.00, 'Reintegro');
INSERT INTO `productos_part_mov` VALUES (10, 6, 30, 2000.00, 'Ingreso');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `prog_mov_pagos`
-- 

CREATE TABLE `prog_mov_pagos` (
  `cod_pag` int(11) NOT NULL auto_increment,
  `ced_per` varchar(12) NOT NULL,
  `mon_pag` double(9,2) NOT NULL,
  `con_pag` varchar(150) NOT NULL,
  `fch_pag` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `cod_dep` varchar(5) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_vac` date NOT NULL,
  `cod_tcar` int(11) NOT NULL,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) NOT NULL,
  PRIMARY KEY  (`cod_pag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `prog_mov_pagos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `prog_movimientos`
-- 

CREATE TABLE `prog_movimientos` (
  `cod_mov` int(11) NOT NULL auto_increment,
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `accion` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fch_asg` date NOT NULL,
  `des_car` varchar(1) NOT NULL,
  `fch_vac` date NOT NULL,
  PRIMARY KEY  (`cod_mov`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `prog_movimientos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `programas`
-- 

CREATE TABLE `programas` (
  `cod_pro` varchar(5) NOT NULL COMMENT 'codigo del Programa',
  `nom_pro` varchar(150) NOT NULL COMMENT 'nombre del Programa',
  PRIMARY KEY  (`cod_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `programas`
-- 

INSERT INTO `programas` VALUES ('1', 'PROGRAMA 01');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `proveedores`
-- 

CREATE TABLE `proveedores` (
  `cod_pro` int(4) unsigned zerofill NOT NULL auto_increment,
  `fch_pro` date NOT NULL,
  `rif_pro` varchar(10) NOT NULL,
  `nom_pro` varchar(50) NOT NULL,
  `dir_pro` varchar(255) NOT NULL,
  `act_pro` varchar(50) NOT NULL,
  `cap_pro` double(9,2) NOT NULL,
  `cas_pro` double(9,2) NOT NULL,
  `tel_pro` varchar(11) NOT NULL,
  `fax_pro` varchar(11) NOT NULL,
  `per_pro` varchar(1) NOT NULL,
  `ofi_reg_pro` varchar(50) NOT NULL,
  `num_reg_pro` int(5) NOT NULL,
  `tom_reg_pro` varchar(25) NOT NULL,
  `fol_reg_pro` varchar(4) NOT NULL,
  `fch_reg_pro` date NOT NULL,
  `num_mod_pro` int(5) NOT NULL,
  `tom_mod_pro` varchar(25) NOT NULL,
  `fol_mod_pro` varchar(4) NOT NULL,
  `fch_mod_pro` date NOT NULL,
  `pat_pro` varchar(25) NOT NULL,
  `ivss_pro` varchar(25) NOT NULL,
  `ince_pro` varchar(25) NOT NULL,
  `nom_rep_pro` varchar(50) NOT NULL,
  `ape_rep_pro` varchar(50) NOT NULL,
  `ced_rep_pro` varchar(8) NOT NULL,
  `dir_rep_pro` varchar(255) NOT NULL,
  `tel_rep_pro` varchar(11) NOT NULL,
  `car_rep_pro` varchar(50) NOT NULL,
  PRIMARY KEY  (`cod_pro`),
  UNIQUE KEY `rif_pro` (`rif_pro`),
  UNIQUE KEY `nom_pro` (`nom_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los Proveedores' AUTO_INCREMENT=104 ;

-- 
-- Volcar la base de datos para la tabla `proveedores`
-- 

INSERT INTO `proveedores` VALUES (0001, '2011-01-01', 'J298994940', 'Asociacion Cooperativa Manely', 'AV. Principal casa N°1-1Sector Zumba', 'comerciante', 2500.00, 2500.00, '02742710374', '', 'J', 'Registro Mercantil Primero del edo Merida', 29, 'Noveno', '241', '2010-04-28', 0, '', '', '0000-00-00', '', '01931016', '', 'Henry de Jesus', 'Angulo', '08019717', 'Sector Zumba Casa N°1-1 AV Principal', '04161770759', 'Presidente');
INSERT INTO `proveedores` VALUES (0002, '2011-01-01', 'J294469752', 'Asociacion cooperativa coofiandina', 'Calle 16 Entre Av 5y6 N°5 - 72', 'Compra y venta de reparacion de equipos de oficina', 50.00, 50.00, '02742528046', '02742528046', 'J', 'Registro Mercantil Primero del edo Merida', 41, '26', '305', '2007-05-15', 0, '', '', '0000-00-00', '', '', '', 'Hugo Ali ', 'Sulbaran ', '08020874', 'calle 16 entre Av 5y6 N°572', '04147455104', 'Cordinador General ');
INSERT INTO `proveedores` VALUES (0003, '2011-01-01', 'J296861234', 'Supermercado Plaza Montalban C.A.', 'Av Bolivar local N°196 -17 sector montalban', 'comerciante', 300.00, 300.00, '02742214682', '', 'J', 'Registro Mercantil Primero del edo Merida', 379, '16', '16', '2008-10-23', 379, '16', '16', '0000-00-00', '', '', '', 'Yhon Isaac', 'Areas Barrios', '08025049', 'Av Bolivar Sector Montalban', '02742214682', 'GERENTE PERSONAL');
INSERT INTO `proveedores` VALUES (0004, '2011-01-01', 'J300180174', 'Sodexho Pass ', 'con Av los chaguaramos torre corp banca piso 16 la castellar ', 'Emision, comercializacion', 2250000.00, 2250000.00, '02122065625', '02122065620', 'J', 'Registro mercantil segundo  del esdo miranda', 75, '154-A', '5', '2007-07-07', 75, '154-A', '5', '0000-00-00', '', '', '', 'Nicolas ', 'Vaga', '06972690', '', '', 'Gerente General');
INSERT INTO `proveedores` VALUES (0005, '2011-01-01', 'V123536335', 'Distribuidora Alexwend', 'Av Bolivar N°231 local 02 ejido', 'Compra y venta de papeleria ', 6000.00, 6000.00, '02742210571', '', 'J', 'registro mercantil primero del estado merida ', 91, '5', '5', '2007-08-16', 91, '5', '5', '0000-00-00', '', '', '', 'Amilcar Alexander ', 'Nieto Contreras ', '12353633', 'Av Bolivar N°231 Local de ejido', '04165222227', 'propietario');
INSERT INTO `proveedores` VALUES (0006, '2011-01-01', 'J298778377', 'Asociacion cooperativa ofimatica', 'oficina matriz Av 25 de noviembre urbanizacion el trapiche bloque 4 edif 1 apto 3-3', 'Comerciante', 0.00, 0.00, '02744174697', '', 'J', 'Registro Publico del Municipio Campo Elias', 21, '2', '132', '2010-05-26', 0, '', '', '0000-00-00', '', '', '', 'Jose Felix', 'Rivas Gomez', '13524339', 'URB El trapiche bloque 4 edf 1 pisos 3', '04247577644', 'gerente');
INSERT INTO `proveedores` VALUES (0007, '2011-01-01', 'V037653213', 'Prove Oficina Sosa', 'Av Don tulio febres cordero entre calle 29y30merida ', 'comerciante', 10.00, 10.00, '02742529551', '', 'J', 'Megistro mercantil primero del estado merida', 64, '5', '5', '2007-06-26', 64, '5', '5', '0000-00-00', '', '', '', 'Alexis', 'Sosa', '03765321', 'Av don tulio febres cordero entre calle 29y30 merida', '04147742018', 'representante legal');
INSERT INTO `proveedores` VALUES (0008, '2011-01-01', 'V080232108', 'Jose Andres Briceño', 'urb. JJ osuna Rodriguez Vereda 17 N°07 Merida', 'contralor', 0.00, 0.00, '02742714008', '', 'P', '', 0, '0', '0', '2011-01-01', 0, '', '', '0000-00-00', '', '', '', 'Jose Andres', 'Briceño', '08023210', 'urb jj Osuna rodriguez', '04162749656', 'contralor');
INSERT INTO `proveedores` VALUES (0009, '2011-01-01', 'V080297358', 'Nora Cerrada', 'EL Manzano Alto Via Jaji Panamericana', 'Secretaria', 2.00, 1.00, '04267032172', '', 'P', 'S/N', 0, '0', '009', '2011-01-01', 4, 'S/N', '0001', '2011-01-01', '0', '0', '0', 'Nora Coromoto', 'Cerrada', '08029735', 'El Manzano Alto via jaji panamericana', '04167032172', 'Secretaria');
INSERT INTO `proveedores` VALUES (0010, '2011-01-01', 'G200100141', 'Corporación Eléctrica Nacional S.A.', 'Cento Electronico Nacional ', 'comerciante', 0.00, 0.00, '02432722711', '', 'J', 'N/R', 0, '0009', '0009', '2011-01-01', 0, '0009', '0009', '2011-01-01', '0', '0', '0', 'S/R', 'S/R', '00000000', '', '', 'S/R');
INSERT INTO `proveedores` VALUES (0011, '2011-01-01', 'J090370196', 'Sovenpfa C.A', 'Av 4 calle 21edif Don Atilio Nivel Mezanina', 'Comerciante', 0.00, 0.00, '02742510444', '', 'J', 'N/R', 1, '0000005', '980', '2011-01-01', 1, '0000005', '980', '2011-01-01', '', '', '', 'S/R', 'N/R', '88888888', 'Av 4 con Calle 21 edif Don Atilio Nivel Mezanina', '02742510444', 'N/R');
INSERT INTO `proveedores` VALUES (0012, '2011-01-01', 'J001241345', 'CANTV', 'Final Av libertador edif CNT Caracas', 'Cervicio Telefonoco', 1.00, 1.00, '02125001111', '', 'J', 'S/N', 8, '0000005', '5436', '2011-01-01', 789, 'S/N', '070', '2011-01-01', '0', '0', '0', 'CANTV', 'J', '01241345', 'Final Av Libertedor edif CNT Caracas', '02125001111', 'Propietario');
INSERT INTO `proveedores` VALUES (0013, '2011-01-01', 'V127803591', 'Multiservicios del centro de Ramón Ruiz ', 'Av Bolivar Casa N° 162 Ejido edo Merida', 'Puesto del Estacionamiento', 1.00, 1.00, '02744168266', '', 'J', 'S/N', 999, '0', '465', '2011-01-01', 999, '0', '465', '2011-01-01', '0', '0', '0', 'Ramon A ', 'Ruiz Fernandez', '12780359', 'Av Bolivar Casa N°162 Ejido edo Merida', '02744168266', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0014, '2011-01-01', 'J000389233', 'Seguros Caracas de Liberty Mutual', 'Av francisco de Miranda CC el Parque Torre Seguros Caracas Nivel C-4 los Palos Grandes Caracas ', 'Seguros', 1.00, 1.00, '02122099556', '', 'J', 'S/N', 0, '0', '0', '2011-01-01', 0, '0', '0', '2011-01-01', '0', '0', '0', 'Seguros ', 'Caracas', '00389233', 'Av Francisco de Miranda CC el ParqueTorre Seguros Caracas Nivel c -4 Con Palos Grandes Caracas', '02122099556', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0015, '2011-01-01', 'J000268401', 'Seguros Nuevo Mundo', 'Av luis roche con carrera trasversal edif nuevo mundo Sector Altamira', 'Seguros Nuevo Mundo', 1.00, 1.00, '02122011111', '02122011157', 'J', 'S/N', 8, '0', '0', '2011-01-01', 8, 'S/N', '0', '2011-01-01', '0', '0', '0', 'Seguros', 'nuevo mundo', '00026840', 'Av Luis Rochec/3 Torre Nuevo Mundo Sector Altamira', '02122011111', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0016, '2011-01-01', 'J070017376', 'Seguros los Andes', 'Av las Pilas, urb . Santa Ines edif Seguros los Andes', 'Seguros lo Andes', 1.00, 1.00, '02763402611', '', 'J', 'S/N', 9, '0', '0', '2011-01-01', 8, 'S/N', '008', '2011-01-01', '0', '0', '0', 'Seguros', ' Andes', '70017376', 'Av las Pilas urb Santa Ines edif Seguros los Andes', '02763402611', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0017, '2011-01-01', 'J311801596', 'Condominio  Centro Comercial Centenario ', 'Av Centenario. Centro Comercial Centenrio Nivel PR Oficina Administrador', 'Condomineo', 1.00, 1.00, '02744158866', '', 'J', 'S/N', 6, '0', '0', '2011-01-01', 6, '0', '0', '2011-01-01', '0', '0', '0', 'condomineo', 'centenario', '31180159', 'Av centenario . centro comercial ', '02744158866', 'administrador');
INSERT INTO `proveedores` VALUES (0018, '2011-01-01', 'J305877432', 'Capreamce', 'Ejido edo Merida', 'Caja De Ahorro', 1.00, 1.00, '02742214982', '', 'J', 'S/N', 9, '0', '0', '2011-01-01', 9, 'S/N', '0006', '2011-01-01', '0', '0', '0', 'Adonaida ', 'peña ', '11460606', 'ejido edo merida', '02742214982', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0019, '2011-01-01', 'G200061944', 'Aguas de Ejido', 'Av Centenario .Nucleo Norte ,Nivel 1 Local 62', 'Servicio de la Comunidad', 1.00, 1.00, '02742215774', '', 'J', 'S/N', 8, '0', '0', '2011-01-01', 8, 'S/N', '007', '2011-01-01', '0', '0', '0', 'AGUAS', 'EJIDO', '20006194', 'Av centenario . centro comercial centenario nucleo norte nivel 1 local 62', '02742215774', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0020, '2011-01-01', 'J299956066', 'Asociacion Cooperativa  Andimed R.L.', 'Edif el Ramiral Piso Planta Baja Sagrado Calle 26', 'Comercio, Compras, Ventas, Y Exportacion', 1.00, 1.00, '02742526096', '', 'J', 'publico d el municipio libertedor estado merida', 38, '22', '222', '2010-10-21', 38, '22', '222', '2010-10-21', '', '', '', 'Aurimar Aurora ', 'Angulo Escalante', '10717555', 'edif el Ramiral Piso Planta Baja Sagrado calle 26', '04166740194', 'Representante Legal');
INSERT INTO `proveedores` VALUES (0021, '2011-01-03', 'G200000031', 'Tesoro Nacional', 'Caracas', 'Gubernamental', 0.00, 0.00, '02120000000', '', 'J', '0', 0, '', '', '2011-01-03', 0, '', '', '0000-00-00', '', '', '', 'Tesoro Nacional', 'Tesoro Nacional', '00000000', '', '', 'Tesoro Nacional');
INSERT INTO `proveedores` VALUES (0022, '2011-01-03', 'G200019310', 'CONTRALORIA MUNICIPAL CAMPO ELIAS', 'AV. CENTENARIO C.C. CENTENARIO LOCAL Nº 54 ', 'Gubernamental', 0.00, 0.00, '02742214982', '', 'J', '', 0, '', '', '2009-12-08', 0, '', '', '0000-00-00', '', '', '', 'ABOG. JOSE ANDRES ', 'BRICEÑO VALERO', '08023210', 'URB. J.J OSUNA', '04265709418', 'CONTRALOR MUNICIPAL');
INSERT INTO `proveedores` VALUES (0023, '2011-08-12', 'J303529852', 'Distribuidora y Receptora VILMA C.A.', 'Av. 7 entre alles 22 y 23 Nº 22-49 Mérida', 'Publicitaria', 0.00, 0.00, '02742525632', '02742522975', 'J', '', 0, '', '', '2011-08-12', 0, '', '', '0000-00-00', '', '', '', 'Libia ', 'Dugarte', '00000000', '', '', 'Propietaria');
INSERT INTO `proveedores` VALUES (0024, '2011-08-01', 'V031426460', 'José Gilberto Uzcátegui Muchacho', 'Calle 22 Edif. Edipla Piso 1 Of 1-1 Mérida', 'Contable', 0.00, 0.00, '02742528294', '', 'P', '', 0, '', '', '2011-08-01', 0, '', '', '0000-00-00', '', '', '', 'José Gilberto', 'Uzcategui Muchacho', '03142646', '', '', 'Contador');
INSERT INTO `proveedores` VALUES (0025, '2011-08-01', 'G200040769', 'Instituto Venezolano de los Seguros Sociales', 'Caracas', 'Gubernamental', 0.00, 0.00, '02120000000', '', 'J', '', 0, '', '', '2011-08-01', 0, '', '', '0000-00-00', '', '', '', 'José ', 'Pérez', '00000000', '', '', 'Gerente');
INSERT INTO `proveedores` VALUES (0026, '2011-08-01', 'G200000856', 'BANAVIH FAOV EN LINEA', 'Av. Venezuela Torre Banavih. El Rosal. Caracas', 'Gubernamental', 0.00, 0.00, '02129571581', '', 'J', '', 0, '', '', '2011-08-01', 0, '', '', '2011-08-01', '', '', '', 'Luis', 'Garcia', '00000000', '', '', 'DIRECTOR');
INSERT INTO `proveedores` VALUES (0027, '2011-08-01', 'V143046571', 'Leidy  Pereira Ochoa', 'Av. Centenario Residencia El Molino Torre 10 Apto 1-4', 'Funcionario Público', 0.00, 0.00, '04147458942', '', 'P', '', 0, '', '', '2011-08-01', 0, '', '', '2011-08-01', '', '', '', 'Leidy', 'Pereira', '14304657', '', '', 'Funcionario Publico');
INSERT INTO `proveedores` VALUES (0028, '2011-08-01', 'V119605977', 'Jesús Silva', 'Calle Camejo Res. El Trigal Edif D. Apto 2-3', 'Funcionario Público', 0.00, 0.00, '04149785308', '', 'P', '', 0, '', '', '2011-08-01', 0, '', '', '2011-08-01', '', '', '', 'Jesús', 'Silva', '11960597', '', '', 'Funcionario Publico');
INSERT INTO `proveedores` VALUES (0029, '2011-08-01', 'J000029610', 'Banco Mercantil', 'Caracas', 'Bancaria', 0.00, 0.00, '02120000000', '', 'J', '', 0, '', '', '2011-07-07', 0, '', '', '0000-00-00', '', '', '', 'Juan ', 'Perez', '00000000', '', '', 'Gerente General');
INSERT INTO `proveedores` VALUES (0030, '2011-07-05', 'J316785670', 'Tassei Motors C.A.', 'Av. Andrés Bello Nº 1-248 sector Zumba', 'Servicio', 0.00, 0.00, '02742711684', '', 'J', '', 0, '', '', '2011-04-06', 0, '', '', '0000-00-00', '', '', '', 'José', 'Carrillo', '00000000', '', '02742716857', 'Gerente');
INSERT INTO `proveedores` VALUES (0031, '2011-09-07', 'V080469728', 'Licoreria Montalbán', 'Av. Bolivar casa nº 161 sector Montalban Ejido', 'Comerciante', 0.00, 0.00, '02742210105', '', 'P', '', 0, '', '', '2011-02-10', 0, '', '', '0000-00-00', '', '', '', 'Ivan Daniel', 'Hernandez Torres', '08046972', 'Ejido', '2210105', 'Dueño');
INSERT INTO `proveedores` VALUES (0032, '2011-09-12', 'V159233258', 'El sazón de Sioly', 'Av. Gonzalo Picón C.C. Mercado Periferico', 'Comerciante', 0.00, 0.00, '04160926189', '', 'P', '', 0, '', '', '2011-06-07', 0, '', '', '0000-00-00', '', '', '', 'Sioly', 'Villazmil', '15923325', '', '04160926189', 'Dueña');
INSERT INTO `proveedores` VALUES (0033, '2011-04-06', 'J301652665', 'E/S CENTENARIO C.A.', 'Ejido', 'Estación de servicio', 0.00, 0.00, '02742210000', '', 'J', '', 0, '', '', '2011-04-08', 0, '', '', '0000-00-00', '', '', '', 'Julio', 'Rojas', '00000000', '', '2210000', 'Dueño');
INSERT INTO `proveedores` VALUES (0034, '2011-03-10', 'V172394201', 'Floristería Vidiana', 'Av. Fernandez Peña Ejido', 'Comerciante', 0.00, 0.00, '02742210000', '', 'P', '', 0, '', '', '2011-05-13', 0, '', '', '0000-00-00', '', '', '', 'Jesús María', 'Rojas Duque', '17239420', '', '', 'Dueño');
INSERT INTO `proveedores` VALUES (0035, '2011-04-06', 'J090000666', 'Automotores Ciro C.A.', 'Av. Andres Bello', 'Servicio', 0.00, 0.00, '02742661621', '', 'J', '', 0, '', '', '2011-05-11', 0, '', '', '0000-00-00', '', '', '', 'Jesus', 'Lobo', '00000000', '', '2661621', 'Dueño');
INSERT INTO `proveedores` VALUES (0036, '2011-09-26', 'J313614882', 'AVELIDERES', 'MERIDA', 'ACADEMICA', 0.00, 0.00, '02742210000', '', 'J', '', 0, '', '', '2011-09-26', 0, '', '', '0000-00-00', '', '', '', 'MARCO', 'ALCANTARA', '00000000', '', '', 'Gerente');
INSERT INTO `proveedores` VALUES (0037, '2011-06-22', 'V046983730', 'Deportes Mérida', 'Av. 3 Independencia Nº 27-48 Mérida', 'Deportiva', 0.00, 0.00, '02742528430', '', 'P', '', 0, '', '', '2011-06-16', 0, '', '', '0000-00-00', '', '', '', 'Elsy', 'Chacón', '00000000', '', '', 'Dueña');
INSERT INTO `proveedores` VALUES (0038, '2011-06-17', 'J302437822', 'Licoreria Mi Encanto S.R.L.', 'EJIDO', 'Comerciante', 0.00, 0.00, '02742210000', '', 'J', '', 0, '', '', '2011-07-12', 0, '', '', '0000-00-00', '', '', '', 'DULCE', 'Perez', '00000000', '', '00000000000', 'Dueña');
INSERT INTO `proveedores` VALUES (0039, '2011-07-12', 'V017130852', 'Yrene Coromoto Guillen Andrade', 'Ejido Aguas Calientes, Sector Mesa del Tanque Casa # 3-25', 'Profesional', 0.00, 0.00, '04267039271', '', 'P', '', 0, '', '', '2011-09-29', 0, '', '', '0000-00-00', '', '', '', 'Yrene Coromoto ', 'Guillen Andrade', '17130852', '', '', 'Profesional');
INSERT INTO `proveedores` VALUES (0040, '2011-06-16', 'J308894133', 'Instituto de Desarrollo Profesional A.C.', 'Av. 7 Pasaje Simona Edif. Sultanan del Mocoties Mérida', 'ACADEMICA', 0.00, 0.00, '02742210000', '', 'J', '', 0, '', '', '2011-03-18', 0, '', '', '0000-00-00', '', '', '', 'Maria', 'Andrade', '00000000', '', '', 'Gerente');
INSERT INTO `proveedores` VALUES (0041, '2011-05-10', 'J000029482', 'Banco de Venezuela', 'Caracas', 'FINANCIERA', 0.00, 0.00, '02120000000', '', 'J', '', 0, '', '', '2011-07-13', 0, '', '', '2011-07-14', '', '', '', 'Luis ', 'Angulo', '00000000', '', '0000000000', 'GERENTE');
INSERT INTO `proveedores` VALUES (0042, '2011-06-17', 'V101089122', 'CIRCUITO AM Y FM LA VOZ DEL SUR', 'Calle ppal. casa nro 65 San Rafael de Tabay', 'Publicitaria', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2011-06-16', 0, '', '', '0000-00-00', '', '', '', 'Angel', 'Perez', '00000000', '', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0043, '2011-04-19', 'J305507414', 'MEPLOKA C.A.', 'Av. Andres Bello con 16 de septiembre E/S Pie del llano', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-08-17', 0, '', '', '0000-00-00', '', '', '', 'Laura', 'Mendez', '00000000', 'Av. Andres Bello', '04247663044', 'Dueño');
INSERT INTO `proveedores` VALUES (0044, '2011-06-17', 'J302818893', 'Frosinone C.A.', 'Calle 02 Perimetral Edif. Frosinone Piso PB Los Curos Mérida', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-07-01', 0, '', '', '2011-08-17', '', '', '', 'Ruth ', 'Lares', '00000000', '', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0045, '2011-10-10', 'V135776617', 'ALEXI ANTONIO QUINTERO GUILLEN', 'AV. CENTENARIO URB. LUIS OMAR SULBARAN CASA 18 EJIDO', 'HERRERIA', 0.00, 0.00, '02742219457', '', 'P', '', 0, '', '', '2011-07-14', 0, '', '', '0000-00-00', '', '', '', 'ALEXI', 'QUINTERO', '13577661', 'EJIDO', '02742219457', 'Dueño');
INSERT INTO `proveedores` VALUES (0046, '2011-08-25', 'J294512992', 'CEGEP', 'Calle 2 lote 4 casa nro. 4-27 Urb. Valle Hondo Primera Edo. Lara', 'Educativa', 0.00, 0.00, '02512679352', '', 'J', '', 0, '', '', '2011-07-21', 0, '', '', '0000-00-00', '', '', '', 'Rutilio', 'Mendoza', '00000000', '', '04147355288', 'Dueño');
INSERT INTO `proveedores` VALUES (0047, '2011-07-19', 'J297535160', 'Spiralia C.A.', 'Urb. Alto Chama Calle F. No. 214 Quinta Gianenuma Mérida', 'Educativa', 0.00, 0.00, '02749357747', '', 'J', '', 0, '', '', '2011-07-06', 0, '', '', '0000-00-00', '', '', '', 'Mariangela', 'Petrizzo', '00000000', 'Alto Chama', '04161773315', 'Dueño');
INSERT INTO `proveedores` VALUES (0048, '2011-10-12', 'J308194123', 'Rodamientos y Repuestos San Miguel C.A.', 'AV. CENTENARIO C. C.CENTENARIO NIVEL PLANTA BAJA LOCAL 14 EJIDO', 'Comercial', 0.00, 0.00, '02742667474', '', 'J', '', 0, '', '', '2011-07-14', 0, '', '', '0000-00-00', '', '', '', 'MARIA', 'ALVAREZ', '00000000', 'EJIDO', '02742218993', 'Dueño');
INSERT INTO `proveedores` VALUES (0049, '2011-09-15', 'V169337159', 'ALEJANDRO SAYAGO', 'EL SALADO EJIDO', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2011-09-16', 0, '', '', '0000-00-00', '', '', '', 'ALEJANDRO', 'SAYAGO', '16933715', 'EJIDO', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0050, '2011-11-01', 'V080361199', 'Elda Soraya Hill Dávila', 'Sector Chamita calle las Acacias Nº1-278 Mérida ', 'PERSONAL', 0.00, 0.00, '02742665325', '', 'P', '', 0, '', '', '2011-11-01', 0, '', '', '0000-00-00', '', '', '', 'Elda Soraya', 'Hill Dávila', '08036119', 'Sector Chamita calle las Acacias Nº1-278 Mérida ', '', 'Dueño');
INSERT INTO `proveedores` VALUES (0051, '2011-11-08', 'V139585778', 'Auto Racing Sport''s', '', '', 0.00, 0.00, '', '', '', '', 0, '', '', '0000-00-00', 0, '', '', '0000-00-00', '', '', '', '', '', '', '', '', '');
INSERT INTO `proveedores` VALUES (0052, '2011-08-17', 'J306684506', 'SEGUROS QUALITAS C.A.', 'Mérida', 'Seguros', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-09-14', 0, '', '', '2011-09-15', '', '', '', 'Nieves', 'Bruzual', '00000000', 'Merida', '0000000000', 'Corredora');
INSERT INTO `proveedores` VALUES (0053, '2011-08-10', 'J306528482', 'Carpinteria Colonial S.R.L.', 'Calle Cruz Verde, casa nro 7-6 sector La Otra Banda', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-08-19', 0, '', '', '2011-09-22', '', '', '', 'Jesus Manuel', 'Romero', '80044867', 'Mérida', '02742511347', 'Dueño');
INSERT INTO `proveedores` VALUES (0054, '2011-11-23', 'J316989135', 'ELUSI PUBLICIDAD C.A.', 'Av. Fernandez Peña C.C. El Trapiche Nivel estacionamiento Local 03 Sector Ejido', 'Publicitaria', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-09-15', 0, '', '', '2011-09-15', '', '', '', 'Luis', 'Petrizzo', '00000000', 'EJIDO', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0055, '2011-09-20', 'J308650803', 'Fundación Leopoldo Garrido', 'Colegio de Ingenieros ', 'Educativa', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-10-12', 0, '', '', '2011-10-18', '', '', '', 'Leopoldo ', 'Garrido', '00000000', '', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0056, '2011-09-22', 'J317206568', 'Corporación Urbe Andina C.A.', 'Calle Principal El Llanito La Otra Banda', 'Educativa', 0.00, 0.00, '04145420092', '', 'J', '', 0, '', '', '2011-09-21', 0, '', '', '2011-09-20', '', '', '', 'Edgar', 'Ramirez', '00000000', 'Merida', '04145420092', 'Dueño');
INSERT INTO `proveedores` VALUES (0057, '2011-11-21', 'V123460282', 'JOSE DE JESUS CORREDOR', 'Manzano Bajo', 'Inspector de Obras ', 0.00, 0.00, '02744165181', '', 'P', '', 0, '', '', '2011-11-22', 0, '', '', '2011-11-15', '', '', '', 'JOSE', 'CORREDOR', '12346028', 'Manzano', '02744165181', 'Dueño');
INSERT INTO `proveedores` VALUES (0058, '2011-12-06', 'V080132502', 'Jairo Venancio Rangel Muñoz', 'Urb. Los Curos, calle principal, bloque 14, piso 3 apto 3-4', 'Abogado', 0.00, 0.00, '04145004109', '', 'P', '', 0, '', '', '2011-12-06', 0, '', '', '0000-00-00', '', '', '', 'Jairo Venancio ', 'Rangel Muñoz', '08013250', 'Urb. Los Curos, calle principal, bloque 14, piso 3 apto 3-4', '04145004109', 'Dueño');
INSERT INTO `proveedores` VALUES (0059, '2011-12-07', 'V151740983', 'Centro Recreacional La Piscina de Ruzmary', 'Calle Principal Casa  S/N Sector La Alegría, Lagunillas, Mérida', 'Comercial', 0.00, 0.00, '02747893271', '', 'P', '', 0, '', '', '2011-12-06', 0, '', '', '0000-00-00', '', '', '', 'Ruzmary ', 'Quintero', '15174098', 'Calle Principal Casa  S/N Sector La Alegría, Lagunillas, Mérida', '04247327568', 'Propietaria');
INSERT INTO `proveedores` VALUES (0060, '2011-10-12', 'J003192350', 'MAKRO COMERCIALIZADORA S.A', 'EJIDO', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-10-12', 0, '', '', '2011-10-20', '', '', '', 'JOSE', 'Perez', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0061, '2011-09-21', 'J295332033', 'Asociacion Cooperativa  Bolívar Systems R.L.', 'Av. Prinicipal Edif. 3, Bloque 47, Piso 2, Apto. 02/03, Urb. Los Curos Mérida', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-11-17', 0, '', '', '2011-11-22', '', '', '', 'Gregorio ', 'Romero', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0062, '2011-11-22', 'J295885199', 'Smai Developers C.A.', 'Aragua', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-11-24', 0, '', '', '2011-12-13', '', '', '', 'Sixto', 'Perez', '00000000', 'Aragua', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0063, '2011-10-12', 'V042642640', 'Carlos Julio Puentes', 'EJIDO', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2011-10-05', 0, '', '', '2011-10-12', '', '', '', 'Carlos', 'Puentes', '00000000', 'EJIDO', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0064, '2011-10-12', 'V107144834', 'Miniteka Show Like de Alfredo Gutiérrez', 'Calle Principal San Félix Picón casa B-10. Urb. San Benito. Lagunillas', 'Comercial', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2011-10-14', 0, '', '', '2011-10-12', '', '', '', 'Alfredo', 'Gutierrez', '00000000', 'lagunillas', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0065, '2011-12-19', 'V063432675', 'Luz Marina Salcedo Pernia', 'EJIDO', 'Natural', 0.00, 0.00, '02742458416', '', 'P', '', 0, '', '', '2011-10-11', 0, '', '', '2011-10-12', '', '', '', 'Luz Marina', 'Salcedo', '00000000', 'EJIDO', '04266261826', 'Dueño');
INSERT INTO `proveedores` VALUES (0066, '2011-12-19', 'V150310977', 'Luis Felipe Marquez', 'EJIDO', 'Natural', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2011-10-11', 0, '', '', '2011-10-19', '', '', '', 'Luis ', 'Marquez', '00000000', 'EJIDO', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0067, '2011-12-14', 'V080302645', 'Zulay Beatriz Peña Peña', 'Calle Principal casa nro 5-40 sector Campo de Oro Mérida', 'PERSONAL', 0.00, 0.00, '04167757253', '', 'P', '', 0, '', '', '2011-12-15', 0, '', '', '0000-00-00', '', '', '', 'Zulay', 'Peña', '08030264', 'Campo de Oro', '04167757253', 'Dueño');
INSERT INTO `proveedores` VALUES (0068, '2012-01-10', 'V017896654', 'Yosmary Carolina Vergara', 'EJIDO', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-01-03', 0, '', '', '2012-01-04', '', '', '', 'Yosmary', 'Vergara', '00000000', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0069, '2012-01-10', 'V114667095', 'Producciones Nealper de Nelson Pérez', 'Calle Carabobo Casa nro 4 sector matriz Ejido', 'Publicitaria', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-01-11', 0, '', '', '2012-01-12', '', '', '', 'Nelson ', 'Perez', '11466709', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0070, '2012-02-07', 'V034951345', 'Multiservicios Uzcátegui de José Jacinto Uzcátegui', 'Calle Bermudez Nro 0-5 El Llanito La otra banda Mérida ', 'Comercial', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-02-08', 0, '', '', '2012-02-09', '', '', '', 'José', 'Uzcategui', '00000000', 'Mérida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0071, '2012-02-13', 'V054487440', 'Cerveceria y Restaurant La Sultana  ', 'Bailadores', 'Comercial', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-02-07', 0, '', '', '2012-02-09', '', '', '', 'Rene', 'Pereira', '00000000', 'Bailadores', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0072, '2012-02-27', 'J307023180', 'TECNO ZONE ACCESORIOS C.A.', 'AV. FERNANDEZ PEÑA LOCAL 178 EJIDO', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2012-02-27', 0, '', '', '0000-00-00', '', '', '', 'Carlos', 'Peña', '00000000', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0073, '2012-03-13', 'V019422657', 'Glendys Carolina Angulo Rojas', 'Sector El Moral. Ejido', 'Natural', 0.00, 0.00, '04262757333', '', 'P', '', 0, '', '', '2012-02-08', 0, '', '', '2012-03-06', '', '', '', 'Glendys', 'Angulo', '19422657', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0074, '2012-03-19', 'G200045744', 'COFAE', 'Av. Andres Bello. Edif. Sede de la Contraloria General de la Republica', 'Educativa', 0.00, 0.00, '02125781165', '', 'J', '', 0, '', '', '2012-03-19', 0, '', '', '2012-03-19', '', '', '', 'Leonardo', 'Salas', '00000000', 'Caracas', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0075, '2012-04-03', 'G200069350', 'IDECEL', 'Sede de Contraloría General del Estado Lara Urb. El Parque calle A-1 Barquisimeto', 'Educativa', 0.00, 0.00, '02512677033', '', 'J', '', 0, '', '', '2012-04-04', 0, '', '', '2012-04-04', '', '', '', 'Milagro', 'Crespo', '00000000', 'Barquisimeto', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0076, '2012-04-02', 'V080086390', 'Luis Antonio Aguilar', 'Calle 1A Casa Nro 263. Urb. Padre Duque Ejido', 'Publicitaria', 0.00, 0.00, '04147078752', '', 'P', '', 0, '', '', '2012-04-02', 0, '', '', '2012-04-03', '', '', '', 'Luis', 'Aguilar', '00000000', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0077, '2012-04-03', 'V080184502', 'Producciones Gómez', 'Av 7 entre calles 20 y 21 casa nro 20-62 Merida', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-04-03', 0, '', '', '2012-04-04', '', '', '', 'Gustavo', 'Gomez', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0078, '2012-04-19', 'J316215679', 'DIESOCA', 'Av. 5 esquina calle 15 casa nro 15 sector milla', 'Comercial', 0.00, 0.00, '02742523108', '', 'J', '', 0, '', '', '2012-04-20', 0, '', '', '2012-05-07', '', '', '', 'Desire', 'Rivero', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0079, '2012-05-09', 'J308314226', 'ALDEA VALLE ENCANTADO C.A.', 'CTRA PANAMERICANA EDIF ALDEA VALLE ENCANTADO SALADO ALTO', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2012-05-09', 0, '', '', '2012-05-09', '', '', '', 'FRANCISCO', 'Uzcategui', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0080, '2011-05-31', 'J090134000', 'Multinacional de Seguros', 'Caracas, Venezuela', 'Seguros', 9999999.99, 0.00, '02740000000', '', 'J', '', 0, '', '', '2011-05-31', 0, '', '', '0000-00-00', '', '', '', 'FRANCISCO', 'Rivero', '00000000', '', '', 'Dueño');
INSERT INTO `proveedores` VALUES (0081, '2012-06-25', 'V080487394', 'Lobo Moreno Cesar Adelmo', 'Calle Principal Edif. J, Pelicano Piso 6', 'Abogado', 0.00, 0.00, '04247403518', '', 'J', '', 0, '', '', '2012-06-25', 0, '', '', '0000-00-00', '', '', '', 'Cesar Adelmo', 'Lobo Moreno', '08048739', 'Calle Principal Edif. J, Pelicano Piso 6', '04247403518', 'Propietario');
INSERT INTO `proveedores` VALUES (0082, '2012-06-21', '0243504190', 'Peña Peña Ronald Alfredo', 'Ejido', 'Pasante', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-06-20', 0, '', '', '2012-06-27', '', '', '', 'Ronald', 'Peña', '24350419', '', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0083, '2012-07-06', 'J308450391', 'Inversiones Daca c.a.', 'Av. Fernandez Peña Nº 133-c', 'Comercial', 0.00, 0.00, '02742216774', '0', 'J', '', 0, '', '', '2012-07-06', 0, '', '', '0000-00-00', '', '', '', '.', '.', '00000000', '.', '02742216774', '.');
INSERT INTO `proveedores` VALUES (0084, '2012-07-09', 'V155138500', 'Juana Castillo Sanz', 'Ejido', 'Pasante', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-07-10', 0, '', '', '2012-07-10', '', '', '', 'Juana', 'Castillo', '15513850', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0085, '2012-07-03', 'V052040880', 'Xiomara Andrea Rangel Quintero', 'Merida', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-07-04', 0, '', '', '2012-07-05', '', '', '', 'Xiomara', 'Rangel', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0086, '2012-06-14', 'V-80196520', 'Oswaldo Ramos', 'Merida', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-06-13', 0, '', '', '2012-06-22', '', '', '', 'Oswaldo', 'Ramos', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0087, '2012-06-20', 'J294399304', 'ASOCOMUC', 'Av. Henry Ford C.C.Centro Civico Romulo Betancourt Nivel 2', 'Capacitación', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2012-06-22', 0, '', '', '2012-06-26', '', '', '', 'Dilcia', 'Gomez', '00000000', 'Valencia', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0088, '2012-07-04', 'J090283846', 'Banco Sofitasa', 'Ejido', 'Bancaria', 0.00, 0.00, '02740000000', '0', 'J', '', 0, '', '', '2012-07-12', 0, '', '', '0000-00-00', '', '', '', 'Yajaira ', 'Mejias', '00000000', 'Ejido', '0000000000', 'Gerente');
INSERT INTO `proveedores` VALUES (0089, '2012-08-16', 'V169070705', 'Desiree Katherine Salas Velasquez', 'Merida', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-08-16', 0, '', '', '2012-08-16', '', '', '', 'Desiree', 'Salas', '00000000', 'Merida', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0090, '2012-08-22', 'V156211900', 'Yohana Pérez', 'Ejido', 'PERSONAL', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-07-19', 0, '', '', '2012-07-19', '', '', '', 'Yohana', 'Perez', '15621190', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0091, '2012-09-04', 'J307302445', 'CORPORACION OTIESCA COMPUTACION C.A.', 'AV. 4 Bolivar Edif. Masini Piso 2 Merida', 'Comercial', 0.00, 0.00, '02742528611', '', 'J', '', 0, '', '', '2012-09-06', 0, '', '', '2012-09-08', '', '', '', 'Arelis', 'Peña', '00000000', 'Merida', '02742528611', 'Vendedora');
INSERT INTO `proveedores` VALUES (0092, '2012-09-03', 'J309155350', 'PEÑA.NET C.A', 'C.C Centenario nucleo sur local 76 Ejido', 'Comercial', 0.00, 0.00, '02744159021', '', 'J', '', 0, '', '', '2012-09-25', 0, '', '', '2012-09-25', '', '', '', 'Juan', 'Perez', '00000000', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0093, '2012-09-03', 'V080277390', 'Alba del Carmen Vasquez Añez', 'Ejido', 'Abogado', 0.00, 0.00, '02740000000', '', 'P', '', 0, '', '', '2012-09-05', 0, '', '', '2012-09-05', '', '', '', 'Alba', 'Vasquez', '00000000', 'Ejido', '0000000000', 'Dueño');
INSERT INTO `proveedores` VALUES (0094, '2012-10-02', 'V-12352976', 'MARIA LORENA PAREDES PEÑA', 'AV. LAS AMERICAS RES.MONSEÑOR CHACON TORRE J APTO 2-2', 'PERSONAL', 0.00, 0.00, '04147130551', '', 'P', '', 0, '', '', '2012-10-11', 0, '', '', '0000-00-00', '', '', '', 'MARIA LORENA', 'PAREDES PEÑA', '12352976', '', '0417130551', 'Dueño');
INSERT INTO `proveedores` VALUES (0095, '2012-10-16', 'J090046240', 'PROFOT C.A.', 'Av. Don Tulio Febres Cordero entre calles 26 y 27 Sector Centro', 'Comercial', 0.00, 0.00, '02740000000', '', 'J', '', 0, '', '', '2012-10-17', 0, '', '', '2012-10-17', '', '', '', 'Nayrlet', 'Sosa', '00000000', 'Merida', '0000000000', 'Vendedora');
INSERT INTO `proveedores` VALUES (0096, '2012-11-09', 'V107156280', 'YOLY AVENDAÑO MEZA', 'Urb. Santa Elena Nro 9 10-45C Mérida', 'PERSONAL', 0.00, 0.00, '04247448211', '', 'P', '', 0, '', '', '2012-11-09', 0, '', '', '2012-11-09', '', '', '', 'Yoly', 'Avendaño', '10715628', 'Merida', '04247448211', 'Dueño');
INSERT INTO `proveedores` VALUES (0097, '2012-11-09', 'J308008028', 'SUPER RUEDAS, C.A.', 'AV. CENTENARIO GALPON TODOCAUCHOS NRO 1 SECTOR CENTENARIO', 'Comercial', 0.00, 0.00, '02742218952', '', 'J', '', 0, '', '', '2012-11-09', 0, '', '', '2012-11-09', '', '', '', 'EDGAR', 'RODRIGUEZ', '12874585', 'AV. CENTENARIO EJIDO EDO MERIDA', '02742218952', 'DUEÑO');
INSERT INTO `proveedores` VALUES (0098, '2012-10-25', 'V178303933', 'ARAQUE TORRES GLORIANA', 'AV. PPAL. EDIF ', 'PERSONAL', 0.00, 0.00, '02742664764', '', 'P', '', 0, '', '', '2012-11-20', 0, '', '', '0000-00-00', '', '', '', 'GLORIANA', 'ARAQUE TORRES', '17830393', 'AV. PPAL. EDIF ', '02742664764', 'Propietario');
INSERT INTO `proveedores` VALUES (0099, '2012-10-22', 'V080215661', 'SULBARAN FAJARDO JESUS GONZALO', 'AV. ELEAZAR LOPEZ CONTRERAS', 'PERSONAL', 0.00, 0.00, '02742663746', '', 'P', '', 0, '', '', '2012-10-22', 0, '', '', '0000-00-00', '', '', '', 'JESUS GONZALO', 'SULBARAN FAJARDO', '08021566', '', '', 'Propietario');
INSERT INTO `proveedores` VALUES (0100, '2012-12-03', 'J299986640', 'Corredor Hermanos Disctribuciones C.A.', 'Av. 2 Lora entre calles 18 y 19, Edif. Corredor Hermanos, Nº ?? Piso 1, local 1', 'Distribuidor', 0.00, 0.00, '02742527289', '', 'J', '', 0, '', '', '2012-12-03', 0, '', '', '0000-00-00', '', '', '', 'H', 'Corredor', '29998664', 'Av. 2 Lora entre calles 18 y 19, Edif. Corredor Hermanos, Nº ?? Piso 1, local 1', '02742527289', 'Gerente');
INSERT INTO `proveedores` VALUES (0101, '2012-11-21', 'V182087722', 'Hildo Antonio Alarcón García', 'Av. 4', 'PERSONAL', 0.00, 0.00, '04163707830', '', 'P', '', 0, '', '', '2012-12-17', 0, '', '', '0000-00-00', '', '', '', 'Hildo Antonio', 'Alarcón García', '18208772', 'Av 4', '04163707830', 'Propietario');
INSERT INTO `proveedores` VALUES (0102, '2012-12-17', 'J070013010', 'Merida Motors C. A.', 'Av. Andrés Bello, Country Club Mérida, Estado Mérida', 'Comercial', 0.00, 0.00, '02742662229', '', 'J', '', 0, '', '', '2012-12-17', 0, '', '', '0000-00-00', '', '', '', 'Merida', 'Motors', '70013010', 'Av. Andres Bello', '02742662229', 'Propietario');
INSERT INTO `proveedores` VALUES (0103, '2012-12-17', 'J312603453', 'Voz Data C. A.', 'Av. 5 con calle 20 Edif. San Gabriel PB Local 2 Mérida', 'Comercial', 0.00, 0.00, '02742528825', '', 'J', '', 0, '', '', '2012-12-17', 0, '', '', '0000-00-00', '', '', '', 'Voz', 'Data', '31260345', 'Av. 5 con calle 20 Edif. San Gabriel PB Local 2 Mérida', '02742528825', 'Propietario');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `retenciones_isrl`
-- 

CREATE TABLE `retenciones_isrl` (
  `cod_ret_isrl` int(11) NOT NULL auto_increment,
  `com_ret_isrl` int(5) unsigned zerofill NOT NULL,
  `egr_ret_isrl` int(6) unsigned zerofill NOT NULL,
  `fch_ret_isrl` date NOT NULL,
  `obs_ret_isrl` varchar(255) NOT NULL,
  PRIMARY KEY  (`cod_ret_isrl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `retenciones_isrl`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `retenciones_iva`
-- 

CREATE TABLE `retenciones_iva` (
  `cod_ret_iva` int(11) NOT NULL auto_increment,
  `com_ret_iva` int(8) unsigned zerofill NOT NULL,
  `egr_ret_iva` int(6) unsigned zerofill NOT NULL,
  `fch_ret_iva` date NOT NULL,
  `obs_ret_iva` varchar(255) NOT NULL,
  PRIMARY KEY  (`cod_ret_iva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las retenciones de IVA' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `retenciones_iva`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `secciones`
-- 

CREATE TABLE `secciones` (
  `cod_sec` int(2) NOT NULL auto_increment COMMENT 'codigo de la seccion',
  `nom_sec` varchar(25) NOT NULL COMMENT 'nombre de la seccion',
  `des_sec` varchar(255) NOT NULL COMMENT 'descripcion de la seccion',
  `dir_sec` varchar(50) NOT NULL COMMENT 'direccion para apuntar a la seccion',
  `tar_sec` varchar(15) NOT NULL COMMENT 'target de apertura de la seccion',
  `ord_sec` double(9,2) NOT NULL COMMENT 'orden de aparicion en el menu',
  PRIMARY KEY  (`cod_sec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Secciones Disponibles para los Usuarios' AUTO_INCREMENT=29 ;

-- 
-- Volcar la base de datos para la tabla `secciones`
-- 

INSERT INTO `secciones` VALUES (1, 'Personal', 'Datos del Personal adscrito a: ', 'personal.php', 'contenido', 7.00);
INSERT INTO `secciones` VALUES (2, 'Dependencias', 'Dependencias adcritas a:', 'dependencias.php', 'contenido', 2.00);
INSERT INTO `secciones` VALUES (3, 'Cargos', 'Cargos adscritos a:', 'cargos.php', 'contenido', 4.00);
INSERT INTO `secciones` VALUES (4, 'Nomina', 'Nomina adscrita a:', 'nomina.php', 'contenido', 12.00);
INSERT INTO `secciones` VALUES (5, 'Incidencias', 'Incidencias adcritas a:', 'incidencias.php', 'contenido', 13.00);
INSERT INTO `secciones` VALUES (6, 'Descuentos', 'Descuentos del Personal adscrito a:', 'descuentos.php', 'contenido', 6.00);
INSERT INTO `secciones` VALUES (7, 'Concesiones', 'Concesiones del Personal adscrito a:', 'concesiones.php', 'contenido', 5.00);
INSERT INTO `secciones` VALUES (8, 'Sueldos', 'Tabla de Sueldos', 'sueldos.php', 'contenido', 3.00);
INSERT INTO `secciones` VALUES (9, 'Correspondencia Enviada', 'Registro de correspondencia Interna y Externa de: ', 'oficios_enviados.php', 'contenido', 15.00);
INSERT INTO `secciones` VALUES (10, 'Comp./Pago./Egre.', 'Módulo de compras de: ', 'compras.php', 'contenido', 14.00);
INSERT INTO `secciones` VALUES (11, 'Actividades', 'Actividades administrativas de:', 'actividades.php', 'contenido', 1.00);
INSERT INTO `secciones` VALUES (12, 'Pre-Nomina', 'Prenomina Adscrita a: ', 'nominapre.php', 'contenido', 11.00);
INSERT INTO `secciones` VALUES (13, 'Cesta Ticket', 'Reporte de Cesta Ticket del personal adscrito a:', 'cesta_ticket.php', 'contenido', 10.00);
INSERT INTO `secciones` VALUES (14, 'Inasistencias', 'Registro de las inasistencias del personal adscrito a:', 'inasistencias.php', 'contenido', 8.00);
INSERT INTO `secciones` VALUES (15, 'Feriados', 'Registro de días feriados para', 'feriados.php', 'contenido', 9.00);
INSERT INTO `secciones` VALUES (16, 'Proveedores', 'Registro de Proveedores registrados a ', 'proveedores.php', 'contenido', 13.50);
INSERT INTO `secciones` VALUES (17, 'Control de Bienes', 'Sistema de Control de Bienes', 'bienes/index.php', 'contenido', 16.00);
INSERT INTO `secciones` VALUES (18, 'Valores', 'Valores utilizados para los cáculos', 'valores.php', 'contenido', 0.00);
INSERT INTO `secciones` VALUES (19, 'Presupuesto', 'Manipulación de Partidas y Movimientos Presupuestarios', 'presupuesto.php', 'contenido', 0.10);
INSERT INTO `secciones` VALUES (20, 'Solicitud Vacaciones', 'Registro de la Solicitud de Vacaciones', 'vacaciones.php', 'contenido', 8.50);
INSERT INTO `secciones` VALUES (21, 'Solicitud de Permiso', 'Registro de la Solicitud de Permisos', 'permisos.php', 'contenido', 8.60);
INSERT INTO `secciones` VALUES (22, 'Sol. Justificativos', 'Registro de la Solicitud de Justificativos', 'justificativos.php', 'contenido', 8.70);
INSERT INTO `secciones` VALUES (23, 'Pago de Medicinas', 'Pago de gastos medicos y de farmacia a los empleados de la contraloría', 'medicinas.php', 'contenido', 8.30);
INSERT INTO `secciones` VALUES (24, 'Ingresos', 'Auditoria Ingresos', 'ing_index.php', 'contenido', 16.00);
INSERT INTO `secciones` VALUES (25, 'Conformar Cheques', 'Confirmación de cheques emitidos', 'conformar_cheques.php', 'contenido', 17.00);
INSERT INTO `secciones` VALUES (26, 'Constancias', 'Solicitud de Constancias', 'constancias.php', 'contenido', 17.00);
INSERT INTO `secciones` VALUES (27, 'Correspondencia Recibida', 'Registro de Correspondencia recibida por:', 'oficios_recibidos.php', 'contenido', 15.01);
INSERT INTO `secciones` VALUES (28, 'Bancos', 'Bancos y movimientos de ', 'bancos.php', 'contenido', 0.05);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `sueldos`
-- 

CREATE TABLE `sueldos` (
  `cod_sue` int(2) NOT NULL auto_increment COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY  (`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `sueldos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `sueldos_ant`
-- 

CREATE TABLE `sueldos_ant` (
  `cod_sue` int(2) NOT NULL auto_increment COMMENT 'codigo del sueldo',
  `mon_sue` double(9,2) NOT NULL COMMENT 'monto del sueldo',
  `des_sue` varchar(50) NOT NULL COMMENT 'descripcion del sueldo',
  PRIMARY KEY  (`cod_sue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `sueldos_ant`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `temp_presup`
-- 

CREATE TABLE `temp_presup` (
  `fch_prs` date NOT NULL,
  `det_prs` varchar(255) NOT NULL,
  `nor_prs` varchar(7) NOT NULL,
  `asg_prs` double(9,2) NOT NULL,
  `inc_prs` double(9,2) NOT NULL,
  `ing_prs` double(9,2) NOT NULL,
  `egr_prs` double(9,2) NOT NULL,
  `com_prs` double(9,2) NOT NULL,
  `cau_prs` double(9,2) NOT NULL,
  `pag_prs` double(9,2) NOT NULL,
  `rin_prs` double(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporal para generar ejecucion presupuestaria';

-- 
-- Volcar la base de datos para la tabla `temp_presup`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `tipos_cargos`
-- 

CREATE TABLE `tipos_cargos` (
  `cod_tcar` int(11) NOT NULL auto_increment,
  `nom_tcar` varchar(50) NOT NULL,
  `abr_tcar` varchar(2) character set latin1 collate latin1_spanish_ci NOT NULL,
  PRIMARY KEY  (`cod_tcar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `tipos_cargos`
-- 

INSERT INTO `tipos_cargos` VALUES (0, 'Alto Nivel', 'AN');
INSERT INTO `tipos_cargos` VALUES (1, 'Dirección', 'DR');
INSERT INTO `tipos_cargos` VALUES (2, 'Fijo', 'FJ');
INSERT INTO `tipos_cargos` VALUES (3, 'Contratado', 'CT');
INSERT INTO `tipos_cargos` VALUES (4, 'Obrero fijo', 'OF');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuarios`
-- 

CREATE TABLE `usuarios` (
  `cod_usr` varchar(12) NOT NULL COMMENT 'codigo del usuario',
  `nom_usr` varchar(50) NOT NULL COMMENT 'nombre del usuario',
  `ape_usr` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `freg_usr` date NOT NULL COMMENT 'fecha de registro del usuario',
  `log_usr` varchar(15) NOT NULL COMMENT 'login del usuario',
  `pas_usr` varchar(15) NOT NULL COMMENT 'password del usuario',
  `sts_usr` varchar(1) NOT NULL default 'A' COMMENT 'Estado en el que se encuentra el Usuario',
  `int_usr` int(1) NOT NULL default '0' COMMENT 'Intentos fallidos de inicio de sesi�n',
  `fcam_usr` date NOT NULL COMMENT 'fecha de cambio del usuario',
  `cod_grp` int(2) NOT NULL COMMENT 'codigo del grupo',
  PRIMARY KEY  (`cod_usr`),
  KEY `cod_grp` (`cod_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Usuarios del Sistema';

-- 
-- Volcar la base de datos para la tabla `usuarios`
-- 

INSERT INTO `usuarios` VALUES ('10109577', 'María Isabel', 'Rivas de Osorio', '2011-05-04', 'isabelr', 'adt123', 'A', 0, '2012-10-24', 3);
INSERT INTO `usuarios` VALUES ('10715628', 'Yoly Josefina', 'Avendaño Meza', '2011-05-04', 'yolya', 'adt123', 'A', 0, '2012-09-14', 3);
INSERT INTO `usuarios` VALUES ('11111111111', 'Administrador', 'Administrador', '2011-02-16', 'admin', 'controlador', 'A', 0, '2013-02-17', 1);
INSERT INTO `usuarios` VALUES ('11960597', 'Jesus Ramón', 'Silva Osuna', '2011-05-04', 'jesuss', '1975slt', 'A', 0, '2012-12-04', 3);
INSERT INTO `usuarios` VALUES ('12346028', 'José de Jesús', 'Corredor Alarcón', '2011-05-04', 'josec', 'slt1975', 'A', 0, '2012-06-20', 3);
INSERT INTO `usuarios` VALUES ('12352976', 'María Lorena', 'Paredes Peña', '2012-11-08', 'lorenap', 'lore123', 'A', 0, '2012-11-08', 7);
INSERT INTO `usuarios` VALUES ('14304657', 'Leidy Josefina', 'Pereira Ochoa', '2011-05-04', 'leidyp', 'adt123', 'A', 0, '2012-10-23', 3);
INSERT INTO `usuarios` VALUES ('15031097', 'Luis Felipe', 'Márquez Briceño', '2011-01-01', 'felix', 'nueva', 'A', 0, '2013-03-11', 2);
INSERT INTO `usuarios` VALUES ('15621190', 'Yohana', 'Pérez de Valero', '2011-05-04', 'yohanap', 'adt123', 'A', 0, '2012-08-17', 3);
INSERT INTO `usuarios` VALUES ('16657924', 'María Alejandra', 'Pérez Pérez', '2012-02-01', 'mariap', 'mari123', 'A', 0, '2012-10-30', 3);
INSERT INTO `usuarios` VALUES ('16907070', 'Desirée Katherine', 'Salas Velásquez', '2012-08-01', 'desivela', 'desirée', 'D', 3, '2012-12-10', 2);
INSERT INTO `usuarios` VALUES ('16933715', 'Alejandro Antonio', 'Sayago González', '2011-05-04', 'alejandros', 'servju', 'A', 0, '2012-09-24', 3);
INSERT INTO `usuarios` VALUES ('17130852', 'Yrene Coromoto', 'Guillén Andrade', '2012-02-22', 'yrenec', 'adt123', 'A', 0, '2012-11-14', 3);
INSERT INTO `usuarios` VALUES ('17830393', 'Gloriana', 'Araque Torres', '2012-11-06', 'atgloriana', 'gat123', 'A', 0, '2012-12-10', 6);
INSERT INTO `usuarios` VALUES ('18208772', 'Hildo Antonio', 'Alarcón García', '2012-12-03', 'hildoalarcon', 'hildo123', 'A', 0, '2012-12-03', 3);
INSERT INTO `usuarios` VALUES ('19422787', 'Luzmar', 'Contreras S', '2011-07-08', 'luzmar', '123456', 'A', 0, '2011-10-08', 5);
INSERT INTO `usuarios` VALUES ('21184968', 'Yaquelin', 'Rivas Vera', '2011-04-25', 'yaquelin', 'yaquerivas', 'A', 0, '2011-05-06', 4);
INSERT INTO `usuarios` VALUES ('24350419', 'Ronal', 'Peña', '2012-06-06', 'ronalp', 'pasante', 'A', 0, '2012-06-06', 4);
INSERT INTO `usuarios` VALUES ('3766280', 'Ramón Vicente', 'Lobo Dugarte', '2012-02-01', 'ramonl', 'adt123', 'A', 0, '2012-10-09', 3);
INSERT INTO `usuarios` VALUES ('4264264', 'Carlos Julio', 'Puentes Uzcátegui', '2011-05-04', 'carlosp', 'slt1975', 'A', 0, '2012-07-10', 3);
INSERT INTO `usuarios` VALUES ('5204088', 'Xiomara Andrea', 'Rangel Quintero', '2012-08-13', 'xiomara', 'xiomara', 'A', 0, '2012-08-13', 2);
INSERT INTO `usuarios` VALUES ('6343267', 'Luz Marina ', 'Salcedo ', '2011-01-01', 'luzmarina', 'luzmar1', 'A', 1, '2012-10-04', 2);
INSERT INTO `usuarios` VALUES ('8019652', 'Oswaldo Ramón', 'ramos', '2011-05-04', 'oswaldor', 'os123', 'A', 0, '2012-10-09', 3);
INSERT INTO `usuarios` VALUES ('8021566', 'Jesús Gonzalo', 'Sulbarán Fajardo', '2012-12-03', 'jesusg', 'jesus123', 'A', 0, '2012-12-04', 3);
INSERT INTO `usuarios` VALUES ('8023210', 'José Andres', 'Briceño Valero', '2011-04-29', 'josemeridave', 'jose_1961', 'A', 0, '2012-06-01', 2);
INSERT INTO `usuarios` VALUES ('8029735', 'Nora Coromoto', 'Cerrada', '2011-05-04', 'norac', 'secejec', 'D', 3, '2012-09-05', 6);
INSERT INTO `usuarios` VALUES ('8036119', 'Elda Soraya', 'Hill Dávila', '2011-05-04', 'sorayah', 'servju', 'A', 0, '2012-08-16', 3);
INSERT INTO `usuarios` VALUES ('9028505', 'Ramón Antonio', 'Fernández Rojas', '2011-05-04', 'ramonf', 'ramon', 'A', 0, '2012-09-10', 3);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vac_dias_per`
-- 

CREATE TABLE `vac_dias_per` (
  `ced_per` varchar(12) NOT NULL,
  `cod_car` int(11) NOT NULL,
  `fch_pag_vac` date NOT NULL,
  `dias_vac` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de días de Vacaciones a disfrutar';

-- 
-- Volcar la base de datos para la tabla `vac_dias_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vacaciones_per`
-- 

CREATE TABLE `vacaciones_per` (
  `cod_sol_vac` int(11) NOT NULL auto_increment,
  `fch_sol_vac` date NOT NULL,
  `ced_per` varchar(12) NOT NULL,
  `nom_per` varchar(100) NOT NULL,
  `nom_dep` varchar(150) NOT NULL,
  `nom_car` varchar(100) NOT NULL,
  `fch_ing` date NOT NULL,
  `dias_sol_vac` int(2) NOT NULL,
  `ini_sol_vac` date NOT NULL,
  `fin_sol_vac` date NOT NULL,
  `tip_sol_vac` varchar(20) NOT NULL,
  `peri_sol_vac` date NOT NULL,
  `perf_sol_vac` date NOT NULL,
  `obs_sol_vac` longtext NOT NULL,
  `apro_sol_vac` varchar(2) NOT NULL,
  PRIMARY KEY  (`cod_sol_vac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Registro de la solicitud de Vacaciones de los empleados' AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `vacaciones_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `valores`
-- 

CREATE TABLE `valores` (
  `cod_val` int(11) NOT NULL auto_increment COMMENT 'Codigo del Valor',
  `des_val` varchar(25) NOT NULL COMMENT 'Descripcion del Valor',
  `val_val` double(9,3) NOT NULL COMMENT 'Valor del Valor',
  `con_val` varchar(100) NOT NULL COMMENT 'Concepto del Valor',
  PRIMARY KEY  (`cod_val`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- 
-- Volcar la base de datos para la tabla `valores`
-- 

INSERT INTO `valores` VALUES (1, 'BV', 15.000, 'Días de Bono Vacacional para personal Contratado según LOT');
INSERT INTO `valores` VALUES (2, 'AG', 90.000, 'Días de Aguinaldos');
INSERT INTO `valores` VALUES (3, 'UT', 90.000, 'Valor de la Unidad Tributaria');
INSERT INTO `valores` VALUES (4, 'SSO', 0.040, 'Porcentaje de Retención Seguro Social Obligatorio');
INSERT INTO `valores` VALUES (5, 'LPH', 0.010, 'Porcentaje de Retención Fondo Ahorro Obligatorio para Vivienda');
INSERT INTO `valores` VALUES (6, 'SPF', 0.005, 'Porcentaje de Retención Pérdida Involuntaria de Empleo (PIE)');
INSERT INTO `valores` VALUES (7, 'CAH', 0.080, 'Porcentaje de Retención Caja de Ahorros (CAPREAMCE)');
INSERT INTO `valores` VALUES (8, 'SFU', 60.000, 'Monto Seguro Funerario (SOVENPFA)');
INSERT INTO `valores` VALUES (9, 'SM', 2047.520, 'Monto de Sueldo Mínimo');
INSERT INTO `valores` VALUES (10, 'SMMSSO', 5.000, 'Cantidad de Sueldos Máximos para el SSO');
INSERT INTO `valores` VALUES (12, 'APSSO', 0.090, 'Porcentaje de Aporte Seguro Social Obligatorio');
INSERT INTO `valores` VALUES (13, 'APSPF', 0.020, 'Porcentaje de Aporte Pérdida Involuntaria de Empleo (PIE)');
INSERT INTO `valores` VALUES (14, 'APCAH', 0.080, 'Porcentaje de Aporte Caja de Ahorros (CAPREAMCE)');
INSERT INTO `valores` VALUES (15, 'PRMHOG', 2.500, 'Cantidad de Unidades Tributarias Prima por Hogar');
INSERT INTO `valores` VALUES (16, 'PRMHIJ', 0.500, 'Cantidad de Unidades Tributarias Prima por Hijos');
INSERT INTO `valores` VALUES (17, 'PRMANT1', 1.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 6 a 8 años de antiguedad');
INSERT INTO `valores` VALUES (18, 'PRMPRO1', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 1er Tipo');
INSERT INTO `valores` VALUES (19, 'PRMJER', 300.000, 'Monto Prima Jerarquía');
INSERT INTO `valores` VALUES (20, 'PRMHJE', 1.000, 'Cantidad de Unidades Tributarias Prima por Hijos Excepcionales');
INSERT INTO `valores` VALUES (21, 'PRMOTR1', 2.500, 'Cantidad de Unidades Tributarias Otra Prima 1er Tipo');
INSERT INTO `valores` VALUES (22, 'APLPH', 0.020, 'Porcentaje de Aporte Fondo Ahorro Obligatorio para Vivienda');
INSERT INTO `valores` VALUES (23, 'PRMANT2', 2.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 9 a 11 años de antiguedad');
INSERT INTO `valores` VALUES (24, 'PRMANT3', 2.500, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 12 a 14 años de antiguedad');
INSERT INTO `valores` VALUES (25, 'PRMANT4', 3.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad después de 15 años');
INSERT INTO `valores` VALUES (27, 'VACPAG1', 40.000, 'Días de Bono Vacacional para personal fijo 1er Quinquenio');
INSERT INTO `valores` VALUES (28, 'VACPAG2', 40.000, 'Días de Bono Vacacional para personal fijo 2do Quinquenio');
INSERT INTO `valores` VALUES (29, 'VACPAG3', 40.000, 'Días de Bono Vacacional para personal fijo 3er Quinquenio');
INSERT INTO `valores` VALUES (30, 'VACPAG4', 40.000, 'Días de Bono Vacacional para personal fijo 4to Quinquenio');
INSERT INTO `valores` VALUES (31, 'VACPAG5', 40.000, 'Días de Bono Vacacional para personal fijo 5to Quinquenio');
INSERT INTO `valores` VALUES (32, 'VACDIF1', 20.000, 'Días de disfrute de Vacaciones para personal fijo 1er Quinquenio');
INSERT INTO `valores` VALUES (33, 'VACDIF2', 23.000, 'Días de disfrute de Vacaciones para personal fijo 2do Quinquenio');
INSERT INTO `valores` VALUES (34, 'VACDIF3', 25.000, 'Días de disfrute de Vacaciones para personal fijo 3er Quinquenio');
INSERT INTO `valores` VALUES (35, 'VACDIF4', 28.000, 'Días de disfrute de Vacaciones para personal fijo 4to Quinquenio');
INSERT INTO `valores` VALUES (36, 'VACDIF5', 30.000, 'Días de disfrute de Vacaciones para personal fijo 5to Quinquenio');
INSERT INTO `valores` VALUES (38, 'DESPIO', 0.000, 'Porcentaje de Monte Pio');
INSERT INTO `valores` VALUES (39, 'PRMPRO2', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalización 2do Tipo');
INSERT INTO `valores` VALUES (40, 'PRMOTR2', 3.000, 'Cantidad de Unidades Tributarias Otra Prima 2do Tipo');
INSERT INTO `valores` VALUES (41, 'IVA', 0.120, 'Impuesto al Valor Agregado');
INSERT INTO `valores` VALUES (42, 'CCT', 2.750, 'Comisión gastos administrativos Cesta Ticket');
INSERT INTO `valores` VALUES (43, 'MED_2012', 1500.000, 'Monto para gastos Médicos y de Farmacia para el año 2012');
INSERT INTO `valores` VALUES (44, 'PRMANT0', 1.000, 'Cantidad de días de Salarios Mínimos mensuales por Prima de Antiguedad de 3 a 5 años de antiguedad');
INSERT INTO `valores` VALUES (45, 'PRMOTR3', 0.000, 'Cantidad de Unidades Tributarias Prima por Profesionalizacion Titulo Doctorado');
INSERT INTO `valores` VALUES (46, 'SUSTRAC_ISRL', 63.330, 'Sustraendo para el ISRL cuando retancion sea 1%');
INSERT INTO `valores` VALUES (47, 'PART_IVA', 72.000, 'Codigo de la Partida correspondiente al IVA');
INSERT INTO `valores` VALUES (48, 'PRESUP_MAX', 35.000, 'Número de partidas por página en reporte General de Presupuesto');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_cargos_per`
-- 

CREATE TABLE `vista_cargos_per` (
  `ced_per` varchar(12) default NULL,
  `cod_car` int(11) default NULL,
  `cod_dep` varchar(5) default NULL,
  `num_car` int(3) unsigned zerofill default NULL,
  `nom_car` varchar(100) default NULL,
  `fch_asg` date default NULL,
  `fch_vac` date default NULL,
  `des_car` varchar(1) default NULL,
  `nom_per` varchar(50) default NULL,
  `ape_per` varchar(50) default NULL,
  `nom_tcar` varchar(50) default NULL,
  `mon_sue` double(9,2) default NULL,
  `des_sue` varchar(50) default NULL,
  `mon_sue_ant` double(9,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_cargos_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_cheques`
-- 

CREATE TABLE `vista_cheques` (
  `fch_egr` date default NULL,
  `nom_pro` varchar(50) default NULL,
  `chq_egr` varchar(25) default NULL,
  `nom_ban` varchar(50) default NULL,
  `cod_egr` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_cheques`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_deducciones_per`
-- 

CREATE TABLE `vista_deducciones_per` (
  `cod_dsp` int(11) default NULL,
  `ced_per` varchar(12) default NULL,
  `nom_des` varchar(50) default NULL,
  `con_dsp` varchar(100) default NULL,
  `ncuo_dsp` int(3) default NULL,
  `ncp_dsp` int(3) default NULL,
  `nom_car` varchar(100) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_deducciones_per`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_ing_pat`
-- 

CREATE TABLE `vista_ing_pat` (
  `id` int(11) default NULL,
  `fecha` date default NULL,
  `serie` int(3) default NULL,
  `inicio` int(8) default NULL,
  `fin` int(8) default NULL,
  `tipo` varchar(2) default NULL,
  `serie_id` int(11) default NULL,
  `recibo` int(11) default NULL,
  `monto` decimal(9,2) default NULL,
  `cod_cat` int(2) default NULL,
  `con_esp` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_ing_pat`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_movimientos`
-- 

CREATE TABLE `vista_movimientos` (
  `cod_mov` int(11) default NULL,
  `ced_per` varchar(12) default NULL,
  `cod_car` int(11) default NULL,
  `accion` int(11) default NULL,
  `estado` int(1) default NULL,
  `fch_asg` date default NULL,
  `des_car` varchar(1) default NULL,
  `cod_dep` varchar(5) default NULL,
  `num_car` int(3) unsigned zerofill default NULL,
  `nom_car` varchar(100) default NULL,
  `cod_sue` int(2) default NULL,
  `cod_tcar` int(11) default NULL,
  `des_sue` varchar(50) default NULL,
  `mon_sue` double(9,2) default NULL,
  `nom_tcar` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_movimientos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_nomin_tot_sum`
-- 

CREATE TABLE `vista_nomin_tot_sum` (
  `ano_nom` int(4) default NULL,
  `mes_nom` int(2) default NULL,
  `por_nom` int(1) default NULL,
  `cod_tcar` int(11) default NULL,
  `abr_tcar` varchar(2) default NULL,
  `cod_dep` varchar(5) default NULL,
  `nom_tcar` varchar(50) default NULL,
  `tot_sue` double(19,2) default NULL,
  `tot_lph` double(19,2) default NULL,
  `tot_spf` double(19,2) default NULL,
  `tot_sso` double(19,2) default NULL,
  `tot_cah` double(19,2) default NULL,
  `tot_sfu` double(19,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_nomin_tot_sum`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_nominapre_proc`
-- 

CREATE TABLE `vista_nominapre_proc` (
  `ano_nom` int(4) default NULL,
  `mes_nom` int(2) default NULL,
  `por_nom` int(1) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_nominapre_proc`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_nominas_pagadas`
-- 

CREATE TABLE `vista_nominas_pagadas` (
  `ano_nom` int(4) default NULL,
  `mes_nom` int(2) default NULL,
  `por_nom` int(1) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_nominas_pagadas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_nominpre_tot_sum`
-- 

CREATE TABLE `vista_nominpre_tot_sum` (
  `ano_nom` int(4) default NULL,
  `mes_nom` int(2) default NULL,
  `por_nom` int(1) default NULL,
  `cod_tcar` int(11) default NULL,
  `abr_tcar` varchar(2) default NULL,
  `cod_dep` varchar(5) default NULL,
  `nom_tcar` varchar(50) default NULL,
  `tot_sue` double(19,2) default NULL,
  `tot_lph` double(19,2) default NULL,
  `tot_spf` double(19,2) default NULL,
  `tot_sso` double(19,2) default NULL,
  `tot_cah` double(19,2) default NULL,
  `tot_sfu` double(19,2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_nominpre_tot_sum`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_ordenespago`
-- 

CREATE TABLE `vista_ordenespago` (
  `fch_egr` date default NULL,
  `nom_pro` varchar(50) default NULL,
  `con_egr` varchar(255) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_ordenespago`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_partidas_comprometidas`
-- 

CREATE TABLE `vista_partidas_comprometidas` (
  `cod_par` int(11) default NULL,
  `sec_par` varchar(11) default NULL,
  `pro_par` varchar(11) default NULL,
  `act_par` varchar(11) default NULL,
  `ram_par` varchar(11) default NULL,
  `par_par` varchar(11) default NULL,
  `gen_par` varchar(11) default NULL,
  `esp_par` varchar(11) default NULL,
  `sub_par` varchar(11) default NULL,
  `des_par` varchar(255) default NULL,
  `obs_par` varchar(11) default NULL,
  `frm_com` int(6) unsigned zerofill default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_partidas_comprometidas`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_partidas_presupuesto`
-- 

CREATE TABLE `vista_partidas_presupuesto` (
  `cod_par` int(11) default NULL,
  `sec_par` varchar(11) default NULL,
  `pro_par` varchar(11) default NULL,
  `act_par` varchar(11) default NULL,
  `ram_par` varchar(11) default NULL,
  `par_par` varchar(11) default NULL,
  `gen_par` varchar(11) default NULL,
  `esp_par` varchar(11) default NULL,
  `sub_par` varchar(11) default NULL,
  `des_par` varchar(255) default NULL,
  `obs_par` varchar(11) default NULL,
  `ano_par_mov` int(4) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_partidas_presupuesto`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_personal`
-- 

CREATE TABLE `vista_personal` (
  `ced_per` varchar(12) default NULL,
  `nombre` varchar(101) default NULL,
  `nac_per` varchar(1) default NULL,
  `abr_per` varchar(10) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_personal`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_salida_sincargo`
-- 

CREATE TABLE `vista_salida_sincargo` (
  `cod_pag` int(11) default NULL,
  `mon_pag` double(9,2) default NULL,
  `ced_per` varchar(12) default NULL,
  `con_pag` varchar(150) default NULL,
  `des_car` varchar(1) default NULL,
  `fch_pag` date default NULL,
  `cod_dep` varchar(5) default NULL,
  `nom_dep` varchar(150) default NULL,
  `cod_car` int(11) default NULL,
  `nom_car` varchar(100) default NULL,
  `fch_vac` date default NULL,
  `cod_tcar` int(11) default NULL,
  `nom_tcar` varchar(50) default NULL,
  `abr_tcar` varchar(2) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `vista_salida_sincargo`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_asignaciones_per`
-- 

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `zuly-1406`.`vista_asignaciones_per` AS select `asg`.`cod_cnp` AS `cod_cnp`,`asg`.`ced_per` AS `ced_per`,`cn`.`nom_con` AS `nom_con`,`asg`.`con_cnp` AS `con_cnp`,`asg`.`ncuo_cnp` AS `ncuo_cnp`,`asg`.`ncp_cnp` AS `ncp_cnp`,`c`.`nom_car` AS `nom_car` from ((`zuly-1406`.`asignaciones` `asg` join `zuly-1406`.`concesiones` `cn`) join `zuly-1406`.`cargos` `c`) where ((`asg`.`cod_con` = `cn`.`cod_con`) and (`c`.`cod_car` = `asg`.`cod_car`));

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vista_banco_mov`
-- 

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `zuly-1406`.`vista_banco_mov` AS (select `zuly-1406`.`egresos`.`cod_egr` AS `codigo`,_utf8'egresos' AS `desde`,`zuly-1406`.`egresos`.`con_egr` AS `concepto`,sum(`zuly-1406`.`pagos`.`mon_pag`) AS `monto`,`zuly-1406`.`egresos`.`cod_ban` AS `banco`,`zuly-1406`.`egresos`.`chq_egr` AS `referencia`,`zuly-1406`.`egresos`.`frm_egr` AS `operacion`,`zuly-1406`.`egresos`.`fch_egr` AS `fecha`,_utf8'egreso' AS `tipo_mov`,`zuly-1406`.`egresos`.`sta_ban_egr` AS `estado` from (`zuly-1406`.`egresos` join `zuly-1406`.`pagos`) where (`zuly-1406`.`egresos`.`frm_egr` = `zuly-1406`.`pagos`.`frm_egr`) group by `zuly-1406`.`pagos`.`frm_egr`) union (select `zuly-1406`.`banco_movimientos`.`cod_ban_mov` AS `codigo`,_utf8'banco_movimientos' AS `desde`,`zuly-1406`.`banco_movimientos`.`des_ban_mov` AS `concepto`,`zuly-1406`.`banco_movimientos`.`mon_ban_mov` AS `monto`,`zuly-1406`.`banco_movimientos`.`cod_ban` AS `banco`,`zuly-1406`.`banco_movimientos`.`ref_ban_mov` AS `referencia`,_utf8'' AS `operacion`,`zuly-1406`.`banco_movimientos`.`fch_ban_mov` AS `fecha`,`zuly-1406`.`banco_movimientos`.`tip_ban_mov` AS `tipo_mov`,`zuly-1406`.`banco_movimientos`.`sta_ban_mov` AS `estado` from `zuly-1406`.`banco_movimientos`);
